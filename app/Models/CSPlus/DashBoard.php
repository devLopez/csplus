<?php

    namespace CSPlus\Models\CSPlus;

    use DB;
    use CSPlus\Services\Database\Traits\ConnectionTrait;

    /**
     * DashBoard
     *
     * Realiza o gerenciamento do dashboard do associado
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 1.1.0
     * @since   22/06/2017
     */
    class DashBoard
    {
        use ConnectionTrait;

        /**
         * @var string
         */
        protected $connection = 'csplus';

        /**
         * Realiza a busca dos dados da dashboard
         *
         * @param   int  $matricula
         * @return  array
         */
        public function getDashBoard($matricula)
        {
            $this->changeConnection();

            $dashboard = DB::connection('csplus')->select("select
                --Numero de dependentes ativos (clique leva ao cadastro de dependentes)
                (select count(*) from dep001 where cotadependente = a.NUMERODACOTA and ativo = 'T') as dependentes,
                -- Convites emitidos no ano
                (select count(*) from convites where cota = a.NUMERODACOTA and extract(year from dataini) = extract(year from current_date)) as convites,
                -- Mes em aberto
                (select min(data) from taxas where cota = a.NUMERODACOTA and recibo is null) as debito
                -- Sem email cadastrado
                , iif(coalesce(caixapostal, '') = '', 'Sem email cadastrado', null) as EMAIL
                --Endereco incompleto
                , iif(coalesce(a.ENDRES_LOGRADOURO, '') = '' or coalesce(a.CIDADERESIDENCIAL, '') = '' or coalesce(a.ESTADORESIDENCIAL, '') = '' or coalesce(a.cepRESIDENCIAL, '') = '', 'Endereco Residencial incompleto', null) as ENDERECO_RESIDENCIAL
                , iif(coalesce(a.ENDCOM_LOGRADOURO, '') = '' or coalesce(a.CIDADECOMERCIAL, '') = '' or coalesce(a.ESTADOCOMERCIAL, '') = '' or coalesce(a.cepCOMERCIAL, '') = '', 'Endereco Comercial incompleto', null) as ENDERECO_COMERCIAL
                from ass001 a where numerodacota = ?
            ", [$matricula]);

            return $dashboard;
        }
    }
