<?php

    namespace CSPlus\Models\CSPlus;

    use DB;
    use CSPlus\Services\Database\AlternativeConnection;

    /**
     * Evento
     *
     * Realiza o gerenciamento dos dados dos clubes e geração de taxas
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 1.3.0
     * @since   17/01/2017
     */
    class Evento
    {
        /**
         * Registra os valores de cada taxa, dividos pelo número do mês
         *
         * @var array
         */
        public static $valores = [
            '07' => [
                1 => 280.00,
                2 => 420.00,
                3 => 560.00,
                4 => 700.00,
                5 => 840.00,
                6 => 980.00
            ],
            '08' => [
                1 => 315.00,
                2 => 565.00,
                3 => 815.00,
                4 => 1065.00,
                5 => 1315.00,
                6 => 1565.00
            ],
            '09' => [
                1 => 350.00,
                2 => 650.00,
                3 => 950.00,
                4 => 1250.00,
                5 => 1550.00,
                6 => 1850.00
            ]
        ];

        /**
         * Realiza a busca dos dados do clube
         *
         * @param   string  $cnpj
         * @return  string
         */
        public static function getClubData($cnpj)
        {
            $cotas = DB::connection('eventos')
                        ->select('select NUMERODACOTA, NOMEDOTITULAR from ASS001 a where a.CIC = :cnpj', [
                            'cnpj' => $cnpj
                        ]);

            if($cotas) {
                foreach ($cotas as $value) {
                    $cota['NUMERODACOTA']   = $value->NUMERODACOTA;
                    $cota['NOMEDOTITULAR']  = utf8_encode($value->NOMEDOTITULAR);
                }

                return $cota;
            }

            return json_encode(false);
        }

        /**
         * Realiza a criação das taxas e boletos para o associado
         *
         * @param   \Illuminate\Http\Response  $socio
         * @return  bool|int
         */
        public static function createTax($socio)
        {
            // Cria o Log de acesso
            static::createEventoLog($socio);

            // Recebe o número do mês, para que seja buscado os valores corretos
            $mes = date('m');

            // Realiza a criaçao das taxas
            DB::connection('eventos')
                ->insert("insert into TAXAS(COTA, DATA, PRODUTO, HISTORICO, HORA, VALOR, SITUACAO, QUANTIDADE)
                  values(?, current_date + 3, 47, 'Inscricao Evento', current_time, ?, 'V', ?)",
                [
		            $socio->NUMERODACOTA,
		            static::$valores[$mes][$socio->quantidade],
		            $socio->quantidade
		        ]);

            // Realiza a geração do código da cobrança
            $codigoCobranca = DB::connection('eventos')
                ->select("select CodigoCobranca from proc_geraCobranca_Geralv3(
                    substring(100 + extract(month from current_date) from 2 for 2) || '/' ||
                    extract(year from current_date), 0, 104, current_date + 3,'$socio->NUMERODACOTA', 0, 'Cobranca referente ao Encontro Nacional dos Usuarios do CSPLUS - 09 de Setembro de 2016',
                    addMonth(cast(substring(100 + extract(month from current_date) from 2 for 2) || '/01/' ||
                    extract(year from current_date) as Date), 1) - 1,
                    null,null,null,0,null,'F','F','F','F',
                    'FILTROTAXAS0131FFT  *000047','T','T','T', null)"
                );

            if($codigoCobranca) {
                foreach ($codigoCobranca as $c) {
                    $codigoCobranca = $c->CODIGOCOBRANCA;
                }

                // Roda a procedure para geração de boletos
                DB::connection('eventos')->statement('EXECUTE PROCEDURE PROC_GERACOBRANCA_BOLETOS('. $codigoCobranca .')');

                return $codigoCobranca;
            }

            return false;
        }

        /**
         * Realiza a criação de um log contendo os daddos do associado
         *
         * @param   \Illuminate\Http\Request  $socio
         */
        public static function createEventoLog($socio)
        {
            $conexao = AlternativeConnection::getConnection('eventos', false);

            $sql = ibase_prepare("INSERT INTO CONTATOS (COTA, DATAHORA, DATA, HORA, SITUACAO, OBSERVACAO)
                    values(?, current_timestamp, current_date, current_time, 'Inscricao Evento', ? || ' - ' || ? || ' - ' || ?)"
            );

            ibase_execute($sql, $socio->NUMERODACOTA, $socio->nome, $socio->cpf, $socio->cargo);

            ibase_close($conexao);
        }
    }
