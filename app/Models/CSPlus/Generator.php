<?php

    namespace CSPlus\Models\CSPlus;

    use DB;
    use CSPlus\Services\Database\Traits\ConnectionTrait;
    use Illuminate\Database\Eloquent\Model;

    /**
     * Generator
     *
     * Realiza a geração de IDs a partir dos generators criados no banco de dados
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 1.3.0
     * @since   22/06/2017
     */
    class Generator extends Model
    {
        use ConnectionTrait;

        /**
         * @var string
         */
        protected $connection = 'csplus';

        /**
         * Realiza a geração de novo ID baseado nas generators do banco de dados
         *
         * @param   string  $generator
         * @return  mixed
         */
        public static function newId($generator)
        {
            $static = new static();
            $static->changeConnection();

            $newId = DB::connection('csplus')
                    ->select('SELECT GEN_ID('.$generator.', 1) FROM RDB$DATABASE');

            if($newId) {
                foreach ($newId as $id) {
                    return $id->GEN_ID;
                }
            }
        }
    }
