<?php

    namespace CSPlus\Models\CSPlus;

    use CSPlus\Services\Database\Traits\ConnectionResolver;
    use Illuminate\Database\Eloquent\Model;

    /**
     * Configuracao
     *
     * Realiza o gerenciamento da tabela de configuração
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 1.1.0
     * @since   22/06/2017
     */
    class Configuracao extends Model
    {
        use ConnectionResolver;

        /**
         * @var string
         */
        protected $connection = 'csplus';

        /**
         * @var string
         */
        protected $table = 'CONFIGURACAO';

        /**
         * @var string
         */
        protected $primaryKey = 'CHAVE';

        /**
         * @var bool
         */
        public $timestamps = false;
    }
