<?php

    namespace CSPlus\Models\CSPlus;

    use CSPlus\Services\Database\Traits\ConnectionResolver;
    use Illuminate\Database\Eloquent\Model;

    /**
     * MenuAssociado
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 1.0.0
     * @since   31/07/2017
     * @package CSPlus\Models\CSPlus
     */
    class MenuAssociado extends Model
    {
        use ConnectionResolver;

        /**
         * @var string
         */
        protected $connection = 'csplus';

        /**
         * @var string
         */
        protected $table = 'MENU_ASSOCIADOS';

        /**
         * @var string
         */
        protected $primaryKey = 'ID';

        /**
         * @var bool
         */
        public $incrementing = false;

        /**
         * @var bool
         */
        public $timestamps  = false;
    }
