<?php

    namespace CSPlus\Models\CSPlus\Telefonia;

    use CSPlus\Services\Database\Traits\ConnectionResolver;
    use Illuminate\Database\Eloquent\Model;

    /**
     * ClaroConta
     *
     * Realiza o gerenciamento da tabela CLARO_CONTA
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 1.1.0
     * @since   22/06/2017
     */
    class ClaroConta extends Model
    {
        use ConnectionResolver;

        /**
         * @var string
         */
        protected $connection = 'csplus';

        /**
         * @var string
         */
        protected $table = 'CLARO_CONTA';

        /**
         * @var string
         */
        protected $primaryKey = 'ID';

        /**
         * @var bool
         */
        public $incrementing = false;

        /**
         * @var bool
         */
        public $timestamps = false;
    }
