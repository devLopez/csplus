<?php

    namespace CSPlus\Models\CSPlus\Telefonia;

    use CSPlus\Services\Database\Traits\ConnectionResolver;
    use Illuminate\Database\Eloquent\Model;

    /**
     * ClaroTitular
     *
     * Classe desenvolvida para gerenciamento da tabela CLARO_TITULAR
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 1.1.0
     * @since   22/06/2017
     */
    class ClaroTitular extends Model
    {
        use ConnectionResolver;

        /**
         * @var string
         */
        protected $connection = 'csplus';

        /**
         * @var string
         */
        protected $table = 'CLARO_TITULAR';

        /**
         * @var string
         */
        protected $primaryKey = 'ID_CLARO_TITULAR';

        /**
         * @var bool
         */
        public $incrementing = false;

        /**
         * @var bool
         */
        public $timestamps = false;
    }
