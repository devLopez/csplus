<?php

    namespace CSPlus\Models\CSPlus\Convites;

    use Carbon\Carbon;
    use CSPlus\Services\Database\Traits\ConnectionResolver;
    use CSPlus\Services\Repository\Contracts\ModelContract;
    use CSPlus\Services\Repository\GeneratorRetriever;
    use Illuminate\Database\Eloquent\Model;

    /**
     * Convidado
     *
     * Model responsável pelo gerenciamento dos Convidados
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 1.2.0
     * @since   22/06/2017
     */
    class Convidado extends Model implements ModelContract
    {
        use ConnectionResolver, GeneratorRetriever;

        /**
         * @var string
         */
        protected $connection = 'csplus';

        /**
         * @var string
         */
        protected $table = 'CONVIDADOS';

        /**
         * @var string
         */
        protected $primaryKey = 'CODIGO';

        /**
         * @var bool
         */
        public $incrementing = false;

        /**
         * @var bool
         */
        public $timestamps = false;

        /**
         * @var array
         */
        protected $fillable = [
            'CODIGO',
            'NOME',
            'CI',
            'DATANASC',
            'OBSERVACAO'
        ];

        /**
         * @var string
         */
        protected $generator = 'GEN_CONVIDADOS_CODIGO';

        /**
         * @param   string  $data
         * @return  string
         */
        public function getDataNascAttribute($data)
        {
            if($data){
                return Carbon::parse($data)->format('d/m/Y');
            }
        }

        /**
         * @return  \Illuminate\Database\Eloquent\Relations\HasMany
         */
        public function convites()
        {
            return $this->hasMany(Convite::class, 'CONVIDADO');
        }
    }
