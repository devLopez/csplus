<?php

    namespace CSPlus\Models\CSPlus\Convites;

    use DB;
    use CSPlus\Services\Database\Traits\ConnectionResolver;
    use Illuminate\Database\Eloquent\Model;

    /**
     * DadosConvite
     *
     * Classe esponsável pela geração dos dados do convite
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 1.1.0
     * @since   22/06/2017
     */
    class DadosConvite extends Model
    {
        use ConnectionResolver;

        /**
         * @var string
         */
        protected $connection = 'csplus';

        /**
         * @var string
         */
        protected $table = '';

        /**
         * @var bool
         */
        public $incrementing = false;

        /**
         * @var bool
         */
        public $timestamps = false;

        /**
         * @param   int  $matricula
         * @param   string  $referencia
         * @param   Convidado $convidado
         * @return  array
         */
        public static function carregaDadosConvite($matricula, $referencia, Convidado $convidado)
        {
            new static();

            return (object) DB::connection('csplus')->select("select RCNV_Idade, RCNV_Situacao, RCNV_ConviteGratuito,
                RCNV_LimiteMesmoConvidado, RCNV_LimitePeriodoMAX, RCNV_LimitePeriodo,
                RCNV_Mensagem from proc_CarregaDadosConvite(?, ?, ?, ?, ?)
            ", [0, 0, $matricula, $referencia, $convidado->CODIGO])[0];
        }
    }
