<?php

    namespace CSPlus\Models\CSPlus\Convites;

    use CSPlus\Services\Database\Traits\ConnectionResolver;
    use CSPlus\Models\CSPlus\Associado\Ass001;
    use Illuminate\Database\Eloquent\Model;


    /**
     * Convite
     *
     * Realiza o gerenciamento da tabela convites
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 2.1.0
     * @since   22/06/2017
     */
    class Convite extends Model
    {
        use ConnectionResolver;

        /**
         * @var string
         */
        protected $connection = 'csplus';

        /**
         * @var string
         */
        protected $table = 'CONVITES';

        /**
         * @var string
         */
        protected $primaryKey = 'CODIGO';

        /**
         * @var bool
         */
        public $incrementing = false;

        /**
         * @var bool
         */
        public $timestamps = false;

        /**
         * @var array
         */
        protected $fillable = [
            'HORAINI',
            'HORAFIM'
        ];

        /**
         * @return  \Illuminate\Database\Eloquent\Relations\BelongsTo
         */
        public function associado()
        {
            return $this->belongsTo(Ass001::class, 'NUMERODACOTA', 'COTA');
        }

        /**
         * @return  \Illuminate\Database\Eloquent\Relations\BelongsTo
         */
        public function convidado()
        {
            return $this->belongsTo(Convidado::class, 'CONVIDADO');
        }
    }
