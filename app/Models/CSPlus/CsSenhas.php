<?php

    namespace CSPlus\Models\CSPlus;

    use CSPlus\Services\Database\Traits\ConnectionResolver;
    use Illuminate\Database\Eloquent\Model;

    /**
     * CsSenhas
     *
     * Realiza o gerenciamento da tabela de usuários e senhas administrativas
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 1.2.0
     * @since   22/06/2017
     */
    class CsSenhas extends Model
    {
        use ConnectionResolver;

        /**
         * @var string
         */
        protected $connection = 'csplus';

        /**
         * @var string
         */
        protected $table = 'CSSENHAS';

        /**
         * @var string
         */
        protected $primaryKey = 'CODUSUARIO';

        /**
         * @var bool
         */
        public $timestamps = false;

        /**
         * @var array
         */
        protected $hidden = ['SENHA'];
    }
