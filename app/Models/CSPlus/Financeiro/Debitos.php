<?php

    namespace CSPlus\Models\CSPlus\Financeiro;

    use DB;
    use CSPlus\Services\Database\Traits\ConnectionResolver;
    use Illuminate\Database\Eloquent\Model;

    /**
     * Debitos
     *
     * Realiza o gerenciamento dos débitos do associado
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 2.1.0
     * @since   22/06/2017
     */
    class Debitos extends Model
    {
        use ConnectionResolver;

        /**
         * @var string
         */
        protected $connection = 'csplus';
    }
