<?php

    namespace CSPlus\Models\CSPlus\Financeiro;

    use CSPlus\Services\Database\Traits\ConnectionResolver;
    use Illuminate\Database\Eloquent\Model;

    /**
     * Taxas
     *
     * Realiza o gerenciamento da tabela de taxas
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 1.1.0
     * @since   22/06/2017
     */
    class Taxas extends Model
    {
        use ConnectionResolver;

        /**
         * @var string
         */
        protected $connection = 'csplus';

        /**
         * @var string
         */
        protected $table = 'TAXAS';

        /**
         * @var string
         */
        protected $primaryKey = 'CODIGO';

        /**
         * @var bool
         */
        public $incrementing = false;

        /**
         * @var bool
         */
        public $timestamps = false;

        /**
         * @var array
         */
        protected $fillable = [
            'CODIGO',
            'COTA',
            'DEP',
            'DATA',
            'PRODUTO',
            'HISTORICO',
            'QUANTIDADE',
            'VALOR'
        ];
    }
