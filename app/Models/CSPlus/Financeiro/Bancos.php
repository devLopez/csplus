<?php

    namespace CSPlus\Models\CSPlus\Financeiro;

    use CSPlus\Services\Database\Traits\ConnectionResolver;
    use Illuminate\Database\Eloquent\Model;

    /**
     * Bancos
     *
     * Realiza o gerenciamento da tabela bancos
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 2.1.0
     * @since   22/06/2017
     */
    class Bancos extends Model
    {
        use ConnectionResolver;

        /**
         * @var string
         */
        protected $connection = 'csplus';

        /**
         * @var string
         */
        protected $table= 'BANCOS';

        /**
         * @var string
         */
        protected $primaryKey = 'CODIGO';

        /**
         * @var bool
         */
        public $timestamps = false;
    }
