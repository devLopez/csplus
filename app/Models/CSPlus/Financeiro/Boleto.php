<?php

    namespace CSPlus\Models\CSPlus\Financeiro;

    use DB;
    use CSPlus\Exceptions\Database\InvoiceNotCreated;

    /**
     * Boleto
     *
     * Classe responsável pela geração do boleto bancário para o associado
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 1.2.0
     * @since   01/07/2017
     */
    class Boleto
    {
        /**
         * @var int
         */
        protected $matricula;

        /**
         * @var Taxas
         */
        protected $taxas;

        /**
         * @var int
         */
        public $cobranca;

        public function __construct()
        {
            $this->taxas = new Taxas();
        }

        /**
         * @param   int  $matricula
         * @return  $this
         */
        public function setMatricula($matricula)
        {
            $this->matricula = $matricula;

            return $this;
        }

        /**
         * Seta o código da cobrança
         *
         * @return  $this
         * @throws  Exception
         */
        public function setCobranca()
        {
            $cobranca = DB::connection('csplus')
                ->select("select CodigoCobranca from proc_geraCobranca_Geralv3(
                    substring(100 + extract(month from current_date) from 2 for 2) || '/' || extract(year from current_date),
                    0, 237, current_date + 3, ".$this->matricula.", 0, '',
                    addMonth(cast(substring(100 + extract(month from current_date) from 2 for 2) || '/28/' || extract(year from current_date) as Date),
                    1) - 1, null,null,null,0,null,'F','F','F','F', '','T','T','T', null)");

            if($cobranca != null) {
                foreach ($cobranca as $c) {
                    $this->cobranca = $c->CODIGOCOBRANCA;
                }
            } else {
                throw new InvoiceNotCreated('Impossível resgatar o código da cobrança. Para impressão do seu boleto, acesse o seu painel ou entre em contato com o seu clube');
            }

            return $this;
        }

        public function setCobranca_inicial()
        {
            //Fixei aqui o dia para 28 para pegar teoricamente o ultimo dia do proximo mes
            // para o limite da pesquisa do débito

            $cobranca = DB::connection('csplus')
                ->select("select CodigoCobranca from proc_geraCobranca_Geralv3(
                    substring(100 + extract(month from current_date) from 2 for 2) || '/' || extract(year from current_date),
                    0, 237, current_date + 3, ".$this->matricula.", 0, '',
                    addMonth(cast(substring(100 + extract(month from current_date) from 2 for 2) || '/28/' || extract(year from current_date) as Date),
                    1) - 1, null,null,null,0,null,'F','F','F','F', '','T','T','T', null)");

            if($cobranca != null) {
                foreach ($cobranca as $c) {
                    $this->cobranca = $c->CODIGOCOBRANCA;
                }
            } else {
                throw new InvoiceNotCreated('Impossível resgatar o código da cobrança. Para impressão do seu boleto, acesse o seu painel ou entre em contato com o seu clube');
            }

            return $this;
        }

        /**
         * @return  $this
         */
        public function generate()
        {
            DB::connection('csplus')->statement('EXECUTE PROCEDURE PROC_GERACOBRANCA_BOLETOS('.$this->cobranca.')');

            return $this;
        }

        /**
         * @return int
         */
        public function getCobranca()
        {
            return (int) $this->cobranca;
        }
    }
