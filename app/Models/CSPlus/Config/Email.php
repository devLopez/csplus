<?php

    namespace CSPlus\Models\CSPlus\Config;

    use CSPlus\Services\Database\Traits\ConnectionResolver;
    use Illuminate\Database\Eloquent\Model;

    /**
     * ConfiguracaoEmail
     *
     * Classe responsavel pela tabela CONFIGURACAO_EMAIL
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 1.0.1
     * @since   30/06/2017
     */
    class Email extends Model
    {
        use ConnectionResolver;

        /**
         * @var string
         */
        protected $connection = 'csplus';

        /**
         * @var string
         */
        protected $table = 'CONFIGURACAO_EMAIL';

        /**
         * @var string
         */
        protected $primaryKey = 'ID';

        /**
         * @var boolean
         */
        public $timestamps = false;
    }
