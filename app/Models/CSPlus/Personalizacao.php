<?php

    namespace CSPlus\Models\CSPlus;

    use CSPlus\Services\Database\Traits\ConnectionResolver;
    use Illuminate\Database\Eloquent\Model;

    /**
     * Personalizacao
     *
     * Classe responsável pelo gerenciamento da tabela Personalizacao
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 1.1.0
     * @since   22/06/2017
     */
    class Personalizacao extends Model
    {
        use ConnectionResolver;

        /**
         * @var string
         */
        protected $connection = 'csplus';

        /**
         * @var string
         */
        protected $table = 'PERSONALIZACAO';

        /**
         * @var bool
         */
        protected $primaryKey = false;

        /**
         * @var bool
         */
        public $incrementing = false;
    }
