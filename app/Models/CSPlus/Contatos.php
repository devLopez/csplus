<?php

    namespace CSPlus\Models\CSPlus;

    use DB;
    use CSPlus\Services\Database\Traits\ConnectionResolver;
    use CSPlus\Services\Repository\Contracts\ModelContract;
    use CSPlus\Services\Repository\GeneratorRetriever;
    use Illuminate\Database\Eloquent\Model;

    /**
     * Contatos
     *
     * Realiza o gerenciamento de Logs administrativos do sistema
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 2.5.0
     * @since   22/06/2017
     */
    class Contatos extends Model implements ModelContract
    {
        use ConnectionResolver, GeneratorRetriever;

        /**
         * @var string
         */
        protected $connection = 'csplus';

        /**
         * @var string
         */
        protected $table = 'CONTATOS';

        /**
         * @var string
         */
        protected $primaryKey = 'CODIGO';

        /**
         * @var bool
         */
        public $incrementing = false;

        /**
         * @var bool
         */
        public $timestamps = false;

        /**
         * @var array
         */
        protected $fillable     = [
            'CODIGO',
            'COTA',
            'DATAHORA',
            'DATA',
            'HORA',
            'SITUACAO',
            'OBSERVACAO',
            'CODSTATUS'
        ];

        /**
         * @var string
         */
        protected $generator = 'GEN_CONTATOS_CODIGO';

        /**
         * @param   array  $log
         */
        public static function saveLog($log)
        {
            DB::connection('csplus')->unprepared("
                INSERT INTO CONTATOS (COTA, DATAHORA, DATA, HORA, SITUACAO, OBSERVACAO)
                VALUES({$log['COTA']}, '{$log['DATAHORA']}', '{$log['DATA']}', '{$log['HORA']}',
                    '{$log['SITUACAO']}', {$log['OBSERVACAO']}
                )
            ");
        }
    }
