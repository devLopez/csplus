<?php

    namespace CSPlus\Models\CSPlus;

    use CSPlus\Services\Database\AlternativeConnection;

    /**
     * Class LogSistema
     *
     * Gerencia os logs técnicos gerados pelo csplus
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 2.1.0
     * @since   17/01/2017
     */
    class LogSistema
    {
        /**
         * @param   array  $log
         * @return  void
         */
        public static function saveLog($log)
        {
            $conexao = AlternativeConnection::getConnection('csplus');

            /* Realiza o prepare do statement para que os dados sejam salvos corretamente */
            $sql = ibase_prepare("
                INSERT INTO LOG_SISTEMA (
                DATAHORA, CODUSUARIO, PROGRAMA, IP, INSTRUCAO, FORMULARIO, COTA
                ) VALUES (?, ?, ?, ?, ?, ?, ?)
            ");

            ibase_execute($sql,
                $log['DATAHORA'],
                $log['CODUSUARIO'],
                $log['PROGRAMA'],
                $log['IP'],
                $log['INSTRUCAO'],
                $log['FORMULARIO'],
                $log['COTA']
            );

            ibase_close($conexao);
        }
    }
