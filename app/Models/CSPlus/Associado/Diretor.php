<?php

    namespace CSPlus\Models\CSPlus\Associado;

    use CSPlus\Services\Database\Traits\ConnectionResolver;
    use Illuminate\Database\Eloquent\Model;

    /**
     * Diretor
     *
     * Classe responsável pelo gerenciamento da tabela ADM_DIRETORIA
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 1.1.0
     * @since   22/06/2017
     */
    class Diretor extends Model
    {
        use ConnectionResolver;

        /**
         * @var string
         */
        protected $connection = 'csplus';

        /**
         * @var string
         */
        protected $table = 'ADM_DIRETORIA';

        /**
         * @var string
         */
        protected $primaryKey = 'CODIGO';

        /**
         * @var bool
         */
        public $incrementing = false;

        /**
         * @var bool
         */
        public $timestamps = false;

        /**
         * @return  \Illuminate\Database\Eloquent\Relations\BelongsTo
         */
        public function associado()
        {
            return $this->belongsTo(Ass001::class, 'NUMERODACOTA', 'COTA');
        }
    }
