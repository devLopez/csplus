<?php

    namespace CSPlus\Models\CSPlus\Associado;

    use CSPlus\Services\Database\Traits\ConnectionResolver;
    use Illuminate\Database\Eloquent\Model;

    /**
     * Colabora
     *
     * Realiza o gerenciamento da tabela colabora
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 1.2.0
     * @since   22/06/2017
     */
    class Colabora extends Model
    {
        use ConnectionResolver;

        /**
         * @var string
         */
        protected $connection = 'csplus';

        /**
         * @var string
         */
        protected $table = 'COLABORA';

        /**
         * @var string
         */
        protected $primaryKey = 'CODIGO';

        /**
         * @var bool
         */
        public $timestamps = false;
    }
