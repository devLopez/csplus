<?php

    namespace CSPlus\Models\CSPlus\Associado;

    use CSPlus\Services\Database\Traits\ConnectionResolver;
    use Illuminate\Database\Eloquent\Model;

    /**
     * PropriedadesWeb
     *
     * Define as propriedades web que o associado terá
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 1.1.0
     * @since   22/06/2017
     */
    class PropriedadesWeb extends Model
    {
        use ConnectionResolver;

        /**
         * @var string
         */
        protected $connection = 'csplus';

        /**
         * @var string
         */
        protected $table = 'PROPRIEDADES_WEB';

        /**
         * @var bool
         */
        protected $primaryKey = false;
    }
