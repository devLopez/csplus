<?php

    namespace CSPlus\Models\CSPlus\Associado;

    use Illuminate\Database\Eloquent\Model;

    use Date;
    use CSPlus\Services\Database\Traits\ConnectionResolver;
    use CSPlus\Services\Repository\Contracts\ModelContract;
    use CSPlus\Services\Repository\GeneratorRetriever;
    use CSPlus\Services\Words\Mask;
    use JansenFelipe\Utils\Mask as MaskDefinitions;

    /**
     * Ass001
     *
     * Realiza o gerenciamento dos dados do associado
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 2.10.0
     * @since   14/06/2017
     */
    class Ass001 extends Model implements ModelContract
    {
        use ConnectionResolver, GeneratorRetriever;

        /**
         * @var string
         */
        protected $connection = 'csplus';

        /**
         * @var string
         */
        protected $table = 'ASS001';

        /**
         * @var string
         */
        protected $primaryKey = 'NUMERODACOTA';

        /**
         * @var bool
         */
        public $incrementing = false;

        /**
         * @var bool
         */
        public $timestamps = false;

        /**
         * @var string
         */
        protected $generator = 'GEN_ASS';

        /**
         * @var array
         */
        protected $fillable = [
            'NUMERODACOTA',
            'NOMEDOTITULAR',
            'DATAAQUISICAO',
            'APELIDO',
            'CATEGORIADACOTA',
            'CEPRESIDENCIAL',
            'ENDRES_LOGRADOURO',
            'ENDRES_NUMERO',
            'ENDRES_COMPLEMENTO',
            'BAIRRORESIDENCIAL',
            'CIDADERESIDENCIAL',
            'ESTADORESIDENCIAL',
            'TELEFONER1',
            'TELEFONER2',
            'CAIXAPOSTAL',
            'DATANASCIMENTO',
            'RG',
            'RG_ORGAO_EXPEDIDOR',
            'RG_DATA_EXPEDICAO',
            'SEXO',
            'ESTADOCIVIL',
            'OBSERVACOES',
            'EMPRESA',
            'FUNCAO',
            'DEPARTAMENTO_EMPRESA',
            'MATRICULA_EMPRESA',
            'REGISTRO_CLASSE',
            'SITUACAO_EMPRESA',
            'COMPLEMENTOCOMERCIAL',
            'ADMISSAO_EMPRESA',
            'AFASTAMENTO_EMPRESA',
            'CAIXAPOSTAL_EMPRESA',
            'TELEFONEC1',
            'TELEFONEC2',
            'CEPCOMERCIAL',
            'ENDCOM_LOGRADOURO',
            'ENDCOM_NUMERO',
            'ENDCOM_COMPLEMENTO',
            'BAIRROCOMERCIAL',
            'CIDADECOMERCIAL',
            'ESTADOCOMERCIAL',
            'ENDERECOCOBRANCA',
            'DESCONTOFOLHA',
            'APOSENTADO',
            'NACIONALIDADE',
            'NATURALIDADE',
            'PAI',
            'MAE',
            'SANGUE',
            'RESPONSAVEL',
            'CORRETOR',
            'BENEMERITO',
            'BANCO',
            'AGENCIA',
            'TIPOCONTA',
            'CONTACORRENTE',
            'DIGITOCC',
            'DEBITOCONTAAPROVADO',
        ];

        /**
         * @var array
         */
        protected $appends = ['isDiretor'];

        /**
         * @var array
         */
        protected $hidden = ['dependentes'];

        /**
         * @return  \Illuminate\Database\Eloquent\Relations\BelongsTo
         */
        public function categoria()
        {
            return $this->belongsTo(Cat001::class, 'CATEGORIADACOTA', 'CODIGO');
        }

        /**
         * @return  \Illuminate\Database\Eloquent\Relations\HasOne
         */
        public function senha()
        {
            return $this->hasOne(SenhaWeb::class, 'COTA', 'NUMERODACOTA');
        }

        /**
         * @return  \Illuminate\Database\Eloquent\Relations\HasMany
         */
        public function dependentes()
        {
            return $this->hasMany(Dep001::class, 'COTADEPENDENTE', 'NUMERODACOTA');
        }

        /**
         * @return  \Illuminate\Database\Eloquent\Relations\HasMany
         */
        public function convites()
        {
            return $this->hasMany(Convite::class, 'COTA', 'NUMERODACOTA');
        }

        /**
         * @return \Illuminate\Database\Eloquent\Relations\HasMany
         */
        public function cargos()
        {
            return $this->hasMany(Diretor::class, 'COTA', 'NUMERODACOTA');
        }

        /**
         * @param   string  $value
         */
        public function setDataNascimentoAttribute($value)
        {
            if($value) {
                $value = Date::toSql($value);
            }

            $this->attributes['DATANASCIMENTO'] = $value;
        }

        /**
         * @param   string  $value
         */
        public function setTelefoner1Attribute($value)
        {
            if($value) {
                $value = Mask::mask(
                    Mask::unMask($value), MaskDefinitions::TELEFONE
                );
            }

            $this->attributes['TELEFONER1'] = $value;
        }

        /**
         * @param   string  $value
         */
        public function setTelefoner2Attribute($value)
        {
            if($value) {
                $value = Mask::mask(
                    Mask::unMask($value), MaskDefinitions::TELEFONE
                );
            }

            $this->attributes['TELEFONER2'] = $value;
        }

        /**
         * @param   string  $value
         */
        public function setTelefonec1Attribute($value)
        {
            if($value) {
                $value = Mask::mask(
                    Mask::unMask($value), MaskDefinitions::TELEFONE
                );
            }

            $this->attributes['TELEFONEC1'] = $value;
        }

        /**
         * @param   string  $value
         */
        public function setTelefonec2Attribute($value)
        {
            if($value) {
                $value = Mask::mask(
                    Mask::unMask($value), MaskDefinitions::TELEFONE
                );
            }

            $this->attributes['TELEFONEC2'] = $value;
        }

        /**
         * @param   string  $cep
         */
        public function setCepResidencialAttribute($cep)
        {
            if($cep) {
                $cep = Mask::unmask($cep);
            }

            $this->attributes['CEPRESIDENCIAL'] = $cep;
        }

        /**
         * @param   string  $cep
         * @return  string
         */
        public function getCepResidencialAttribute($cep)
        {
            return Mask::mask($cep, '#####-###');
        }

        /**
         * @param   string  $cep
         */
        public function setCepComercialAttribute($cep)
        {
            if($cep) {
                $cep = Mask::unmask($cep);
            }

            $this->attributes['CEPCOMERCIAL'] = $cep;
        }

        /**
         * @param   string  $cep
         * @return  string
         */
        public function getCepComercialAttribute($cep)

        {
            return Mask::mask($cep, '#####-###');
        }

        /**
         * @param   string  $data
         * @return  string
         */
        public function getDataAquisicaoAttribute($data)
        {
            return Date::toBr($data);
        }

        /**
         * @param   string  $data
         * @return  string
         */
        public function getDataNascimentoAttribute($data)
        {
            return Date::toBr($data);
        }

        /**
         * @param   string  $data
         * @return  string
         */
        public function getRgDataExpedicaoAttribute($data)
        {
            return Date::toBr($data);
        }

        /**
         * @param   string  $data
         * @return  string
         */
        public function getAdmissaoEmpresaAttribute($data)
        {
            return Date::toBr($data);
        }

        /**
         * @param   string  $data
         * @return  string
         */
        public function getAfastamentoEmpresaAttribute($data)
        {
            return Date::toBr($data);
        }

        /**
         * @return bool
         */
        public function getIsDiretorAttribute()
        {
            $isDiretor = $this->cargos()
                              ->whereRaw('current_date between inicio_mandato and termino_mandato')
                              ->count();

            return ($isDiretor > 0) ? true : false;
        }
    }
