<?php

    namespace CSPlus\Models\CSPlus\Associado;

    use CSPlus\Services\Database\Traits\ConnectionResolver;
    use Illuminate\Database\Eloquent\Model;

    use CSPlus\Services\Words\Mask;
    use JansenFelipe\Utils\Mask as MaskDefinitions;

    /**
     * Dep001
     *
     * Realiza o gerenciamento dos dependentes do associado
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 1.6.0
     * @since   22/06/2017
     */
    class Dep001 extends Model
    {
        use ConnectionResolver;

        /**
         * @var string
         */
        protected $connection = 'csplus';

        /**
         * @var string
         */
        protected $table = 'DEP001';

        /**
         * @var string
         */
        protected $primaryKey = 'ID';

        /**
         * @var bool
         */
        public $timestamps = false;

        /**
         * Atributos Mass Assigned
         *
         * @var array
         */
        protected $fillable = [
            'CODIGODEPENDENTE',
            'COTADEPENDENTE',
            'NOMEDEPENDENTE',
            'CIC',
            'RG',
            'RG_DATA_EXPEDICAO',
            'RG_ORGAO_EXPEDIDOR',
            'FUNCAO',
            'PAI',
            'MAE',
            'TELEFONE',
            'TELEFONE2',
            'CAIXAPOSTAL',
            'SEXO',
            'ESTADOCIVIL',
            'DATANASCIMENTO',
            'GRAUPARENTESCO',
            'ATIVO'
        ];

        /**
         * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
         */
        public function associado()
        {
            return $this->belongsTo(Ass001::class, 'NUMERODACOTA', 'COTADEPENDENTE');
        }

        /**
         * @param   string  $value
         */
        public function setTelefoneAttribute($value)
        {
            if($value) {
                $value = Mask::mask(
                    Mask::unMask($value), MaskDefinitions::TELEFONE
                );
            }

            $this->attributes['TELEFONE'] = $value;
        }

        /**
         * @param   string  $value
         */
        public function setTelefone2Attribute($value)
        {
            if($value) {
                $value = Mask::mask(
                    Mask::unMask($value), MaskDefinitions::TELEFONE
                );
            }

            $this->attributes['TELEFONE2'] = $value;
        }
    }
