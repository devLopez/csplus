<?php

    namespace CSPlus\Models\CSPlus\Associado;

    use DB;
    use CSPlus\Services\Database\Traits\ConnectionResolver;
    use CSPlus\Services\Database\AlternativeConnection;
    use Illuminate\Database\Eloquent\Model;

    /**
     * Fotos
     *
     * Realiza o gerenciamento da tabela fotos
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 2.4.0
     * @since   22/06/2017
     */
    class Fotos extends Model
    {
        use ConnectionResolver;

        /**
         * @var string
         */
        protected $connection = 'csimagens';

        /**
         * @var string
         */
        protected $table = 'FOTOS';

        /**
         * @var string
         */
        protected $primaryKey = 'CODIGO';

        /**
         * @var bool
         */
        public $timestamps = false;

        /**
         * @var array
         */
        protected $fillable     = [
            'CODIGO',
            'TIPO',
            'IMAGEM',
            'DATAHORA',
            'TIPOIMG'
        ];

        /**
         * @param   int  $associado
         * @param   int  $tipo
         * @return  string
         */
        public static function getFoto($associado, $tipo = 0)
        {
            new static();

            $foto = DB::connection('csimagens')
                        ->select("select CODIGO, TIPO, coalesce(imagem_mini, imagem) as IMAGEM from FOTOS where codigo = ? and tipo = ?", [
                            $associado, $tipo
                        ]);

            if($foto) {
                foreach ($foto as $f) {
                    $imagem = base64_encode($f->IMAGEM);
                }

                return $imagem;
            }

            return '';
        }

        /**
         * @param   int  $matricula
         * @param   int  $tipo
         * @param   string  $foto
         * @return  bool
         */
        public static function addFoto($matricula, $tipo, $foto)
        {
            $conexao = AlternativeConnection::getConnection('csimagens');

            /* Realiza o prepare do statement para que os dados sejam salvos corretamente */
            $img_inserido= ibase_prepare("
                INSERT INTO FOTOS (CODIGO, TIPO, IMAGEM, DATAHORA, TIPOIMG) VALUES (?, ?, ?, current_timestamp, 'jpg')
            ");

            $inseriu = ibase_execute($img_inserido, $matricula, $tipo, $foto);
            ibase_close($conexao);
            return $inseriu;
        }
    }
