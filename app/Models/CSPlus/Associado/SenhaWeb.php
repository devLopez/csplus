<?php

    namespace CSPlus\Models\CSPlus\Associado;

    use Illuminate\Database\Eloquent\Model;
    use CSPlus\Services\Database\Traits\ConnectionResolver;
    use CSPlus\Services\Repository\Contracts\ModelContract;
    use CSPlus\Services\Repository\GeneratorRetriever;

    /**
     * SenhaWeb
     *
     * Gerencia a tabela de senhas do associado
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 1.4.0
     * @since   22/06/2017
     */
    class SenhaWeb extends Model implements ModelContract
    {
        use ConnectionResolver, GeneratorRetriever;

        /**
         * @var string
         */
        protected $connection = 'csplus';

        /**
         * @var string
         */
        protected $table = 'SENHAWEB';

        /**
         * @var string
         */
        protected $primaryKey = 'ID';

        /**
         * @var bool
         */
        public $timestamps = false;

        /**
         * @var bool
         */
        public $incrementing = false;

        /**
         * @var string
         */
        protected $generator = 'GEN_SENHAWEB_ID';
    }
