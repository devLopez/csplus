<?php

    namespace CSPlus\Models\CSPlus\Associado;

    use CSPlus\Services\Database\Traits\ConnectionResolver;
    use CSPlus\Services\Repository\Contracts\ModelContract;
    use CSPlus\Services\Repository\GeneratorRetriever;
    use Illuminate\Database\Eloquent\Model;

    /**
     * Cat001
     *
     * Realiza o gerenciamento da tabela de categorias de cota
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 1.3.0
     * @since   22/06/2017
     */
    class Cat001 extends Model implements ModelContract
    {
        use ConnectionResolver, GeneratorRetriever;

        /**
         * @var string
         */
        protected $connection = 'csplus';

        /**
         * @var string
         */
        protected $table = 'CAT001';

        /**
         * @var string
         */
        protected $primaryKey = 'CODIGO';

        /**
         * @var bool
         */
        public $incrementing = false;

        /**
         * @var bool
         */
        public $timestamps =  false;

        /**
         * @var string
         */
        protected $generator = 'GEN_CAT001_CODIGO';
    }
