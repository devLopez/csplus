<?php

	namespace CSPlus\Models\CSMobile;

	use Illuminate\Foundation\Auth\User as Authenticatable;

	/**
	 * AdminUser
	 *
	 * Classe desenvolvida para gerenciamento dos usuários administrativos
	 * Não é utilizada em conjunto com o CSPlus
	 *
	 * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
	 * @version 1.0.0
     * @since   19/04/2017
	 */
	class AdminUser extends Authenticatable
	{
        /**
         * @var string
         */
	    protected $table = 'ADMIN_USERS';

        /**
         * @var array
         */
		protected $fillable = [
			'NAME', 'EMAIL', 'PASSWORD'
		];

        /**
         * @var array
         */
		protected $hidden = [
			'PASSWORD', 'REMEMBER_TOKEN'
		];
	}
