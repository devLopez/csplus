<?php

    namespace CSPlus\Models\CSMobile;

    use Illuminate\Database\Eloquent\Model;

    /**
     * Cliente
     *
     * Realiza o gerenciamento dos dados dos clientes que terão acesso ao sistema
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 1.3.0
     * @since   26/06/2017
     */
    class Cliente extends Model
    {
        /**
         * @var string
         */
        protected $table = 'CLIENTES';

        /**
         * @var string
         */
        protected $primaryKey = 'CLIENTE';

        /**
         * @var bool
         */
        public $incrementing = false;

        /**
         * @var array
         */
        protected $fillable = [
            'CLIENTE',
            'BD_PRINCIPAL',
            'BD_IMAGENS',
            'HOST',
            'USER',
            'PASSWORD',
            'ROLE',
            'AREA_ASSOCIADO',
            'AREA_ADMINISTRATIVA',
            'LOGO',
            'COR_PRINCIPAL',
            'COR_SECUNDARIA',
            'USA_PAGSEGURO',
            'USA_CARTAO_CREDITO',
            'MAXIMO_PARCELAS',
            'PAGSEGURO_SANDBOX',
            'PARCELAS_SEM_JUROS'
        ];

        /**
         * @var array
         */
        protected $casts = [
            'AREA_ASSOCIADO'        => 'boolean',
            'AREA_ADMINISTRATIVA'   => 'boolean',
            'USA_PAGSEGURO'         => 'boolean',
            'USA_CARTAO_CREDITO'    => 'boolean',
            'PAGSEGURO_SANDBOX'     => 'boolean',
        ];

        /**
         * @var bool
         */
        public $timestamps = false;

        /**
         * Realiza o relacionamento com a tabela payment_tokens
         *
         * @return  \Illuminate\Database\Eloquent\Relations\HasOne
         */
        public function token()
        {
            return $this->hasOne(PaymentToken::class, 'CLIENTE_ID', 'CLIENTE');
        }
    }
