<?php

    namespace CSPlus\Models\CSMobile;

    use Illuminate\Database\Eloquent\Model;

    /**
     * PaymentToken
     *
     * Classe desenvolvida para gerenciamento da tabela payment_tokens
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 2.0.0
     * @since   26/06/2017
     */
    class PaymentToken extends Model
    {
        protected $table = 'PAYMENT_TOKENS';

        /**
         * @var array
         */
        protected $fillable = [
            'CLIENTE_ID',
            'EMAIL',
            'TOKEN_PRODUCAO',
            'TOKEN_SANDBOX',
            'VENDEDOR_SANDBOX'
        ];

        /**
         * @var bool
         */
        public $timestamps = false;

        /**
         * @return  \Illuminate\Database\Eloquent\Relations\BelongsTo
         */
        public function cliente()
        {
            return $this->belongsTo(Cliente::class, 'CLIENTE_ID');
        }
    }
