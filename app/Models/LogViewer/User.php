<?php

namespace CSPlus\Models\LogViewer;

use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * User
 *
 * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
 * @version 1.0.0
 * @since   26/01/2017
 * @package CSPlus\Models\LogViewer
 */
class User extends Authenticatable
{
    public $connection = 'logviewer';

    public $timestamps = false;

    /**
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password'
    ];

    /**
     * @var array
     */
    protected $hidden = [
        'hidden', 'remember_token'
    ];
}
