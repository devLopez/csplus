<?php

    namespace CSPlus\Exceptions\Database;

    use Exception;

    /**
     * InvoiceNotCreated
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 1.0.0
     * @since   01/07/2017
     */
    class InvoiceNotCreated extends Exception
    {
        /**
         * @var string
         */
        protected $message = 'Sua cota foi criada, mas seu boleto não pôde ser gerado. Acesse a área restrita para retirá-lo ou entre em contato com seu clube';
    }