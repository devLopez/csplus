<?php

    namespace CSPlus\Exceptions\Database;

    use Exception;

    /**
     * PasswordNotSaved
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 1.0.0
     * @since   01/07/2017
     */
    class PasswordNotSaved extends Exception
    {
        /**
         * @var string
         */
        protected $message = 'Sua senha não foi cadastrada. O seu login deve ser feito utilizando o seu CPF';
    }