<?php

    namespace CSPlus\Exceptions\FileUpload;

    use Exception;

    /**
     * PictureUploadException
     *
     * Exception desenvolvida para ser utilizada pela API de upload de imagens
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 2.0.0
     * @since   13/06/2017
     */
    class PictureUploadException extends Exception
    {
        protected $message = 'Não foi possível fazer o upload da imagem. Tente Novamente';
    }
