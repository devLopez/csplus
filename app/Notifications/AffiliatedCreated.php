<?php

    namespace CSPlus\Notifications;

    use Carbon\Carbon;
    use CSPlus\Models\CSPlus\Associado\Ass001 as Associado;
    use CSPlus\Services\Notification\Contracts\NotificationContract;
    use Illuminate\Notifications\Notification;
    use Illuminate\Notifications\Messages\MailMessage;

    /**
     * AffiliatedCreated
     *
     * Formata e envia uma notificação de cadastro de associados
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 1.1.0
     * @since   06/06/2017
     */
    class AffiliatedCreated extends Notification implements NotificationContract
    {
        /**
         * @var \CSPlus\Models\CSPlus\Associado\Ass001
         */
        protected $associado;

        /**
         * @var string
         */
        protected $sender;

        /**
         * Construtor da classe
         *
         * @param   Associado  $associado
         */
        public function __construct(Associado $associado)
        {
            $this->associado = $associado;
        }

        /**
         * @param   string  $sender
         * @return  void
         */
        public function setSender($sender)
        {
            $this->sender = $sender;
        }

        /**
         * Get the notification's delivery channels.
         *
         * @param  mixed  $notifiable
         * @return array
         */
        public function via($notifiable)
        {
            return ['mail'];
        }

        /**
         * Get the mail representation of the notification.
         *
         * @param   mixed  $notifiable
         * @return  \Illuminate\Notifications\Messages\MailMessage
         */
        public function toMail($notifiable)
        {
            return (new MailMessage)
                        ->from($this->sender)
                        ->subject('Novo cadastro de associado')
                        ->greeting('Olá')
                        ->line('Um novo associado acaba de se cadastrar')
                        ->line('Nome: ' . utf8_encode($this->associado->NOMEDOTITULAR))
                        ->line('Categoria: ' . utf8_encode($this->associado->categoria->DESCRICAO))
                        ->line('Data de Cadastro: ' . Carbon::now()->format('d/m/Y H:i:s'))
                        ->line('Obrigado por utilizar os Softwares ClubSystem');
        }
    }
