<?php

    namespace CSPlus\Notifications;

    use CSPlus\Models\CSPlus\Associado\Ass001 as Associado;
    use CSPlus\Services\Notification\Contracts\UserNotificationContract;
    use Illuminate\Notifications\Notification;
    use Illuminate\Notifications\Messages\MailMessage;

    class AccountCreated extends Notification implements UserNotificationContract
    {
        /**
         * @var Associado
         */
        protected $associado;

        /**
         * @var string
         */
        protected $sender;

        /**
         * @var string
         */
        protected $password;

        /**
         * Construtor da classe
         *
         * @param   Associado  $associado
         * @param   string  $password
         */
        public function __construct(Associado $associado, $password)
        {
            $this->associado    = $associado;
            $this->password     = $password;
        }

        /**
         * @param   string  $sender
         * @return  void
         */
        public function setSender($sender)
        {
            $this->sender = $sender;
        }

        /**
         * @return  string
         */
        public function getRouteForEmail()
        {
            return $this->associado->CAIXAPOSTAL;
        }

        /**
         * Get the notification's delivery channels.
         *
         * @param  mixed  $notifiable
         * @return array
         */
        public function via($notifiable)
        {
            return ['mail'];
        }

        /**
         * Get the mail representation of the notification.
         *
         * @param  mixed  $notifiable
         * @return \Illuminate\Notifications\Messages\MailMessage
         */
        public function toMail($notifiable)
        {
            $url = route('login.index', [\Client::clube(), 'associado']);

            return (new MailMessage)
                    ->from($this->sender)
                    ->success()
                    ->subject('Cadastro efetuado')
                    ->greeting('Prezado Associado')
                    ->line('Seu cadastro ABRAPHE foi realizado com sucesso')
                    ->line('Seguem os dados para seu acesso')
                    ->line('Seu usuário é: ' . $this->associado->CIC)
                    ->line('Sua Senha é:' . $this->password)
                    ->action('Acessar o sistema', $url)
                    ->line('Atenciosamente')
                    ->salutation('Equipe de Cadastro');
        }
    }
