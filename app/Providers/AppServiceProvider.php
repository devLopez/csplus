<?php

namespace CSPlus\Providers;

use Illuminate\Support\ServiceProvider;
use Validator;
use View;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->bootViewNamespaces();
        $this->registerValidationRules();
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        
    }

    /**
     * Realiza o boot dos namespaces de views
     *
     * @return  void
     */
    private function bootViewNamespaces()
    {
        View::addNamespace('Admin', resource_path('views/paginas/admin'));
        View::addNamespace('Socio', resource_path('views/paginas/associado'));
        view::addNamespace('PDF', resource_path('views/paginas/pdf'));

        View::addNamespace('Relatorio', resource_path('views/paginas/admin/relatorios'));

        View::addNamespace('Subscription', resource_path('views/paginas/subscription'));

        View::addNamespace('Bussiness', resource_path('views/paginas/bussiness'));

        View::addNamespace('Components', resource_path('views/components'));
    }

    /**
     * Registra as regras de validação customizadas
     *
     * @return  void
     */
    public function registerValidationRules()
    {
        Validator::extend('cpf_cnpj', 'CSPlus\Services\Validation\Validator@cpfCnpj');
    }
}
