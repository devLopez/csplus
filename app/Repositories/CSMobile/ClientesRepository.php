<?php

    namespace CSPlus\Repositories\CSMobile;

    use CSPlus\Models\CSMobile\Cliente;
    use CSPlus\Services\Repository\Repository;

    /**
     * ClientesRepository
     *
     * Classe responsável pelo gerenciamento do model Cliente
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 1.1.0
     * @since   27/06/2017
     */
    class ClientesRepository extends Repository
    {
        /**
         * @return string
         */
        public function model()
        {
            return Cliente::class;
        }

        /**
         * @param   int  $cliente
         * @return  int
         */
        public function findExistingClient($cliente)
        {
            return $this->model
                        ->where('cliente', $cliente)
                        ->count();
        }
    }