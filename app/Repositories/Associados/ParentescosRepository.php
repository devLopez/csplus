<?php

    namespace CSPlus\Repositories\Associados;

    use DB;

    use CSPlus\Models\CSPlus\Associado\Parentesco;
    use CSPlus\Services\Repository\Repository;
    use CSPlus\Services\Repository\Exceptions\ModelNotSavedException;

    /**
     * ParentescosRepository
     *
     * Realiza o gerenciamento do model Parentesco
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 1.0.0
     * @since   06/02/2017
     * @package CSPlus\Repositories\Associados
     */
    class ParentescosRepository extends Repository
    {
        /**
         * @return  Parentesco
         */
        public function model()
        {
            return Parentesco::class;
        }

        /**
         * Salva um novo parentesco
         *
         * @param   array  $data
         * @return  \Illuminate\Database\Eloquent\Model
         * @throws  ModelNotSavedException
         */
        public function save(array $data)
        {
            $data = array_filter(array_map('utf8_decode', $data));
            $data = array_map('trim', $data);

			// Recebe o nome da chave primária do model
			$primaryKey                 = $this->model->getKeyName();
			$this->model->$primaryKey   = $this->getId();

			foreach ($data as $k => $v) {
				$this->model->$k = $v;
			}

			if($this->model->save()) {
				return $this->model;
			}

			throw new ModelNotSavedException;
        }

        /**
         * @return  int
         */
        public function getId()
        {
            $model = $this->getBuilder();

            return $model->select(
                DB::raw('coalesce(max(codigo), 0) + 1 as codigo')
            )->first()->CODIGO;
        }
    }
