<?php

namespace CSPlus\Repositories\Associados;

use CSPlus\Models\CSPlus\Associado\Cat001;
use CSPlus\Services\Repository\Repository;

/**
 * CategoriasRepository
 *
 * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
 * @version 1.2.0
 * @since   20/11/2017
 * @package CSPlus\Repositories\Associados
 */
class CategoriasRepository extends Repository
{
    /**
     * @return  string
     */
    public function model()
    {
        return Cat001::class;
    }

    /**
     * Realiza a busca das categorias para criação de novo associado
     *
     * @param   bool  $inArray
     * @param   null|string  $tipo
     * @return  \Illuminate\Support\Collection
     */
    public function getCategoriesForSubscription($inArray = true, $tipo = null)
    {
        $query = $this->getBuilder();

        $categorias = $query->select('c.CODIGO', 'c.DESCRICAO', 'PVENDA')
            ->from('CAT001 as c')
            ->leftJoin('TAXAS_PERIODICAS as p', 'p.CATEGORIA', '=', 'c.CODIGO')
            ->leftJoin('VIEW_PRODUTO_VALOR as v', 'v.CODIGO', '=', 'p.PRODUTO');

        switch ($tipo) {
            case 'aluno':
                $categorias->whereIn('c.CODIGO', [8, 9]);
                break;
            case 'piloto':
                $categorias->whereIn('c.CODIGO', [10, 11]);
                break;
            default:
                $categorias->whereIn('c.CODIGO', [8, 9, 10, 11]);
                break;
        }

        if($inArray) {
            return $categorias->pluck('DESCRICAO', 'CODIGO');
        }

        return $categorias->get();
    }

    /**
     * Realiza a formatação das categorias disponíveis
     *
     * @param   \Illuminate\Support\Collection  $categorias
     * @return  array
     */
    public function formatCategories($categorias)
    {
        if(!$categorias->isEmpty()) {
            foreach ($categorias as $c) {
                $list[$c->CODIGO] = "Categoria: ".utf8_encode($c->DESCRICAO).' - R$'.money2br($c->PVENDA);
            }

            return $list;
        }
    }

    /**
     * @return array
     */
    public function listCategorias()
    {
        return array_map('utf8_encode', $this->pluck('DESCRICAO', 'CODIGO'));
    }
}
