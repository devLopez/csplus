<?php

    namespace CSPlus\Repositories\Associados;

    use CSPlus\Models\CSPlus\Associado\SenhaWeb;
    use CSPlus\Services\Repository\Repository;

    /**
     * SenhaRepository
     *
     * Realiza o gerenciamento das operações de banco de dados com o model SenhaWeb
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 2.0.0
     * @since   06/02/2017
     */
    class SenhaRepository extends Repository
    {
        /**
         * @return  SenhaWeb
         */
        public function model()
        {
            return SenhaWeb::class;
        }
    }
