<?php

    namespace CSPlus\Repositories\Associados;

    use CSPlus\Services\Repository\Repository;
    use CSPlus\Models\CSPlus\Associado\Ass001;
    use CSPlus\Services\Auth\Exceptions\AuthFailedException;
    use DB;
    use Exception;


    /**
     * AssociadoRepository
     *
     * Repositório que gerencia os dados do associado
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 1.2.0
     * @since   29/04/2017
     */
    class AssociadoRepository extends Repository
    {
        /**
         * @return  string
         */
        public function model()
        {
            return Ass001::class;
        }

        /**
         * Realiza a busca para o clube que realiza o controle via cota alfa
         *
         * @param   array  $termos
         * @return  mixed
         */
        public function getAssociadoByTermos(array $termos)
        {
            $termos = array_map('utf8_decode', $termos);
            $query  = $this->getBuilder();

            $query->select('NUMERODACOTA', 'NOMEDOTITULAR', 'DATANASCIMENTO', 'TELEFONER1')
                  ->where('COTAALFA', $termos['texto']);

            if(isset($termos['ativo'])) {
                $query->where('ATIVO', 'T');
            }

            if(!isset($termos['tipo_cadastro'])) {
                $query->whereNull('TIPOCADASTRO');
            }

            return $query->simplePaginate();
        }

        /**
         * Realiza a busca pela matrícula
         *
         * @param   int  $id
         * @return  mixed
         */
        public function getByMatricula($id)
        {
            $query = $this->getBuilder();

            return $query->select('NUMERODACOTA', 'NOMEDOTITULAR', 'DATANASCIMENTO', 'TELEFONER1')
                         ->where('NUMERODACOTA', $id)
                         ->simplePaginate();
        }

        /**
         * Realiza a busca de associados pelo nome
         *
         * @param   array  $termos
         * @return  mixed
         */
        public function getAssociadosByNome(array $termos)
        {
            $termos = array_map('utf8_decode', $termos);
            $query  = $this->getBuilder();

            if($termos['tipo'] == 'after') {
                $texto = $termos['texto'].'%';
            } else {
                $texto = '%'.$termos['texto'].'%';
            }

            $query->select('NUMERODACOTA', 'NOMEDOTITULAR', 'DATANASCIMENTO', 'TELEFONER1')
                  ->where('NOMEDOTITULAR', 'like', $texto);

            if(isset($termos['ativo'])) {
                $query->where('ATIVO', 'T');
            }

            if(!isset($termos['tipo_cadastro'])) {
                $query->whereNull('TIPOCADASTRO');
            }

            return $query->orderBy('NOMEDOTITULAR')->simplePaginate();
        }

        /**
         * Retorna os gêneros disponíveis
         *
         * @return  array
         */
        public function getGenero()
        {
            return [
                'M' => 'Masculino',
                'F' => 'Feminino'
            ];
        }

        /**
         * Retorna os estados civis disponíveis
         *
         * @return  array
         */
        public function getEstadoCivil()
        {
            return [
                'CA'    => 'Casado (a)',
                'SO'    => 'Solteiro (a)',
                'SE'    => 'Separado (a)',
                'DI'    => 'Divorciado (a)',
                'VI'    => 'Viúvo (a)',
                'UN'    => 'União Estável'
            ];
        }

        /**
         * Realiza o login do associado
         *
         * @param   string  $usuario
         * @param   string  $senha
         * @return  \CSPlus\Models\CSPlus\Associado\Ass001
         * @throws  AuthFailedException
         */
        public function login($usuario, $senha)
        {
            $user = $this->model
                         ->with(['senha'])
                         ->where('CIC', '=', $usuario)
                         ->where('ATIVO', '=', 'T')
                         ->first(['CIC', 'NOMEDOTITULAR', 'NUMERODACOTA', 'SEXO']);

            if(!$user) {
                throw new AuthFailedException('Usuario não encontrado');
            }

            if($user->senha) {
                if($user->senha->SENHA != $senha) {
                    throw new AuthFailedException('Senha incorreta');
                }
            }

            return $user;
        }

        /**
         * Verifica a existência de um associado pelo nome e cpf
         *
         * @param   string  $nome
         * @param   string  $cpf
         * @return  int
         */
        public function verifyIfExists($nome, $cpf)
        {
            return $this->model
                        ->where('NOMEDOTITULAR', utf8_decode($nome))
                        ->where('CIC', $cpf)
                        ->count();
        }
    }
