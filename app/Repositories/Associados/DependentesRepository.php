<?php

    namespace CSPlus\Repositories\Associados;

    use DB;
    use Date;
    use Exception;

    use CSPlus\Models\CSPlus\Associado\Dep001;
    use CSPlus\Services\Repository\Exceptions\ModelNotSavedException;
    use CSPlus\Services\Repository\Repository;

    use Illuminate\Contracts\Container\Container;
	use Illuminate\Contracts\Support\Arrayable;

    class DependentesRepository extends Repository
    {
        protected $associados;

        public function __construct(AssociadoRepository $associados)
        {
            $this->associados = $associados;

            $container  = app()->make(Container::class);
            $collection = app()->make(Arrayable::class);

            parent::__construct($container, $collection);
        }

        public function model()
        {
            return Dep001::class;
        }

        /**
         * Realiza a criação de um novo dependente
         *
         * @param   int  $matricula
         * @param   array  $dependente
         * @return  bool
         * @throws  ModelNotSavedException
         */
        public function salvar($matricula, $dependente)
        {
            $associado = $this->associados->find($matricula);

            $dependente['COTADEPENDENTE']       = $associado->NUMERODACOTA;
            $dependente['CODIGODEPENDENTE']     = $this->generateId($associado->NUMERODACOTA);
            $dependente['DATANASCIMENTO']       = Date::toSql($dependente['DATANASCIMENTO']);
            $dependente['RG_DATA_EXPEDICAO']    = Date::toSql($dependente['RG_DATA_EXPEDICAO']);
            $dependente['ATIVO']                = 'T';

            $dependente = array_map('utf8_decode', array_filter($dependente));

            if($associado->dependentes()->insert($dependente)) {
                return true;
            }

            throw new ModelNotSavedException('Não foi possível realizar o cadastro do dependente');
        }

        /**
         * Realiza a geração de um novo código para o dependente
         *
         * @param   int  $matricula
         * @return  int
         */
        public function generateId($matricula)
        {
            $query = $this->getBuilder();

            $novo_id = $query->select(\DB::raw('coalesce(max(CODIGODEPENDENTE), 0) as COD'))
                             ->from('DEP001')
                             ->where('COTADEPENDENTE', $matricula)
                             ->first();

            return $novo_id->COD + 1;
        }

        /**
         * @param   int  $matricula
         * @param   int  $dependente
         * @return  CSPlus\Models\Associados\Dep001
         * @throws  Exception
         */
        public function getDependente($matricula, $dependente)
        {
            $dependente = $this->model
                               ->where('COTADEPENDENTE', $matricula)
                               ->where('CODIGODEPENDENTE', $dependente)
                               ->first();

            if($dependente) {
                return $dependente;
            }

            throw new Exception('Não encontramos o dependente para realizar a atualização');
        }

        /**
         * Atualiza os dados de um dependente cadastrado
         *
         * @param   int  $matricula
         * @param   int  $dependente
         * @param   array  $dados
         * @return  bool
         */
        public function atualizar($matricula, $dependente, $dados)
        {
            $dependente = $this->getDependente($matricula, $dependente);

            return $dependente->update($dados);
        }
    }
