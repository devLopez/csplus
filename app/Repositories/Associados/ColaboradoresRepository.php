<?php

    namespace CSPlus\Repositories\Associados;

    use Exception;

    use CSPlus\Services\Repository\Repository;
    use CSPlus\Models\CSPlus\Associado\Colabora;

    /**
     * ColaboradoresRepository
     *
     * Repositório que gerencia os dados dos colaboradores
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 1.0.0
     * @since   05/06/2017
     */
    class ColaboradoresRepository extends Repository
    {
        /**
         * @return  Colabora
         */
        public function model()
        {
            return Colabora::class;
        }
    }
