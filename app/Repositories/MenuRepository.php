<?php

    namespace CSPlus\Repositories;

    use CSPlus\Criteria\MenuAssociado\GetLinksForMenu;
    use CSPlus\Models\CSPlus\MenuAssociado;
    use CSPlus\Services\Repository\Repository;

    /**
     * MenuRepository
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 1.1.0
     * @since   01/08/2017
     * @package CSPlus\Repositories
     */
    class MenuRepository extends Repository
    {
        /**
         * @return string
         */
        public function model()
        {
            return MenuAssociado::class;
        }

        /**
         * @return \Illuminate\Support\Collection
         */
        public function getLinksForMenu()
        {
            return $this->pushCriteria(new GetLinksForMenu)
                        ->all(['DESCRICAO', 'HIPERLINK', 'TIPO_CHAMADA']);
        }
    }