<?php

    namespace CSPlus\Repositories\Convites;

    use DB;
    use Exception;
    use PDO;

    use CSPlus\Models\CSPlus\Financeiro\Produtos;
    use CSPlus\Models\CSPlus\Generator;
    use CSPlus\Models\CSPlus\Convites\Convite;
    use CSPlus\Models\CSPlus\Configuracao;
    use CSPlus\Models\CSPlus\Contatos;
    use CSPlus\Repositories\Financeiro\TaxasRepository;
    use CSPlus\Services\Repository\Repository;

    use Illuminate\Contracts\Container\Container;
    use Illuminate\Contracts\Support\Arrayable;

    /**
     * ConvitesRepository
     *
     * Classe responsável por gerenciar toda a regra de negócio da emissão de convites
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 2.0.0
     * @since   08/03/2017
     */
    class ConvitesRepository extends Repository
    {
        /**
         * @var \PDO
         */
        protected $pdo;

        /**
         * Recebe a configuração sobre os convites
         *
         * @var \CSPlus\Models\CSPlus\Configuracao
         */
        private $config;

        /**
         * Recebe is dados do convite
         *
         * @var \Illuminate\Http\Request
         */
        private $convite;

        /**
         * Recebe a matrícula do associado
         *
         * @var int
         */
        private $matricula;

        /**
         * @param   Container  $app
         * @param   Arrayable  $collection
         */
        public function __construct(Container $app, Arrayable $collection)
        {
            parent::__construct($app, $collection);

            $this->pdo = $this->getPdo();
        }

        /**
         * @inheritDoc
         */
        public function model()
        {
            return Convite::class;
        }

        /**
         * Realiza a busca dos convites do associado
         *
         * @param   int  $matricula
         * @param   bool  $admin
         * @return  \Illuminate\Database\Eloquent\Collection
         */
        public function getConvites($matricula, $admin)
        {
            // Define o where personalizado, de acordo com o tipo de usuário
            if($admin != true) {
                $where = '';
            } else {
                $where = "and C.OBSERVACAO = 'GERADO SITE'";
            }

            $sql = "select CN.NOME, CN.CI, C.DATAINI, C.CODIGO,
                case when coalesce(C.CODTAXA, 0) = 0 then 'GRATUITO' else 'Convite PAGO' end as tipo,
                case when (DATAFIM >= current_date and ENTRADA is null) then 0 else 1 end as vencido,
                case when ENTRADA is not null then 'Utilizado' else 'Vencido' end as definicao,
                iif(HORAINI is null, '-', 'SIM') as Impresso,
                iif (HORAFIM is null, 'Seleciona', 'Remove') as seleciona
                from CONVITES as C
                left join CONVIDADOS as CN on CN.CODIGO = C.CONVIDADO
                where C.COTA = :matricula
                {$where}
                order by DATAINI desc, NOME
            ";

            $stmt = $this->pdo->prepare($sql);
            $stmt->bindParam(':matricula', $matricula);
            $stmt->execute();

            return collect($stmt->fetchAll(PDO::FETCH_OBJ));
        }

        /**
         * Realiza a busca dos comvites para impressão
         *
         * @param   int  $matricula
         * @param   array  $convites
         * @return  \Illuminate\Database\Eloquent\Collection
         */
        public function printInvites($matricula, $convites)
        {
            $query = $this->getBuilder();

            return $query->select('cnv.NOME', 'cnv.CI', 'c.DATAINI', 'c.DATAFIM',
                    'c.DATAEMISSAOCONVITE', 'a.NOMEDOTITULAR', 'cart.CODIGO_GERADO',
                    DB::raw("case
                        when TIPO_CONVITE = 20 then 'Convite válido para Finais de Semana e Feriados'
                        when TIPO_CONVITE = 10 then 'Convite válido para qualquer dia, dentro de sua validade'
                        when TIPO_CONVITE = 30 then 'Convite válido para dias de semana'
                        when TIPO_CONVITE = 0 then 'Convite SEM DEFINICAO'
                        when TIPO_CONVITE = 2 then 'Convite para SEGUNDA-FEIRA'
                        when TIPO_CONVITE = 3 then 'Convite para TERÇA-FEIRA'
                        when TIPO_CONVITE = 4 then 'Convite para QUARTA-FEIRA'
                        when TIPO_CONVITE = 5 then 'Convite para QUINTA-FEIRA'
                        when TIPO_CONVITE = 6 then 'Convite para SEXTA-FEIRA'
                        end as descricao_tipo_convite")
                    )
                ->from('CONVITES as c')
                ->join('CONVIDADOS as cnv', 'cnv.CODIGO', '=', 'c.CONVIDADO')
                ->join('ASS001 as a', 'a.NUMERODACOTA', '=', 'c.COTA')
                ->leftJoin('CARTEIRAS as cart',
                    DB::raw('cast("c"."CARTEIRA" as varchar(10))'),
                    '=',
                    DB::raw('substring("cart"."CODIGO" from 1 for (character_length("cart"."CODIGO") - 1))')
                )
                ->where('c.COTA', $matricula)
                ->whereIn('c.CODIGO', $convites)
                ->get();
        }

        /**
         * Marca os convites selecionados como impresso
         *
         * @param   int  $matricula
         * @param   array  $convites
         */
        public function disableInvites($matricula, $convites)
        {
            $query = $this->getBuilder();

            $query->whereIn('CODIGO', $convites)
                ->where('COTA', $matricula)
                ->update([
                    'HORAINI'   => DB::raw('current_time'),
                    'HORAFIM'   => ''
                ]);
        }

        /**
         * Realiza a busca do termo de aceite do associado
         *
         * @param   int  $matricula
         * @return  mixed
         */
        public function getAceite($matricula)
        {
            $query = $this->getBuilder();

            return $query->select(
                DB::raw("extract(day from datahora) || '/' || extract (month from datahora) || '/' || extract(year from datahora) as DATAHORA")
            )->from('CONTATOS')
            ->where('COTA', $matricula)
            ->where('SITUACAO', 'ACEITE CONTRATO WEB')
            ->where('DATA', '>=', DB::raw("(select first 1 dataaquisicao from ass001 where numerodacota = $matricula order by datahora desc)"))
            ->first();
        }

        /**
         * Realiza a geração do convite
         *
         * @param   \Illuminate\Http\Request  $convite
         * @param   int  $matricula
         * @return  bool
         */
        public function generateInvite($convite, $matricula)
        {
            $this->convite      = $convite;
            $this->matricula    = $matricula;

            if($this->convite->pago == 'T') {
                $this->getConfig();
                return $this->convitePago();
            } else {
                return $this->salvarConvite();
            }
        }

        /**
         * Realiza a criação do convite pago
         *
         * @return  bool
         * @throws  Exception
         */
        private function convitePago()
        {
            if ($this->convite->tipoconvite == $this->config->F1_D1)
                $produto = $this->config->F1_1;
            else if ($this->convite->tipoconvite == $this->config->F1_D2)
                $produto = $this->config->F1_2;
            else if ($this->convite->tipoconvite == $this->config->F1_D3)
                $produto = $this->config->F1_3;
            else if ($this->convite->tipoconvite == $this->config->F2_D1)
                $produto = $this->config->F2_1;
            else if ($this->convite->tipoconvite == $this->config->F2_D2)
                $produto = $this->config->F2_2;
            else if ($this->convite->tipoconvite == $this->config->F2_D3)
                $produto = $this->config->F2_3;

            if(!isset($produto)) {
                throw new Exception('Não foi possível determinar o produto do convite. Entre em contato com o clube');
            }

            // Recebe o valor do convite baseado do produto buscado
            $valorConvite = Produtos::select('PVENDA')->from('VIEW_PRODUTO_VALOR')->where('CODIGO', $produto)->first();

            if($valorConvite == null) {
                throw new Exception('Não foi possível resgatar o valor do convite. Tente novamente, ou entre em contato com o clube');
            }

            // Cria uma nova taxa para o associado baseado no convite
            $taxa = TaxasRepository::setTaxaConvite($this->matricula, $this->convite->convidado, $produto, $valorConvite->PVENDA);

            return $this->salvarConvite($taxa, $this->convite->tipoconvite, $produto);
        }

        /**
         * Salva um convite gratuito na base de dados
         *
         * @return  bool
         */
        private function salvarConvite($taxa = 0, $tipoConvite = 10, $produto = 0)
        {

            $codigo     = Generator::newId('GEN_CONVITES_ID');
            $convidado  = utf8_decode($this->convite->convidado);
            $matricula  = $this->matricula;
            $referencia = $this->convite->referencia;

            $sql = "insert into CONVITES (CODIGO, CONVIDADO, COTA, DATAINI, DATAFIM, CODTAXA, OBSERVACAO, DATAEMISSAOCONVITE,
            TIPO_CONVITE, PRODUTO, HORAFIM) VALUES (:codigo, :convidado, :cota, :referencia, addmonth(:data_fim, 1) -1,
            :taxa, 'GERADO SITE', CURRENT_DATE, :tipo_convite, :produto, CURRENT_TIME)";

            $stmt = $this->pdo->prepare($sql);
            $stmt->bindParam(':codigo', $codigo);
            $stmt->bindParam(':convidado', $convidado);
            $stmt->bindParam(':cota', $matricula);
            $stmt->bindParam(':referencia', $referencia);
            $stmt->bindParam(':data_fim', $referencia);
            $stmt->bindParam(':taxa', $taxa);
            $stmt->bindParam(':tipo_convite', $tipoConvite);
            $stmt->bindParam(':produto', $produto);

            return $stmt->execute();
        }

        /**
         * Realiza a busca das configurações referentes ao convite
         */
        private function getConfig()
        {
            $this->config = Configuracao::select(
                DB::raw("coalesce(IDADE_FAIXA1CNV, 0) as faixa1"), DB::raw('coalesce(PRODUTOCNV_FAIXA1_1, 0) as f1_1'),
                DB::raw("coalesce(DIADASEMANACNV1_1, 0) f1_d1"), DB::raw('coalesce(PRODUTOCNV_FAIXA1_2, 0) as f1_2'),
                DB::raw('coalesce(DIADASEMANACNV1_2, 0) f1_d2'), DB::raw('coalesce(PRODUTOCNV_FAIXA1_3, 0) as f1_3'),
                DB::raw('coalesce(DIADASEMANACNV1_3, 0) f1_d3'), DB::raw('coalesce(IDADE_FAIXA2CNV, 0) as faixa2'),
                DB::raw('coalesce(PRODUTOCNV_FAIXA2_1, 0) as f2_1'), DB::raw('coalesce(DIADASEMANACNV2_1, 0) f2_d1'),
                DB::raw('coalesce(PRODUTOCNV_FAIXA2_2, 0) as f2_2'), DB::raw('coalesce(DIADASEMANACNV2_2, 0) f2_d2'),
                DB::raw('coalesce(PRODUTOCNV_FAIXA2_3, 0) as f2_3'), DB::raw('coalesce(DIADASEMANACNV2_3, 0) f2_d3'),
                DB::raw('coalesce(IDADE_CONTROLE_CONVITE, 0) as Idade_Controle')
            )->first();
        }
    }
