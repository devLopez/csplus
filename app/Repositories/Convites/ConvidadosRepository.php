<?php

    namespace CSPlus\Repositories\Convites;

    use Date;
    use CSPlus\Models\CSPlus\Convites\Convidado;
    use CSPlus\Services\Repository\Repository;
    use CSPlus\Services\Repository\Exceptions\ModelNotSavedException;

    /**
     * ConvidadosRepository
     *
     * Classe desenvolvida para gerenciamentoi do model Convidado
     *
     * @author  Matheus Lopes Santos
     * @version 1.0.0
     * @since   08/03/2017
     */
    class ConvidadosRepository extends Repository
    {
        /**
         * @inheritDoc
         */
        public function model()
        {
            return Convidado::class;
        }

        /**
         * Realiza a criação do novo convidado
         *
         * @param   array  $dados
         * @return  Convidado
         * @throws  ModelNotSavedException
         */
        public function createConvidado($dados)
        {
            $dados['DATANASC'] = Date::toSql($dados['DATANASC']);

            $convidado = $this->model->firstOrNew([
                'NOME'      => $dados['NOME'],
                'CI'        => $dados['CI'],
                'DATANASC'  => $dados['DATANASC']
            ]);

            if($convidado->exists == false) {
                $convidado->CODIGO      = $this->getId();
                $convidado->NOME        = $dados['NOME'];
                $convidado->CI          = $dados['CI'];
                $convidado->DATANASC    = $dados['DATANASC'];
                $convidado->OBSERVACAO  = $dados['OBSERVACAO'];

                if(!$convidado->save()) {
                    throw new ModelNotSavedException('Não foi possível cadastrar o convidado. Tente Novamente');
                }
            }

            return $convidado;
        }
    }
