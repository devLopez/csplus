<?php

    namespace CSPlus\Repositories;

    use DB;
    use CSPlus\Models\CSPlus\Contatos;
    use CSPlus\Services\Repository\Repository;

    /**
     * Gerencia o model Contatos
     *
     * @author  Matheus Lopes Santos
     * @version 1.0.0
     * @since   08/03/2017
     */
    class ContatosRepository extends Repository
    {
        /**
         * @inheritDoc
         */
        public function model()
        {
            return Contatos::class;
        }

        /**
         * Realiza a busca do aceite principal do sistema
         *
         * @param   int  $matricula
         * @return  Contatos|null
         */
        public function getAceiteWeb($matricula)
        {
            $query = $this->getBuilder();

            return $query->select(
                DB::raw("extract(day from datahora) || '/' || extract (month from datahora) || '/' || extract(year from datahora) as DATAHORA")
            )->from('CONTATOS')
                ->where('COTA', $matricula)
                ->where('SITUACAO', 'ACEITE CONTRATO PRINCIPAL')
                ->where('DATA', '>=', DB::raw("(select first 1 datacadastro from ass001 where numerodacota = $matricula order by datahora desc)"))
                ->first();
        }
    }
