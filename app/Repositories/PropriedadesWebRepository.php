<?php

    namespace CSPlus\Repositories;

    use CSPlus\Models\CSPlus\Associado\PropriedadesWeb;
    use CSPlus\Services\Repository\Repository;

    /**
     * PropriedadesWebRepository
     *
     * Classe responsável pelo gerenciamento do model Personalizacao
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 1.0.0
     * @since   22/04/2017
     */
    class PropriedadesWebRepository extends Repository
    {
        /**
         * @inheritDoc
         */
        public function model()
        {
            return PropriedadesWeb::class;
        }
    }
