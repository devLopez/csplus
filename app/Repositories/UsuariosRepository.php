<?php

    namespace CSPlus\Repositories;


    use CSPlus\Models\CSPlus\CsSenhas as Usuario;
    use CSPlus\Services\Auth\Exceptions\AuthFailedException;
    use CSPlus\Services\Repository\Repository;

    /**
     * UsuariosRepository
     *
     * Classe responsável pelo gerenciamento do model CSSenhas
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 1.0.0
     * @since   21/04/2017
     */
    class UsuariosRepository extends Repository
    {
        /**
         * @inheritDoc
         */
        public function model()
        {
            return Usuario::class;
        }

        /**
         * Realiza o login do usuário
         *
         * @param   string  $usuario
         * @param   string  $senha
         * @return  \CSPlus\Models\CSPlus\CsSenhas
         * @throws  AuthFailedException
         */
        public function login($usuario, $senha)
        {
            $usuario = $this->model
                            ->where('USUARIO', '=', $usuario)
                            ->where('SENHA', '=', $senha)
                            ->first(['CODUSUARIO', 'USUARIO']);

            if(!$usuario) {
                throw new AuthFailedException('Usuário ou senha incorretos');
            }

            return $usuario;
        }
    }
