<?php namespace CSPlus\Repositories;

    use DB;

    use CSPlus\Models\CSPlus\Telefonia\ClaroTitular;
    use CSPlus\Models\CSPlus\Telefonia\ClaroConta;
    use Illuminate\Support\Collection;

    /**
     * TelefoniaRepository
     *
     * Classe responsável pelo gerenciamento das query's telefonicas
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 1.4.0
     * @since   15/06/2017
     */
    class TelefoniaRepository
    {
        public function __construct()
        {
            new ClaroConta();
        }

        /**
         * Realiza a busca das operadoras cadastradas
         *
         * @param   int  $matricula
         * @return  \Illuminate\Database\Eloquent\Collection
         */
        public function getOperadoras($matricula)
        {
            return ClaroTitular::select('o.CODIGO', 'o.DESCRICAO')
                ->from('CLARO_TITULAR as t')
                ->leftJoin('CLARO_PLANO as p', 'p.CODIGO', '=', 't.COD_PLANO')
                ->leftJoin('CLARO_OPERADORA as o', 'o.CODIGO', '=', 'p.OPERADORA')
                ->where('t.MATRICULA', $matricula)
                ->distinct()
                ->get();
        }

        /**
         * Retorna o último vencimento disponível para o associado
         *
         * @param   int  $matricula
         * @return  string
         */
        public function getDataVencimento($matricula)
        {
            return ClaroConta::
                leftJoin('CLARO_TITULAR as t', 'CLARO_CONTA.ID_CLARO_TITULAR', '=', 't.ID_CLARO_TITULAR')
                ->where('t.MATRICULA', $matricula)
                ->max('CLARO_CONTA.VENCIMENTO');
        }

        /**
         * Realiza a busca das contas referentes à operadora selecionada
         *
         * @param   int  $matricula
         * @param   int  $operadora
         * @param   string  $dataInicial
         * @param   string  $dataFinal
         * @return  \Illuminate\Support\Collection
         */
        public function getContas($matricula, $operadora, $dataInicial, $dataFinal)
        {
            $tipo = ($operadora == '0') ? '>' : '=';

            $conta  = new ClaroConta();
            $contas = DB::connection('csplus')->select('select "c"."VENCIMENTO", "t"."TELEFONE", "c"."VALOR", "c"."DATA_PAG",
                (select first 1 1 from "CLARO_DETALHE" as "d" where "d"."CONTA_SEQUENCIAL" = "c"."SEQUENCIAL") as existe_detalhe, 
                "o"."DESCRICAO" as "OPERADORA"
                from "CLARO_CONTA" as "c" 
                inner join "CLARO_TITULAR" as "t" on "t"."ID_CLARO_TITULAR" = "c"."ID_CLARO_TITULAR"
                left join "CLARO_PLANO" as "p" on "p"."CODIGO" = "t"."COD_PLANO"
                left join "CLARO_OPERADORA" as "o" on "o"."CODIGO" = "p"."OPERADORA"
                where "t"."MATRICULA" = :matricula
                and "PUBLICADO" = \'T\' 
                and "o"."CODIGO" '.$tipo.' :operadora 
                and "c"."VENCIMENTO" between :dataFinal and :dataInicial
                order by "c"."VENCIMENTO" desc', [
                'matricula'     => $matricula,
                'operadora'     => $operadora,
                'dataInicial'   => $dataInicial,
                'dataFinal'     => $dataFinal
            ]);

            return new Collection($contas);
        }

        /**
         * Realiza a busca do relatório detalhado da conta telefônica
         *
         * @param   int  $matricula
         * @param   string  $vencimento
         * @param   string  $telefone
         * @return  \Illuminate\Support\Collection
         */
        public function relatorioDetalhado($matricula, $vencimento, $telefone)
        {
            $detalhes = DB::connection('csplus')->select('SELECT D.DATA_HORA_INI, D.DURACAO, D.HISTORICO, D.DISCADO, 
              D.VALOR, D.TIPO_LIGACAO , D.TARIFA FROM CLARO_DETALHE D 
              JOIN CLARO_CONTA C ON C.SEQUENCIAL = D.CONTA_SEQUENCIAL 
              JOIN CLARO_TITULAR T ON T.ID_CLARO_TITULAR = C.ID_CLARO_TITULAR 
              where T.MATRICULA = :matricula 
              AND C.VENCIMENTO = :vencimento 
              and T.TELEFONE = :telefone order by D.DATA_HORA_INI', [
                'matricula'     => $matricula,
                'vencimento'    => $vencimento,
                'telefone'      => $telefone
            ]);

            return new Collection($detalhes);
        }

        /**
         * Realiza a busca do relatório agrupado da conta telefônica
         *
         * @param   int  $matricula
         * @param   string  $vencimento
         * @param   string  $telefone
         * @return  \Illuminate\Support\Collection
         */
        public function relatorioAgrupado($matricula, $vencimento, $telefone)
        {
            $detalhes = DB::connection('csplus')->select('SELECT d.TELEFONE, min(d.DATA_HORA_INI) as DATA_HORA_INI,
              sum(d.VALOR) as VALOR, t.MATRICULA, a.NOMEDOTITULAR, d.TIPO_LIGACAO
              FROM CLARO_DETALHE d 
              JOIN CLARO_CONTA c ON c.SEQUENCIAL = d.CONTA_SEQUENCIAL
              LEFT JOIN CLARO_TITULAR t ON t.ID_CLARO_TITULAR = c.ID_CLARO_TITULAR
              LEFT JOIN ASS001 a ON a.NUMERODACOTA = t.MATRICULA
              where t.MATRICULA = :matricula
              AND c.VENCIMENTO = :vencimento
              AND t.TELEFONE = :telefone 
              GROUP BY D.TELEFONE, T.MATRICULA, A.NOMEDOTITULAR, D.TIPO_LIGACAO', [
                'matricula'     => $matricula,
                'vencimento'    => $vencimento,
                'telefone'      => $telefone
            ]);

            return new Collection($detalhes);
        }
    }