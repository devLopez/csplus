<?php

    namespace CSPlus\Repositories;

    use CSPlus\Models\CSPlus\Personalizacao;
    use CSPlus\Services\Repository\Repository;

    /**
     * PersonalizacaoRepository
     *
     * Classe responsável pelo gerenciamento do model Personalizacao
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 1.0.0
     * @since   21/04/2017
     */
    class PersonalizacaoRepository extends Repository
    {
        /**
         * @inheritDoc
         */
        public function model()
        {
            return Personalizacao::class;
        }
    }
