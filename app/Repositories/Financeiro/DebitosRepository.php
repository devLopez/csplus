<?php namespace CSPlus\Repositories\Financeiro;

    use DB;
    use Date;
    use Exception;
    use Log;
    use CSPlus\Models\CSPlus\Financeiro\Debitos;
    use CSPlus\Models\CSPlus\Financeiro\Taxas;

    /**
     * DebitosRepository
     *
     * Classe desenvolvida para gerenciamento dos débitos do associado
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 1.3.0
     * @since   18/07/2017
     */
    class DebitosRepository
    {
        /**
         * @var Debitos
         */
        protected $debitos;

        /**
         * @var int
         */
        protected $cobranca;

        /**
         * @var int
         */
        protected $matricula;

        /**
         * @var string
         */
        protected $vencimento;

        /**
         * Recebe a data máxima para pesquisa de itens
         *
         * @var string
         */
        protected $dataPesquisa;

        /**
         * @param   Debitos  $debitos
         */
        public function __construct(Debitos $debitos)
        {
            $this->debitos = $debitos;
        }

        /**
         * Realiza a busca dos débitos do associado
         *
         * @param   \Illuminate\Http\Request  $parametros
         * @return  \Illuminate\Support\Collection
         * @throws  Exception
         */
        public function getDebitos($parametros)
        {
            $this->matricula    = $parametros->matricula;
            $this->vencimento   = Date::toSql($parametros->data_vencimento);
            $this->dataPesquisa = Date::toSql($parametros->data_pesquisa);

            $this->geraCobranca();

            if($this->cobranca == null) {
                throw new Exception('Não foi possível gerar o número da cobrança. Tente novamente');
            }

            if($parametros->pesquisar_por == 'quitados') {
                return collect($this->selectCobrancasQuitadas());
            }

            return collect($this->getCobrancasAbertas());
        }

        /**
         * Realiza a busca do código de cobrança cadastrado para o associado
         *
         * @throws  Exception
         */
        private function geraCobranca()
        {
            try {
                $this->cobranca = $this->debitos->select("CODIGOCOBRANCA")
                                       ->from(DB::raw("proc_GeraCobranca_geralv3('07/2016', 0, null,
                                                '{$this->vencimento}', {$this->matricula}, 0, '',
                                                '{$this->dataPesquisa}', null,null,null,0,null,'F','F',
                                                'F','F','','T','T','T', null)"
                                            )
                                       )->first()->CODIGOCOBRANCA;
            } catch(Exception $e) {
                Log::emergency($e->getMessage());
                throw new Exception('Ocorreu um erro na busca da cobrança. Tente novamente');
            }
        }

        /**
         * Realiza a busca das cobranças em aberto
         *
         * @return  array
         */
        private function getCobrancasAbertas()
        {
            $cobranca = $this->cobranca;

            return DB::connection('csplus')
                ->select("select r.Pagamento DataPagamento, t.Data, t.situacao,
                    case when (p.Produto = t.historico)
                        or (coalesce(t.historico, '') = '')
                        or (t.taxa_periodica is not null )
                        or (t.historico containing('gerado manualmente pelo'))
                        then p.produto else p.Produto || ' - ' || Coalesce(t.historico, '')
                    end as produto, t.valor, t.Codigo,
                    coalesce(d.NOMEDEPENDENTE,
                        case when t.taxa_periodica is not null then 'Periódica ' ||
                        case when tp.categoria is not null then '- categoria'
                        when tp.codigo is null then 'EXCLUIDA' else tp.codigo end else t.historico end
                    ) as historico, t.Recibo, t.Boleta, t.quantidade, t.Produto CodProduto, t.referencia,
                    p.codigo as codproduto, t.nfs, ci.Juros, ci.Multa, (t.Valor + ci.juros + ci.Multa) ValorTotal,
                    ci.codigo as codci from taxas t
                    Left Join Recibo r on (r.Recibo = t.Recibo)
                    Left Join Produtos P on (t.produto = p.codigo)
                    Join Cobranca_Itens ci on (ci.CodigoCobranca = :cobranca) and (ci.CodigoItem = t.Codigo)
                    left join dep001 d on d.COTADEPENDENTE = t.COTA and d.CODIGODEPENDENTE = t.DEP
                    left join TAXAS_PERIODICAS tp on tp.CODIGO = t.TAXA_PERIODICA
                    where t.Cota = :matricula and Coalesce(t.valor, 0) <> 0 and coalesce(t.Situacao, 'V') = 'V'
                    order by t.data desc",
                    [
                        'cobranca'  => $this->cobranca,
                        'matricula' => $this->matricula
                    ]
                );
        }

        /**
         * Realiza a busca das cobranças quitadas
         *
         * @return  array
         */
        private function selectCobrancasQuitadas()
        {
            return DB::connection('csplus')
                ->select("select r.Pagamento DataPagamento, t.Data, t.situacao,
                    case when (p.Produto = t.historico)
                        or (coalesce(t.historico, '') = '') or (t.taxa_periodica is not null )
                        or (t.historico containing('gerado manualmente pelo'))
                        then p.produto else p.Produto || ' - ' || Coalesce(t.historico, '')
                    end as produto, t.valor, t.Codigo,
                    coalesce(d.NOMEDEPENDENTE,
                        case when t.taxa_periodica is not null then 'Periódica ' ||
                        case when tp.categoria is not null then '- categoria'
                        when tp.codigo is null then 'EXCLUIDA' else tp.codigo end else t.historico end
                    ) as historico,
                    t.Recibo, t.Boleta, t.quantidade, t.Produto CodProduto, t.referencia,
                    p.codigo as codproduto, t.nfs, 0 as Juros, 0 as Multa, t.Valor ValorTotal,
                    0 as codci from taxas t
                    Left Join Recibo r on (r.Recibo = t.Recibo)
                    Left Join Produtos P on (t.produto = p.codigo)
                    left join dep001 d on d.COTADEPENDENTE = t.COTA and d.CODIGODEPENDENTE = t.DEP
                    left join TAXAS_PERIODICAS tp on tp.CODIGO = t.TAXA_PERIODICA
                        where t.Cota = :matricula and Coalesce(t.valor, 0) <> 0 and coalesce(t.situacao, 'V') = 'P'
                        order by t.data desc",
                    [
                        'matricula' => $this->matricula
                    ]);
        }

        /**
         * Realiza a busca dos dados dos itens para pagamento
         *
         * @param   int  $cobranca
         * @param   int  $matricula
         * @param   array  $items
         * @return  \Illuminate\Support\Collection
         */
        public function getItensForPayment($cobranca, $matricula, $items)
        {
            // Realiza a contagem dos itens selecionados para bind
            $bind = trim(str_repeat('?,', count($items)), ',');

            $bind_values[0] = (int) $cobranca;
            $bind_values[1] = (int) $matricula;

            foreach ($items as $i) {
                array_push($bind_values, (int) $i);
            }

            $items = DB::connection('csplus')
                ->select("select distinct r.Pagamento DataPagamento, t.Data, t.situacao,
                    case when (p.Produto = t.historico)
                        or (coalesce(t.historico, '') = '')
                        or (t.taxa_periodica is not null )
                        or (t.historico containing('gerado manualmente pelo'))
                        then p.produto else p.Produto || ' - ' || Coalesce(t.historico, '')
                    end as produto, t.valor, t.Codigo,
                    coalesce(d.NOMEDEPENDENTE,
                        case when t.taxa_periodica is not null then 'Periódica ' ||
                        case when tp.categoria is not null then '- categoria'
                        when tp.codigo is null then 'EXCLUIDA' else tp.codigo end else t.historico end
                    ) as historico, t.Recibo, t.Boleta, t.quantidade, t.Produto CodProduto, t.referencia,
                    p.codigo as codproduto, t.nfs, ci.Juros, ci.Multa, (t.Valor + ci.juros + ci.Multa) ValorTotal,
                    ci.codigo as codci, c.DESCONTO_ATE_VENCIMENTO as Desconto
                    from taxas t
                    Left Join Recibo r on (r.Recibo = t.Recibo)
                    Left Join Produtos P on (t.produto = p.codigo)
                    Join Cobranca_Itens ci on (ci.CodigoItem = t.Codigo)
                    join COBRANCA as c on c.CODIGO = ci.codigocobranca
                    left join dep001 d on d.COTADEPENDENTE = t.COTA and d.CODIGODEPENDENTE = t.DEP
                    left join TAXAS_PERIODICAS tp on tp.CODIGO = t.TAXA_PERIODICA
                    where ci.codigocobranca = ? and t.Cota = ? and Coalesce(t.valor, 0) <> 0 and coalesce(t.Situacao, 'V') = 'V'
                    and t.CODIGO in({$bind})
                    order by t.data desc",
                    $bind_values
                );


            return collect($items);
        }

        /**
         * Retorna o valor da cobrança
         *
         * @return int
         */
        public function getCobranca()
        {
            return $this->cobranca;
        }
    }
