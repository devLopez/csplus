<?php

    namespace CSPlus\Repositories\Financeiro;

    use DB;
    use stdClass;

    use CSPlus\Models\CSPlus\Financeiro\Cobranca;
    use CSPlus\Services\Repository\Repository;

    /**
     * CobrancaRepository
     *
     * Classe desenvolvida para gerenciamento do repositório de cobrancas
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 1.1.1
     * @since   25/04/2017
     */
    class CobrancaRepository extends Repository
    {
        /**
         * @return  Cobranca
         */
        function model()
        {
            return Cobranca::class;
        }

        /**
         * Realiza a busca de itens para a baixa da cobrança
         *
         * @param   int  $id
         * @return  Cobranca
         */
        public function getCobrancaForBaixa($id)
        {
            return $this->model->select('c.BOLETO', 'c.COTA', 'c.RECIBO','a.NOMEDOTITULAR', 'c.VALOR')
                        ->from('COBRANCA AS c')
                        ->leftJoin('ASS001 as a', 'c.COTA', '=', 'a.NUMERODACOTA')
                        ->leftJoin('RECIBO as r', 'r.RECIBO', '=', 'c.RECIBO')
                        ->where('c.CODIGO', $id)
                        ->first();
        }

        /**
         * Realiza a atualização das taxas e cobrança caso a transação no
         * PagSeguro não tenha sido um sucesso
         *
         * @param   stdClass $transacao
         * @return  void
         */
        public function updateCobrancaAndTaxas(stdClass $transacao)
        {
            // Realiza a atualização da cobrança
            DB::connection('csplus')->update("update cobranca set tipo_cobranca = 700 + ? where CODIGO = ?", [
                $transacao->status,
                $transacao->idCobranca
            ]);

            // Realiza a Atualização das Taxas
            DB::connection('csplus')->update("update taxas set recibo = null, situacao = null where codigo in (select codigoitem from cobranca_itens where codigocobranca = ? and tipoitem = 'T') and coalesce(recibo, 0) < 1", [
                $transacao->idCobranca
            ]);
        }

        /**
         * Retorna o valor do desconto cadastrado para a cobrança passada
         *
         * @param   string  $id
         * @return  float|mixed
         */
        public function getDiscountValue($id) {
            $cobranca = $this->model->find($id, ['DESCONTO_ATE_VENCIMENTO']);

            return ($cobranca) ? $cobranca->DESCONTO_ATE_VENCIMENTO : 0.00;
        }
    }
