<?php

	namespace CSPlus\Repositories\Financeiro;

	use Exception;
	use DB;

	use CSPlus\Models\CSPlus\Financeiro\Taxas;
	use CSPlus\Models\CSPlus\Generator;
	use CSPlus\Services\Database\AlternativeConnection;
	use CSPlus\Services\Repository\Repository;

	use Illuminate\Contracts\Container\Container;
	use Illuminate\Contracts\Support\Arrayable;
	/**
	 * BoletosRepository
	 *
	 * Classe desenvolvida para gerenciamento dos boletos a serem emitidos
	 *
	 * @author 	Matheus Lopes Santos <fale_com_lopez@hotmail.com>
	 * @version 1.1.0
	 * @since 	17/01/2017
	 */
	class BoletosRepository extends Repository
	{
		/**
		 * @var int
		 */
		protected $cobranca;

		/**
		 * @var int
		 */
		protected $matricula;

		/**
		 * @var int
		 */
		protected $cobrancaSelecao;

		/**
		 * @var array
		 */
		protected $taxas;

        /**
         * @var DebitosRepository
         */
        protected $debitos;

        /**
         * @var int
         */
        protected $localCobranca = 0;

        /**
         * Construtor da classe
         *
         * @param   App  $app
         * @param   DebitosRepository  $debitos
         */
        public function __construct(Container $container, Arrayable $arrayable, DebitosRepository $debitos)
        {
            parent::__construct($container, $arrayable);

            $this->debitos = $debitos;
        }

        /**
		 * @return Taxas
		 */
		function model()
		{
			return Taxas::class;
		}

		/**
		 * Seta as Taxas dos produtos selecionados
		 *
		 * @param 	array  $taxas
         * @return  $this
		 */
		public function setTaxas($taxas)
		{
			$this->taxas = $taxas;

            return $this;
		}

        /**
         * Seta a matrícula do usuário
         *
         * @param   int  $matricula
         * @return  $this
         */
        public function setMatricula($matricula)
        {
            $this->matricula = $matricula;

            return $this;
        }

        /**
         * Seta o local da cobrança
         *
         * @param   int  $localCobranca
         * @return  $this
         */
        public function setLocalCobranca($localCobranca)
        {
            $this->localCobranca = $localCobranca;

            return $this;
        }

        /**
         * Executa todos os procedimentos de geração de boletos
         *
         * @return  $this
         * @throws 	Exception
         */
        public function runProcedures()
        {
            $this->setCobrancaSelecao()
                 ->insertCobrancaSelecao()
                 ->geraCobrancaDefinitiva();

            return $this;
        }

        /**
         * Retorna o ID da cobrança gerada
         *
         * @return  int
         */
        public function getCobranca()
        {
            return $this->cobranca;
        }

        /**
         * Seta o valor do novo ID de cobrança seleção
         *
         * @return  $this
         */
        private function setCobrancaSelecao()
        {
            $this->cobrancaSelecao = Generator::newId('GEN_COBRANCA_SELECAO_ID');

            return $this;
        }

        /**
         * Realiza a busca da maior data entre os produtos selecionados
         *
         * @return  mixed
         * @throws  Exception
         */
        private function getMajorDate()
        {
            $data = Taxas::select(DB::raw('max(DATA) as DATA'))
                        ->whereIn('CODIGO', $this->taxas)
                        ->first();

            if(!$data || !$data->DATA) {
                throw new Exception('Para prosseguir com a geração é necessário que oo produto tenha uma data de lançamento válida');
            } else {
                return $data->DATA;
            }
        }

		/**
		 * Insere os itens da cobrança no banco de dados
		 *
		 * @return 	$this
		 * @throws 	Exception
		 */
		private function insertCobrancaSelecao()
		{
			if(!$this->taxas) {
				throw new Exception('Para realizar o pagamento é necessário selecionar, pelo menos, uma taxa');
			}

			foreach ($this->taxas as $taxa) {
				DB::connection('csplus')
					->insert('insert into cobranca_selecao(id, codigo_taxa) values(?, ?)', [
						$this->cobrancaSelecao,
						$taxa
					]);
			}

			return $this;
		}

		/**
		 * Realiza a busca do código da cobrança
		 *
		 * @return 	$this
		 * @throws 	Exception
		 */
		private function geraCobrancaDefinitiva()
		{
            // Recebe as datas utilizadas na geração do boleto
            $data_maxima    = $this->getMajorDate();
            $mes_referencia = date('m/Y', strtotime($data_maxima));

            $cobranca = \DB::connection('csplus')->select("select codigocobranca from PROC_GERACOBRANCA_V5 (?, '0', ?, current_date + 3, '', ?, null, 0, null, 'F', 'F','A', 'F', '', 'T', 'T', 'T', '', ?)", [
                $mes_referencia, $this->localCobranca, $data_maxima, $this->cobrancaSelecao
            ]);

            foreach ($cobranca as $c) {
                $cobranca = $c->CODIGOCOBRANCA;
            }

            if($cobranca) {
                $this->cobranca = $cobranca;
            } else {
                throw new Exception('Impossível resgatar o código da cobrança. Para impressão do seu boleto, acesse o seu painel ou entre em contato com o seu clube');
            }

            return $this;
		}

        /**
         * Roda a procedure referente a geração do boleto
         *
         * @return	void
         */
        public function runProcedureBoleto()
        {
            DB::connection('csplus')->statement('EXECUTE PROCEDURE PROC_GERACOBRANCA_BOLETOS(?)', [$this->cobranca]);
        }

		/**
		 * Atualiza os dados das taxas para pagamentos com PagSeguro
		 *
		 * @return	void
		 */
		// private function updateTaxas()
		// {
		// 	DB::connection('csplus')->update("update taxas set recibo = null, situacao = 'C' where codigo in (select codigoitem from cobranca_itens where codigocobranca = ? and tipoitem = 'T')", [
		// 		$this->cobranca
		// 	]);
		// }
	}
