<?php

    namespace CSPlus\Repositories\Financeiro;

    use CSPlus\Models\CSPlus\Financeiro\Bancos;
    use CSPlus\Services\Repository\Repository;

    /**
     * BancosRepository
     *
     * Realiza o gerenciamento do model Bancos
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 1.0.0
     * @since   06/02/2017
     * @package CSPlus\Repositories\Financeiro
     */
    class BancosRepository extends Repository
    {
        /**
         * @return  Bancos
         */
        public function model()
        {
            return Bancos::class;
        }

        /**
         * @return array
         */
        public function listBancos()
        {
            return array_map('utf8_encode', $this->pluck('NOME', 'CODIGO'));
        }
    }