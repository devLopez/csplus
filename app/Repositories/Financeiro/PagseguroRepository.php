<?php

    namespace CSPlus\Repositories\Financeiro;

    use App;
    use Client;
    use DB;
    use Exception;
    use PagSeguro;
    use Session;
    use stdClass;

    use CSPlus\Models\CSPlus\Associado\Ass001;
    use CSPlus\Models\CSMobile\Cliente;
    use CSPlus\Models\CSPlus\Contatos;
    use CSPlus\Models\CSPlus\Financeiro\Cobranca;
    use CSPlus\Models\CSPlus\Generator;
    use CSPlus\Services\PagSeguro\PagSeguroConfig;
    use CSPlus\Services\PagSeguro\Log as PSLog;
    use JansenFelipe\Utils\Utils;

    /**
     * PagseguroRepository
     *
     * Classe responsavel por gerenciar os dados para a requisiçao do pagseguro
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 2.5.1
     * @since   31/01/2017
     */
    class PagseguroRepository
    {
        /**
         * @var DebitosRepository
         */
        protected $debitos;

        /**
         * @var Ass001
         */
        protected $associado;

        /**
         * @var stdClass
         */
        protected $paymentData;

        /**
         * @var int
         */
        protected $matricula;

        /**
         * @var Ass001
         */
        protected $sender;

        /**
         * @var int
         */
        protected $cobranca;

        /**
         * @var CobrancaRepository
         */
        protected $billings;

        /**
         * Define os tipos de retorno que a API do PagSeguro devolve
         *
         * @var array
         */
        protected $paymentStatus = [
            1 => 'Aguardando Pagamento',
            2 => 'Em Análise',
            3 => 'Paga',
            4 => 'Disponível',
            5 => 'Em Disputa',
            6 => 'Devolvida',
            7 => 'Cancelada',
            8 => 'Debitado',
            9 => 'Retenção temporária'
        ];

        /**
         * Recebe os valores que serao inseridos no corpo da transaçao
         *
         * @var array
         */
        protected $paymentRequest = [
            'shippingAddressCountry'    => 'BRA',
            'shippingType'              => 3,
            'shippingCost'              => "0.00"
        ];

        /**
         * Construtor da classe
         *
         * @param   DebitosRepository  $debitos
         * @param   Ass001  $associado
         * @param   PagSeguroConfig  $pagseguroConfig
         * @param   CobrancaRepository  $bilings
         */
        public function __construct(
            DebitosRepository $debitos,
            Ass001 $associado,
            PagSeguroConfig $pagseguroConfig,
            CobrancaRepository $bilings)
        {
            $this->debitos      = $debitos;
            $this->associado    = $associado;
            $this->billings     = $bilings;

            $pagseguroConfig->setEnvironmentConfig();
            $this->setNotificationUrl();
        }

        /**
         * Seta a URL de notificação a ser utilizada pelo pagseguro
         *
         * @return  void
         */
        private function setNotificationUrl()
        {
            $this->paymentRequest['notificationURL'] = route('payment.do', [Client::clube()]);
        }

        /**
         * Seta a matricula do associado
         *
         * @param   int  $matricula
         * @return  $this
         */
        public function setMatricula($matricula)
        {
            $this->matricula = $matricula;

            return $this;
        }

        /**
         * Seta os dados do associado no array da request
         *
         * @return  $this
         */
        public function setSenderData()
        {
            $this->sender   = $this->associado->findOrFail($this->matricula);
            $associado      = $this->sender;

            if(config('pagseguro.sandbox') == true) {
                $this->paymentRequest['senderEmail'] = $this->getVendedorSandbox();
            } else {
                $this->paymentRequest['senderEmail'] = $associado->CAIXAPOSTAL;
            }

            // Separa os telefones do associado
            $telefone   = $associado->TELEFONER1 ?: $associado->TELEFONER2;
            $telefone   = Utils::unmask($telefone);
            $ddd        = substr($telefone, 0, 2);
            $telefone   = substr($telefone, 2);

            $this->paymentRequest['senderName']                 = utf8_encode($associado->NOMEDOTITULAR);
            $this->paymentRequest['senderCPF']                  = $associado->CIC;
            $this->paymentRequest['senderAreaCode']             = $ddd;
            $this->paymentRequest['senderPhone']                = $telefone;
            $this->paymentRequest['shippingAddressPostalCode']  = $associado->CEPRESIDENCIAL;
            $this->paymentRequest['shippingAddressStreet']      = utf8_encode($associado->ENDRES_LOGRADOURO);
            $this->paymentRequest['shippingAddressNumber']      = $associado->ENDRES_NUMERO;
            $this->paymentRequest['shippingAddressComplement']  = utf8_encode($associado->ENDRES_COMPLEMENTO);
            $this->paymentRequest['shippingAddressDistrict']    = utf8_encode($associado->BAIRRORESIDENCIAL);
            $this->paymentRequest['shippingAddressCity']        = utf8_encode($associado->CIDADERESIDENCIAL);
            $this->paymentRequest['shippingAddressState']       = utf8_encode($associado->ESTADORESIDENCIAL);

            return $this;
        }

        /**
         * Recebe os dados de pagamento do cliente
         *
         * @param   stdClass  $paymentData
         * @return  $this
         */
        public function setPaymentData(stdClass $paymentData)
        {
            $this->paymentData = $paymentData;

            return $this;
        }

        /**
         * Seta o ID da cobrança gerada
         *
         * @param   int  $cobranca
         * @return  $this
         */
        public function setCobranca($cobranca)
        {
            $this->cobranca = $cobranca;

            return $this;
        }

        /**
         * Seta os dados do produto no array que será enviado nos dados do pagseguro
         *
         * @return  $this
         * @throws  Exception
         */
        public function setProductData()
        {
            $cobranca = $this->billings->find($this->cobranca);

            if(!$cobranca) {
                throw new Exception('Não foi possível recuperar os dados da cobrança. Tente Novamente');
            } else {
                $this->paymentRequest['itemId1']            = $this->cobranca;
                $this->paymentRequest['itemDescription1']   = 'Pagamento de Taxas Periódicas';
                $this->paymentRequest['itemAmount1']        = number_format($cobranca->VALOR, 2, '.', '');
                $this->paymentRequest['itemQuantity1']      = 1;

            }

            return $this;
        }

        /**
         * Realiza o pagamento utilizando cartao de credito
         *
         * @return  array
         */
        public function payWithCreditCard()
        {
            $this->paymentRequest['paymentMethod']                  = 'creditCard';
            $this->paymentRequest['senderHash']                     = $this->paymentData->senderHash;
            $this->paymentRequest['creditCardToken']                = $this->paymentData->cardToken;
            $this->paymentRequest['installmentQuantity']            = $this->paymentData->installments;
            $this->paymentRequest['installmentValue']               = $this->paymentData->installmentAmount;
            $this->paymentRequest['noInterestInstallmentQuantity']  = $this->paymentData->parcelas_possiveis;
            $this->paymentRequest['creditCardHolderName']           = $this->paymentData->holderName;
            $this->paymentRequest['creditCardHolderCPF']            = $this->paymentData->holderCpf;
            $this->paymentRequest['creditCardHolderBirthDate']      = $this->paymentData->holderBirthDate;
            $this->paymentRequest['creditCardHolderAreaCode']       = $this->paymentData->holderAreaCode;
            $this->paymentRequest['creditCardHolderPhone']          = $this->paymentData->holderPhone;
            $this->paymentRequest['billingAddressPostalCode']       = $this->sender->CEPRESIDENCIAL;
            $this->paymentRequest['billingAddressStreet']           = utf8_encode($this->sender->ENDRES_LOGRADOURO);
            $this->paymentRequest['billingAddressNumber']           = $this->sender->ENDRES_NUMERO;
            $this->paymentRequest['billingAddressComplement']       = utf8_encode($this->sender->ENDRES_COMPLEMENTO);
            $this->paymentRequest['billingAddressDistrict']         = utf8_encode($this->sender->BAIRRORESIDENCIAL);
            $this->paymentRequest['billingAddressCity']             = utf8_encode($this->sender->CIDADERESIDENCIAL);
            $this->paymentRequest['billingAddressState']            = utf8_encode($this->sender->ESTADORESIDENCIAL);
            $this->paymentRequest['billingAddressCountry']          = 'BRA';

            return PagSeguro::doPayment($this->paymentRequest);
        }

        /**
         * Realiza a busca do vendedor de testes
         *
         * @return  string
         */
        private function getVendedorSandbox()
        {
            $cliente = Session::get('cliente');

            return Cliente::findOrFail($cliente)->token->vendedor_sandbox;
        }

        /**
         * Processa um pagamento vindo do Pagseguro
         *
         * @param   string  $notificationCode
         * @return  void
         */
        public function proccessPayment($notificationCode, $clube)
        {
            $transacao  = PagSeguro::paymentOrderConsult($notificationCode);

            if(is_array($transacao)) {

                // Recebe os dados do pagseguro em um objeto
                $baixa = new stdClass;
                $baixa->idCobranca      = (int) $transacao['transaction']['items']['item']['id'];
                $baixa->status          = $transacao['transaction']['status'];
                $baixa->dataRetorno     = date('Y-m-d', strtotime($transacao['transaction']['date']));
                $baixa->horaRetorno     = date('H:i:s', strtotime($transacao['transaction']['date']));
                $baixa->valorPago       = (float) $transacao['transaction']['items']['item']['amount'];

                $log['clube']     = $clube;
                $log['cobranca']  = $baixa->idCobranca;
                $log['status']    = $this->paymentStatus[$baixa->status];
                $log['valor']     = $baixa->valorPago;
                $log['msg']       = '';
                $this->savePSLog($log);

                $cobranca = $this->billings->getCobrancaForBaixa($baixa->idCobranca);

                if($cobranca) {
                    if($cobranca->RECIBO != null) {
                        $this->setLog($cobranca->COTA, $baixa->dataRetorno, $baixa->horaRetorno, 'Cobranca ja quitada');
                        return;
                    }

                    if($baixa->status != 3) {
                        $this->setLog($cobranca->COTA, $baixa->dataRetorno, $baixa->horaRetorno, $this->paymentStatus[$baixa->status]);
                        $this->billings->updateCobrancaAndTaxas($baixa);
                        return;
                    }

                    $this->quitar($cobranca, $baixa);
                }

            } else {
                $log['clube']     = $clube;
                $log['cobranca']  = '';
                $log['status']    = '';
                $log['valor']     = '';
                $log['msg']       = $transacao;
                $this->savePSLog($log);
            }
        }

        public function savePSLog(array $log)
        {
            $log = (object) $log;

            PSLog::generate($log);
        }

        /**
         * Cria um registro de log quando a cobranca já tiver sido quitada ou
         * o código de status do PagSeguro for diferente de 3 (Quitado)
         *
         * @param   int  $cota
         * @param   string  $data
         * @param   string  $hora
         * @param   string  $observacao
         */
        private function setLog($cota, $data, $hora, $observacao)
        {
            Contatos::saveLog([
                'COTA'         => $cota,
                'DATAHORA'     => date('Y.m.d H:i:s'),
                'DATA'         => $data,
                'HORA'         => $hora,
                'SITUACAO'     => 'Retorno PAGSEGURO',
                'OBSERVACAO'   => "'".utf8_decode($observacao)."'"
            ]);
        }

        /**
         * Quita uma cobrança em aberto
         *
         * @param   Cobranca  $cobranca
         * @param   stdClass  $transacao
         * @return  void
         */
        private function quitar(Cobranca $cobranca, stdClass $transacao)
        {
            $valorBase      = (float) $cobranca->VALOR;
            $valorPagSeguro = $transacao->valorPago;
            $valorJuros     = 0;
            $valorDesconto  = 0;

            if($valorPagSeguro > $valorBase) {
                $valorJuros = $valorPagSeguro - $valorBase;
            } else if($valorPagSeguro < $valorBase) {
                $valorDesconto = $valorBase - $valorPagSeguro;
            }

            $recibo = (int) Generator::newId('GEN_RECIBO');

            \DB::connection('csplus')->statement('EXECUTE PROCEDURE PROC_EXECUTABAIXA_COBRANCA_FIN(:boleto, :recibo, :valorPago, :juros, 700, :dataPagamento, 0, :desconto, :valorBase, :dataTarifa, :dataFinanceiro)', [
                'boleto'            => $cobranca->BOLETO,
                'recibo'            => $recibo,
                'valorPago'         => $transacao->valorPago,
                'juros'             => $valorJuros,
                'dataPagamento'     => $transacao->dataRetorno,
                'desconto'          => $valorDesconto,
                'valorBase'         => $valorBase,
                'dataTarifa'        => $transacao->dataRetorno,
                'dataFinanceiro'    => $transacao->dataRetorno,
            ]);
        }
    }
