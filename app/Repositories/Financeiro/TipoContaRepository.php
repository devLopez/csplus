<?php

    namespace CSPlus\Repositories\Financeiro;

    /**
     * TipoContaRepository
     *
     * Gerencia os tipos de conta
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 1.0.0
     * @since   06/02/2017
     * @package CSPlus\Repositories\Financeiro
     */
    class TipoContaRepository
    {
        /**
         * @var array
         */
        protected $tiposConta = [
            1   => 'Conta Corrente',
            2   => 'Conta Simples PF',
            3   => 'Conta PJ',
            6   => 'Entidades Públicas',
            13  => 'Poupança',
            22  => 'Poupança PJ',
            23  => 'Caixa Fácil',
            28  => 'Crédito Imobiliário'
        ];

        /**
         * @return array
         */
        public function getTiposConta()
        {
            return $this->tiposConta;
        }
    }