<?php

    namespace CSPlus\Repositories\Financeiro;

    use DB;
    use CSPlus\Models\CSPlus\Financeiro\Taxas;
    use CSPlus\Models\CSPlus\Generator;
    use Exception;

    /**
     * TaxasRepository
     *
     * Repositório desenvolvido para gerenciamento de queries mais complexas no
     * gerenciamento do model Taxas
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 2.0.0
     * @since   21/02/2017
     */
    class TaxasRepository
    {
        /**
         * Realiza a geração das taxas para o convite
         *
         * @param   int  $matricula
         * @param   int  $convidado
         * @param   int  $produto
         * @param   float  $valorConvite
         * @return  mixed
         * @throws  Exception
         */
        public static function setTaxaConvite($matricula, $convidado, $produto, $valorConvite)
        {
            $codTaxa = Generator::newId('GEN_TAXAS');

            $taxa = new Taxas();
            $taxa->CODIGO       = $codTaxa;
            $taxa->COTA         = $matricula;
            $taxa->DEP          = 0;
            $taxa->DATA         = DB::raw("(select cast(diavencimento || '.' || extract(month from proximo) || '.' || extract(year from proximo) as date)
				from (SELECT case when c.DESCRICAO containing 'proprietario' then 08 else 01 end as diavencimento,
					case when c.DESCRICAO containing 'proprietario' then
	        		case when extract(day from current_date) < 16 then addmonth(current_date, 1) else addmonth(current_date, 2) end
					else
					case when extract(day from current_date) < 10 then addmonth(current_date, 1) else addmonth(current_date, 2) end
					end proximo
					from ass001 a
					left join CAT001 c on c.CODIGO = a.CATEGORIADACOTA
					where a.numerodacota = 1)
				)");
            $taxa->PRODUTO      = $produto;
            $taxa->HISTORICO    = DB::raw("'Convidado:' || (select first 1 c.nome from convidados c where c.codigo = $convidado)");
            $taxa->QUANTIDADE   = 1;
            $taxa->VALOR        = $valorConvite;

            if($taxa->save()) {
                return $codTaxa;
            } else {
                throw new Exception('Ocorreu um erro na geração da taxa. Tente realizar a emissão novamente');
            }
        }
    }
