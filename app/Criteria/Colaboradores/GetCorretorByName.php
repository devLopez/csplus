<?php

    namespace CSPlus\Criteria\Colaboradores;

    use CSPlus\Services\Repository\Criteria;
    use CSPlus\Services\Repository\Repository;

    /**
     * GetCorretorByName
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 1.0.0
     * @since   09/08/2017
     * @package CSPlus\Criteria\MenuAssociado
     */
    class GetCorretorByName extends Criteria
    {
        /**
         * @var string
         */
        protected $name;

        /**
         * @param   string  $name
         */
        public function __construct($name)
        {
            $this->name = utf8_decode($name);
        }

        /**
         * @param   mixed  $model
         * @param   Repository  $repository
         * @return  mixed
         */
        public function apply($model, Repository $repository)
        {
            return $model->where('NOME', 'like', "%".$this->name."%")
                         ->orderBy('NOME')
                         ->take(5);
        }
    }
