<?php

    namespace CSPlus\Criteria\Senhas;

    use CSPlus\Services\Repository\Criteria;
    use CSPlus\Services\Repository\Repository;

    /**
     * AffiliatePassword
     *
     * Realiza a busca de senhas cadastradas pela matrícula
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 1.0.0
     * @since   06/06/2017
     */
    class AffiliatePassword extends Criteria
    {
        /**
         * @var int
         */
        protected $matricula;

        /**
         * Construtor da classe
         *
         * @param   int  $matricula
         */
        public function __construct($matricula)
        {
            $this->matricula = $matricula;
        }

        /**
         * Aplica a Criteria ao query builder
         *
         * @param   mixed  $model
         * @param   Repository  $repository
         * @return  mixed
         */
        public function apply($model, Repository $repository)
        {
            return $model->where('COTA', $this->matricula)->where('DEP', NULL);
        }
    }
