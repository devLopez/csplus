<?php

    namespace CSPlus\Criteria\Associados;

    use CSPlus\Services\Repository\Criteria;
    use CSPlus\Services\Repository\Repository;

    /**
     * GetByName
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 1.0.0
     * @since   07/08/2017
     * @package CSPlus\Criteria\MenuAssociado
     */
    class GetByName extends Criteria
    {
        /**
         * @var string
         */
        protected $name;

        /**
         * @param   string  $name
         */
        public function __construct($name)
        {
            $this->name = utf8_decode($name);
        }

        /**
         * @param   mixed  $model
         * @param   Repository  $repository
         * @return  mixed
         */
        public function apply($model, Repository $repository)
        {
            return $model->where('NOMEDOTITULAR', 'like', "%".$this->name."%")
                         ->orderBy('NOMEDOTITULAR')
                         ->take(5);
        }
    }
