<?php

    namespace CSPlus\Criteria\MenuAssociado;

    use CSPlus\Services\Repository\Criteria;
    use CSPlus\Services\Repository\Repository;

    /**
     * GetLinksForMenu
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 1.0.0.
     * @since   31/07/2017
     * @package CSPlus\Criteria\MenuAssociado
     */
    class GetLinksForMenu extends Criteria
    {
        /**
         * @param   mixed  $model
         * @param   Repository  $repository
         * @return  mixed
         */
        public function apply($model, Repository $repository)
        {
            return $model->orderBy('ORDEM');
        }
    }