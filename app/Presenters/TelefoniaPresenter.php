<?php

    namespace CSPlus\Presenters;

    use CSPlus\Presenter\PresenterInterface;
    use League\Fractal\Manager;
    use League\Fractal\Resource\Collection;
    use JansenFelipe\Utils\Mask as MaskDefinitions;
    use CSPlus\Services\Words\Mask;
    use CSPlus\Services\Support\Date;

    /**
     * TelefoniaPresenter
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 1.0.0
     * @since   03/08/2017
     * @package CSPlus\Presenters
     */
    class TelefoniaPresenter implements PresenterInterface
    {
        protected $fractal;

        protected $date;

        public function __construct(Manager $manager)
        {
            $this->fractal  = $manager;
            $this->date     = new Date();
        }

        /**
         * Formata os dados do relatório agrupado
         *
         * @return  array
         */
        public function relatorioAgrupado($data)
        {
            $response = new Collection($data, function ($r){
                return [
                    Mask::mask($r->TELEFONE, MaskDefinitions::TELEFONE),
                    $this->date->format($r->DATA_HORA_INI, 'd/m/Y H:i:s'),
                    'R$' . money2br($r->VALOR),
                    utf8_encode($r->NOMEDOTITULAR),
                    $r->TIPO_LIGACAO
                ];
            });

            return $this->fractal->createData($response)->toArray();
        }

        /**
         * Formata os dados do relatorio detalhado
         *
         * @return  array
         */
        public function relatorioDetalhado($data)
        {
            $response = new Collection($data, function ($d) {
                return [
                    $this->date->format($d->DATA_HORA_INI, 'd/m/Y H:i:s'),
                    $d->DURACAO,
                    utf8_encode($d->HISTORICO),
                    Mask::mask($d->DISCADO, MaskDefinitions::TELEFONE),
                    'R$'.money2br($d->VALOR),
                    $d->TIPO_LIGACAO,
                    'R$'.money2br($d->TARIFA)
                ];
            });

            return $this->fractal->createData($response)->toArray();
        }
    }
