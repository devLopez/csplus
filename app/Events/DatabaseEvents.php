<?php

    namespace CSPlus\Events;

    use DB;
    use CSPlus\Models\CSPlus\LogSistema;
    use CSPlus\Services\Database\Traits\ConnectionResolver;

    /**
     * DatabaseEvents
     *
     * Realiza a criação dos logs sql executados pelo sistema
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmal.com>
     * @version 1.2.0
     * @since   22/06/2017
     */
    class DatabaseEvents
    {
        use ConnectionResolver;

        /**
         * @var string
         */
        protected $connection = 'csplus';

        /**
         * @var string
         */
        protected $query;

        /**
         * @var array
         */
        protected $bindings;

        /**
         * Coleta as informações do log e as salva na base de dados
         */
        public function operation()
        {
            $queryLog = DB::connection('csplus')->getQueryLog();
            $queryLog = end($queryLog);
            
            $this->query    = $queryLog['query'];
            $this->bindings = $queryLog['bindings'];

            $this->formatSql();

            LogSistema::saveLog([
                'DATAHORA'      => date('Y.m.d H:i:s'),
                'CODUSUARIO'    => session('admin.identifier'),
                'PROGRAMA'      => 'Modulo CSMobile',
                'IP'            => $_SERVER['REMOTE_ADDR'],
                'INSTRUCAO'     => $this->query,
                'FORMULARIO'    => session('arquivo'),
                'COTA'          => session('matricula')
            ]);
        }

        /**
         * Formata o sql recebido, adicionando os parâmetros do binding
         */
        private function formatSql()
        {
            $bindings   = $this->bindings;
            $query      = $this->query;

            // Formata o bind para inserção no sql
            foreach ($bindings as $i => $binding) {
                if (is_string($binding)) {
                    $bindings[$i] = '"'.$binding.'"';
                }
            }

            // Insert bindings into query
            $query          = str_replace(array('%', '?'), array('%%', '%s'), $query);
            $this->query    = vsprintf($query, $bindings);
        }
    }