<?php

    namespace CSPlus\Http\Middleware;

    use Closure;
    use CSPlus\Services\Client\AccessConfig;

    /**
     * PagseguroClientMiddleware
     *
     * Middleware desenvolvido para inicialização do banco de dados
     * de acordo o número do clube, vindo com a requisição do pagseguro
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 1.1.0
     * @since   17/01/2017
     */
    class PagseguroClientMiddleware
    {
        /**
         * Handle an incoming request.
         *
         * @param  \Illuminate\Http\Request  $request
         * @param  \Closure  $next
         * @return mixed
         */
        public function handle($request, Closure $next)
        {
            if(!AccessConfig::setEnv($request->clube, 'associado')) {
                return response('We can\'t determine your client access. Try again', 422);
            }

            return $next($request);
        }
    }
