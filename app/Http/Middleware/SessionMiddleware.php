<?php

    namespace CSPlus\Http\Middleware;

    use AdminAuth;
    use AssociateAuth;
    use Client;
    use Closure;

    class SessionMiddleware
    {
        /**
         * Handle an incoming request.
         *
         * @param   \Illuminate\Http\Request  $request
         * @param   \Closure  $next
         * @return  mixed
         */
        public function handle($request, Closure $next)
        {
            session()->regenerate();
            
            if(Client::access() == 'associado') {
                if(!AssociateAuth::check()) {
                    return redirect()
                            ->route('login.index', [Client::clube(), Client::access()])
                            ->with('erro', 'Para acesso ao sistema é necessário fazer login');
                }
            }

            if(Client::access() == 'admin') {
                if(!AdminAuth::check()) {
                    return redirect()
                        ->route('login.index', [Client::clube(), Client::access()])
                        ->with('erro', 'Para acesso ao sistema é necessário fazer login');
                }
            }

            return $next($request);
        }
    }
