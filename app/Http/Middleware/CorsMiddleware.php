<?php

    namespace CSPlus\Http\Middleware;

    use Closure;

    /**
     * CorsMiddleware
     *
     * Classe desenvolvida para gerenciamento do Cors (Cross Origin Resource Sharing)
     * utilizado pela integração com o pagseguro
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 1.0.0
     * @since   16/12/2016
     */
    class CorsMiddleware
    {
        /**
         * Handle an incoming request.
         *
         * @param   \Illuminate\Http\Request  $request
         * @param   \Closure  $next
         * @return  mixed
         */
        public function handle($request, Closure $next)
        {
            return $next($request)
                ->header('Access-Control-Allow-Origin', 'https://sandbox.pagseguro.uol.com.br')
                ->header('Access-Control-Allow-Methods', 'POST');
        }
    }