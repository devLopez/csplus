<?php

    namespace CSPlus\Http\Middleware;

    use Auth;
    use Closure;
    use Policy;
    use CSPlus\Repositories\ContatosRepository as Contatos;

    /**
     * AceiteContrato
     *
     * Classe responsável pelo gerenciamento do aceite aos termos pelo novo
     * associado
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 1.1.0
     * @since   25/04/2017
     */
    class AceiteContrato
    {
        /**
         * @var \CSPlus\Repositories\ContatosRepository
         */
        protected $contatos;

        /**
         * Contrutor da classe
         *
         * @param   Contatos  $contatos
         */
        public function __construct(Contatos $contatos)
        {
            $this->contatos = $contatos;
        }

        /**
         * Handle an incoming request.
         *
         * @param  \Illuminate\Http\Request  $request
         * @param  \Closure  $next
         * @return mixed
         */
        public function handle($request, Closure $next)
        {
            $matricula = Auth::matricula();

            if(!Policy::isAdmin() and $this->contatos->getAceiteWeb($matricula) == null) {
                return redirect()->route('termos.ler', [$matricula]);
            }

            return $next($request);
        }
    }
