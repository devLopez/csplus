<?php

    namespace CSPlus\Http\Middleware;

    use Closure;

    /**
     * ConvertToNull
     *
     * Clçasse responsável por converter o texto null para NULL, corrigindo assim
     * o erro da pesquisa de associados
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 1.0.0
     * @since   05/06/2017
     */
    class ConvertToNull
    {
        /**
         * Handle an incoming request.
         *
         * @param  \Illuminate\Http\Request  $request
         * @param  \Closure  $next
         * @return mixed
         */
        public function handle($request, Closure $next)
        {
            $input = $request->all();

            if(isset($input['texto'])) {
                $input['texto'] = ($input['texto'] != 'null') ? $input['texto']: null;
                $request->replace($input);
            }

            return $next($request);
        }
    }
