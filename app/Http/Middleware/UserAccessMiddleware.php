<?php

    namespace CSPlus\Http\Middleware;

    use Auth;
    use Closure;
    use Policy;

    class UserAccessMiddleware
    {
        /**
         * Realiza a manipulação da requisição
         *
         * @param   \Illuminate\Http\Request  $request
         * @param   \Closure  $next
         * @return  mixed
         */
        public function handle($request, Closure $next)
        {
            $acesso     = Auth::access();
            $idUsuario  = Auth::matricula();
            $matricula  = $request->route('matricula');

            if($acesso == 'admin') {
                return $next($request);
            }

            if($idUsuario == $matricula) {
                return $next($request);
            }

            abort(403);
        }
    }
