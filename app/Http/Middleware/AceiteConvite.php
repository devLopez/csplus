<?php

    namespace CSPlus\Http\Middleware;

    use Closure;
    use Policy;
    use CSPlus\Repositories\Convites\ConvitesRepository as Convites;

    class AceiteConvite
    {
        /**
         * @var \CSPlus\Repositories\Convites\ConvitesRepository
         */
        protected $convites;

        /**
         * @param   Convites  $convites
         */
        public function __construct(Convites $convites)
        {
            $this->convites = $convites;
        }

        /**
         * Handle an incoming request.
         *
         * @param  \Illuminate\Http\Request  $request
         * @param  \Closure  $next
         * @return mixed
         */
        public function handle($request, Closure $next)
        {
            $matricula = $request->route('matricula');

            if($this->convites->getAceite($matricula) == null and !Policy::isAdmin()) {
                return redirect()->route('convite.aceite', [$matricula]);
            }

            return $next($request);
        }
    }
