<?php

    namespace CSPlus\Http\Middleware;

    use Auth;
    use Closure;

    class AdminAccessMiddleware
    {
        /**
         * Handle an incoming request.
         *
         * @param  \Illuminate\Http\Request  $request
         * @param  \Closure  $next
         * @return mixed
         */
        public function handle($request, Closure $next)
        {
            if(Auth::isAdmin()) {
                return $next($request);
            }

            abort(403);
        }
    }
