<?php

    namespace CSPlus\Http\Requests\CSMobile;

    use Illuminate\Foundation\Http\FormRequest;

    class ClienteRequest extends FormRequest
    {
        /**
         * Determine if the user is authorized to make this request.
         *
         * @return bool
         */
        public function authorize()
        {
            return true;
        }

        /**
         * Get the validation rules that apply to the request.
         *
         * @return array
         */
        public function rules()
        {
            return [
                'cliente'               => 'integer|required',
                'nome_clube'            => 'max:40',
                'bd_principal'          => 'required',
                'bd_imagens'            => 'required',
                'user'                  => 'required',
                'password'              => 'required',
                'area_administrativa'   => 'boolean',
                'area_associado'        => 'boolean',
                'usa_pagseguro'         => 'boolean',
                'logo'                  => 'mimes:png|mimetypes:image/png|max:256',
                'maximo_parcelas'       => 'required_if:usa_pagseguro,1|integer',
                'parcelas_sem_juros'    => 'required_if:usa_pagseguro,1|integer',
                'email'                 => 'required_if:usa_pagseguro,1|email',
                'token_producao'        => 'required_if:usa_pagseguro,1',
                'token_sandbox'         => 'required_if:usa_pagseguro,1',
                'vendedor_sandbox'      => 'required_if:usa_pagseguro,1|email'
            ];
        }

        public function messages()
        {
            return [
                'bd_principal.required'             => 'O endereço do banco é obrigatório',
                'bd_imagens.required'               => 'O endereço do banco de imagens é obrigatório',
                'user.required'                     => 'Usuário do banco é obrigatòrio',
                'password.required'                 => 'A senha do banco é obrigatória',
                'logo.mimes'                        => 'A logo deve ser uma imagen PNG',
                'logo.mimetypes'                    => 'A logo deve ser uma imagen válida',
                'logo.size'                         => 'A logo não pode ter mais que 256kb',
                'maximo_parcelas.required_if'       => 'A quantidade de parcelas é obrigatória',
                'maximo_parcelas.integer'           => 'A quantidade de parcelas deve ser um número',
                'parcelas_sem_juros.required_if'    => 'As parcelas sem juros são obrigatórias',
                'parcelas_sem_juros.integer'        => 'As parcelas sem juros devem ser um número',
                'email.required_if'                 => 'O email é obrigatorio',
                'token_producao.required_if'        => 'O token de produção é obrigatorio',
                'token_sandbox.required_if'         => 'O token do sandbox é obrigatorio',
                'vendedor_sandbox.required_if'      => 'O email do sandbox é obrigatório',
                'vendedor_sandbox.email'            => 'O email do sandbox deve ser um email válido'
            ];
        }
    }
