<?php

    namespace CSPlus\Http\Requests;

    use CSPlus\Http\Requests\Request;

    class ParentescoRequest extends Request
    {
        /**
         * Determine if the user is authorized to make this request.
         *
         * @return bool
         */
        public function authorize()
        {
            return true;
        }

        /**
         * Get the validation rules that apply to the request.
         *
         * @return array
         */
        public function rules()
        {
            return [
                'DESCRICAO' => 'required|min:3|max:20'
            ];
        }

        public function messages()
        {
            return [
                'DESCRICAO.required' => 'O parentesco é obrigatório',
                'DESCRICAO.min' => 'O parentesco deve ter no mínimo 3 caracteres',
                'DESCRICAO.max' => 'O parentesco deve ter no máximo 20 caracteres'
            ];
        }
    }
