<?php

    namespace CSPlus\Http\Requests;

    use CSPlus\Http\Requests\Request;

    class CategoriasRequest extends Request
    {
        /**
         * Determine if the user is authorized to make this request.
         *
         * @return bool
         */
        public function authorize()
        {
            return true;
        }

        /**
         * Get the validation rules that apply to the request.
         *
         * @return array
         */
        public function rules()
        {
            return [
                'DESCRICAO' => 'required|min:5|max:35',
                'TEXTO_FICHAINSCRICAO' => 'required|min:50'
            ];
        }

        public function messages()
        {
            return [
                'DESCRICAO.required' => 'A descrição é obrigatória',
                'DESCRICAO.min' => 'A descrição deve ter no mínimo 10 caracteres',
                'DESCRICAO.max' => 'A descrição deve ter no máximo 35 caracteres',
                'TEXTO_FICHAINSCRICAO.required' => 'O texto é obrigatório',
                'TEXTO_FICHAINSCRICAO.min' => 'O texto deve ter no mínimo 50 caracteres'
            ];
        }
    }
