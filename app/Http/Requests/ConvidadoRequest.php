<?php namespace CSPlus\Http\Requests;

    /**
     * ConvidadoRequest
     *
     * Realiza a validação de dados vindos da request
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 1.0.0
     * @since   09/08/2016
     */
    class ConvidadoRequest extends Request
    {
        /**
         * Determine if the user is authorized to make this request.
         *
         * @return bool
         */
        public function authorize()
        {
            return true;
        }

        /**
         * Get the validation rules that apply to the request.
         *
         * @return array
         */
        public function rules()
        {
            return [
                'NOME'          => 'required|max:40',
                'CI'            => 'required|max:11',
                'DATANASC'      => 'required|date_format:d/m/Y',
                'REFERENCIA'    => 'required|date_format:m/Y'
            ];
        }

        /**
         * Define as mensagens personalizadas da validação
         * 
         * @return array
         */
        public function messages()
        {
            return [
                'NOME.required'             => 'O nome é obrigatório',
                'NOME.max'                  => 'O nome não pode ter mais que 40 caracteres',
                'CI.required'               => 'A identidade é obrigatória',
                'CI.max'                    => 'A identidade não pode ter mais que 11 caracteres',
                'DATANASC.required'         => 'A data de nascimento é obrigatória',
                'DATANASC.date_format'      => 'A data de nascimento é inválida',
                'REFERENCIA.required'       => 'A referência é obrigatória',
                'REFERENCIA.date_format'    => 'A referência é inválida'
            ];
        }
    }
