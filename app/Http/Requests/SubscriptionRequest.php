<?php namespace CSPlus\Http\Requests;

    use CSPlus\Http\Requests\Request;

    class SubscriptionRequest extends Request
    {
        /**
         * Determine if the user is authorized to make this request.
         *
         * @return bool
         */
        public function authorize()
        {
            return true;
        }

        /**
         * Get the validation rules that apply to the request.
         *
         * @return array
         */
        public function rules()
        {
            return [
                'CATEGORIADACOTA'       => 'required',
                'NOMEDOTITULAR'         => 'required|max:40',
                'CIC'                   => 'required|max:14|cpf_cnpj',
                'CAIXAPOSTAL'           => 'required|email|max:100',
                'REGISTRO_CLASSE'       => 'required|digits:6',
                'DATANASCIMENTO'        => 'required|date_format:d/m/Y',
                'NACIONALIDADE'         => 'required|max:10',
                'TELEFONER1'            => 'required|max:20',
                'TELEFONER2'            => 'max:20',
                'CEPRESIDENCIAL'        => 'required|min:9|max:9',
                'ENDRES_LOGRADOURO'     => 'required|max:40',
                'ENDRES_NUMERO'         => 'required|max:11',
                'ENDRES_COMPLEMENTO'    => 'max:15',
                'BAIRRORESIDENCIAL'     => 'required|max:20',
                'CIDADERESIDENCIAL'     => 'required|max:20',
                'ESTADORESIDENCIAL'     => 'required|min:2|max:2',
                'EMPRESA'               => 'max:35',
                'CEPCOMERCIAL'          => 'max:9',
                'ENDCOM_LOGRADOURO'     => 'max:40',
                'ENDCOM_NUMERO'         => 'max:11',
                'ENDCOM_COMPLEMENTO'    => 'max:15',
                'BAIRROCOMERCIAL'       => 'max:20',
                'CIDADECOMERCIAL'       => 'max:20',
                'ESTADOCOMERCIAL'       => 'max:2',
                'TELEFONEC1'            => 'max:20',
                'TELEFONEC2'            => 'max:20',
                'SENHA'                 => 'required|max:8|confirmed'
            ];
        }

        public function messages()
        {
            return [
                'CATEGORIADACOTA.required'      => 'Categoria obrigatória',
                'NOMEDOTITULAR.required'        => 'Nome obrigatório',
                'NOMEDOTITULAR.max'             => 'O nome deve ter no máximo 40 caracteres',
                'CIC.required'                  => 'CPF obrigatório',
                'CIC.max'                       => 'O CPF deve ter no máximo 14 caracteres',
                'CAIXAPOSTAL.required'          => 'E-mail obrigatório',
                'CAIXAPOSTAL.email'             => 'O e-mail deve ser um e-mail válido',
                'CAIXAPOSTAL.max'               => 'O e-mail deve ter no máximo 100 caracteres',
                'REGISTRO_CLASSE.required'      => 'Cod. ANAC é obrigatório',
                'REGISTRO_CLASSE.digits'        => 'O cód. ANAC deve ter 6 dígitos',
                'DATANASCIMENTO.required'       => 'Data de nascimento obrigatória',
                'DATANASCIMENTO.date_format'    => 'A data de nascimento é inválida',
                'NACIONALIDADE.required'        => 'Nacionalidade obrigatória',
                'NACIONALIDADE.max'             => 'A nacionalidade deve ter no máximo 10 caracteres',
                'TELEFONER1.required'           => 'O telefone é obrigatório',
                'TELEFONER1.max'                => 'O telefone 1 deve ter no máximo 20 caracteres',
                'TELEFONER2.max'                => 'O telefone 2 deve ter no máximo 20 caracteres',
                'CEPRESIDENCIAL.required'       => 'CEP obrigatório',
                'CEPRESIDENCIAL.min'            => 'O CEP deve ter 9 caracteres',
                'CEPRESIDENCIAL.max'            => 'O CEP deve ter 9 caracteres',
                'ENDRES_LOGRADOURO.required'    => 'Endereço obrigatório',
                'ENDRES_LOGRADOURO.max'         => 'O endereço deve ter no máximo 40 caracteres',
                'ENDRES_NUMERO.required'        => 'Número obrigatório',
                'ENDRES_NUMERO.max'             => 'O número deve ter no máximo 11 caracteres',
                'ENDRES_COMPLEMENTO.max'        => 'O complemento deve ter no máximo 15 caracteres',
                'BAIRRORESIDENCIAL.required'    => 'Bairro obrigatório',
                'BAIRRORESIDENCIAL.max'         => 'O bairro deve ter no máximo 20 caracteres',
                'CIDADERESIDENCIAL.required'    => 'Cidade obrigatória',
                'CIDADERESIDENCIAL.max'         => 'A cidade deve ter no máximo 20 caracteres',
                'ESTADORESIDENCIAL.required'    => 'Estado Obrigatório',
                'ESTADORESIDENCIAL.min'         => 'O estado deve ter no mínimo 2 caracteres',
                'ESTADORESIDENCIAL.max'         => 'O estado deve ter no máximo 2 caracteres',
                'EMPRESA.max'                   => 'A empresa deve ter no máximo 35 caracteres',
                'CEPCOMERCIAL.min'              => 'O CEP comercial deve ter no mínimo 9 caracteres',
                'CEPCOMERCIAL.max'              => 'O CEP comercial deve ter no máximo 9 caracteres',
                'ENDCOM_LOGRADOURO.max'         => 'O end. comercial deve ter no máximo 40 caracteres',
                'ENDCOM_NUMERO.max'             => 'O num. comercial deve ter no máximo 11 caracteres',
                'ENDCOM_COMPLEMENTO.max'        => 'O comp. comercial deve ter no máximo 15 caracteres',
                'BAIRROCOMERCIAL.max'           => 'O bairro comercial deve ter no máximo 20 caracteres',
                'CIDADECOMERCIAL.max'           => 'A cidade comercial deve ter no máximo 20 caracteres',
                'ESTADOCOMERCIAL.min'           => 'O estado comercial deve ter no mínimo 2 caracteres',
                'ESTADOCOMERCIAL.max'           => 'O estado comercial deve ter no máximo 2 caracteres',
                'TELEFONEC1.max'                => 'O tel. comercial deve ter no máximo 20 caracteres',
                'TELEFONEC2.max'                => 'O fax deve ter no máximo 20 caracteres',
                'SENHA.required'                => 'A senha é obrigatória',
                'SENHA.max'                     => 'A senha deve conter no máximo 8 caracteres',
                'SENHA.confirmed'               => 'A confirmação de senha não confere'
            ];
        }
    }
