<?php

    namespace CSPlus\Http\ControllersDomains\CSPlus;

    use CSPlus\Http\Controllers\Controller;
    use CSPlus\Models\CSPlus\Evento;
    use Illuminate\Http\Request;

    /**
     * EventosController
     *
     * Realiza o gerenciamento das requisições relacionadas aos eventos
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 1.2.0
     * @since   20/07/2016
     */
    class EventosController extends Controller
    {
        /**
         * Exibe o formulário de inscrição
         *
         * @return  \Illuminate\Contracts\View\Factory
         *          \Illuminate\View\View
         */
        public function index()
        {
            // Recebe os valores do mês
            $mes        = date('m');
            $valores    = Evento::$valores[$mes];

            foreach ($valores as $key => $valor) {
                $precos[$key] = $key.' Pessoa(s) - R$'.number_format($valor, 2, ',', '.');
            }

            $dados = [
                'form' => [
                    'route' => 'eventos.salvar',
                    'class' => 'smart-form'
                ],
                'cargos' => [
                    'gerente'       => 'Gerente',
                    'diretor'       => 'Membro da Diretoria',
                    'presidente'    => 'Presidente',
                    'outro'         => 'Outra Função'
                ],
                'precos' => $precos
            ];
            return view('paginas.eventos.index', $dados);
        }

        /**
         * Realiza a criação das taxas para geração do boleto
         *
         * @param   Request $request
         * @return  \Illuminate\Http\RedirectResponse
         */
        public function store(Request $request)
        {
            $cobranca = Evento::createTax($request);

            if ($cobranca == false) {
                return redirect()->back()->withInput()->with('erro', true);
            }

            return redirect()->route('eventos.index')->with('cobranca', $cobranca)->with('cliente', $request->NUMERODACOTA);
        }

        /**
         * Realiza a busca dos dados do clube pelo CNPJ informado
         *
         * @param   Request  $request
         * @return  string
         */
        public function getClube(Request $request)
        {
            return Evento::getClubData($request->cnpj);
        }
    }
