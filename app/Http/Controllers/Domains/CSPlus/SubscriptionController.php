<?php

namespace CSPlus\Http\Controllers\Domains\CSPlus;

use AssociateAuth;
use Client;
use CSPlus\Criteria\Senhas\AffiliatePassword;
use CSPlus\Exceptions\Database\InvoiceNotCreated;
use CSPlus\Exceptions\Database\PasswordNotSaved;
use CSPlus\Http\Controllers\Controller;
use CSPlus\Http\Requests\SubscriptionRequest;
use CSPlus\Models\CSPlus\Financeiro\Boleto;
use CSPlus\Notifications;
use CSPlus\Repositories\Associados\AssociadoRepository as Associados;
use CSPlus\Repositories\Associados\CategoriasRepository as Categorias;
use CSPlus\Repositories\Associados\SenhaRepository as Senhas;
use CSPlus\Services\Client\AccessConfig;
use CSPlus\Services\Notification\SendNotification;
use Date;
use Exception;

/**
 * SubscriptionController
 *
 * Realiza o gerenciamento dos novos cadastros de associados
 *
 * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
 * @version 2.3.0
 * @since   07/06/2017
 */
class SubscriptionController extends Controller
{
    use SendNotification;

    /**
     * @var \CSPlus\Repositories\Associados\AssociadoRepository
     */
    protected $associados;

    /**
     * @var \CSPlus\Repositories\Associados\CategoriasRepository
     */
    private $categorias;

    /**
     * @var \CSPlus\Repositories\Associados\SenhaRepository
     */
    private $senhas;

    /**
     * @param   int|null  $clube
     * @return  \Illuminate\View\View
     */
    public function index($clube = null)
    {
        $erro = '';

        if(!$clube) {
            $erro = 'Para se inscrever é necessário informar o clube de Origem.';

        } else if(!AccessConfig::setEnv($clube, 'associado')) {

            $erro = 'Os dados do clube não foram encontrados. Verifique se o seu clube possui acesso ao sistema.';
        }

        return view('Subscription::index', compact('erro', 'clube'));
    }

    /**
     * Realiza a criação do novo associado
     *
     * @param   SubscriptionRequest  $request
     * @param   Boleto  $boleto
     * @return  \Illuminate\Http\RedirectResponse
     * @throws  Exception
     */
    public function store(SubscriptionRequest $request, Boleto $boleto)
    {
        try {
            $this->setDependencies();

            $associado = $this->associados->save(
                $request->except(['_token', 'clube', 'tipo_associado','SENHA', 'SENHA_confirmation'])
            );

            // Tenta realizar a geração do boleto para o associado
            $this->setTax($associado, $boleto);

            // Seta a senha do associado
            $this->setPassword($associado, $request->SENHA);

            // Envia os dados do novo associado para o email
            $notyAdmin  = new Notifications\AffiliatedCreated($associado);
            $notyUser   = new Notifications\AccountCreated($associado, $request->SENHA);

            $this->sendNotification($notyAdmin);
            $this->sendUserNotification($notyUser);

            return $this->login($associado, $request->SENHA);

        } catch (PasswordNotSaved $e) {

            return redirect()->route('login.index', [$request->clube, 'associado'])
                             ->with('erro', $e->getMessage());

        } catch (InvoiceNotCreated $e) {

            return redirect()->route('login.index', [$request->clube, 'associado'])
                             ->with('erro', $e->getMessage());

        } catch (Exception $e) {

            return redirect()->back()->withInput()->with('erro', $e->getMessage());
        }
    }

    /**
     * Realiza o Login do novo associado
     *
     * @param   \CSPlus\Models\CSPlus\Associado\Ass001  $associado
     * @param   string  $password
     * @return  \Illuminate\Http\RedirectResponse
     * @throws  Exception
     */
    private function login($associado, $password)
    {
        if(AssociateAuth::login($associado->CIC, $password)) {
            return redirect()->route('termos.ler', [$associado->NUMERODACOTA])
                             ->with('sucesso', 'A sua cota foi criada. Realize o pagamento através da opção Débitos');
        }

        throw new Exception('Seu cadastro foi efetuado, mas não foi possível fazer o login. Acesse a página inicial e entre com seus dados');
    }

    /**
     * Seta a senha para o novo usuário
     *
     * @param   \CSPlus\Models\CSPlus\Associado\Ass001  $associado
     * @param   mixed  $nova_senha
     * @throws  PasswordNotSaved
     */
    private function setPassword($associado, $nova_senha)
    {
        $novaSenha = [
            'COTA'          => $associado->NUMERODACOTA,
            'SENHA'         => $nova_senha,
            'ATUALIZACAO'   => Date::today('Y-m-d')
        ];

        $criteria           = new AffiliatePassword($associado->NUMERODACOTA);
        $senhaCadastrada    = $this->senhas->pushCriteria($criteria)->first();

        if(!$senhaCadastrada) {
            $resposta = $this->senhas->create($novaSenha);

        } else {

            $senhaCadastrada->SENHA = $nova_senha;

            $resposta = $senhaCadastrada->save();
        }

        if(!$resposta) {
            throw new PasswordNotSaved();
        }
    }

    /**
     * Realiza a criação da taxa para o associado
     *
     * @param   \CSPlus\Models\CSPlus\Associado\Ass001  $associado
     * @param   Boleto $boleto
     * @throws  InvoiceNotCreated
     */
    private function setTax($associado, Boleto $boleto)
    {
        $boleto->setMatricula($associado->NUMERODACOTA)
               ->setCobranca_inicial()
               ->generate();

        if(!is_int($boleto->getCobranca())) {
            throw new InvoiceNotCreated();
        }
    }

    /**
     * Seta as dependências para a criação do novo usuário
     *
     * @return  void
     */
    private function setDependencies()
    {
        $this->categorias   = app()->make(Categorias::class);
        $this->associados   = app()->make(Associados::class);
        $this->senhas       = app()->make(Senhas::class);
    }

    /**
     * Realiza a busca das categorias de acordo o tipo de associado
     *
     * @param   Categorias  $categoriasRepository
     * @param   string  $tipo
     * @return  array
     */
    public function getCategorias(Categorias $categoriasRepository, $tipo)
    {
        try {
            $categorias = $categoriasRepository->formatCategories(
                $categoriasRepository->getCategoriesForSubscription(false, $tipo)
            );

            return $categorias;

        } catch(\Exception $e) {

            return response($e->getMessage(), 500);
        }
    }
}
