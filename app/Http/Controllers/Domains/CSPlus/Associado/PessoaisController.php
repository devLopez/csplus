<?php

    namespace CSPlus\Http\Controllers\Domains\CSPlus\Associado;

    use Date;
    use Exception;

    use CSPlus\Http\Controllers\Controller;
    use CSPlus\Notifications;
    use CSPlus\Repositories\Associados\AssociadoRepository as Associados;
    use CSPlus\Services\Log\LogAssociado;
    use CSPlus\Services\Log\RegisterLog;
    use CSPlus\Services\Notification\SendNotification;
    use CSPlus\Services\Repository\Exceptions\ModelNotSavedException;
    use CSPlus\Services\Words\Mask;
    use Illuminate\Http\Request;

    /**
     * PessoaisController
     *
     * Realiza o gerenciamento dos dados pessoais do associad
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 2.7.3
     * @since   29/06/2017
     */
    class PessoaisController extends Controller
    {
        use RegisterLog, SendNotification;

        /**
         * @var \Illuminate\Http\Request
         */
        protected $dados_pessoais;

        /**
         * @var \CSPlus\Repositories\Associados\AssociadoRepository
         */
        protected $associados;

        public function __construct()
        {
            $this->middleware(function($request, $next) {
                $this->associados = app()->make(Associados::class);

                return $next($request);
            });
        }

        /**
         * Atualiza os dados de um associado cadastrado
         *
         * @param   Request $request
         * @param   $matricula
         * @return  \Illuminate\Http\Response
         */
        public function atualizarAssociado(Request $request, $matricula)
        {
            try {
                // Seta os dados vindos da request
                $this->dados_pessoais = $request;

                // Realiza a busca pelos dados do associado
                $associado = $this->associados->find($matricula);

                switch ($this->dados_pessoais->modo) {
                    case 'pessoais':
                        $dados = $this->getDadosPessoais();
                        break;
                    case 'profissionais':
                        $dados = $this->getDadosProfissionais();
                        break;
                    case 'complementares':
                        $dados = $this->getDadosComplementares();
                        break;
                    case 'bancarios':
                        $dados = $this->getDadosBancarios();
                        break;
                    default:
                        return response('Você não tem autorização para continuar', 401);
                        break;
                }

                $log            = new LogAssociado($matricula, $dados, $associado->toArray());
                $dados_novos    = $log->getDadosModificados();

                $this->logNewData($log);

                if($log->hasAlteracao()) {
                    if($this->associados->update($matricula, $dados_novos)) {
                        $this->setLog($matricula, $this->dados_pessoais->arquivo);

                        return response()->json(['message' => 'Os dados foram atualizados'], 200);
                    }

                    throw new Exception('Aconteceu um erro ao atualizar os dados do associado. Tente Novamente');
                }

                return response()->json(['message' => 'Não havia novos dados a serem atualizados'], 200);

            } catch(Exception $e) {
                return response()->json($e->getMessage(), 500);
            }
        }

        /**
         * Seta os dados pessoais do membro
         *
         * @return  array
         */
        private function getDadosPessoais()
        {
            return [
                'NOMEDOTITULAR'         => $this->dados_pessoais->NOMEDOTITULAR,
                'DATAAQUISICAO'         => Date::toSql($this->dados_pessoais->DATAAQUISICAO),
                'APELIDO'               => $this->dados_pessoais->APELIDO,
                'CATEGORIADACOTA'       => $this->dados_pessoais->CATEGORIADACOTA,
                'CEPRESIDENCIAL'        => Mask::unmask($this->dados_pessoais->CEPRESIDENCIAL),
                'ENDRES_LOGRADOURO'     => $this->dados_pessoais->ENDRES_LOGRADOURO,
                'ENDRES_NUMERO'         => $this->dados_pessoais->ENDRES_NUMERO,
                'ENDRES_COMPLEMENTO'    => $this->dados_pessoais->ENDRES_COMPLEMENTO,
                'BAIRRORESIDENCIAL'     => $this->dados_pessoais->BAIRRORESIDENCIAL,
                'CIDADERESIDENCIAL'     => $this->dados_pessoais->CIDADERESIDENCIAL,
                'ESTADORESIDENCIAL'     => $this->dados_pessoais->ESTADORESIDENCIAL,
                'TELEFONER1'            => $this->dados_pessoais->TELEFONER1,
                'TELEFONER2'            => $this->dados_pessoais->TELEFONER2,
                'CAIXAPOSTAL'           => $this->dados_pessoais->CAIXAPOSTAL,
                'DATANASCIMENTO'        => Date::toSql($this->dados_pessoais->DATANASCIMENTO),
                'RG'                    => $this->dados_pessoais->RG,
                'RG_ORGAO_EXPEDIDOR'    => $this->dados_pessoais->RG_ORGAO_EXPEDIDOR,
                'RG_DATA_EXPEDICAO'     => Date::toSql($this->dados_pessoais->RG_DATA_EXPEDICAO),
                'SEXO'                  => $this->dados_pessoais->SEXO,
                'ESTADOCIVIL'           => $this->dados_pessoais->ESTADOCIVIL,
                'OBSERVACOES'           => $this->dados_pessoais->OBSERVACOES
            ];
        }

        /**
         * @return  array
         */
        private function getDadosProfissionais()
        {
            return [
                'EMPRESA'               => $this->dados_pessoais->EMPRESA,
                'FUNCAO'                => $this->dados_pessoais->FUNCAO,
                'DEPARTAMENTO_EMPRESA'  => $this->dados_pessoais->DEPARTAMENTO_EMPRESA,
                'MATRICULA_EMPRESA'     => $this->dados_pessoais->MATRICULA_EMPRESA,
                'REGISTRO_CLASSE'       => $this->dados_pessoais->REGISTRO_CLASSE,
                'SITUACAO_EMPRESA'      => $this->dados_pessoais->SITUACAO_EMPRESA,
                'COMPLEMENTOCOMERCIAL'  => $this->dados_pessoais->COMPLEMENTOCOMERCIAL,
                'ADMISSAO_EMPRESA'      => Date::toSql($this->dados_pessoais->ADMISSAO_EMPRESA),
                'AFASTAMENTO_EMPRESA'   => Date::toSql($this->dados_pessoais->AFASTAMENTO_EMPRESA),
                'CAIXAPOSTAL_EMPRESA'   => $this->dados_pessoais->CAIXAPOSTAL_EMPRESA,
                'TELEFONEC1'            => $this->dados_pessoais->TELEFONEC1,
                'TELEFONEC2'            => $this->dados_pessoais->TELEFONEC2,
                'CEPCOMERCIAL'          => Mask::unmask($this->dados_pessoais->CEPCOMERCIAL),
                'ENDCOM_LOGRADOURO'     => $this->dados_pessoais->ENDCOM_LOGRADOURO,
                'ENDCOM_NUMERO'         => $this->dados_pessoais->ENDCOM_NUMERO,
                'ENDCOM_COMPLEMENTO'    => $this->dados_pessoais->ENDCOM_COMPLEMENTO,
                'BAIRROCOMERCIAL'       => $this->dados_pessoais->BAIRROCOMERCIAL,
                'CIDADECOMERCIAL'       => $this->dados_pessoais->CIDADECOMERCIAL,
                'ESTADOCOMERCIAL'       => $this->dados_pessoais->ESTADOCOMERCIAL,
                'ENDERECOCOBRANCA'      => $this->dados_pessoais->ENDERECOCOBRANCA,
                'DESCONTOFOLHA'         => $this->dados_pessoais->DESCONTOFOLHA,
                'APOSENTADO'            => $this->dados_pessoais->APOSENTADO,
            ];
        }

        /**
         * @return array
         */
        private function getDadosComplementares()
        {
            return [
                'NACIONALIDADE' => $this->dados_pessoais->NACIONALIDADE,
                'NATURALIDADE'  => $this->dados_pessoais->NATURALIDADE,
                'PAI'           => $this->dados_pessoais->PAI,
                'MAE'           => $this->dados_pessoais->MAE,
                'SANGUE'        => $this->dados_pessoais->SANGUE,
                'RESPONSAVEL'   => $this->dados_pessoais->RESPONSAVEL,
                'CORRETOR'      => $this->dados_pessoais->CORRETOR,
                'BENEMERITO'    => $this->dados_pessoais->BENEMERITO
            ];
        }

        /**
         * @return array
         */
        private function getDadosBancarios()
        {
            return [
                'BANCO'                 => $this->dados_pessoais->BANCO,
                'AGENCIA'               => $this->dados_pessoais->AGENCIA,
                'TIPOCONTA'             => $this->dados_pessoais->TIPOCONTA,
                'CONTACORRENTE'         => $this->dados_pessoais->CONTACORRENTE,
                'DIGITOCC'              => $this->dados_pessoais->DIGITOCC,
                'DEBITOCONTAAPROVADO'   => $this->dados_pessoais->DEBITOCONTAAPROVADO,
            ];
        }

        /**
         * Salva um novo associado na base de dados
         *
         * @param   Request $request
         * @return  \Illuminate\Http\Response
         */
        public function salvar(Request $request)
        {
            $dados = [
                'NOMEDOTITULAR'         => $request->NOMEDOTITULAR,
                'JURIDICO'              => $request->JURIDICO,
                'CIC'                   => $request->CIC,
                'CATEGORIADACOTA'       => $request->CATEGORIADACOTA,
                'SEXO'                  => $request->SEXO,
                'ESTADOCIVIL'           => $request->ESTADOCIVIL,
                'CEPRESIDENCIAL'        => $request->CEPRESIDENCIAL,
                'ENDRES_LOGRADOURO'     => $request->ENDRES_LOGRADOURO,
                'ENDRES_NUMERO'         => $request->ENDRES_NUMERO,
                'ENDRES_COMPLEMENTO'    => $request->ENDRES_COMPLEMENTO,
                'BAIRRORESIDENCIAL'     => $request->BAIRRORESIDENCIAL,
                'CIDADERESIDENCIAL'     => $request->CIDADERESIDENCIAL,
                'ESTADORESIDENCIAL'     => $request->ESTADORESIDENCIAL,
                'TELEFONER1'            => $request->TELEFONER1,
                'TELEFONER2'            => $request->TELEFONER2,
                'CAIXAPOSTAL'           => $request->CAIXAPOSTAL,
                'DATANASCIMENTO'        => $request->DATANASCIMENTO,
                'RG'                    => $request->RG
            ];

            try {
                $associado = $this->associados->save($dados);

                $this->setLog(
                    $associado->NUMERODACOTA,
                    $request->arquivo
                );

                // Envia os dados do novo associado por email
                if($associado->exists) {
                    $notyAdmin  = new Notifications\AffiliatedCreated($associado);
                    $notyUser   = new Notifications\AccountCreated($associado, $associado->CIC);

                    $this->sendNotification($notyAdmin);
                    $this->sendUserNotification($notyUser);
                }

                return redirect()->route('associados.exibir', [$associado->NUMERODACOTA, 'editar'])
                                 ->with('sucesso', 'Associado cadastrado com sucesso');

            } catch (ModelNotSavedException $e) {
                return redirect()->back()->withInput()->with('erro', $e->getMessage());
            }
        }
    }
