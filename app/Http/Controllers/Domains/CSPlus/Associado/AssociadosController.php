<?php

    namespace CSPlus\Http\Controllers\Domains\CSPlus\Associado;

    use Policy;

    use Illuminate\Http\Request;

    use CSPlus\Http\Controllers\Controller;
    use CSPlus\Models\CSPlus\Associado\Fotos;
    use CSPlus\Repositories\Associados\AssociadoRepository as Associados;

    /**
     * AssociadosController
     *
     * Realiza o gerenciamento dos associados
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 2.7.1
     * @since   06/03/2017
     */
    class AssociadosController extends Controller
    {
        /**
         * @var Associados
         */
        protected $associados;

        /**
         * @var bool
         */
        protected $cotaAlfa;

        public function __construct()
        {
            $this->middleware(function($request, $next) {
                $this->associados   = app()->make(Associados::class);
                $this->cotaAlfa     = (session('admin.personalizacao.controla_cota_alfa') == 'T') ? true : false;

                return $next($request);
            });
        }

        /**
         * Exibe o formulário para consulta de associados
         *
         * @param   \Illuminate\Http\Request
         * @return  \Illuminate\View\View
         */
        public function consulta(Request $request)
        {
            $termos     = $request->all();
            $associados = (!$termos) ? collect() : $this->search($termos);

            $data = [
                'controlaCotaAlfa'  => $this->cotaAlfa,
                'associados'        => $associados,
                'termos'            => $termos,
                'form'              => [
                    'route'     => 'associados.consulta',
                    'method'    => 'GET',
                    'class'     => 'smart-form',
                ]
            ];

            return view('Admin::associados.consulta', $data);
        }

        /**
         * Realiza a busca dos associados cadastrados
         *
         * @param   array  $termos
         * @return  mixed
         */
        public function search(array $termos)
        {
            if($this->cotaAlfa and $termos['tipo_busca'] == 'matricula') {
                return $this->associados->getAssociadoByTermos($termos);
            } else {

                $matricula = $matricula = (int) $termos['texto'];

                if(is_int($matricula) and $matricula > 0) {
                    return $this->associados->getByMatricula($matricula);
                } else {
                    return $this->associados->getAssociadosByNome($termos);
                }
            }
        }

        /**
         * Exibe os dados de um associado
         *
         * @param   int  $matricula
         * @param   bool  $action
         * @return  \Illuminate\Contracts\View\Factory
         *          \Illuminate\View\View
         */
        public function exibir($matricula, $action = false)
        {
            $associado = $this->associados->with(['dependentes'])->find($matricula);
            $foto      = (\Session::has('usuario.foto')) ? session('usuario.foto') : Fotos::getFoto($associado->NUMERODACOTA);

            $dados = [
                'associado' => $associado,
                'foto'      => $foto,
                'action'    => $action,
            ];

            return view('Socio::pessoais.index', $dados);
        }

        /**
         * Exibe o fomulário de criação de associados
         *
         * @return  \Illuminate\Contracts\View\Factory
         *          \Illuminate\View\View
         */
        public function novo()
        {
            return view('Admin::associados.novo');
        }
    }
