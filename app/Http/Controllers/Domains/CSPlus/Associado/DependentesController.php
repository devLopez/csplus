<?php

    namespace CSPlus\Http\Controllers\Domains\CSPlus\Associado;

    use Date;
    use Exception;
    use Policy;

    use CSPlus\Http\Controllers\Controller;
    use Illuminate\Http\Request;

    use CSPlus\Repositories\Associados\DependentesRepository as Dependentes;
    use CSPlus\Services\Log\LogDependente;
    use CSPlus\Services\Log\RegisterLog;

    /**
     * DependentesController
     *
     * Realiza o gerenciamento das operações envolvendo os dependentes
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 2.6.1
     * @since   29/06/2017
     */
    class DependentesController extends Controller
    {
        use RegisterLog;

        /**
         * @var \Illuminate\Http\Request
         */
        protected $dados_dependente;

        /**
         * @var \CSPlus\Repositories\Associados\DependentesRepository
         */
        protected $dependentes;

        public function __construct()
        {
            $this->middleware(function($request, $next) {
                $this->dependentes = app()->make(Dependentes::class);

                return $next($request);
            });
        }

        /**
         * Salva um novo dependente na base de dados
         *
         * @param   Request  $request
         * @param   int  $matricula
         * @return  string
         */
        public function novo(Request $request, $matricula)
        {
            $arquivo = $request->arquivo;

            try {
                if($this->dependentes->salvar($matricula, $request->all())) {
                    $this->setLog($matricula, $arquivo);

                    return response()->json(['sucesso' => true]);
                }
            } catch(Exception $e) {
                return response($e->getMessage(), 500);
            }
        }

        /**
         * Atualiza os dados de um dependente cadastrado
         *
         * @param   Request  $request
         * @param   int  $matricula
         * @param   int  $dependente
         * @return  \Illuminate\Http\JsonResponse
         */
        public function atualizar(Request $request, $matricula, $dependente)
        {
            $this->dados_dependente = $request;

            try {
                $dados_antigos  = $this->dependentes->getDependente($matricula, $dependente);
                $novos_dados    = $this->getDadosDependente();

                $log            = new LogDependente($matricula, $novos_dados, $dados_antigos->toArray());
                $dados_novos    = $log->getDadosModificados();

                $this->logNewData($log);

                if($log->hasAlteracao()) {
                    if($this->dependentes->atualizar($matricula, $dependente, $dados_novos)) {
                        $this->setLog($matricula, $request->arquivo);
                        return response()->json(['message' => 'Os dados do dependente foram atualizados']);
                    }

                    throw new Exception('Não foi possível alterar os dados do dependente. Tente novamente');
                }

                return response()->json(['message' => 'Não havia dados a serem atualizados']);
            } catch(Exception $e) {
                return response($e->getMessage(), 500);
            }
        }

        /**
         * Retorna os dados do dependente passados no PUT
         *
         * @return  array
         */
        private function getDadosDependente()
        {
            return [
                'COTADEPENDENTE'        => $this->dados_dependente->COTADEPENDENTE,
                'CODIGODEPENDENTE'      => $this->dados_dependente->CODIGODEPENDENTE,
                'NOMEDEPENDENTE'        => $this->dados_dependente->NOMEDEPENDENTE,
                'CIC'                   => $this->dados_dependente->CIC,
                'RG'                    => $this->dados_dependente->RG,
                'RG_DATA_EXPEDICAO'     => Date::toSql($this->dados_dependente->RG_DATA_EXPEDICAO),
                'RG_ORGAO_EXPEDIDOR'    => $this->dados_dependente->RG_ORGAO_EXPEDIDOR,
                'FUNCAO'                => $this->dados_dependente->FUNCAO,
                'PAI'                   => $this->dados_dependente->PAI,
                'MAE'                   => $this->dados_dependente->MAE,
                'TELEFONE'              => $this->dados_dependente->TELEFONE,
                'TELEFONE2'             => $this->dados_dependente->TELEFONE2,
                'CAIXAPOSTAL'           => $this->dados_dependente->CAIXAPOSTAL,
                'SEXO'                  => $this->dados_dependente->SEXO,
                'ESTADOCIVIL'           => $this->dados_dependente->ESTADOCIVIL,
                'DATANASCIMENTO'        => Date::toSql($this->dados_dependente->DATANASCIMENTO),
                'GRAUPARENTESCO'        => $this->dados_dependente->GRAUPARENTESCO
            ];
        }

        /**
         * Inativa um dependente cadastrado
         *
         * @param   Request $request
         * @param   int  $matricula
         * @param   int  $dependente
         * @return  \Illuminate\Http\Response
         */
        public function inativar(Request $request, $matricula, $dependente)
        {
            try {
                $arquivo    = $request->arquivo;

                // se salvar, cria o log
                if($this->dependentes->atualizar($matricula, $dependente, ['ATIVO' => $request->ativo])) {
                    $this->setLog($matricula, $arquivo);
                    return json_encode(true);
                }

            } catch (Exception $e) {
                return response($e->getMessage(), 500);
            }
        }
    }
