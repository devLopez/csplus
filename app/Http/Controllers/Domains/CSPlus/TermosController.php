<?php

    namespace CSPlus\Http\Controllers\Domains\CSPlus;

    use Date;
    use Exception;

    use CSPlus\Http\Controllers\Controller;
    use CSPlus\Repositories\Associados\AssociadoRepository as Associados;
    use CSPlus\Repositories\Associados\CategoriasRepository as Categorias;
    use CSPlus\Repositories\ContatosRepository as Contatos;
    use Illuminate\Http\Request;

    /**
     * TermosController
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 1.0.1
     * @since   03/06/2017
     */
    class TermosController extends Controller
    {
        /**
         * @var Associados
         */
        protected $associados;

        /**
         * @var Categorias
         */
        protected $categorias;

        /**
         * @var Contatos
         */
        protected $contatos;

        public function __construct()
        {
            $this->middleware(function($request, $next){
                $this->associados   = app()->make(Associados::class);
                $this->categorias   = app()->make(Categorias::class);
                $this->contatos     = app()->make(Contatos::class);

                return $next($request);
            });
        }

        /**
         * Exibe o texto da ficha de inscrição para o associado
         *
         * @param   int  $matricula
         * @return  \Illuminate\View\View
         */
        public function index($matricula)
        {
            $associado  = $this->associados->find($matricula, ['CATEGORIADACOTA']);
            $categoria  = $this->categorias->find($associado->CATEGORIADACOTA, ['TEXTO_FICHAINSCRICAO']);

            return view('Socio::termos.index', compact('categoria', 'matricula'));
        }

        /**
         * Realiza o aceite dos termos do contrato
         *
         * @param   Request  $request
         * @param   int  $matricula
         * @return  \Illuminate\Http\Response
         * @throws  Exception
         */
        public function aceiteTermos(Request $request, $matricula)
        {
            // Realiza a validação do formulário
            $this->validate($request, [
                'aceite' => 'accepted'
            ]);

            try {
                $data   = Date::today('Y.m.d');
                $hora   = Date::time('H:i:s');

                $aceite = [
                    'DATAHORA'  => $data.' '.$hora,
                    'DATA'      => $data,
                    'HORA'      => $hora,
                    'COTA'      => $matricula,
                    'SITUACAO'  => 'ACEITE CONTRATO PRINCIPAL'
                ];

                if($this->contatos->save($aceite)) {
                    return redirect()->route('painel.dashboard')->with('sucesso', 'Os termos de utilização foram aceitos');
                }

                throw new Exception('Ocorreu um erro ao salvar o seu aceite. Tente novamente');
            } catch(Exception $e) {
                return redirect()->back()->withInput()->with('erro', $e->getMessage());
            }
        }
    }
