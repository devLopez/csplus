<?php

    namespace CSPlus\Http\Controllers\Domains\CSPlus\Telefonia;

    use Date;

    use Carbon\Carbon;
    use CSPlus\Http\Controllers\Controller;
    use CSPlus\Presenters\TelefoniaPresenter;
    use CSPlus\Repositories\Associados\AssociadoRepository as Associados;
    use CSPlus\Repositories\TelefoniaRepository as Telefonia;
    use CSPlus\Services\Generators;
    use Illuminate\Http\Request;


    /**
     * TelefoniaController
     *
     * Realiza o gerenciamento das requisições relacionadas à telefonia
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 1.6.0
     * @since   15/06/2017
     */
    class TelefoniaController extends Controller
    {
        /**
         * @var Telefonia
         */
        protected $telefonia;

        /**
         * @var Associados
         */
        protected $associados;

        /**
         * @var Generators\CSV
         */
        protected $csv;

        /**
         * @var Generators\PDF
         */
        protected $pdf;

        protected $presenter;

        /**
         * @param   Generators\CSV  $csv
         * @param   Generators\PDF  $pdf
         */
        public function __construct(Generators\CSV $csv, Generators\PDF $pdf, TelefoniaPresenter $presenter)
        {
            $this->csv = $csv;
            $this->pdf = $pdf;

            $this->presenter = $presenter;

            $this->middleware(function($request, $next){
                $this->telefonia    = app()->make(Telefonia::class);
                $this->associados   = app()->make(Associados::class);

                return $next($request);
            });

            $this->pdf->setOptions([
                'margin-top' => 0,
                'margin-left' => 0,
                'margin-right' => 0
            ]);
        }

        /**
         * Exibe o formulário para busca dos dados telefônicos
         *
         * @param   $matricula
         * @return  \Illuminate\View\View
         */
        public function index($matricula)
        {
            $operadoras = $this->telefonia->getOperadoras($matricula);
            $select     = $operadoras->pluck('DESCRICAO', 'CODIGO')->toArray();
            $select[0]  = 'TODAS';

            $vencimento = $this->telefonia->getDataVencimento($matricula);
            $data_final = '';

            if($vencimento) {
                $data_final = Carbon::parse($vencimento)->subMonths(3);
            }

            $data = [
                'matricula'     => $matricula,
                'operadoras'    => $operadoras,
                'select'        => $select,
                'vencimento'    => $vencimento,
                'data_final'    => $data_final,
                'form'          => [
                    'route'     => ['telefonia.buscar', $matricula],
                    'class'     => 'smart-form',
                    'method'    => 'GET'
                ]
            ];

            return view('Socio::telefonia.index', $data);
        }

        /**
         * Realiza a busca das contas
         *
         * @param   Request  $request
         * @param   int  $matricula
         * @return  \Illuminate\View\View
         */
        public function buscar(Request $request, $matricula)
        {
            $contas = $this->telefonia->getContas(
                $matricula,
                $request->operadora,
                Date::toSql($request->data_inicio),
                Date::toSql($request->data_final)
            );

            return view('Socio::telefonia.buscar', compact('contas', 'matricula'));
        }

        /**
         * Realiza a busca do relatório detalhado
         *
         * @param   Request  $request
         * @param   int  $matricula
         * @return  \Illuminate\View\View| \Illuminate\Http\Response
         */
        public function detalhado(Request $request, $matricula)
        {
            $vencimento = $request->vencimento;
            $telefone   = $request->telefone;
            $download   = $request->download;
            $url        = $request->fullUrl();

            $relatorio  = $this->telefonia->relatorioDetalhado($matricula, $vencimento, $telefone);
            $associado  = $this->associados->find($matricula);

            if($download == 'csv') {
                return $this->detalhadoCsv($relatorio);
            }

            if($download == 'pdf') {
                $this->pdf->setOrientation('landscape');

                return $this->pdf->load(
                    'PDF::telefonia.detalhado', compact('relatorio', 'associado'),
                    false, 'telefonia_detalhado.pdf'
                );
            }

            return view('Socio::telefonia.detalhado', compact('relatorio', 'url'));
        }

        /**
         * Realiza a busca do relatório agrupado
         *
         * @param   Request  $request
         * @param   int  $matricula
         * @return  \Illuminate\View\View| \Illuminate\Http\Response
         */
        public function agrupado(Request $request, $matricula)
        {
            $vencimento = $request->vencimento;
            $telefone   = $request->telefone;
            $download   = $request->download;
            $url        = $request->fullUrl();

            $relatorio  = $this->telefonia->relatorioAgrupado($matricula, $vencimento, $telefone);
            $associado  = $this->associados->find($matricula);

            if($download == 'csv') {
                return $this->agrupadoCsv($relatorio);
            }

            if($download == 'pdf') {
                $this->pdf->setOrientation('landscape');

                return $this->pdf->load(
                    'PDF::telefonia.agrupado', compact('relatorio', 'associado'),
                    false, 'telefonia_agrupado.pdf'
                );
            }

            return view('Socio::telefonia.agrupado', compact('relatorio', 'url'));
        }

        /**
         * Realiza a criação do arquivo CSV
         *
         * @param   \Illuminate\Support\Collection  $data
         * @return  \Illuminate\Http\Response
         */
        private function agrupadoCsv($data)
        {
            $header     = ['Telefone', 'Data/Hora', 'Valor', 'Titular', 'Tipo de Ligação'];
            $presenter  = $this->presenter->relatorioAgrupado($data);

            return $this->csv->generate($presenter['data'], $header)->download('telefonia_agrupada.csv');
        }

        /**
         * Realiza a criação do CSV do relatório detalhado
         *
         * @param   \Illuminate\Support\Collection  $data
         * @return  \Illuminate\Http\Response
         */
        private function detalhadoCsv($data)
        {
            $header     = ['Hora', 'Duração', 'Histórico', 'Número', 'Valor', 'Tipo de Ligação', 'Tarifa'];
            $presenter  = $this->presenter->relatorioDetalhado($data);

            return $this->csv->generate($presenter['data'], $header)->download('telefonia_detalhada.csv');
        }
    }
