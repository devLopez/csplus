<?php

    namespace CSPlus\Http\Controllers\Domains\CSPlus;

    use Auth;
    use CSPlus\Http\Controllers\Controller;
    use CSPlus\Models\CSPlus\DashBoard;

    /**
     * PainelController
     *
     * Realiza oi gerenciamento do painel do usuário
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 2.1.1
     * @since   25/04/2017
     */
    class PainelController extends Controller
    {
        /**
         * @var \CSPlus\Models\CSPlus\DashBoard
         */
        protected $dash;

        public function __construct()
        {
            $this->middleware(function($request, $next) {
                $this->dash = app()->make(DashBoard::class);

                return $next($request);
            });
        }

        /**
         * Exibe o template principal do painel
         *
         * @return  \Illuminate\View\View
         */
        public function index()
        {
            return view('paginas.painel');
        }

        /**
         * Exibe o dashboard
         *
         * @return  \Illuminate\Contracts\View\Factory|\Illuminate\View\View
         */
        public function dashboard()
        {
            $matricula  = Auth::matricula();
            $dashboard  = $this->dash->getDashBoard($matricula)[0];

            return view('paginas.dashboard', compact('dashboard'));
        }
    }
