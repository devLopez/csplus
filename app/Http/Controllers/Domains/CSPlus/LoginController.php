<?php 

    namespace CSPlus\Http\Controllers\Domains\CSPlus;

    use AdminAuth;
    use AssociateAuth;
    use Client;

    use CSPlus\Http\Controllers\Controller;
    use CSPlus\Services\Client\AccessConfig;
    use CSPlus\Services\Auth\Exceptions\AccessDeniedException;
    use CSPlus\Services\Auth\Exceptions\AuthFailedException;
    use CSPlus\Services\Auth\Exceptions\InvalidAccessTypeException;
    use CSPlus\Services\Client\Exceptions\AccessNotFoundException;
    use CSPlus\Services\Log\RegisterLog;
    use Illuminate\Http\Request;

    /**
     * LoginController
     *
     * Realiza o gerenciamento das operações de login do sistema
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 2.3.0
     * @since   08/07/2017
     */
    class LoginController extends Controller
    {
        use RegisterLog;

        /**
         * @param   int|null  $clube
         * @param   int|null  $acesso
         * @return  \Illuminate\View\View
         */
        public function login($clube = null, $acesso = null)
        {
            $dados = ['erro' => null];

            try {
                Client::checkAccessType($acesso);

                AccessConfig::setEnv($clube, $acesso);

                Client::checkAccessPermission();

            } catch (InvalidAccessTypeException $e) {
                $dados['erro'] = $e->getMessage();
            } catch (AccessNotFoundException $e) {
                $dados['erro'] = $e->getMessage();
            } catch (AccessDeniedException $e) {
                $dados['erro'] = $e->getMessage();
            } catch (\Exception $e) {
                $this->logError($e);

                $dados['erro'] = 'Ocorreu um erro interno. Tente novamente';
            }

            return view('paginas.login', $dados);
        }

        /**
         * Realiza o login no sistema
         *
         * @param   Request  $request
         * @return  \Illuminate\Http\RedirectResponse
         */
        public function logar(Request $request)
        {
            try {
                $acesso = $request->acesso;

                if($acesso == 'associado') {
                    AssociateAuth::login($request->usuario, $request->password);
                    $route = 'painel.dashboard';
                } else {
                    AdminAuth::login($request->usuario, $request->password);
                    $route = 'associados.consulta';
                }

                return redirect()->route($route);

            } catch (AuthFailedException $e) {
                $retorno = $e->getMessage();
            } catch (Exception $e) {
                $this->logError($e);
                $retorno = 'Ocoreu um erro ao fazer o login, tente novamente';
            }

            return redirect()->back()->withInput()->with('error', $retorno);
        }

        /**
         * Realiza o logoff do sistema
         *
         * @return \Illuminate\Http\RedirectResponse
         */
        public function logout()
        {
            (Client::access() == 'associado') ? AssociateAuth::logout() : AdminAuth::logout();

            return redirect()->route('login.index', [Client::clube(), Client::access()]);
        }
    }
