<?php

namespace CSPlus\Http\Controllers\Domains\CSPlus\Debitos;

use CSPlus\Http\Controllers\Controller;
use CSPlus\Repositories\Financeiro\BoletosRepository as Boletos;
use CSPlus\Repositories\Financeiro\CobrancaRepository as Cobrancas;
use CSPlus\Repositories\Financeiro\DebitosRepository as Debitos;
use Exception;
use Illuminate\Http\Request;


/**
 * DebitosController
 *
 * Classe responsável por realizar as requisições relacionadas aos débitos
 * do associado
 *
 * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
 * @version 2.4.0
 * @since   02/08/2017
 */
class DebitosController extends Controller
{
    /**
     * @var Debitos
     */
    protected $debitos;

    /**
     *@var Boletos
     */
    protected $boletos;

    /**
     * @var Cobrancas
     */
    protected $cobrancas;

    public function __construct()
    {
        $this->middleware(function($request, $next){
            $this->debitos      = app()->make(Debitos::class);
            $this->boletos      = app()->make(Boletos::class);
            $this->cobrancas    = app()->make(Cobrancas::class);

            return $next($request);
        });
    }

    /**
     * Exibe o formulário de pesquisa
     *
     * @return  \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $pesquisa   = '';
        $debitos    = null;
        $cobranca   = null;
        $erro       = null;
        $matricula  = $request->matricula;

        try {
            if($request->has('data_pesquisa') || $request->has('data_vencimento')) {
                $pesquisa   = $request->pesquisar_por;
                $debitos    = $this->debitos->getDebitos($request);
                $cobranca   = $this->debitos->getCobranca();
            }

        } catch (Exception $e) {
            $erro = $e->getMessage();
        }

        return view('Socio::debitos.index', compact('pesquisa', 'debitos', 'matricula', 'cobranca', 'erro'));
    }

    /**
     * Função responsável por receber os dados das cobranças selecionadas para
     * iniciar as transações de pagamento de débitos
     *
     * @param   Request  $request
     * @return  \Illuminate\View\View|\Illuminate\Http\RedirectResponse
     * @throws  Exception
     */
    public function pagar(Request $request)
    {
        try {
            $codigo_debitos = $request->debitos;
            $matricula      = $request->matricula;
            $cobranca       = $request->cobranca;

            if(!$codigo_debitos) {
                throw new Exception("Para continuar é necessário selecionar pelo menos um débito");
            }

            if(!$cobranca) {
                throw new Exception('Nenhuma cobrança foi gerada. Realize a pesquisa novamente para gerar a cobrança');
            }

            $debitos    = $this->debitos->getItensForPayment($cobranca, $matricula, $codigo_debitos);
            $desconto   = $this->cobrancas->getDiscountValue($cobranca);

            return view('Socio::debitos.pagar', compact('debitos', 'matricula', 'desconto'));

        } catch (Exception $e) {
            return redirect()->back()->with('erro', $e->getMessage());
        }
    }
}
