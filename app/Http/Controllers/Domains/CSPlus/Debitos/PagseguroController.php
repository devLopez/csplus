<?php

namespace CSPlus\Http\Controllers\Domains\CSPlus\Debitos;

use Client;
use CSPlus\Http\Controllers\Controller;
use CSPlus\Repositories\Financeiro\BoletosRepository as Boletos;
use CSPlus\Services\Payment\Contracts\CreditCardContract;
use CSPlus\Services\Payment\Financeiro\DiscountManager;
use CSPlus\Services\Payment\Parsers\IntegrationErrors;
use CSPlus\Services\Payment\PayBill;
use CSPlus\Services\Payment\SendPayment;
use Exception;
use Illuminate\Http\Request;
use Payment;

/**
 * PagseguroController
 *
 * Classe responsável por gerenciar o pagamento de mensalidades pelo associado
 *
 * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
 * @version 2.2.0
 * @since   27/11/2017
 */
class PagseguroController extends Controller
{
    /**
     * @var Boletos
     */
    protected $boletos;

    /**
     * @var int
     */
    protected $cobranca;

    /**
     * @var int
     */
    protected $matricula;

    /**
     * @var \CSPlus\Services\Payment\Contracts\CreditCardContract
     */
    protected $creditCard;

    public function __construct()
    {
        $this->middleware(function($request, $next){
            $this->boletos      = app()->make(Boletos::class);
            $this->creditCard   = app()->make(CreditCardContract::class);

            return $next($request);
        });
    }

    /**
     * Realiza a busca dos errors gerados pela API do PAgSeguro
     *
     * @param   Request  $request
     * @return  \Illuminate\Http\JsonResponse
     * @throws  \Exception
     */
    public function getErrors(Request $request)
    {
        $pagseguro          = $request->errors;
        $errors['error']    = [];

        foreach ($pagseguro as $p) {
            $errors['error'][]['code'] = $p;
        }

        $pagseguroErrors = new IntegrationErrors($errors);

        return response()->json($pagseguroErrors->getErrors());
    }

    /**
     * Realiza o pagamento dos debitos
     *
     * @param   Request  $request
     * @param   int  $matricula
     * @return  \Illuminate\Http\JsonResponse
     */
    public function pagamento(Request $request, $matricula)
    {
        $pagamento          = (object) $request->except(['_token', 'cobranca']);
        $this->matricula    = $matricula;

        try {
            $this->generateCobranca($pagamento);

            if($request->tipo_pagamento == 'boleto') {
                return $this->boleto();
            } else {
                return $this->cartao($pagamento);
            }
        } catch (Exception $e) {
            return response()->json(['errors' => $e->getMessage()], 500);
        }
    }

    /**
     * Realiza a geração da cobrança
     *
     * @param   \stdClass  $pagamento
     */
    private function generateCobranca($pagamento)
    {
        $localCobranca = ($pagamento->tipo_pagamento == 'boleto') ? 237 : 700;

        $this->boletos
             ->setMatricula($this->matricula)
             ->setTaxas($pagamento->produtos)
             ->setLocalCobranca($localCobranca)
             ->runProcedures();

        $this->cobranca = $this->boletos->getCobranca();
    }

    /**
     * @param   \stdClass  $paymentData
     * @return  \Illuminate\Http\JsonResponse
     */
    private function cartao($paymentData)
    {
        $this->creditCard
             ->setMatricula($this->matricula)
             ->setCobranca($this->cobranca)
             ->setPaymentData($paymentData)
             ->generatePaymentRequest();

        $sendPayment   = new SendPayment($this->creditCard);
        $response      = $sendPayment->register();

        if(is_array($response)) {
            return response()->json($response, 422);
        }

        return response()->json(['url_pagamento' => false]);
    }

    /**
     * @return  \Illuminate\Http\JsonResponse
     */
    private function boleto()
    {
        $this->boletos->runProcedureBoleto();

        $url_boleto = "http://www.sistemasbrasil.com.br/abraphe/boleto/boleto.php?codigocobranca={$this->cobranca}&cliente={$this->matricula}";

        return response()->json(['url_pagamento' => $url_boleto]);
    }

    /**
     * Realiza a baixa da cobrança paga via pagseguro
     *
     * @param   Request $request
     * @param   PayBill  $payBill
     * @param   int  $clube
     * @return  \Illuminate\Http\Response
     */
    public function pagar(Request $request, PayBill $payBill, $clube)
    {
        try {
            $payBill->setClube($clube)
                    ->getTransaction($request->notificationCode);

            return response('Os dados da transação foram modificados');
        } catch (Exception $e) {
            return response($e->getMessage(), 500);
        }
    }

    /**
     * Realiza a geração das parcelas para pagamento
     *
     * @param   Request  $request
     * @return  array
     */
    public function getParcelas(Request $request)
    {
        return Payment::generateInstallments($request->valor, $request->bandeira, $request->cobrancas);
    }
}
