<?php

    namespace CSPlus\Http\Controllers\Domains\CSPlus\Bussines;

    use Illuminate\Http\Request;

    use CSPlus\Http\Requests;
    use CSPlus\Http\Controllers\Controller;

    /**
     * ClienteController
     *
     * Realiza o gerenciamento dos clientes da ClubSystem
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 1.0.0
     * @since   31/03/2017
     * @package CSPlus\Http\Controllers\Bussines
     */
    class ClientesController extends Controller
    {
        /**
         * Exibe o formulário de cadastro de novos clientes
         *
         * @return  \Illuminate\Contracts\View\Factory
         *          \Illuminate\View\View
         */
        public function novo()
        {
            return view('Bussiness::clientes.novo');
        }
    }
