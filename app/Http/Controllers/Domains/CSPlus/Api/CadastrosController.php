<?php

    namespace CSPlus\Http\Controllers\Domains\CSPlus\Api;

    use Exception;

    use Illuminate\Http\Request;
    use CSPlus\Http\Controllers\Controller;
    use CSPlus\Http\Requests;

    use CSPlus\Repositories\Associados\CategoriasRepository as Categorias;
    use CSPlus\Repositories\Associados\ParentescosRepository as Parentescos;

    /**
     * CadastrosController
     *
     * Classe desenvolvida para realizar o gerenciamento de cadastros que são
     * realizados via API
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 1.1.0
     * @since   23/06/2017
     */
    class CadastrosController extends Controller
    {
        /**
         * @param   Requests\CategoriasRequest  $request
         * @param   Categorias  $categorias
         * @return  \Illuminate\Http\JsonResponse
         */
        public function postCategoria(Requests\CategoriasRequest $request, Categorias $categorias)
        {
            try {
                $categoria = $categorias->save(
                    $request->except('_token')
                );

                if($categoria) {
                    return response()->json([
                        'id' => $categoria->CODIGO,
                        'descricao' => utf8_encode($categoria->DESCRICAO)
                    ]);
                }

            } catch(Exception $e) {
                return response($e->getMessage(), 500);
            }
        }

        /**
         * Realiza o cadastro de um novo parentesco
         *
         * @param   Requests\ParentescoRequest  $request
         * @param   Parentescos  $parentescos
         * @return  \Illuminate\Http\JsonResponse
         */
        public function postParentesco(Requests\ParentescoRequest $request, Parentescos $parentescos)
        {
            try {
                $parentesco = $parentescos->save(
                    $request->except('_token')
                );

                if($parentesco) {
                    return response()->json([
                        'id' => $parentesco->CODIGO,
                        'descricao' => utf8_encode($parentesco->DESCRICAO)
                    ]);
                }

            } catch (Exception $e) {
                return response($e->getMessage(), 500);
            }
        }
    }
