<?php

    namespace CSPlus\Http\Controllers\Domains\CSPlus\Api;

    use Exception;

    use Illuminate\Http\Request;
    use CSPlus\Criteria\Associados\GetByName;
    use CSPlus\Criteria\Colaboradores\GetCorretorByName;
    use CSPlus\Http\Controllers\Controller;
    use CSPlus\Repositories\Associados\AssociadoRepository as Associados;
    use CSPlus\Repositories\Associados\ColaboradoresRepository as Colaboradores;

    /**
     * AssociadosController
     *
     * Classe responsável pelo gerenciamento da api relativa aos usuários
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 1.0.1
     * @since   02/06/2017
     */
    class AssociadoController extends Controller
    {
        /**
         * @var \CSPlus\Repositories\Associados\AssociadoRepository
         */
        protected $associados;

        /**
         * @var \CSPlus\Repositories\Associados\ColaboradoresRepository
         */
        protected $colaboradores;

        public function __construct()
        {
            $this->middleware(function($request, $next) {
                $this->associados       = app()->make(Associados::class);
                $this->colaboradores    = app()->make(Colaboradores::class);

                return $next($request);
            });
        }

        /**
         * Realiza a busca do nome do associado pela matrícula
         *
         * @param   int|null  $matricula
         * @return  string
         */
        public function getAssociado($matricula = null)
        {
            if($matricula) {
                $associado = $this->associados->find($matricula);

                if($associado) {
                    return json_encode(['nome' => $associado->NOMEDOTITULAR], JSON_HEX_APOS);
                }

                return json_encode(false);
            }

            return json_encode(false);
        }

        /**
         * Verifica se existe associado com cpf e nome passados
         *
         * @param   Request  $request
         * @return  \Illuminate\Http\Response
         * @throws  Exception
         */
        public function verifyExistenceOfUser(Request $request)
        {
            try {

                $existe = $this->associados->verifyIfExists(
                    $request->nome,
                    $request->cpf
                );

                $existe = ($existe != 0) ? true : false;

                return response()->json(['existe' => $existe]);
            } catch(Exception $e) {
                return response($e->getMessage(), 500);
            }
        }

        /**
         * Realiza a busca de um colaborador pelo id
         *
         * @param   int|null $id
         * @return  string
         */
        public function getColaborador($id = null)
        {
            if($id) {
                $colaborador = $this->colaboradores->find($id);

                if($colaborador) {
                    return json_encode(['nome' => $colaborador->NOME], JSON_HEX_APOS);
                }

                return json_encode(false);
            }

            return json_encode(false);
        }

        /**
         * @param   Request  $request
         * @return  \Illuminate\Http\JsonResponse
         */
        public function getAssociadoByName(Request $request, $tipo)
        {
            $name = $request->nome;

            if($name) {
                if($tipo ==  'associado') {
                    $criteria   = new GetByName($name);
                    $associados = $this->associados->pushCriteria($criteria)->all(['NUMERODACOTA', 'NOMEDOTITULAR']);

                    $associados = $associados->map(function($a) {
                        return [
                            'cota' => $a->NUMERODACOTA,
                            'nome' => utf8_encode($a->NOMEDOTITULAR)
                        ];
                    });

                } else {
                    $criteria   = new GetCorretorByName($name);
                    $associados = $this->colaboradores->pushCriteria($criteria)->all(['CODIGO', 'NOME']);

                    $associados = $associados->map(function($a) {
                        return [
                            'cota' => $a->CODIGO,
                            'nome' => utf8_encode($a->NOME)
                        ];
                    });
                }

                if($associados->isNotEmpty()) {
                    return response()->json(['associados' => $associados]);
                }

                return response()->json(['associados' => false]);
            }

            return response()->json(['associados' => false]);
        }
    }
