<?php

    namespace CSPlus\Http\Controllers\Domains\CSPlus\Api;

    use Client;
    use Exception;
    use ZipCode;

    use CSPlus\Http\Controllers\Controller;

    use CSPlus\Models\CSPlus\Associado\Fotos;
    use CSPlus\Exceptions\FileUpload\PictureUploadException;

    use Illuminate\Http\Request;

    /**
     * ApiController
     *
     * Classe responsável pela api pública do sistema
     *
     * @author  Matheus Lopes Santos
     * @version 2.2.2
     * @since   11/05/2017
     */
    class ApiController extends Controller
    {
        /**
         * Realiza a busca de um endereço baseado no cep fornecido
         *
         * @param   string  $cep
         * @return  string
         */
        public function cep($cep)
        {
            $cep = str_replace(['.', '/'], '', $cep);

            try {
                $endereco = ZipCode::find($cep);

                if($endereco instanceof \Canducci\ZipCode\ZipCodeInfo) {
                    return $endereco->getJson();
                }

                throw new Exception('Nenhum endereço foi encontrado para o cep');
            } catch (Exception $e) {
                return response($e->getMessage(), 500);
            }
        }

        /**
         * Realiza o upload de fotos para inserção no cadastro dos associados e
         * dependentes
         *
         * @param   Request  $request
         * @return  string
         */
        public function uploadFoto(Request $request)
        {
            try {
                $matricula      = $request->matricula;
                $tipo           = $request->tipo;
                $foto           = $request->file('file');
                $clube          = Client::clube();
                $upload_path    = public_path('assets/img/fotos_associados');

                // Verifica erros no upload
                if($foto->isValid()) {

                    // Verifica o mime type e o tamanho do arquivos
                    if($foto->getClientMimeType() == 'image/jpeg' && $foto->getClientSize() < 500000) {

                        // Realiza o upload
                        $novo_nome = $clube.'_'.$matricula.'_'.$tipo.'.jpg';
                        $foto->move($upload_path, $novo_nome);

                        $arquivo    = $upload_path.'/'.$novo_nome;
                        $dados_foto = file_get_contents($arquivo);

                        // Atualiza a foto no banco de dados
                        $result = Fotos::addFoto($matricula, $tipo, $dados_foto);
                        unlink($arquivo);

                        if($result) {
                            // Caso seja um associado, seta a nova foto na seção
                            if($tipo == 0 and Client::access() == 'associado') {
                                session(['usuario.foto' => base64_encode($dados_foto)]);
                            }

                            return response()->json(['success' => true]);
                        }

                        throw new PictureUploadException('Não foi possível atualizar a sua fotografia. Tente Novamente');
                    }

                    throw new PictureUploadException('A fotografia enviada não é uma imagem');
                }

                throw new PictureUploadException('A fotografia enviada é inválida');
            } catch (PictureUploadException $e) {
                return response()->json(['error' => $e->getMessage()]);
            }
        }
    }
