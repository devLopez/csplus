<?php

    namespace CSPlus\Http\Controllers\Domains\CSPlus\Api;

    use Date;
    use Exception;

    use CSPlus\Http\Controllers\Controller;
    use CSPlus\Repositories\Associados\SenhaRepository as Senhas;
    use CSPlus\Services\Auth\Auth;
    use CSPlus\Services\Auth\PasswordLog as Log;
    use Illuminate\Http\Request;

    /**
     * UsersController
     *
     * Classe desenvolvida para realizar o gerenciamento de requisições REST
     * relacionadas aos usuários do sistema
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 1.0.1
     * @since   02/06/2017
     */
    class UsersController extends Controller
    {
        /**
         * @var \CSPlus\Services\Auth\Auth
         */
        protected $auth;

        /**
         * @var \CSPlus\Services\Auth\PasswordLog
         */
        protected $log;

        /**
         * @var \CSPlus\Repositories\Associados\SenhaRepository
         */
        protected $senhas;

        public function __construct()
        {
            $this->middleware(function($request, $next) {
                $this->auth     = app()->make(Auth::class);
                $this->log      = app()->make(Log::class);
                $this->senhas   = app()->make(Senhas::class);

                return $next($request);
            });
        }

        /**
         * Realiza a troca de senha do usuário administrativo logado
         *
         * @param   Request  $request
         * @return  \Illuminate\Http\RedirectResponse
         */
        public function adminPassword(Request $request)
        {
            $user = $this->auth->user();

            $this->validate($request, [
                'SENHA' => 'required|confirmed',
                'SENHA_confirmation' => 'required'
            ]);

            try {
                $user->SENHA = $request->SENHA;

                if($user->save()) {
                    return redirect()->back()->with('sucesso', 'A senha do usuario foi atualizada');
                }

                throw new Exception('Ocorreu um erro ao atualizar os dados. Tente novamente');
            } catch(Exception $e) {
                return redirect()->back()->with('erro', $e->getMessage());
            }
        }

        /**
         * Altera a senha do associado logado
         *
         * @param   Request  $request
         * @return  \Illuminate\Http\RedirectResponse
         * @throws  Exception
         */
        public function userPassword(Request $request)
        {
            $this->validate($request, [
                'SENHA' => 'required|confirmed',
                'SENHA_confirmation' => 'required'
            ]);

            $user = $this->auth->user();
            $pass = $request->SENHA;

            try {
                if($user->senha == null) {
                    $this->createPasswordForUser($user, $pass);
                } else {
                    $user->senha->SENHA         = $pass;
                    $user->senha->ATUALIZACAO   = date('d.m.Y', time());

                    if(!$user->senha->save()) {
                        throw new Exception('Não foi possível alterar a senha do usuário. Tente novamente');
                    }
                }

                $this->log->save();

                return redirect()->back()->with('sucesso', 'A senha do usuário foi alterada');

            } catch (Exception $e) {
                return redirect()->back()->with('erro', $e->getMessage());
            }
        }

        /**
         * Cria uma nova senha para o associado logados
         *
         * @param   \CSPlus\Models\CSPlus\Associado\Ass001  $user
         * @param   string  $pass
         * @return  void
         * @throws  Exception
         */
        private function createPasswordForUser($user, $pass)
        {
            $pass = [
                'COTA'          => $user->NUMERODACOTA,
                'SENHA'         => $pass,
                'ATUALIZACAO'   => date('d.m.Y', time())
            ];

            if(!$this->senhas->save($pass)) {
                throw new Exception('Não foi possível criar a senha do usuário. Tente Novamente');
            }
        }
    }
