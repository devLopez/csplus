<?php

    namespace CSPlus\Http\Controllers\Domains\CSPlus\Pagseguro;

    use Exception;
    use Client;
    use CSPlus\Http\Controllers\Controller;
    use CSPlus\Services\Payment\PayBill;
    use CSPlus\Services\Payment\Transaction;
    use Illuminate\Http\Request;


    /**
     * TransacoesController
     *
     * Classe desenvolvida para o gerenciamento das transações envolvendo o
     * Pagseguro
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 1.2.0
     * @since   12/05/2017
     */
    class TransacoesController extends Controller
    {
        /**
         * Exibe a página de busca ao usuário
         *
         * @return  \Illuminate\View\View
         */
        public function index()
        {
            return view('Admin::pagseguro.transacoes.index');
        }

        /**
         * Realiza a consulta de transações por um período informado
         *
         * @param   \Illuminate\Http\Request  $request
         * @param   \CSPlus\Services\Payment\Transaction  $transaction
         * @return  array
         */
        public function consultar(Request $request, Transaction $transaction)
        {
            try {
                $transactions = $transaction->setDates(
                    $request->data_inicio,
                    $request->data_final
                )->checkForTransactions();

                return $transactions;

            } catch(Exception $e) {
                return response($e->getMessage(), 500);
            }
        }

        /**
         * Recebe o código de notificação e executa a baixa da mensalidade
         * selecionada
         *
         * @param   Request  $request
         * @param   PayBill  $payBill
         * @return  \Illuminate\Http\Response
         */
        public function getPayment(Request $request, PayBill $payBill, Transaction $transactions)
        {
            $clube = Client::clube();

            try {
                $transacao = $transactions->getTransaction($request->notificationCode);

                $payBill->setClube($clube)->pay($transacao);
                return response()->json(['atualizado' => true]);
            } catch (Exception $e) {
                return response($e->getMessage(), 500);
            }
        }
    }
