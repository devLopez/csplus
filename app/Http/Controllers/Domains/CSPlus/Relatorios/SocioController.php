<?php

    namespace CSPlus\Http\Controllers\Domains\CSPlus\Relatorios;

    use Exception;

    use CSPlus\Http\Controllers\Controller;
    use Illuminate\Http\Request;

    use CSPlus\Repositories\Financeiro\BancosRepository as Bancos;
    use CSPlus\Repositories\Associados\CategoriasRepository as Categorias;
    use CSPlus\Services\Generators\PDF;
    use CSPlus\Services\Reports\Partners;


    /**
     * SocioController
     *
     * Realiza o gerenciamento das requisições relacionadas ao relatório de
     * associados
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 1.5.0
     * @since   17/06/2017
     */
    class SocioController extends Controller
    {
        /**
         * @var \CSPlus\Repositories\Financeiro\BancosRepository
         */
        protected $bancos;

        /**
         * @var \CSPlus\Repositories\Associados\CategoriasRepository
         */
        protected $categorias;

        /**
         * @var PDF
         */
        protected $pdf;

        /**
         * @param   PDF  $pdf
         */
        public function __construct(PDF $pdf)
        {
            $this->pdf = $pdf;

            $this->pdf->setOptions(['margin-top' => 0, 'margin-left' => 0, 'margin-right' => 0]);

            $this->middleware(function($request, $next){
                $this->bancos       = app()->make(Bancos::class);
                $this->categorias   = app()->make(Categorias::class);

                return $next($request);
            });
        }

        /**
         * Exibe a tela de consulta
         *
         * @return  \Illuminate\View\View
         */
        public function index()
        {
            $data = [
                'tipo'          => config('reports.associados.tipo'),
                'situacao'      => config('reports.associados.situacao'),
                'aposentados'   => config('reports.associados.aposentado'),
                'desconto'      => config('reports.associados.desconto'),
                'banco'         => $this->bancos->listBancos(),
                'categorias'    => $this->categorias->listCategorias()
            ];

            return view('Relatorio::associados.index', $data);
        }

        /**
         * Realiza a geração do relatório
         *
         * @param   Request   $request
         * @param   Partners  $partners
         * @return  \Illuminate\Http\Response
         * @throws  Exception
         */
        public function gerar(Request $request, Partners $partners)
        {
            $report_items = $request->all();

            $associados = $partners->setReportItems($report_items)->generate();
            $tipo       = $request->tipo;
            $relatorio  = config('reports.associados.tipo.' . $tipo);
            $orientacao = config('reports.associados.orientacao.' . $tipo);

            if($tipo == 'REL_ASS_GERAL') {
                throw new Exception('Este relatório ainda não foi implementado');
            }

            $this->pdf->setOrientation($orientacao);

            return $this->pdf->load(
                'PDF::relatorios.associados.'.$tipo,
                compact('associados', 'relatorio'),
                false,
                str_slug($relatorio, '_').'.pdf'
            );
        }
    }
