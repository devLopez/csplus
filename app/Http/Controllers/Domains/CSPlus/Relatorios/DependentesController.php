<?php

    namespace CSPlus\Http\Controllers\Domains\CSPlus\Relatorios;

    use SnappyPDF;

    use CSPlus\Http\Controllers\Controller;
    use CSPlus\Services\Reports\Dependentes;
    use CSPlus\Services\Generators\PDF;
    use Illuminate\Http\Request;


    /**
     * DependentesController
     *
     * Realiza o gerenciamento das requisições de relatórios
     * dos dependentes
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 2.0.0
     * @since   07/03/2017
     * @package CSPlus\Http\Controllers\Relatorios
     */
    class DependentesController extends Controller
    {
        /**
         * @var PDF
         */
        protected $pdf;

        /**
         * @param   PDF  $pdf
         */
        public function __construct(PDF $pdf)
        {
            $this->pdf = $pdf;

            $this->pdf->setOptions([
                'margin-top'    => 0,
                'margin-left'   => 0,
                'margin-right'  => 0
            ]);
        }

        /**
         * Exibe o formulário para geração do relatório
         *
         * @return  \Illuminate\View\View
         */
        public function index()
        {
            return view('Relatorio::dependentes.index');
        }

        /**
         * Realiza a geração do PDF
         *
         * @param   Request  $request
         * @param   Dependentes  $dependentes
         * @return  \Illuminate\Http\Response
         */
        public function gerar(Request $request, Dependentes $dependentes)
        {
            $relatorio = $dependentes->setOptions($request->all())->generate();

            $this->pdf->setOrientation('landscape');

            return $this->pdf->load(
                'PDF::relatorios.dependentes.dependentes',
                compact('relatorio'),
                false,
                'relatorio_dependentes.pdf'
            );
        }
    }
