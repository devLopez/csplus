<?php

    namespace CSPlus\Http\Controllers\Domains\CSPlus\Convites;

    use Policy;

    use CSPlus\Http\Controllers\Controller;
    use CSPlus\Http\Requests;
    use CSPlus\Repositories\Convites\ConvitesRepository as Convites;
    use CSPlus\Repositories\Convites\ConvidadosRepository as Convidados;

    /**
     * ConvidadosController
     *
     * Classe responsável pela gerência dos convidados do associado
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 1.0.1
     * @since   03/06/2017
     */
    class ConvidadosController extends Controller
    {
        /**
         * @var \CSPlus\Repositories\Convites\ConvitesRepository
         */
        protected $convidados;

        /**
         * @var \CSPlus\Repositories\Convites\ConvidadosRepository
         */
        protected $convites;

        public function __construct()
        {
            $this->middleware(function($request, $next){
                $this->convites     = app()->make(Convites::class);
                $this->convidados   = app()->make(Convidados::class);

                return $next($request);
            });
        }

        /**
         * Exibe a página de cadastro de convidados
         *
         * @param   int  $matricula
         * @param   int  $convite
         * @return  \Illuminate\View\View
         */
        public function index($matricula, $convite = null)
        {
            $convidado = null;

            if($convite) {
                $convite    = $this->convites->find($convite);
                $convidado  = $convite->convidado;
            }

            $data = [
                'convidado' => $convidado,
                'matricula' => $matricula
            ];

            return view('Socio::convidado.novo', compact('convidado', 'matricula'));
        }

        /**
         * Salva um novo convidado na base de dados,
         *
         * @param   Requests\ConvidadoRequest $request
         * @param   int  $matricula
         * @return  \Illuminate\Http\RedirectResponse
         */
        public function salvar(Requests\ConvidadoRequest $request, $matricula)
        {
            try {
                $convidado = $this->convidados->createConvidado($request->all());

                session([
                    'convite.convidado'     => $convidado,
                    'convite.referencia'    => $request->REFERENCIA
                ]);

                return redirect()->route('convite.novo', [$matricula])->with('sucesso', 'O convidado foi cadastrado com sucesso');

            } catch(\Exception $e) {
                return redirect()->back()->withInput()->with('erro', $e->getMessage());
            }
        }
    }
