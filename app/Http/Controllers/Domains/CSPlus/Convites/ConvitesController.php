<?php

    namespace CSPlus\Http\Controllers\Domains\CSPlus\Convites;

    use Date;
    use Exception;
    use Policy;

    use Illuminate\Http\Request;

    use CSPlus\Http\Controllers\Controller;

    use CSPlus\Models\CSPlus\Convites\DadosConvite;
    use CSPlus\Repositories\Convites\ConvitesRepository as Convites;
    use CSPlus\Repositories\ContatosRepository as Contatos;
    use CSPlus\Services\Generators\PDF;


    /**
     * ConvitesController
     *
     * Gerencia as requisições relacionadas aos convites
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 2.0.2
     * @since   15/08/2017
     */
    class ConvitesController extends Controller
    {
        /**
         * @var Convites
         */
        protected $convites;

        /**
         * @var PDF
         */
        protected $pdf;

        /**
         * @param   PDF  $pdf
         */
        public function __construct(PDF $pdf)
        {
            $this->pdf = $pdf;

            $this->middleware(function($request, $next){
                $this->convites = app()->make(Convites::class);

                return $next($request);
            });
        }

        /**
         * Exibe a view inicial para o usuário
         *
         * @return  \Illuminate\View\View
         */
        public function index($matricula)
        {
            $convites = $this->convites->getConvites($matricula, Policy::isAdmin());

            $data = [
                'convites'  => $convites,
                'matricula' => $matricula
            ];

            return view('Socio::convites.index', $data);
        }

        /**
         * Realiza a geração do convite
         *
         * @param   Request $request
         * @return  \Illuminate\Http\Response
         */
        public function gerar(Request $request)
        {
            try {
                $matricula      = $request->associado;
                $id_convites    = $request->convites;

                if(!$id_convites) {
                    throw new Exception('Para geração dos convites, é necessário selecionar pelo menos um convite');
                }

                $convites = $this->convites->printInvites($matricula, $id_convites);

                if($convites->isEmpty()) {
                    throw new Exception('Não foram encontrados dados para impressão');
                }

                // Marca os convites como impressos
                $this->convites->disableInvites($matricula, $id_convites);

                return $this->pdf->load("PDF::convites.print", compact('convites'), false, 'convites.pdf');
            } catch(\Exception $e) {
                $message = $e->getMessage();
                return view('errors.not-found', compact('message'));
            }
        }

        /**
         * Abre o formulário para criação de novo convite
         *
         * @param   int  $matricula
         * @return  \Illuminate\View\View
         */
        public function novo($matricula)
        {
            // Recebe os dados referentes aos convites e convidados
            $convidado  = session('convite.convidado');
            $referencia = Date::toSql('01/'.session('convite.referencia'));
            $convite    = DadosConvite::carregaDadosConvite($matricula, $referencia, $convidado);

            $data = [
                'matricula'     => $matricula,
                'convidado'     => $convidado,
                'referencia'    => $referencia,
                'convite'       => $convite,
                'valores'       => [
                    3   => 'Terça-feira  - R$ 30,00',
                    4   => 'Quarta-feira - R$ 15,00',
                    5   => 'Quinta-feira - R$ 30,00',
                    6   => 'Sexta-feira  - R$ 15,00',
                    20  => 'Sab/dom/feriados - R$ 50,00'
                ]
            ];

            return view('Socio::convites.confirmar', $data);
        }

        /**
         * Exibe a tela de aceite para o usuário
         *
         * @param   int  $matricula
         * @param   int|null  $convite
         * @return  \Illuminate\Http\RedirectResponse
         */
        public function aceite($matricula, $convite = null)
        {
            if($this->convites->getAceite($matricula) == null and !Policy::isAdmin()) {
                return view('Socio::convites.termo-aceite', compact('matricula', 'convite'));
            }

            return redirect()->route('convites.index');
        }

        /**
         * Salva o Aceite no banco de dados
         *
         * @param   int  $matricula
         * @param   int  $convite
         * @return  \Illuminate\Http\RedirectResponse
         */
        public function aceitarTermos(Contatos $contato, $matricula, $convite = null)
        {
            try {
                $data   = date('Y.m.d');
                $hora   = date('H:i:s');

                $aceite = [
                    'DATAHORA'  => $data.' '.$hora,
                    'DATA'      => $data,
                    'HORA'      => $hora,
                    'COTA'      => $matricula,
                    'SITUACAO'  => 'ACEITE CONTRATO WEB'
                ];

                if($contato->save($aceite)) {
                    return redirect()->route('convidado.index', [$matricula, $convite]);
                }
            } catch(\Exception $e) {
                return redirect()->back()->withInput()->with('erro', $e->getMessage());
            }
        }

        /**
         * Salva um novo convite na base de dados
         *
         * @param   Request  $request
         * @param   int  $matricula
         * @return  \Illuminate\Http\RedirectResponse
         */
        public function salvar(Request $request, $matricula)
        {
            try {
                if($this->convites->generateInvite($request, $matricula)) {
                    return redirect()->route('convites.index', $matricula)->with('sucesso', 'O Convite foi gerado');
                } else {
                    return redirect()->route('convites.index', $matricula)->with('erro', 'Não foi possível gerar este convite. Tente novamente');
                }
            } catch (\Exception $e) {
                return redirect()->back()->withInput()->with('erro', $e->getMessage());
            }
        }
    }
