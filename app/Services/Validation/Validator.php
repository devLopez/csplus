<?php

    namespace CSPlus\Services\Validation;

    use Masterkey\ValidDocs\ValidDocs;

    /**
     * Validation
     *
     * Classe de coleta métodos para validação de dados diversos
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 2.0.0
     * @since   17/01/2017
     */
    class Validator
    {
        /**
         * Realiza a validação de CPF
         *
         * @param   string  $attribute
         * @param   string  $value
         * @return  bool
         */
        public function cpfCnpj($attribute, $value, $params, $validator)
        {
            $dados = $validator->getData();

            if(isset($dados['PJ'])) {
                if($dados['PJ'] == 'T') {
                    return ValidDocs::validaCNPJ($value);
                }
            }

            return ValidDocs::validaCPF($value);
        }
    }
