<?php

    namespace CSPlus\Services\Reports;

    use DB;
    use Date;
    use CSPlus\Models\CSPlus\Associado\Dep001 as Dependente;

    /**
     * Dependentes
     *
     * Realiza a geração do relatório de dependentes
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 2.0.0
     * @since   17/01/2017
     */
    class Dependentes
    {
        /**
         * Recebe as opções para geração do relatório
         *
         * @var array
         */
        protected $options;

        /**
         * Recebe a instância do model dependente
         *
         * @var Dependente
         */
        protected $dependentes;

        /**
         * Construtor da classe
         *
         * @param   Dependente  $dependentes
         */
        public function __construct(Dependente $dependentes)
        {
            $this->dependentes = $dependentes;
        }

        /**
         * Seta as opções de relatório
         *
         * @param   array  $options
         * @return  $this
         */
        public function setOptions($options)
        {
            $this->options = $options;

            return $this;
        }

        /**
         * Realiza a geração do relatório
         *
         * @return  \Illuminate\Database\Eloquent\Collection
         */
        public function generate()
        {
            $query = DB::connection('csplus')->table(DB::raw('(select
                "COTADEPENDENTE", "CODIGODEPENDENTE", "NOMEDEPENDENTE", "GRAUPARENTESCO",
                "DATANASCIMENTO", "ATIVO", "RG", "CIC", "ESTADOCIVIL", "SEXO", "PARENTE"."DESCRICAO",
                (select IDADE from PROC_CALCULA_IDADE(DATANASCIMENTO)) as IDADE from DEP001
                left join "PARENTE" on "PARENTE"."CODIGO" = "DEP001"."GRAUPARENTESCO"
                )')
            );

            if($this->options['idade']) {
                $query->where('IDADE', $this->options['idade']);
            }

            // Realiza a busca pelo parentesco
            if($this->options['parentesco']) {
                $query->where('GRAUPARENTESCO', $this->options['parentesco']);
            }

            // Busca pelo sexo
            if($this->options['sexo']) {
                $query->where('SEXO', '=', $this->options['sexo']);
            }

            // Busca pelo estado civil
            if($this->options['estado_civil']) {
                $query->where('ESTADOCIVIL', '=', $this->options['estado_civil']);
            }

            // busca pela data de nascimento
            if($this->options['nascimento_inicio'] && $this->options['nascimento_final']) {
                $inicio = Date::toSql($this->options['nascimento_inicio']);
                $final  = Date::toSql($this->options['nascimento_final']);

                $query->whereBetween('DATANASCIMENTO', [$inicio, $final]);
            }

            return collect($query->get());
        }
    }
