<?php

    namespace CSPlus\Services\Reports;

    use Date;
    use CSPlus\Models\CSPlus\Associado\Ass001 as Socio;

    /**
     * Partners
     *
     * Realiza a geração do relatório de associados, de acordo o pedido solicitado
     * pelo administrador
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 2.0.0
     * @since   17/01/2017
     */
    class Partners
    {
        /**
         * @var array
         */
        private $REL_ASS_CAT = ['NUMERODACOTA', 'NOMEDOTITULAR', 'CAT001.DESCRICAO'];

        /**
         * Define os campos para o relatório de Dados Profissionais
         *
         * @var array
         */
        private $REL_ASS_PROF = [
            'NUMERODACOTA', 'NOMEDOTITULAR', 'EMPRESA', 'FUNCAO', 'DEPARTAMENTO_EMPRESA',
            'ADMISSAO_EMPRESA', 'AFASTAMENTO_EMPRESA', 'MATRICULA_EMPRESA', 'REGISTRO_CLASSE',
            'TELEFONEC1', 'TELEFONEC2'
        ];

        /**
         * Define os campos para o relatório de Endereço Residencial
         *
         * @var array
         */
        private $REL_ASS_END = [
            'NUMERODACOTA', 'NOMEDOTITULAR', 'ENDERECORESIDENCIAL', 'BAIRRORESIDENCIAL', 'CIDADERESIDENCIAL',
            'ESTADORESIDENCIAL', 'CEPRESIDENCIAL', 'DDDRESIDENCIAL1','TELEFONER1', 'DDDRESIDENCIAL2','TELEFONER2'
        ];

        /**
         * Define os campos para o relatório de Endereço Comercial
         *
         * @var array
         */
        private $REL_ASS_COM = [
            'NUMERODACOTA', 'NOMEDOTITULAR', 'ENDERECOCOMERCIAL', 'BAIRROCOMERCIAL', 'CIDADECOMERCIAL',
            'ESTADOCOMERCIAL', 'CEPCOMERCIAL', 'DDDCOMERCIAL1','TELEFONEC1', 'DDDCOMERCIAL2','TELEFONEC2'
        ];

        /**
         * Define os campos para o relatório de Telefones
         *
         * @var array
         */
        private $REL_ASS_TEL = [
            'NUMERODACOTA', 'NOMEDOTITULAR', 'DDDRESIDENCIAL1','TELEFONER1', 'DDDRESIDENCIAL2','TELEFONER2',
            'DDDCOMERCIAL1','TELEFONEC1', 'DDDCOMERCIAL2','TELEFONEC2'
        ];

        /**
         * Define os campos para o relatório de E-mails
         *
         * @var array
         */
        private $REL_ASS_EMAIL = [
            'NUMERODACOTA', 'NOMEDOTITULAR', 'CAIXAPOSTAL', 'CAIXAPOSTAL_EMPRESA'
        ];

        /**
         * Define os campos para o relatório de Data de Nascimento
         *
         * @var array
         */
        private $REL_ASS_NASC = [
            'NUMERODACOTA', 'NOMEDOTITULAR', 'DATANASCIMENTO'
        ];

        /**
         * Define os campos para o relatório de Documentos
         *
         * @var array
         */
        private $REL_ASS_DOC = [
            'NUMERODACOTA', 'NOMEDOTITULAR', 'DATANASCIMENTO', 'DATAAQUISICAO', 'ULTIMACOBRANCA',
            'BANCO', 'AGENCIA', 'CONTACORRENTE', 'DATAAFASTAMENTO', 'NACIONALIDADE', 'NATURALIDADE',
            'SEXO', 'ESTADOCIVIL', 'CIC', 'RG'
        ];

        /**
         * Define os campos para o relatório de Todos os Associados
         *
         * @var array
         */
        private $REL_ASS_GERAL = ['*'];

        /**
         * Recebe a instância do Model para geração do query builder
         *
         * @var Socio
         */
        protected $partner;

        /**
         * Recebe os dados do pedido de relatório
         *
         * @var array
         */
        protected $report;

        /**
         * Construtor da classe
         *
         * @param   Socio  $socio
         */
        public function __construct(Socio $socio)
        {
            $this->partner = $socio;
        }

        /**
         * Recebe os parâmetros para geração do relatorio
         *
         * @param   array  $items
         * @return  $this
         */
        public function setReportItems($items)
        {
            $this->report = $items;

            return $this;
        }

        /**
         * Realiza a geração do relatório
         *
         * @return  \Illuminate\Support\Collection
         */
        public function generate()
        {
            $rel = $this->partner->select($this->{$this->report['tipo']});


            if($this->report['tipo'] == 'REL_ASS_CAT') {
                $rel->leftJoin('CAT001', 'ASS001.CATEGORIADACOTA', '=', 'CAT001.CODIGO');
            }

            // Verifica a situação do associado
            if(isset($this->report['situacao'])) {
                $rel->where('ATIVO', '=', $this->report['situacao']);
            }

            // Adiciona o intervalo de vatrícula à query
            if($this->report['matricula_inicio'] && $this->report['matricula_termino']) {
                $rel->whereBetween('NUMERODACOTA', [
                    $this->report['matricula_inicio'], $this->report['matricula_termino']
                ]);
            }

            // Adiciona os aposentados e desconto em folha à query
            if(isset($this->report['aposentados'])) {
                $rel->where('APOSENTADO', '=', 'T');
            }

            if(isset($this->report['desconto_folha'])) {
                $rel->where('DESCONTOFOLHA', '=', 'T');
            }

            // Adiciona o banco à query
            if(isset($this->report['banco'])) {
                $rel->where('BANCO', '=', $this->report['banco']);
            }

            // Adiciona a categoria à query
            if(isset($this->report['categoria'])) {
                $rel->where('CATEGORIADACOTA', '=', $this->report['categoria']);
            }

            // Adiciona a data de aquisição à query
            if($this->report['aquisicao_inicio'] && $this->report['aquisicao_final']) {
                $rel->whereBetween('DATAAQUISICAO', [
                    Date::toSql($this->report['aquisicao_inicio']),
                    Date::toSql($this->report['aquisicao_final'])
                ]);
            }

            // Adiciona a data de admissão à query
            if($this->report['admissao_inicio'] && $this->report['admissao_final']) {
                $rel->whereBetween('DATACADASTRO', [
                    Date::toSql($this->report['admissao_inicio']),
                    Date::toSql($this->report['admissao_final'])
                ]);
            }

            // Adiciona a data de afastamento à query
            if($this->report['afastamento_inicio'] && $this->report['afastamento_final']) {
                $rel->whereBetween('DATAAFASTAMENTO', [
                    Date::toSql($this->report['afastamento_inicio']),
                    Date::toSql($this->report['afastamento_final'])
                ]);
            }

            return $rel->orderBy('NOMEDOTITULAR')->get();
        }
    }
