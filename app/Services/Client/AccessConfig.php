<?php

    namespace CSPlus\Services\Client;

    use Config;
    use CSPlus\Models\CSMobile\Cliente;
    use CSPlus\Services\Client\Exceptions\AccessNotFoundException;
    use Session;

    /**
     * AccessConfig
     *
     * Realiza verificações de acesso do usuário, baseados no código do clube e
     * no tipo de acesso fornecidos
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 2.2.0
     * @since   08/07/2017
     */
    class AccessConfig
    {
        /**
         * @param   int  $clube
         * @param   string  $acesso
         * @return  bool
         * @throws  AccessNotFoundException
         */
        public static function setEnv($clube, $acesso)
        {
            $config = Cliente::find($clube);

            Session::forget('cliente');

            if($config) {
                $dados_acesso['cliente'] = [
                    'cliente' => $config->CLIENTE,
                    'acesso'  => $acesso,
                    'logo'    => $config->LOGO
                ];

                Session::put($dados_acesso);
                Session::regenerate();

                return true;
            }

            throw new AccessNotFoundException;
        }
    }
