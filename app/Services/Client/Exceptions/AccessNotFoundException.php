<?php

    namespace CSPlus\Services\Client\Exceptions;

    use Exception;

    /**
     * AccessNotFoundException
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 1.0.0
     * @since   08/07/2017
     */
    class AccessNotFoundException extends Exception
    {
        /**
         * @var string
         */
        protected $message = 'Os dados do clube não foram encontrados. Verifique se o seu clube possui acesso ao sistema';
    }