<?php

    namespace CSPlus\Services\Notification\Contracts;

    /**
     * NotificationContract
     *
     * Define métodos importantes que as notificações precisam possuir
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 2.0.0
     * @since   07/06/2017
     * @package CSPlus\Services\Notification\Contracts
     */
    interface NotificationContract
    {
        /**
         * @param   string  $sender
         * @return  mixed
         */
        public function setSender($sender);
    }