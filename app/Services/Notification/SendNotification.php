<?php

    namespace CSPlus\Services\Notification;

    use Config;

    use CSPlus\Models\CSPlus\Config\Email;
    use CSPlus\Services\Notification\Contracts\NotificationContract;
    use CSPlus\Services\Notification\Contracts\UserNotificationContract;
    use Illuminate\Notifications\Notifiable;

    /**
     * SendNotification
     *
     * Trait responsável por abrigar os métodos de envio de notificações para os
     * usuários
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 2.1.0
     * @since   08/06/2017
     */
    trait SendNotification
    {
        use Notifiable;

        /**
         * @var \CSPlus\Models\CSPlus\Config\Email
         */
        protected $emailConfiguration;

        /**
         * @var string|null
         */
        protected $userEmail = null;

        /**
         * Envia a notificação para usuários administrativos
         *
         * @param   NotificationContract  $notification
         */
        protected function sendNotification(NotificationContract $notification)
        {
            $this->setEmail();

            if($this->emailConfiguration) {
                $this->configureSendEmail();
                $notification->setSender($this->emailConfiguration->NOME_USUARIO);

                $this->notify($notification);
            }
        }

        /**
         * Envia a notificação para usuários não administrativos
         *
         * @param   UserNotificationContract  $notification
         */
        protected function sendUserNotification(UserNotificationContract $notification)
        {
            $this->setEmail();

            if($this->emailConfiguration) {
                $this->configureSendEmail();
                $notification->setSender($this->emailConfiguration->NOME_USUARIO);
                $this->userEmail = $notification->getRouteForEmail();

                $this->notify($notification);
                $this->userEmail = null;
            }
        }

        /**
         * Configura a conta de email para envio da notificação
         *
         * @return void
         */
        protected function setEmail()
        {
            $this->emailConfiguration = Email::select(['DESCRICAO', 'SERVIDOR', 'PORTA', 'NOME_USUARIO', 'SENHA'])
                                            ->where('DESCRICAO', 'like', '%cadastro%')
                                            ->first();
        }

        /**
         * Realiza a configuração do envio de email
         *
         * @return  void
         */
        protected function configureSendEmail()
        {
            if($this->emailConfiguration) {
                Config::set('mail', [
                    'driver'    => 'smtp',
                    'host'      => $this->emailConfiguration->SERVIDOR,
                    'port'      => $this->emailConfiguration->PORTA,
                    'username'  => $this->emailConfiguration->NOME_USUARIO,
                    'password'  => $this->emailConfiguration->SENHA
                ]);
            }
        }

        /**
         * Realiza o mapeamento do email para qual a notificação deve ser enviada
         *
         * @return  string
         * @see     https://laravel.com/docs/5.4/notifications#customizing-the-recipient
         */
        public function routeNotificationForMail()
        {
            if($this->userEmail) {
                return $this->userEmail;
            }

            return $this->emailConfiguration->NOME_USUARIO;
        }
    }
