<?php

    namespace CSPlus\Presenter;

    use League\Fractal\Manager;

    /**
     * PresenterInterface
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 1.0.0
     * @since   03/08/2017
     * @package CSPlus\Presenter
     */
    interface PresenterInterface
    {
        /**
         * @param   Manager  $manager
         */
        public function __construct(Manager $manager);
    }