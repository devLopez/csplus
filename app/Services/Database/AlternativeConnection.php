<?php

    namespace CSPlus\Services\Database;

    use Client;
    use CSPlus\Models\CSMobile\Cliente;

    /**
     * AlternativeConnection
     *
     * Gerencia conexões alternativas com o banco de dados
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 2.0.2
     * @since   29/06/2017
     */
    class AlternativeConnection
    {
        /**
         * @param   string  $connection_name
         * @param   bool  $dynamicConnection
         * @return  mixed
         */
        public static function getConnection($connection_name, $dynamicConnection = true)
        {
            $acesso = Cliente::find(
                Client::clube()
            );

            if($dynamicConnection == false) {
                $database   = config('database.connections.'.$connection_name.'.database');
                $host       = config('database.connections.'.$connection_name.'.host');
                $user       = config('database.connections.'.$connection_name.'.username');
                $pass       = config('database.connections.'.$connection_name.'.password');

                $database = $host.':'.$database;
            } else {
                $database = ($connection_name == 'csplus') ? $acesso->BD_PRINCIPAL : $acesso->BD_IMAGENS;

                /** Monta a string com os dados da conexão **/
                $database   = $acesso->HOST.':'.$database;
                $user       = $acesso->USUARIO;
                $pass       = $acesso->SENHA;
            }

            if (!($conexao=ibase_connect($database, $user, $pass, 'ISO8859_1', 0, 3)))
                die('Banco de Dados não encontrado: ' .  ibase_errmsg());

            return $conexao;
        }
    }
