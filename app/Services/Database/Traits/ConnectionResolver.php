<?php

    namespace CSPlus\Services\Database\Traits;

    use Client;
    use Config;
    use DB;
    use Exception;
    use CSPlus\Models\CSMobile\Cliente;

    /**
     * ConnectionResolver
     *
     * Trait responsável por realizar o gerenciamento das conexões aos bancos de dados dos clientes
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 1.0.0
     * @since   22/06/2017
     */
    trait ConnectionResolver
    {
        public function __construct()
        {
            try {
                $connection_name = ($this->connection == 'csplus') ? 'csplus' : 'csimagens';

                $acesso = Cliente::find(
                    Client::clube()
                );

                if($acesso == null) {
                    throw new Exception('Os dados de acesso ao clube não foram identificados. Faça Login Novamente');
                }

                $database   = ($connection_name == 'csplus') ? $acesso->BD_PRINCIPAL : $acesso->BD_IMAGENS;
                $role       = $database.';role='.$acesso->ROLE;

                Config::set('database.connections.'.$connection_name, [
                    'driver'    => 'firebird',
                    'host'      => $acesso->HOST,
                    'database'  => $role,
                    'username'  => $acesso->USUARIO,
                    'password'  => $acesso->SENHA,
                    'charset'   => 'ISO8859_1',
                    'collation' => 'ISO8859_general_ci'
                ]);

                DB::connection('csplus')->enableQueryLog();
            } catch (Exception $e) {
                abort(500, $e->getMessage());
            }
        }
    }
