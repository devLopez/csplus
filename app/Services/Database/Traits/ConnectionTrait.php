<?php

    namespace CSPlus\Services\Database\Traits;

    use Client;
    use Config;
    use DB;
    use Exception;
    use CSPlus\Models\CSMobile\Cliente;
    use CSPlus\Services\Client\AccessConfig;

    /**
     * ConnectionTrait
     *
     * Trait responsável por realizar a configuração de banco de dados on-the-fly.
     * Isso se deve pelo fato de que os bancos de dados dos associados é totalmente
     * dinâmico, sendo carregado de acordo com o código do cliente que está acessando
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 1.3.0
     * @since   12/07/2016
     */
    trait ConnectionTrait
    {
        /**
         * Cria uma nova configuração para o banco de dados e seta a nova conexão
         */
        protected function changeConnection()
        {
            try {
                $connection_name = ($this->connection == 'csplus') ? 'csplus' : 'csimagens';

                $acesso = Cliente::find(
                    Client::clube()
                );

                $database   = ($connection_name == 'csplus') ? $acesso->BD_PRINCIPAL : $acesso->BD_IMAGENS;
                $role       = $database.';role='.$acesso->ROLE;

                Config::set('database.connections.'.$connection_name, [
                    'driver'    => 'firebird',
                    'host'      => $acesso->HOST,
                    'database'  => $role,
                    'username'  => $acesso->USUARIO,
                    'password'  => $acesso->SENHA,
                    'charset'   => 'ISO8859_1',
                    'collation' => 'ISO8859_general_ci'
                ]);

                DB::connection('csplus')->enableQueryLog();
            } catch(Exception $e) {
                abort(403, 'Ocorreu um erro grave ao se conectar ao sistema. Faça login novamente');
            }
        }
    }
