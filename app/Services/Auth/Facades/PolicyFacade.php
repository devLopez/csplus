<?php namespace CSPlus\Services\Auth\Facades;

    use Illuminate\Support\Facades\Facade;

    /**
     * PolicyFacade
     *
     * Facade responsável pelo serviço de policies
     *
     * @author  Matheus Lopes Santos <fale_com_lope@hotmail.com>
     * @version 2.0.0
     * @since   16/01/2017
     */
    class PolicyFacade extends Facade
    {
        /**
         * @return string
         */
        public static function getFacadeAccessor()
        {
            return 'policy';
        }
    }
