<?php namespace CSPlus\Services\Auth\Facades;

    use Illuminate\Support\Facades\Facade;

    /**
     * AdminAuthFacade
     *
     * Facade responsável pelo service de login administrativo
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 2.0.0
     * @since   16/01/2017
     */
    class AdminAuthFacade extends Facade
    {
        /**
         * @return string
         */
        public static function getFacadeAccessor()
        {
            return 'admin.auth';
        }
    }
