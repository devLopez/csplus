<?php namespace CSPlus\Services\Auth\Facades;

    use Illuminate\Support\Facades\Facade;

    /**
     * AssociateAuthFacade
     *
     * Facade responsável pelo serviço de login do associado
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 1.0.0
     * @since   15/06/2016
     */
    class AssociateAuthFacade extends Facade
    {
        /**
         * @return string
         */
        public static function getFacadeAccessor()
        {
            return 'associate.auth';
        }
    }
