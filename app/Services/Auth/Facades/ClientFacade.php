<?php

    namespace CSPlus\Services\Auth\Facades;

    use Illuminate\Support\Facades\Facade;

    class ClientFacade extends Facade
    {
        /**
         * @return string
         */
        public static function getFacadeAccessor()
        {
            return 'client';
        }
    }
