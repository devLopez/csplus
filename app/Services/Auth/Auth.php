<?php

    namespace CSPlus\Services\Auth;

    /**
     * Auth
     *
     * classe responável por abrigar métodos comuns aos usuários do sistema
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 1.1.0
     * @since   14/06/2017
     */
    class Auth
    {
        /**
         * @var bool
         */
        protected $admin;

        /**
         * @var string
         */
        protected $access;

        /**
         * @var mixed
         */
        protected $user;

        protected $client;

        public function __construct(Client $client)
        {
            $this->admin    = session('admin.admin') ?? session('usuario.admin');
            $this->access   = $client->access();
        }

        /**
         * @return  string
         */
        public function access()
        {
            return $this->access;
        }

        /**
         * @return  boolean
         */
        public function isAdmin()
        {
            return $this->admin === true && $this->access === 'admin';
        }

        /**
         * @return  bool
         */
        public function isDiretor()
        {
            return \AssociateAuth::isDiretor();
        }

        /**
         * @return  int
         */
        public function id()
        {
            return \AdminAuth::id();
        }

        /**
         * @return  int
         */
        public function matricula()
        {
            return \AssociateAuth::id();
        }

        /**
         * @return  mixed
         */
        public function user()
        {
            return ($this->admin) ? \AdminAuth::user() : \AssociateAuth::user();
        }

        /**
         * @return  string
         */
        public function name()
        {
            return ($this->admin) ? \AdminAuth::user()->USUARIO : \AssociateAuth::user()->NOMEDOTITULAR;
        }

        /**
         * @return  string
         */
        public function photo()
        {
            $foto   = session('usuario.foto');
            $sexo   = session('usuario.sexo') ?? 'male.png';

            return ($foto)
                ? '<img class="img-circle img-sm img-border" src="data:image/jpeg;base64,'.$foto.'" alt="Eu">'
                : '<img class="img-circle img-sm img-border" src="'.asset("assets/img/profile-photos/" . $sexo ).'" alt="Eu">';
        }

        /**
         * @return  \Illuminate\Support\Collection
         */
        public function getCachedLinks()
        {
            return \AssociateAuth::createLinks();
        }
    }
