<?php

    namespace CSPlus\Services\Auth\Providers;

    use CSPlus\Repositories\CSMobile\ClientesRepository as Clientes;
    use CSPlus\Services\Auth\Auth;
    use CSPlus\Services\Auth\AdminAuth;
    use CSPlus\Services\Auth\AssociateAuth;
    use CSPlus\Services\Auth\Client;
    use CSPlus\Services\Auth\Policy;

    use Illuminate\Support\ServiceProvider;

    class AuthServiceProvider extends ServiceProvider
    {
        /**
         * Bootstrap the application services.
         *
         * @return void
         */
        public function boot()
        {
            //
        }

        /**
         * Register the application services.
         *
         * @return void
         */
        public function register()
        {
            $this->app->singleton('auth.auth', function ($app) {
                $client = new Client($app->make(Clientes::class));

                return new Auth($client);
            });

            $this->app->singleton('admin.auth', function($app) {

                return new AdminAuth(
                    $app->make(\CSPlus\Repositories\UsuariosRepository::class),
                    $app->make(\CSPlus\Repositories\PersonalizacaoRepository::class)
                );
            });


            $this->app->singleton('associate.auth', function ($app) {
                return new AssociateAuth(
                    $app->make(\CSPlus\Repositories\Associados\AssociadoRepository::class),
                    $app->make(\CSPlus\Repositories\PropriedadesWebRepository::class),
                    $app->make(\CSPlus\Services\Log\AccessLog::class),
                    $app->make(\CSPlus\Repositories\MenuRepository::class)
                );
            });

            $this->registerClientComponent();
            $this->registerPolicyComponent();
        }

        private function registerClientComponent()
        {
            $this->app->singleton('client', function($app) {
                return new Client($app->make(Clientes::class));
            });
        }

        private function registerPolicyComponent()
        {
            $this->app->singleton('policy', function ($app) {
                $client = new Client($app->make(Clientes::class));

                return new Policy(
                    new \CSPlus\Services\Auth\Auth($client)
                );
            });
        }
    }
