<?php

    namespace CSPlus\Services\Auth;

    use CSPlus\Repositories\ContatosRepository as Contatos;

    /**
     * PasswordLog
     *
     * Classe desenvolvida para realizar o log de alterações de senha do associado
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmal.com>
     * @version 1.0.0
     * @since   26/04/2017
     */
    class PasswordLog
    {
        /**
         * @var \CSPlus\Repositories\ContatosRepository
         */
        protected $contatos;

        /**
         * Construtor da classe
         *
         * @param   Contatos  $contatos
         */
        public function __construct(Contatos $contatos)
        {
            $this->contatos = $contatos;
        }

        /**
         * @return  void
         */
        public function save()
        {
            $log = [
                'COTA'          => \Auth::matricula(),
                'DATAHORA'      => date('d.m.Y, H:i:s', time()),
                'DATA'          => date('d.m.Y', time()),
                'HORA'          => date('H:i:s', time()),
                'SITUACAO'      => 'ALTERACAO DE SENHA WEB'
            ];

            $this->contatos->save($log);
        }
    }
