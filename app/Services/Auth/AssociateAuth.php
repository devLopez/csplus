<?php

    namespace CSPlus\Services\Auth;

    use Cache;
    use CSPlus\Models\CSPlus\Associado\Fotos;
    use CSPlus\Repositories\Associados\AssociadoRepository as Usuarios;
    use CSPlus\Repositories\PropriedadesWebRepository as PropriedadesWeb;
    use CSPlus\Repositories\MenuRepository as Menu;
    use CSPlus\Services\Auth\Contracts\AuthContract;
    use CSPlus\Services\Log\AccessLog;
    use Session;

    /**
     * Auth
     *
     * Classe desenvolvida para gerenciamento da autenticação a ser realizada no
     * sistema
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 4.1.0
     * @since   14/06/2017
     */
    class AssociateAuth implements AuthContract
    {
        /**
         * @var \CSPlus\Repositories\Associados\AssociadoRepository
         */
        protected $usuarios;

        /**
         * @var \CSPlus\Repositories\PropriedadesWebRepository
         */
        protected $propriedadesWeb;

        /**
         * @var \CSPlus\Models\CSPlus\Associado\Ass001
         */
        protected $user;

        /**
         * @var AccessLog
         */
        protected $log;

        /**
         * @var Menu
         */
        protected $menu;

        /**
         * @param   Usuarios  $usuarios
         * @param   PropriedadesWeb $propriedadesWeb
         * @param   AccessLog  $log
         * @param   Menu  $menu
         */
        public function __construct(Usuarios $usuarios, PropriedadesWeb $propriedadesWeb, AccessLog $log, Menu $menu)
        {
            $this->usuarios         = $usuarios;
            $this->propriedadesWeb  = $propriedadesWeb;

            $this->log  = $log;
            $this->menu = $menu;
        }

        /**
         * Realiza o login no sistema
         *
         * @param   string  $usuario
         * @param   string  $senha
         * @return  mixed
         */
        public function login($usuario, $senha)
        {
            $this->user = $this->usuarios->login($usuario, $senha);
            $this->sexo = (trim($this->user->SEXO) == 'M') ? 'male.png' : 'female.png';

            $this->getFoto();
            $this->initSession();
            $this->getLinks();

            $this->log->generate($this->id());

            return true;
        }

        /**
         * @return  void
         */
        public function logout()
        {
            $cache_id = \Client::clube() . $this->id();

            Session::forget('usuario');
            Cache::forget($cache_id);
        }

        /**
         * @return  void
         */
        private function initSession()
        {
            session([
                'usuario' => [
                    'usuario'       => $this->user,
                    'foto'          => $this->foto,
                    'sexo'          => $this->sexo,
                    'admin'         => false,
                    'diretor'       => $this->user->isDiretor,
                    'propriedades'  => $this->getPropriedadesWeb(),
                ]
            ]);
        }

        /**
         * @return  bool
         */
        public function check()
        {
            return (
                !session('usuario.usuario') &&
                session('usuario.admin') != false
            ) ? false : true;
        }

        /**
         * @return  \CSPlus\Models\CSPlus\Associado\Ass001
         */
        public function user()
        {
            return session('usuario.usuario');
        }

        /**
         * @return  int
         */
        public function id()
        {
            return $this->user()->NUMERODACOTA ?? '';
        }

        /**
         * @return  bool
         */
        public function isDiretor()
        {
            return session('usuario.diretor');
        }

        /**
         * @return  void
         */
        private function getFoto()
        {
            $this->foto = Fotos::getFoto(
                $this->user->NUMERODACOTA
            );
        }

        /**
         * Realiza a busca das propriedades web cadastradas para o associado
         *
         * @return  array
         */
        private function getPropriedadesWeb()
        {
            $propriedades = $this->propriedadesWeb->where('NIVEL', '=','U')->all();

            foreach ($propriedades as $p) {
                $propriedade[$p->CAMPO] = $p->SITUACAO;
            }

            return $propriedade;
        }

        /**
         * @return  \Illuminate\Support\Collection
         */
        public function createLinks()
        {
            $cache_id = \Client::clube() . $this->id();

            return Cache::get($cache_id);
        }

        /**
         * @return void
         */
        private function getLinks()
        {
            $links = $this->menu->getLinksForMenu();

            $cache_id = \Client::clube() . $this->id();

            if(!Cache::has($cache_id)) {
                Cache::put($cache_id, $links, 60);
            }
        }
    }
