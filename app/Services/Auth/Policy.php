<?php

    namespace CSPlus\Services\Auth;

    /**
     * Policy
     *
     * Determina algumas politicas de acesso para algumas resources do sistema
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 2.1.0
     * @since   25/04/2017
     */
    class Policy
    {
        protected $auth;

        /**
         * @var string
         */
        protected $acesso;

        /**
         * @var string
         */
        protected $matricula = '';

        /**
         * @param   Auth  $auth
         */
        public function __construct(Auth $auth)
        {
            $this->auth         = $auth;

            $this->acesso       = $auth->access();
            $this->matricula    = $auth->matricula();
        }

        /**
         * @return  bool
         */
        public function isAdmin()
        {
            return $this->auth->isAdmin();
        }

        /**
         * @param   int  $matricula
         * @return  bool
         */
        public function canUpdate($matricula)
        {
            if($this->acesso == 'admin') {
                return true;
            } else {
                return $this->matricula === $matricula;
            }
        }

        /**
         * @param   int  $matricula
         * @return  bool
         */
        public function canCreate($matricula)
        {
            if($this->acesso == 'admin') {
                return true;
            } else {
                return $this->matricula === $matricula;
            }
        }

        /**
         * @param   int  $matricula
         * @return  bool
         */
        public function canSee($matricula)
        {
            if($this->acesso == 'admin') {
                return true;
            } else {
                return $this->matricula === $matricula;
            }
        }
    }
