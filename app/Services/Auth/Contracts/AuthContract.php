<?php

    namespace CSPlus\Services\Auth\Contracts;

    /**
     * AuthContract
     *
     * Interface que realiza a padronização dos métodos de login do sistema CSPlus
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 3.0.0
     * @since   21/04/2017
     */
    interface AuthContract
    {
        public function login($usuario, $senha);

        public function logout();

        public function check();

        public function user();

        public function id();
    }
