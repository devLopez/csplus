<?php

    namespace CSPlus\Services\Auth;

    use Exception;
    use Session;

    use CSPlus\Repositories\UsuariosRepository as Usuarios;
    use CSPlus\Repositories\PersonalizacaoRepository as Personalizacao;
    use CSPlus\Services\Auth\Contracts\AuthContract;

    /**
     * AdminAuth
     *
     * Realiza o gerenciamento do login administrativo
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 4.0.0
     * @since   21/04/2017
     */
    class AdminAuth implements AuthContract
    {
        /**
         * @var CSPlus\Repositories\UsuariosRepository
         */
        protected $usuarios;

        /**
         * @var CSPlus\Repositories\Personalizacao
         */
        protected $personalizacao;

        /**
         * @var \CSPlus\Models\CSPlus\CsSenhas
         */
        protected $user;

        /**
         * @var array
         */
        protected $opcoes_personalizadas;

        /**
         * Construtor da classe
         *
         * @param   Usuarios  $usuarios
         * @param   Personalizacao  $personalizacao
         */
        public function __construct(Usuarios $usuarios, Personalizacao $personalizacao)
        {
            $this->usuarios         = $usuarios;
            $this->personalizacao   = $personalizacao;
        }

        /**
         * Realiza o login no sistema
         *
         * @param   string  $usuario
         * @param   string  $senha
         * @return  bool|string
         */
        public function login($usuario, $senha)
        {
            $usuario    = utf8_decode($usuario);
            $this->user = $this->usuarios->login($usuario, $senha);

            $this->setPersonalization();
            $this->initSession();

            return true;
        }

        /**
         * @return  void
         */
        public function logout()
        {
            Session::forget('admin');
        }

        /**
         * Verifica o usuário logado
         *
         * @return bool
         */
        public function check()
        {
            return (
                !session('admin.usuario') &&
                session('admin.admin') != true
            ) ? false : true;
        }

        public function user()
        {
            return session('admin.usuario');
        }

        /**
         * Retorna o ID do Usuário logado
         *
         * @return  integer
         */
        public function id()
        {
            return $this->user()->CODUSUARIO ?? '';
        }

        /**
         * @return  void
         */
        private function initSession()
        {
            session([
                'admin' => [
                    'usuario'           => $this->user,
                    'admin'             => true,
                    'personalizacao'    => $this->opcoes_personalizadas
                ]
            ]);
        }

        /**
         * Seta a personalização de diversos elementos do sistema
         *
         * @return  void
         */
        private function setPersonalization()
        {
            $personalização = $this->personalizacao->first(['CONTROLA_COTA_ALFA']);

            $this->opcoes_personalizadas = [
                'controla_cota_alfa' => $personalização->CONTROLA_COTA_ALFA
            ];
        }
    }
