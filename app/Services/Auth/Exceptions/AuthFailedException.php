<?php

    namespace CSPlus\Services\Auth\Exceptions;

    use Exception;

    /**
     * AuthFailedException
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 1.0.0
     * @since   08/07/2017
     */
    class AuthFailedException extends Exception
    {
        /**
         * @var string
         */
        protected $message = 'Usuário ou senha incorretos';
    }