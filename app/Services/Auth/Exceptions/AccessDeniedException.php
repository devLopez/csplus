<?php

    namespace CSPlus\Services\Auth\Exceptions;

    use Exception;

    /**
     * DeniedAccessException
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 1.0.0
     * @since   08/07/2017
     */
    class AccessDeniedException extends Exception
    {
        /**
         * @var string
         */
        protected $message = 'Você não possui acesso a este módulo do sistema. Para mais detalhes, contate o administrador';
    }