<?php

    namespace CSPlus\Services\Auth\Exceptions;

    use Exception;

    /**
     * InvalidAccessTypeException
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 1.0.0
     * @since   08/07/2017
     */
    class InvalidAccessTypeException extends Exception
    {
        /**
         * @var string
         */
        protected $message = 'Não foi possivel verificar o seu acesso. Entre em contato com o administrador do sistema';
    }