<?php

    namespace CSPlus\Services\Auth;

    use CSPlus\Repositories\CSMobile\ClientesRepository as Clientes;
    use CSPlus\Services\Auth\Exceptions\AccessDeniedException;
    use CSPlus\Services\Auth\Exceptions\InvalidAccessTypeException;

    /**
     * Client
     *
     * Determina os principais métodos de acesso aos dados sensíveis do clube,
     * como uso do Pagseguro, código do clube logado e outros dados sensíveis
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 1.1.0
     * @since   28/06/2017
     */
    class Client
    {
        /**
         * @var Clientes
         */
        protected $clientes;

        /**
         * @var array
         */
        protected $canAccess = ['admin', 'associado'];

        /**
         * @param   Clientes  $clientes
         */
        public function __construct(Clientes $clientes)
        {
            $this->clientes = $clientes;
        }

        /**
         * @return  int
         */
        public function clube()
        {
            return session('cliente.cliente');
        }

        /**
         * @return  string
         */
        public function access()
        {
            return session('cliente.acesso');
        }

        /**
         * @return  bool
         */
        public function usesMembersArea()
        {
            return $this->getClientData()->AREA_ASSOCIADO;
        }

        /**
         * @return  bool
         */
        public function usesAdminArea()
        {
            return $this->getClientData()->AREA_ADMINISTRATIVA;
        }

        /**
         * @return  bool
         */
        public function usesPagseguro()
        {
            return $this->getClientData()->USA_PAGSEGURO;
        }

        /**
         * @return  bool
         */
        public function usesCreditCard()
        {
            return session('cliente.usa_cc');
        }

        /**
         * @return  bool
         */
        public function pagseguroSandbox()
        {
            return $this->getClientData()->PAGSEGURO_SANDBOX;
        }

        /**
         * @return  \CSPlus\Models\CSMobile\Cliente
         */
        private function getClientData()
        {
            $cliente = $this->clube();

            return $this->clientes->find($cliente);
        }

        /**
         * @param   string  $access
         * @return  bool
         * @throws  InvalidAccessTypeException
         */
        public function checkAccessType($access)
        {
            if(!in_array($access, $this->canAccess)) {
                throw new InvalidAccessTypeException;
            }

            return true;
        }

        /**
         * @return  bool
         * @throws  AccessDeniedException
         */
        public function checkAccessPermission()
        {
            if($this->access() == 'admin' && !$this->usesAdminArea()) {
                throw new AccessDeniedException;
            } else if($this->access() == 'associado' && !$this->usesMembersArea()) {
                throw new AccessDeniedException;
            }

            return true;
        }
    }
