<?php

namespace CSPlus\Services\Payment\Financeiro;

/**
 * DiscountManager
 *
 * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
 * @version 1.0.0
 * @since   27/11/2017
 * @package CSPlus\Services\Payment\Financeiro
 */
class DiscountManager
{
    /**
     * @var int
     */
    protected $client;

    /**
     * @param   int  $client
     */
    public function setClient(int $client)
    {
        $this->client = $client;

        return $this;
    }

    /**
     * @return  array
     */
    private function getDiscounts()
    {
        return config('csplus.discounts.' . $this->client);
    }

    /**
     * @param   float  $value
     * @param   int  $invoices
     * @return  float
     */
    public function applyDiscount(float $value, int $invoices) : float
    {
        $discounts = $this->getDiscounts();

        $value = ($value / $invoices);

        if( in_array($value, $discounts['prazo']) ) {
            $type = array_search($value, $discounts['prazo']);

            if($type) {
                return number_format(($discounts['vista'][$type] * $invoices), 2);
            }
        }

        return $value;
    }

    /**
     * @param   float  $value
     * @param   int  $invoices
     * @return  float
     */
    public function getDiscoutedValue(float $value, int $invoices) : float
    {
        $discounts = $this->getDiscounts();

        $value = $value / $invoices;

        if( in_array($value, $discounts['prazo']) ) {
            $type = array_search($value, $discounts['prazo']);

            if($type) {

                $maxValue   = $value * $invoices;
                $minValue   = $discounts['vista'][$type] * 2;

                return number_format(
                    ($minValue * 100) / $maxValue, 2
                );

            }

//            if($type) {
//                $value = number_format(( ($value - $discounts['vista'][$type]) * $invoices), 2);
//
//                return $value = -1 * abs($value);
//            }
        }

        return 0.00;
    }
}