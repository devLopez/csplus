<?php

	namespace CSPlus\Services\Payment\Logger;

	use Log;
	use CSPlus\Models\CSPlus\Contatos;
	use Illuminate\Filesystem\Filesystem;

	/**
	 * Logger
	 *
	 * Realiza a criação de log para os dados do pagseguro
	 *
	 * @author 	Matheus Lopes Santos <fale_com_lopez@hotmail.com>
	 * @version 2.1.0
	 * @since 	10/05/2017
	 */
	class Logger
	{
		/**
		 * Gera um log contendo os dados da última transação
		 *
		 * @param  	\stdClass  $log
		 * @return 	void
		 */
		public static function generate($log)
		{
			$fs 	= new Filesystem;
			$path	= storage_path('app/ps');
			$file 	= $path . '/pagseguro.txt';

			$logData = "Data: ".date('d/m/Y H:i')." | Clube: {$log->clube} | Cobrança: {$log->cobranca} | Status: {$log->status} | Valor: {$log->valor} | Mensagem: {$log->msg}" . PHP_EOL;

			($log->error) ? Log::emergency($logData) : Log::info($logData);

			if(!$fs->exists($path)) {
				$fs->makeDirectory($path, 777);
			}

			if ($fs->exists($file)) {
				$fs->prepend($file, $logData);
			} else {
				$fs->put($file, $logData);
			}
		}

		/**
		 * Salva um contato na tebela Contatos
		 *
		 * @param  	int  $cota
		 * @param  	string  $data
		 * @param  	string  $hora
		 * @param  	string  $observacao
		 * @return 	void
		 */
		public static function contatoLog($cota, $data, $hora, $observacao)
        {
            Contatos::saveLog([
                'COTA'         => $cota,
                'DATAHORA'     => date('Y.m.d H:i:s'),
                'DATA'         => $data,
                'HORA'         => $hora,
                'SITUACAO'     => 'Retorno PAGSEGURO',
                'OBSERVACAO'   => "'".utf8_decode($observacao)."'"
            ]);
        }
	}
