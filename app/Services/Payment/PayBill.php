<?php

    namespace CSPlus\Services\Payment;

    use Exception;
    use Payment;
    use stdClass;
    use CSPlus\Models\CSPlus\Generator;
    use CSPlus\Repositories\Financeiro\CobrancaRepository as Billings;
    use CSPlus\Services\Payment\Http\Request;
    use CSPlus\Services\Payment\Logger\Logger;

    /**
     * PayBill
     *
     * Classe desenvolvida para realizar a baixa de um pagamento realizado pelo
     * PagSeguro
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 1.1.0
     * @since   15/05/2017
     */
    class PayBill
    {
        /**
         * @var int
         */
        protected $clube;

        /**
         * @var \CSPlus\Repositories\Financeiro\CobrancaRepository
         */
        protected $billings;

        /**
         * @param   Billings  $billings
         */
        public function __construct(Billings $billings)
        {
            $this->billings = $billings;
        }

        /**
         * @var array
         */
        protected $paymentStatus = [
            1 => 'Aguardando Pagamento',
            2 => 'Em Análise',
            3 => 'Paga',
            4 => 'Disponível',
            5 => 'Em Disputa',
            6 => 'Devolvida',
            7 => 'Cancelada',
            8 => 'Debitado',
            9 => 'Retenção temporária'
        ];

        /**
         * @param   int  $clube
         * @return  $this
         */
        public function setClube($clube)
        {
            $this->clube = $clube;

            return $this;
        }

        /**
         * @param   string  $notificationCode
         * @return  void
         */
        public function getTransaction($notificationCode)
        {
            $request    = new Request;
            $transacao  = $request->getTransaction($notificationCode);

            $this->pay($transacao);
        }

        /**
         * Realiza a busca da transação no pagseguro
         *
         * @param   array  $transacao
         * @return  void
         */
        public function pay($transacao)
        {
            $baixa = new stdClass;
            $baixa->idCobranca      = (int) $transacao['transaction']['items']['item']['id'];
            $baixa->status          = $transacao['transaction']['status'];
            $baixa->dataRetorno     = date('Y-m-d', strtotime($transacao['transaction']['date']));
            $baixa->horaRetorno     = date('H:i:s', strtotime($transacao['transaction']['date']));
            $baixa->valorPago       = (float) $transacao['transaction']['items']['item']['amount'];

            $log['clube']       = $this->clube;
            $log['cobranca']    = $baixa->idCobranca;
            $log['status']      = $this->paymentStatus[$baixa->status];
            $log['valor']       = $baixa->valorPago;
            $log['msg']         = '';
            $log['error']       = false;
            $this->logger($log);

            $this->getCobrancaForBaixa($baixa);
        }

        /**
         * Realiza a busca da cobrança no banco de dados
         *
         * @param   \stdClass  $baixa
         * @return  void
         */
        private function getCobrancaForBaixa($baixa)
        {
            $cobranca = $this->billings->getCobrancaForBaixa($baixa->idCobranca);

            if($cobranca) {
                if($cobranca->RECIBO != null) {
                    Logger::contatoLog($cobranca->COTA, $baixa->dataRetorno, $baixa->horaRetorno, 'Cobranca ja quitada');
                    return;
                }

                if($baixa->status != 3) {
                    Logger::contatoLog($cobranca->COTA, $baixa->dataRetorno, $baixa->horaRetorno, $this->paymentStatus[$baixa->status]);

                    $this->billings->updateCobrancaAndTaxas($baixa);
                    return;
                }

                $this->quitar($cobranca, $baixa);
            }
        }

        /**
         * Salva o Log do pagseguro no disco
         * @param   array  $log
         * @return  void
         */
        private function logger(array $log)
        {
            $log = (object) $log;

            Logger::generate($log);
        }

        /**
         * Realiza a quitação da cobrança
         *
         * @param   \CSPlus\Models\CSPlus\Financeiro\Cobranca  $cobranca
         * @param   \stdClass  $transacao
         * @return  void
         */
        public function quitar($cobranca, $transacao)
        {
            $valorBase      = (float) $cobranca->VALOR;
            $valorPagSeguro = $transacao->valorPago;
            $valorJuros     = 0;
            $valorDesconto  = 0;

            if($valorPagSeguro > $valorBase) {
                $valorJuros = $valorPagSeguro - $valorBase;
            } else if($valorPagSeguro < $valorBase) {
                $valorDesconto = $valorBase - $valorPagSeguro;
            }

            $recibo = (int) Generator::newId('GEN_RECIBO');

            \DB::connection('csplus')->statement('EXECUTE PROCEDURE PROC_EXECUTABAIXA_COBRANCA_FIN(:boleto, :recibo, :valorPago, :juros, 700, :dataPagamento, 0, :desconto, :valorBase, :dataTarifa, :dataFinanceiro)', [
                'boleto'            => $cobranca->BOLETO,
                'recibo'            => $recibo,
                'valorPago'         => $transacao->valorPago,
                'juros'             => $valorJuros,
                'dataPagamento'     => $transacao->dataRetorno,
                'desconto'          => $valorDesconto,
                'valorBase'         => $valorBase,
                'dataTarifa'        => $transacao->dataRetorno,
                'dataFinanceiro'    => $transacao->dataRetorno,
            ]);
        }
    }
