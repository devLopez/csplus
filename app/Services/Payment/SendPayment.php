<?php

    namespace CSPlus\Services\Payment;

    use Exception;
    use CSPlus\Services\Payment\Contracts\PaymentContract;
    use CSPlus\Services\Payment\Parsers\IntegrationErrors;
    use CSPlus\Services\Payment\Parsers\XmlParser;
    use Payment;

    /**
     * SendPayment
     *
     * Realiza o envio do pagamento para o pagseguro
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmal.com>
     * @version 1.0.0
     * @since   03/07/2017
     */
    class SendPayment
    {
        /**
         * @var PaymentContract
         */
        protected $payment;

        /**
         * @param   PaymentContract  $payment
         */
        public function __construct(PaymentContract $payment)
        {
            $this->payment = $payment;
        }

        /**
         * @return  string|array|mixed
         */
        public function register()
        {
            $pagSeguroRequest = $this->payment->getInvoiceRequest();

            try {
                return $pagSeguroRequest->register(
                    Payment::getCredentials()
                );

            } catch (Exception $e) {
                $xml = new XmlParser($e->getMessage());

                $errors = new IntegrationErrors($xml->getResult('errors'));


                return $errors->getErrors();
            }
        }
    }