<?php

    namespace CSPlus\Services\Payment\Facades;

    use CSPlus\Services\Payment\Payment;
    use Illuminate\Support\Facades\Facade;

    /**
     * PaymentFacade
     *
     * Registra a facade do Service Payment
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 1.0.0
     * @since   08/02/2017
     * @package CSPlus\Services\Payment\Facades
     */
    class PaymentFacade extends Facade
    {
        /**
         * @return  Payment
         */
        public static function getFacadeAccessor()
        {
            return Payment::class;
        }
    }