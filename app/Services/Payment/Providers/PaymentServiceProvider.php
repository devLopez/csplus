<?php

    namespace CSPlus\Services\Payment\Providers;

    use Client;
    use CSPlus\Models\CSMobile\Cliente;
    use CSPlus\Services\Payment\Payment;

    use Illuminate\Support\ServiceProvider;

    class PaymentServiceProvider extends ServiceProvider
    {
        public function register()
        {
            $this->app->bind(Payment::class, function(){

                $config = new \stdClass();

                $config->cliente        = Client::clube();
                $config->usaPagseguro   = Client::usesPagseguro();
                $config->onSandbox      = Client::pagseguroSandbox();

                return new Payment(
                    new Cliente,
                    $config
                );
            });

            $this->registerCreditCardBind();
        }

        /**
         * Registra o bind das dependências da classe CreditCard
         *
         * @return  void
         */
        private function registerCreditCardBind()
        {
            $this->app->bind(\CSPlus\Services\Payment\Contracts\CreditCardContract::class, function($app){

                $psCreditCard       = new \PagSeguro\Domains\Requests\DirectPayment\CreditCard;
                $debitosRepository  = $app->make(\CSPlus\Repositories\Financeiro\DebitosRepository::class);
                $associadoModel     = $app->make(\CSPlus\Models\CSPlus\Associado\Ass001::class);
                $cobrancaRepository = $app->make(\CSPlus\Repositories\Financeiro\CobrancaRepository::class);

                return new \CSPlus\Services\Payment\PaymentTypes\CreditCard(
                    $psCreditCard,
                    $debitosRepository,
                    $associadoModel,
                    $cobrancaRepository
                );
            });
        }
    }
