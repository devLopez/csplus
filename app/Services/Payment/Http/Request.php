<?php

    namespace CSPlus\Services\Payment\Http;

    use Exception;
    use CSPlus\Services\Payment\Parsers\XmlParser;
    use Payment;

    /**
     * Request
     *
     * Realiza requisições http diversas
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 1.0.2
     * @since   17/02/2017
     */
    class Request
    {
        /**
         * Realiza a busca da transação
         *
         * @param   string  $notificationCode
         * @return  array
         * @throws  Exception
         */
        public function getTransaction($notificationCode)
        {
            $url = (Payment::onSandbox())
                        ? config('payment.notification_url.sandbox')
                        : config('payment.notification_url.production');

            $user = Payment::getCredentials();

            $email = $user->getEmail();
            $token = $user->getToken();

            $url .= trim($notificationCode) . '?email=' . $email . '&token=' . $token;

            $transaction = $this->execute($url);

            if($transaction == 'Unauthorized') {
                throw new Exception('O sistema PagSeguro não autorizou a busca destas informações');
            }

            $xml = new XmlParser($transaction);
            return $xml->getResult();
        }

        /**
         * Executa uma requisição com o curl
         *
         * @param   string  $url
         * @param   null|bool  $post
         * @param   array  $header
         * @return  string
         */
        public function execute($url, $post = NULL, array $header = [])
        {
            $ch = curl_init($url);

            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

            if(count($header) > 0) {
                curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
            }

            if($post !== null) {
                curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
            }

            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

            $data = curl_exec($ch);

            curl_close($ch);

            return $data;
        }
    }
