<?php

namespace CSPlus\Services\Payment;

use Client;
use CSPlus\Models\CSMobile\Cliente;
use CSPlus\Services\Payment\Financeiro\DiscountManager;
use PagSeguro\Configuration\Configure;
use PagSeguro\Services\Installment;
use PagSeguro\Library;
use PagSeguro\Services\Session;

/**
 * Payment
 *
 * Provê um wrapper para pagamentos via Pagseguro
 *
 * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
 * @version 1.0.0
 * @since   08/02/2017
 * @package CSPlus\Services\Payment
 */
class Payment
{
    /**
     * @var \CSPlus\Models\CSMobile\Cliente
     */
    protected $clienteModel;

    /**
     * @var \CSPlus\Models\CSMobile\Cliente
     */
    protected $cliente;

    /**
     * @var int
     */
    protected $idCliente;

    /**
     * @var bool
     */
    protected $usaPagseguro;

    /**
     * @var bool
     */
    protected $sandbox;

    /**
     * @param   \CSPlus\Models\CSMobile\Cliente  $clienteModel
     * @param   \stdClass  $config
     */
    public function __construct(Cliente $clienteModel, $config)
    {
        $this->clienteModel = $clienteModel;
        $this->idCliente    = $config->cliente;
        $this->usaPagseguro = $config->usaPagseguro;
        $this->sandbox      = $config->onSandbox;

        if($this->usesPagseguro()) {

            $this->initializePagseguro()
                ->findClient()
                ->setPagseguroEnvironment();
        }
    }

    /**
     * Inicializa a biblioteca do PagSeguro
     *
     * @return  $this
     */
    public function initializePagseguro()
    {
        Library::initialize();

        return $this;
    }

    /**
     * @return  bool
     */
    public function usesPagseguro()
    {
        return $this->usaPagseguro;
    }

    /**
     * @return  bool
     */
    public function onSandbox()
    {
        return $this->sandbox;
    }

    /**
     * Find the tokens from the actual client
     *
     * @return  $this
     */
    public function findClient()
    {
        $this->cliente = $this->clienteModel->with('token')->findOrFail($this->idCliente);

        return $this;
    }

    /**
     * Set the pagseguro API data
     *
     * @return  $this
     */
    public function setPagseguroEnvironment()
    {
        if($this->usesPagseguro()) {

            $email_empresa = $this->cliente->token->EMAIL;

            $token = ($this->onSandbox())
                        ? $this->cliente->token->TOKEN_SANDBOX
                        : $this->cliente->token->TOKEN_PRODUCAO;

            $environment = ($this->onSandbox()) ? 'sandbox' : 'production';

            Configure::setEnvironment($environment);
            Configure::setAccountCredentials($email_empresa, $token);
            Configure::getCharset('ISO-8859-1');
            Configure::setLog(true, storage_path('logs/laravel_pagseguro.log'));
        }

        return $this;
    }

    /**
     * @return  string|null
     */
    public function getSessionToken()
    {
        if($this->usesPagseguro()) {
            $result = Session::create(
                $this->getCredentials()
            );

            return $result->getResult();
        }

        return null;
    }

    /**
     * @return \PagSeguro\Domains\AccountCredentials
     */
    public function getCredentials()
    {
        return Configure::getAccountCredentials();
    }

    /**
     * @return  string
     */
    public function getJavascriptUrl()
    {
        if($this->onSandbox()) {
            return config('payment.javascript_url.sandbox');
        }

        return config('payment.javascript_url.production');
    }

    /**
     * @return  int|null
     */
    public function getParcelas()
    {
        if($this->usesPagseguro()) {
            return (int) $this->cliente->MAXIMO_PARCELAS;
        }

        return null;
    }

    /**
     * Retorna as parcelas sem juros que o clube oferece
     *
     * @return  int|null
     */
    public function getParcelasSemJuros()
    {
        if($this->usesPagseguro()) {
            return (int) $this->cliente->PARCELAS_SEM_JUROS;
        }

        return null;
    }

    /**
     * Retorna o email do clube cadastrado no Pagseguro
     *
     * @return  string
     */
    public function getReceiverEmail()
    {
        return $this->cliente->token->EMAIL;
    }

    /**
     * Retorna o email do vendedor em modo sandbox
     *
     * @return  string
     */
    public function getVendedorSandbox()
    {
        return $this->cliente->token->VENDEDOR_SANDBOX;
    }

    /**
     * Realiza a geração das parcelas de acordo o valor e bandeira do cartão
     *
     * @param   float  $valor
     * @param   string  $bandeira
     * @return  array
     */
    public function generateInstallments($valor, $bandeira, $cobrancas = 1)
    {
        $semJuros       = $this->getParcelasSemJuros();
        $parcelas       = ($semJuros <= 1) ? null : $semJuros;
        $totalParcelas  = $this->getParcelas();

        $cliente = Client::clube();

        if($cliente == 301) {
            $amount = $valor / $cobrancas;

            if( $amount < 300.00) {
                $parcelas = 6;
                $totalParcelas = 6;
            } else {
                $totalParcelas = 12;
                $parcelas = 12;
            }
        }

        $options = [
            'amount' => $valor,
            'card_brand' => $bandeira,
            'max_installment_no_interest' => $parcelas
        ];

        $installments = Installment::create(
            $this->getCredentials(),
            $options
        );

        $parcelas = collect();

        foreach($installments->getInstallments() as $i) {
            $parcelas->push(
                $this->formatInstallments($i)
            );
        }

        if($cliente == 301) {

            $discountManager = new DiscountManager();
            $discountManager->setClient($cliente);

            $discountValue = $discountManager->applyDiscount($valor, $cobrancas);

            $parcelas = $parcelas->map(function ($item, $key) use ($discountValue) {
                if ($key == 0) {
                    $item['price'] = $discountValue;
                }

                return $item;

            });
        }

        return $parcelas->take($totalParcelas);
    }

    /**
     * @param   $installment
     * @return  array
     */
    private function formatInstallments($installment)
    {
        return [
            'price'             => $installment->getAmount(),
            'installmentAmount' => $installment->getAmount(),
            'totalAmount'       => $installment->getTotalAmount()
        ];
    }
}
