<?php

namespace CSPlus\Services\Payment\Contracts;

/**
 * PaymentContract
 *
 * Interface desenvolvida para gerenciar os pagamentos via PagSeguro
 *
 * @author  Matheus Loeps Santos <fale_com_lopez@hotmail.com>
 * @version 1.2.0
 * @since   27/11/2017
 */
interface PaymentContract
{
    public function setReceiverEmail();

    public function setReference();

    public function setCurrency();

    public function setNotificationUrl();

    public function addItems();

    public function setCustomerInformation();

    public function setShippingAddress();

    public function setMode();

    public function getInvoiceRequest();
}
