<?php

    namespace CSPlus\Services\Payment\Contracts;

    /**
     * CreditCardContract
     *
     * Classe desenvolvida para prover um ainterface comum aos pagamentos com
     * cartão de crédito pelo sistema
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 1.1.0
     * @since   16/02/2017
     */
    interface CreditCardContract
    {
        public function setBillingAddress();

        public function setCardToken();

        public function setInstallment();

        public function setCardHolderInformation();
    }
