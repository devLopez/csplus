<?php

    namespace CSPlus\Services\Payment;

    use Exception;
    use Illuminate\Support\Collection;
    use Payment;

    /**
     * Invoice
     *
     * Classe desenvolvida ṕara gerenciamento das invoices de novas cobranças
     * a serem geradas pelo sistema
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 3.1.2
     * @since   23/02/2017
     */
    class Invoice
    {
        /**
         * @var Collection
         */
        protected $items;

        /**
         * @param   Collection  $items
         */
        public function setItems(Collection $items)
        {
            $this->items = $items;
        }

        /**
         * @param   float  $desconto
         * @return  string
         */
        public function sumItems($desconto = 0.00)
        {
            $value = 0.00;

            foreach($this->items as $i) {
                $value += $i->VALORTOTAL;
            }

            $value = $this->total($value, $desconto);

            return number_format($value, 2, '.', '');
        }

        /**
         * Aplica o desconto ao valor total
         *
         * @param   float  $total
         * @param   float  $desconto
         * @return  float
         */
        public static function total($total, $desconto)
        {
            //if($desconto != null) {
            //    return $total - $desconto;
            //}

            return $total;
        }

        /**
         * @return  string
         * @throws  Exception
         */
        public function generateDescription()
        {
            if($this->items->isEmpty()) {
                throw new Exception('Para geração da descrição do pagamento, é necessário passar, ao menos, um produto');
            }

            $description = "";

            foreach ($this->items as $i) {
                $description .= utf8_encode($i->PRODUTO) . " - R$" . money2br($i->VALORTOTAL) . "\n";
            }

            return str_limit($description, 95);
        }
    }
