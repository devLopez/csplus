<?php

namespace CSPlus\Services\Payment\PaymentTypes;

use Client;
use CSPlus\Services\Payment\Financeiro\DiscountManager;
use Exception;
use Payment;
use stdClass;

use CSPlus\Models\CSPlus\Associado\Ass001 as Associado;
use CSPlus\Repositories\Financeiro\DebitosRepository;
use CSPlus\Repositories\Financeiro\CobrancaRepository;
use CSPlus\Services\Payment\Contracts\CreditCardContract;
use CSPlus\Services\Payment\Contracts\PaymentContract;
use CSPlus\Services\Payment\Invoice;
use PagSeguro\Domains\Requests\DirectPayment\CreditCard as PagSeguroCreditCard;


/**
 * CreditCard
 *
 * Realiza o pagamento de produtos/serviços com o pagseguro via cartão de crédito
 *
 * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
 * @version 2.0.0
 * @since   03/07/2017
 */
class CreditCard implements PaymentContract, CreditCardContract
{
    /**
     * @var \PagSeguro\Domains\Requests\DirectPayment\CreditCard
     */
    protected $creditCard;

    /**
     * @var \stdClass
     */
    protected $paymentData;

    /**
     * @var \CSPlus\Models\CSPlus\Associado\Ass001
     */
    protected $modelAssociado;

    /**
     * @var int
     */
    protected $matricula;

    /**
     * @var \CSPlus\Models\CSPlus\Associado\Ass001
     */
    protected $associado;

    /**
     * @var int
     */
    protected $cobranca;

    /**
     * @var \CSPlus\Repositories\Financeiro\DebitosRepository
     */
    protected $debitosRepository;

    /**
     * @var \CSPlus\Repositories\Financeiro\CobrancaRepository
     */
    protected $cobrancaRepository;

    /**
     * @param   PagSeguroCreditCard  $creditCard
     * @param   DebitosRepository  $debitos
     * @param   Associado  $associado
     * @param   CobrancaRepository  $billings
     */
    public function __construct(
        PagSeguroCreditCard $creditCard,
        DebitosRepository $debitos,
        Associado $associado,
        CobrancaRepository $billings
    ) {
        $this->creditCard           = $creditCard;
        $this->debitosRepository    = $debitos;
        $this->modelAssociado       = $associado;
        $this->cobrancaRepository   = $billings;
    }

    /**
     * @return  void
     */
    public function generatePaymentRequest()
    {
        $this->setAssociado()
             ->setReceiverEmail()
             ->setReference()
             ->setCurrency()
             ->setMode()
             ->setNotificationUrl()
             ->addItems()
             ->setCustomerInformation()
             ->setShippingAddress()
             ->setBillingAddress()
             ->setCardToken()
             ->setInstallment()
             ->setCardHolderInformation();
    }

    /**
     * @param   int  $matricula
     * @return  $this
     */
    public function setMatricula($matricula)
    {
        $this->matricula = $matricula;
        $this->setAssociado();

        return $this;
    }

    /**
     * @param   int  $cobranca
     * @return  $this
     */
    public function setCobranca($cobranca)
    {
        $this->cobranca = $cobranca;

        return $this;
    }

    /**
     * @return  $this
     */
    public function setAssociado()
    {
        $this->associado = $this->modelAssociado->findOrFail($this->matricula);

        return $this;
    }

    /**
     * @param   stdClass  $paymentData
     * @return  $this
     */
    public function setPaymentData(stdClass $paymentData)
    {
        $this->paymentData = $paymentData;

        return $this;
    }

    /**
     * @return  $this
     */
    public function setReceiverEmail()
    {
        $this->creditCard->setReceiverEmail(Payment::getReceiverEmail());

        return $this;
    }

    /**
     * @return  $this
     */
    public function setReference()
    {
        $this->creditCard->setReference($this->cobranca);

        return $this;
    }

    /**
     * @return  $this
     */
    public function setCurrency()
    {
        $this->creditCard->setCurrency("BRL");

        return $this;
    }

    /**
     * @return  $this
     */
    public function setMode()
    {
        $this->creditCard->setMode('DEFAULT');

        return $this;
    }

    /**
     * @return  $this
     */
    public function setNotificationUrl()
    {
        $url = route('payment.do', [Client::clube()]);

        $this->creditCard->setNotificationUrl($url);

        return $this;
    }

    /**
     * @throws  Exception
     * @return  $this
     */
    public function addItems()
    {
        $cobranca = $this->cobrancaRepository->find($this->cobranca);

        if(!$cobranca) {
            throw new Exception('Não foi possível recuperar os dados da cobrança. Tente Novamente');
        } else {

            $total = Invoice::total(
                $cobranca->VALOR,
                $cobranca->DESCONTO_ATE_VENCIMENTO
            );

            if(Client::clube() == 301 && $this->paymentData->installments == 1) {
                $discountManager = new DiscountManager();
                $discountManager->setClient(Client::clube());

                $total = $discountManager->applyDiscount(
                    $this->paymentData->amount,
                    $this->paymentData->qtde_cobrancas
                );
            }

            $this->creditCard->addItems()->withParameters(
                $this->cobranca,
                'Pagamento de Taxas Periódicas',
                1,
                number_format($total, 2, '.', '')
            );
        }

        return $this;
    }

    /**
     * @return  $this
     */
    public function setCustomerInformation()
    {
        $this->creditCard->setSender()->setName($this->associado->NOMEDOTITULAR);

        if(Payment::onSandbox()) {
            $this->creditCard->setSender()->setEmail(Payment::getVendedorSandbox());
        } else {
            $this->creditCard->setSender()->setEmail($this->associado->CAIXAPOSTAL);
        }

        $this->creditCard->setSender()->setPhone()->withParameters(
            $this->paymentData->holderAreaCode,
            $this->paymentData->holderPhone
        );
        $this->creditCard->setSender()->setDocument()->withParameters('CPF', $this->associado->CIC);
        $this->creditCard->setSender()->setHash($this->paymentData->senderHash);

        return $this;
    }

    /**
     * @return $this
     */
    public function setShippingAddress()
    {
        $this->creditCard->setShipping()->setAddress()->withParameters(
            $this->associado->ENDRES_LOGRADOURO,
            $this->associado->ENDRES_NUMERO,
            $this->associado->BAIRRORESIDENCIAL,
            $this->associado->CEPRESIDENCIAL,
            $this->associado->CIDADERESIDENCIAL,
            $this->associado->ESTADORESIDENCIAL,
            'BRA',
            $this->associado->ENDRES_COMPLEMENTO
        );

        return $this;
    }

    /**
     * @return $this
     */
    public function setBillingAddress()
    {
        $this->creditCard->setBilling()->setAddress()->withParameters(
            $this->associado->ENDRES_LOGRADOURO,
            $this->associado->ENDRES_NUMERO,
            $this->associado->BAIRRORESIDENCIAL,
            $this->associado->CEPRESIDENCIAL,
            $this->associado->CIDADERESIDENCIAL,
            $this->associado->ESTADORESIDENCIAL,
            'BRA',
            $this->associado->ENDRES_COMPLEMENTO
        );

        return $this;
    }

    /**
     * @return  $this
     */
    public function setCardToken()
    {
        $this->creditCard->setToken($this->paymentData->cardToken);

        return $this;
    }

    /**
     * @return  $this
     */
    public function setInstallment()
    {
        $this->creditCard->setInstallment()->withParameters(
            $this->paymentData->installments,
            number_format($this->paymentData->installmentAmount, 2, '.', ''),
            (int) $this->paymentData->parcelas_sem_juros
        );

        return $this;
    }

    /**
     * @return  $this
     */
    public function setCardHolderInformation()
    {
        $this->creditCard->setHolder()->setBirthdate($this->paymentData->holderBirthDate);
        $this->creditCard->setHolder()->setName($this->paymentData->holderName);
        $this->creditCard->setHolder()->setPhone()->withParameters(
            $this->paymentData->holderAreaCode,
            $this->paymentData->holderPhone
        );
        $this->creditCard->setHolder()->setDocument()->withParameters(
            'CPF',
            $this->paymentData->holderCpf
        );

        return $this;
    }

    /**
     * @return  PagSeguroCreditCard
     */
    public function getInvoiceRequest()
    {
        return $this->creditCard;
    }
}
