<?php

    namespace CSPlus\Services\Payment;

    use Date;
    use DateTime;
    use Exception;
    use Payment;

    use Carbon\Carbon;
    use PagSeguro\Services\Transactions\Search\Date as DateSearch;
    use PagSeguro\Services\Transactions\Search\Code;
    use PagSeguro\Parsers\Transaction\Search\Date\Response;

    /**
     * Transaction
     *
     * Classe desenvolvida para gerenciar a busca por transações realizadas
     * junto ao PagSeguro
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 1.0.0
     * @since   12/05/2017
     */
    class Transaction
    {
        /**
         * @var DateTime
         */
        protected $beginDate;

        /**
         * @var DateTime
         */
        protected $endDate;

        /**
         * @param   string  $beginDate
         * @param   string  $endDate
         * @return  $this
         * @throws  Exception
         */
        public function setDates($beginDate, $endDate)
        {
            $this->beginDate    = new DateTime(Date::toSql($beginDate));
            $this->endDate      = new DateTime(Date::toSql($endDate));

            $interval = $this->beginDate->diff($this->endDate);

            if($interval->days > 30) {
                throw new Exception('Para realizar a busca, o intervalo deve ser menor que 30 dias');
            }

            return $this;
        }

        /**
         * Realiza a busca por transações no pagseguro
         *
         * @return  array
         * @throws  Exception
         */
        public function checkForTransactions()
        {
            $options = [
                'initial_date'  => $this->beginDate->format('Y-m-d') . 'T00:00',
                'final_date'    => $this->endDate->format('Y-m-d') . 'T23:59'
            ];

            $result = DateSearch::search(
                Payment::getCredentials(),
                $options
            );

            return $this->formatResults($result);
        }

        /**
         * Formata os Resultados obtidos em um array a ser convertido em json
         *
         * @param   \PagSeguro\Parsers\Transaction\Search\Date\Response  $result
         * @return  array
         */
        private function formatResults(Response $result)
        {
            $data = [
                'nestaPagina'   => $result->getResultsInThisPage(),
                'paginaAtual'   => $result->getCurrentPage(),
                'totalPaginas'  => $result->getTotalPages(),
            ];

            $transacoes = [];

            if($result->getTransactions() != null) {
                foreach($result->getTransactions() as $transaction) {

                    $date = new DateTime($transaction->getDate());

                    $transacoes[] = [
                        'status'            => $transaction->getStatus(),
                        'codigoNotificacao' => $transaction->getCode(),
                        'ultimoEvento'      => $date->format('d/m/Y H:i:s'),
                        'valor'             => $transaction->getGrossAmount()
                    ];
                }
            }

            $data['transacoes'] = $transacoes;

            return $data;
        }

        /**
         * Realiza a busca de uma transação pelo seu código
         *
         * @param  string  $code
         * @return array
         * @throws Exception
         */
        public function getTransaction($code)
        {
            $result = Code::search(
                Payment::getCredentials(),
                $code
            );

            if($result != null) {

                $transacao = [
                    'transaction' => [
                        'items' => [
                            'item' => [
                                'id'        => $result->getReference(),
                                'amount'    => $result->getGrossAmount()
                            ]
                        ],
                        'status'    => $result->getStatus(),
                        'date'      => $result->getDate()
                    ]
                ];

                return $transacao;
            }

            throw new Exception('Não foi encontrada transação com o código passado');
        }
    }
