<?php

    namespace CSPlus\Services\Generators;

    use Zend\Barcode\Object\Code25interleaved;
    use Zend\Barcode\Barcode as ZendBarCode;

    /**
     * BarCode
     *
     * Realiza a renderização de códigos de barras, utilizando a ferramenta
     * Zend BarCode, no padrão 2 of 5 interleaved
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 2.0.0
     * @since   16/01/2017
     */
    class BarCode
    {
        /**
         * @param   int  $number
         * @return  string
         */
        public function generate($number)
        {
            // Opções do código de barras
            $barOptions = [
                'text'      => $number,
                'drawText'  => false
            ];

            // Opções de renderização
            $renderOptions = [
                'imageType' => 'jpg'
            ];

            // Realiza a renderização utilizando os componentes da zend
            $barcode    = new Code25interleaved($barOptions, $renderOptions);

            $renderer = ZendBarCode::factory($barcode)->draw();

            // Salva a imagem em disco
            $file = storage_path('app/barcodes/barcode.jpg');
            imagejpeg($renderer, $file, 100);

            // Recebe o conteúdo e o codifica em base 64
            $image = file_get_contents($file);
            $image = base64_encode($image);

            // Destroi a referencia da imagem e a própria em disco
            imagedestroy($renderer);
            unlink($file);

            return $image;
        }
    }
