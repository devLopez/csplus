<?php

    namespace CSPlus\Services\Generators\Providers;

    use Illuminate\Support\ServiceProvider;
    use CSPlus\Services\Generators\BarCode;

    /**
     * BarCodeServiceProvider
     *
     * Registra o Service Provider do BarCode
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 2.0.0
     * @since   16/01/2017
     */
    class BarCodeServiceProvider extends ServiceProvider
    {
        /**
         * Bootstrap the application services.
         *
         * @return void
         */
        public function boot()
        {
            //
        }

        /**
         * Register the application services.
         *
         * @return void
         */
        public function register()
        {
            $this->app->singleton('barcode', function() {
                return new BarCode();
            });
        }
    }
