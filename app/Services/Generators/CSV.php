<?php

    namespace CSPlus\Services\Generators;

    use League\Csv\Writer;

    /**
     * CSV
     *
     * Realiza a criação de documentos separados por vírgula
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 2.1.0
     * @since   03/08/2017
     */
    class CSV
    {
        /**
         * @var \League\Csv\Writer
         */
        protected $csv;

        public function __construct()
        {
            $this->csv = Writer::createFromFileObject(new \SplTempFileObject());
        }

        /**
         * @param   array  $data
         * @param   array  $header
         * @return  $this
         */
        public function generate(array $data, array $header)
        {
            $this->csv->insertOne($header);
            $this->csv->insertAll($data);

            return $this;
        }

        /**
         * @param   string  $filename
         * @return  mixed
         */
        public function download($filename = 'relatorio.csv')
        {
            return $this->csv->output($filename);
        }
    }
