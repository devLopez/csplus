<?php

    namespace CSPlus\Services\Generators;

    use App;
    use Illuminate\Http\Response;

    /**
     * PDF
     *
     * Classe desenvolvida para geração de documentos PDF
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 3.0.0
     * @since   15/06/2017
     */
    class PDF
    {
        /**
         * @var \Barryvdh\Snappy\PdfWrapper
         */
        protected $pdf;

        public function __construct(array $options = [], $orientation = 'portrait')
        {
            $this->pdf = App::make('snappy.pdf.wrapper');

            $this->setOptions($options)->setOrientation($orientation);
        }

        /**
         * @param   array  $options
         * @return  $this
         */
        public function setOptions($options)
        {
            $this->pdf->setOptions($options);

            return $this;
        }

        /**
         * @param   string  $orientation
         * @return  $this
         */
        public function setOrientation($orientation)
        {
            $this->pdf->setOrientation($orientation);

            return $this;
        }

        /**
         * Realiza o carregamento de um arquivo pdf
         *
         * @param   string  $view
         * @param   array  $data
         * @param   bool  $download
         * @param   string  $nome
         * @return  \Illuminate\Http\Response
         */
        public function load($view, $data = [], $download = false, $nome = 'relatorio.pdf')
        {
            $pdf        = $this->pdf->loadView($view, $data);
            $download   = ($download) ? 'inline' : 'attachment';

            return new Response($pdf->output(), 200, [
                'Content-Type' => 'application/pdf',
                'Content-Disposition' => $download.'; filename="'.$nome.'"',
            ]);
        }
    }
