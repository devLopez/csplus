<?php namespace CSPlus\Services\Generators\Facades;

    use Illuminate\Support\Facades\Facade;

    /**
     * BarCodeFacade
     *
     * Define a facade para os serviços de impressão de códigos de barras
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 2.0.0
     * @since   16/01/2017
     */
    class BarCodeFacade extends Facade
    {
        /**
         * @return string
         */
        public static function getFacadeAccessor()
        {
            return 'barcode';
        }
    }
