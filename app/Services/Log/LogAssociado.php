<?php

    namespace CSPlus\Services\Log;

    use CSPlus\Services\Log\Contracts\MemberLogContract;

    /**
     * LogAssociado
     *
     * Classe desenvolvida para geração de dos comandos de Log do sistema
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 4.1.0
     * @since   25/04/2017
     */
    class LogAssociado implements MemberLogContract
    {
        /**
         * @var array
         */
        protected $novosDados;

        /**
         * @var array
         */
        protected $dadosAntigos;

        /**
         * @var int
         */
        protected $associado;

        /**
         * @var array
         */
        protected $log;

        /**
         * @var string
         */
        protected $observacao = '';

        /**
         * @var array
         */
        protected $dadosModificados = [];

        /**
         * @var bool
         */
        protected $possuiDadosAlterados = true;

        /**
         * Recebe o LineFeed a ser aplicado
         *
         * @var string
         */
        protected $CRLF = '||ASCII_CHAR(13)||ASCII_CHAR(10)||';

        /**
         * Recebe os campos que serão editados e suas mensagens que serão inseridas
         * na tabela de logs
         *
         * @var array
         */
        protected $fields = [
            // Dados Pessoais
            'NOMEDOTITULAR'         => "'Nome antigo: %s'%s'Novo Nome: %s'%s",
            'DATAAQUISICAO'         => "'Aquisição Antiga: %s'%s'Nova Aquisição: %s'%s",
            'APELIDO'               => "'Apelido antigo: %s'%s'Novo Apelido: %s'%s",
            'CATEGORIADACOTA'       => "'Categoria da Cota antiga: %s'%s'Nova categoria: %s'%s",
            'CEPRESIDENCIAL'        => "'Cep Residencial antigo: %s'%s'Cep novo: %s'%s",
            'ENDRES_LOGRADOURO'     => "'Logradouro antigo: %s'%s'Novo Logradouro: %s'%s",
            'ENDRES_NUMERO'         => "'Numero residencial antigo: %s'%s'Novo numero: %s'%s",
            'ENDRES_COMPLEMENTO'    => "'Complemento residencial antigo: %s'%s'Novo complemento: %s'%s",
            'BAIRRORESIDENCIAL'     => "'Bairro residencial antigo: %s'%s'Novo bairro: %s'%s",
            'CIDADERESIDENCIAL'     => "'Cidade residencial antiga: %s'%s'Nova cidade: %s'%s",
            'ESTADORESIDENCIAL'     => "'Estado residencial antigo: %s'%s'Novo estado: %s'%s",
            'TELEFONER1'            => "'Telefone residencial 1 antigo: %s'%s'Novo telefone 1: %s'%s",
            'TELEFONER2'            => "'Telefone residencial 2 antigo: %s'%s'Novo telefone 2: %s'%s",
            'CAIXAPOSTAL'           => "'Caixa postal antiga: %s'%s'Nova caixa postal: %s'%s",
            'DATANASCIMENTO'        => "'Data de nascimento antiga: %s'%s'Nova data de nascimento: %s'%s",
            'RG'                    => "'RG antigo: %s'%s'Novo RG: %s'%s",
            'RG_ORGAO_EXPEDIDOR'    => "'Órgão Expedidor antigo: %s'%s'Novo órgão expedidor: %s'%s",
            'RG_DATA_EXPEDICAO'     => "'Data de expedição antiga: %s'%s'Nova data de expedição: %s'%s",
            'SEXO'                  => "'Sexo antigo: %s'%s'Novo sexo: %s'%s",
            'ESTADOCIVIL'           => "'Estado Civil antigo: %s'%s'Novo estado civil: %s'%s",
            'OBSERVACOES'           => "'Observação antiga: %s'%s'Nova observação: %s'%s",
            // Dados Profissionais
            'EMPRESA'               => "'Empresa antiga: %s'%s'Nova empresa: %s'%s",
            'FUNCAO'                => "'Funcao antiga: %s'%s'Nova funcao: %s'%s",
            'DEPARTAMENTO_EMPRESA'  => "'Departamento antigo: %s'%s'Novo Departamento: %s'%s",
            'MATRICULA_EMPRESA'     => "'Matrícula antiga: %s'%s'Nova matrícula: %s'%s",
            'REGISTRO_CLASSE'       => "'Registro de classe antigo: %s'%s'Novo registro de classe: %s'%s",
            'SITUACAO_EMPRESA'      => "'Situação empresa antiga: %s'%s'Nova situação: %s'%s",
            'COMPLEMENTOCOMERCIAL'  => "'Complemento Comercial antigo: %s'%s'Novo complemento comercial: %s'%s",
            'ADMISSAO_EMPRESA'      => "'Admissão antiga: %s'%s'Nova admissão: %s'%s",
            'AFASTAMENTO_EMPRESA'   => "'Afastamento antigo: %s'%s'Novo afastamento: %s'%s",
            'CAIXAPOSTAL_EMPRESA'   => "'E-mail antigo: %s'%s'Novo email: %s'%s",
            'TELEFONEC1'            => "'Telefone comercial antigo: %s'%s'Novo telefone: %s'%s",
            'TELEFONEC2'            => "'Telefone comercial antigo: %s'%s'Novo telefone : %s'%s",
            'CEPCOMERCIAL'          => "'CEP comercial antigo: %s'%s'Novo CEP: %s'%s",
            'ENDCOM_LOGRADOURO'     => "'Logradouro comercial antigo: %s'%s'Novo logradouro: %s'%s",
            'ENDCOM_NUMERO'         => "'Numero comercial antigo: %s'%s'Novo numero: %s'%s",
            'ENDCOM_COMPLEMENTO'    => "'Complemento comercial antigo: %s'%s'Novo complemento: %s'%s",
            'BAIRROCOMERCIAL'       => "'Bairro comercial antigo: %s'%s'Novo bairro: %s'%s",
            'CIDADECOMERCIAL'       => "'Cidade comercial antiga: %s'%s'Nova cidade: %s'%s",
            'ESTADOCOMERCIAL'       => "'Estado comercial antigo: %s'%s'Novo estado: %s'%s",
            'ENDERECOCOBRANCA'      => "'Endereco de cobranca antiga: %s'%s'Novo endereco de cobranca : %s'%s",
            'DESCONTOFOLHA'         => "'Desconto em folha antigo: %s'%s'Novo desconto em folha: %s'%s",
            'APOSENTADO'            => "'Aposentado antigo: %s'%s'Novo aposentado: %s'%s",
            // Dados Complementares
            'NACIONALIDADE'         => "'Nacionalidade antiga: %s'%s'Nova nacionalidade: %s'%s",
            'NATURALIDADE'          => "'Naturalidade antiga: %s'%s'Nova naturalizada: %s'%s",
            'PAI'                   => "'Pai antigo: %s'%s'Novo pai: %s'%s",
            'MAE'                   => "'Mãe antiga: %s'%s'Nova mãe: %s'%s",
            'SANGUE'                => "'Tipo Sanguíneo antigo: %s'%s'Novo tipo sanguíneo: %s'%s",
            'RESPONSAVEL'           => "'Responsável antigo: %s'%s'Novo responsável: %s'%s",
            'CORRETOR'              => "'Corretor antigo: %s'%s'Novo corretor: %s'%s",
            'BENEMERITO'            => "'Benemérito antigo: %s'%s'Novo benemérito: %s'%s",
            // Dados Bancários
            'BANCO'                 => "'Banco antigo: %s'%s'Novo banco: %s'%s",
            'AGENCIA'               => "'Agencia antiga: %s'%s'Nova agência: %s'%s",
            'TIPOCONTA'             => "'Tipo de conta antiga: %s'%s'Novo tipo: %s'%s",
            'CONTACORRENTE'         => "'Conta antiga: %s'%s'Nova conta: %s'%s",
            'DIGITOCC'              => "'Dígito Conta antigo: %s'%s'Novo dígito: %s'%s",
            'DEBITOCONTAAPROVADO'   => "'Débito automático antigo: %s'%s'Novo débito automático: %s'%s",
        ];

        /**
         * @param   int  $associado
         * @param   array  $novosDados
         * @param   array  $dadosAntigos
         */
        public function __construct($associado, $novosDados, $dadosAntigos)
        {
            $this->associado    = $associado;
            $this->novosDados   = $novosDados;
            $this->dadosAntigos = array_map('utf8_encode', $dadosAntigos);

            $this->setDadosModificados()
                 ->setObservacaoLog()
                 ->setLog();
        }

        /**
         * @return  array
         */
        public function getLog()
        {
            return array_map('utf8_decode', $this->log);
        }

        /**
         * @return  array
         */
        public function getDadosModificados()
        {
            return array_map('utf8_decode', $this->dadosModificados);
        }

        /**
         * @return  $this
         */
        public function setLog()
        {
            $this->log = [
                'COTA'          => $this->associado,
                'DATAHORA'      => date('d.m.Y, H:i:s', time()),
                'DATA'          => date('d.m.Y', time()),
                'HORA'          => date('H:i:s', time()),
                'SITUACAO'      => 'Alteracao CSPlus WEB',
                'OBSERVACAO'    => $this->observacao
            ];

            return $this;
        }

        /**
         * Verifica, juntamente com os dados antigos se algum dado foi realmente
         * modificado. e os separa em um array
         *
         * @return  $this
         */
        public function setDadosModificados()
        {
            foreach($this->novosDados as $key => $value) {
                if($this->dadosAntigos[$key] != $value) {
                    $this->dadosModificados[$key] = $value;
                }
            }

            return $this;
        }

        /**
         * Seta as Observações do Log, de acordo com os novos valores
         *
         * @return  $this
         */
        public function setObservacaoLog()
        {
            if($this->dadosModificados) {
                foreach($this->dadosModificados as $key => $value) {
                    $this->observacao .= sprintf(
                        $this->fields[$key],
                        $this->dadosAntigos[$key],
                        $this->CRLF,
                        $value,
                        $this->CRLF
                    );
                }

                // Remove o último CRLF da string, para evitar problemas na hora de salvar os dados
                $this->observacao = substr_replace($this->observacao, '', -34);
            } else {
                $this->possuiDadosAlterados = false;
            }

            return $this;
        }

        /**
         * @return bool
         */
        public function hasAlteracao()
        {
            return $this->possuiDadosAlterados;
        }
    }
