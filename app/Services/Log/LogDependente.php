<?php

    namespace CSPlus\Services\Log;

    use CSPlus\Services\Log\Contracts\MemberLogContract;

    /**
     * LogDependente
     *
     * Classe desenvolvida para realizar o gerenciamento dos Logs de alterações
     * em dados dos dependentes
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 6.0.0
     * @since   13/06/2017
     */
    class LogDependente implements MemberLogContract
    {
        /**
         * @var array
         */
        protected $novosDados;

        /**
         * @var array
         */
        protected $dadosAntigos;

        /**
         * @var int
         */
        protected $associado;

        /**
         * @var array
         */
        protected $log;

        /**
         * @var string
         */
        protected $observacao = '';

        /**
         * @var bool
         */
        protected $hasDadosModificados = true;

        /**
         * Recebe somente os dados que foram modificados, em comparação aos que
         * já estão gravados na base de dados
         *
         * @var array
         */
        protected $dadosModificados = [];

        /**
         * Recebe o LineFeed a ser aplicado
         *
         * @var string
         */
        protected $CRLF = '||ASCII_CHAR(13)||ASCII_CHAR(10)||';

        /**
         * Recebe os campos que serão editados e suas mensagens que serão inseridas
         * na tabela de logs
         *
         * @var array
         */
        protected $fields = [
            'NOMEDEPENDENTE'        => "'Nome antigo: %s'%s'Novo Nome: %s'%s",
            'CIC'                   => "'CIC antigo: %s'%s'CIC novo: %s'%s",
            'RG'                    => "'RG antigo: %s'%s'Novo RG: %s'%s",
            'RG_DATA_EXPEDICAO'     => "'Data de expedição antiga: %s'%s'Nova data de expedição: %s'%s",
            'RG_ORGAO_EXPEDIDOR'    => "'Órgão expedidor antigo: %s'%s'Novo órgão expedidor: %s'%s",
            'FUNCAO'                => "'Profissão antiga: %s'%s'Nova profissão: %s'%s",
            'PAI'                   => "'Pai antigo: %s'%s'Novo pai: %s'%s",
            'MAE'                   => "'Mãe antiga: %s'%s'Nova mãe: %s'%s",
            'TELEFONE'              => "'Telefone antigo: %s'%s'Novo telefone: %s'%s",
            'TELEFONE2'             => "'Telefone 2 antigo: %s'%s'Novo telefone: %s'%s",
            'CAIXAPOSTAL'           => "'E-mail antigo: %s'%s'Novo e-mail: %s'%s",
            'SEXO'                  => "'Sexo antigo: %s'%s'Novo Sexo: %s'%s",
            'ESTADOCIVIL'           => "'Estado civil antigo: %s'%s'Novo estado civil: %s'%s",
            'DATANASCIMENTO'        => "'Data de nascimento antiga: %s'%s'Novo nascimento: %s'%s",
            'GRAUPARENTESCO'        => "'Parentesco antigo: %s'%s'Novo parentesco: %s'%s"
        ];

        /**
         * @param   int  $associado
         * @param   array  $novosDados
         * @param   array  $dadosAntigos
         */
        public function __construct($associado, $novosDados, $dadosAntigos)
        {
            $this->associado    = $associado;
            $this->novosDados   = $novosDados;
            $this->dadosAntigos = array_map('utf8_encode', $dadosAntigos);

            $this->observacao .= "'Alteracao dos dados do Dependente: {$dadosAntigos['NOMEDEPENDENTE']}'{$this->CRLF}";

            $this->setDadosModificados()
                 ->setObservacaoLog()
                 ->setLog();
        }

        /**
         * @return  array
         */
        public function getLog()
        {
            return $this->log;
        }

        /**
         * @return  array
         */
        public function getDadosModificados()
        {
            return $this->dadosModificados;
        }

        /**
         * @return  $this
         */
        public function setLog()
        {
            $this->log = [
                'COTA'          => $this->associado,
                'DATAHORA'      => date('d.m.Y, H:i:s', time()),
                'DATA'          => date('d.m.Y', time()),
                'HORA'          => date('H:i:s', time()),
                'SITUACAO'      => 'Alteracao CSPlus WEB',
                'OBSERVACAO'    => $this->observacao
            ];

            return $this;
        }

        /**
         * Realiza a comparação entre os dados gravados e os novos dados e seleciona
         * somente os dados modificados
         *
         * @return  $this
         */
        public function setDadosModificados()
        {
            foreach($this->novosDados as $key => $value) {
                if($this->dadosAntigos[$key] != $value) {
                    $this->dadosModificados[$key] = $value;
                }
            }

            return $this;
        }

        /**
         * Verifica os campos que foram alterados e monta a mensagem de observação
         * a ser inserida no Log
         *
         * @return  $this
         */
        public function setObservacaoLog()
        {
            if($this->dadosModificados) {
                foreach ($this->dadosModificados as $key => $value) {
                    $this->observacao .= sprintf(
                        $this->fields[$key],
                        $this->dadosAntigos[$key],
                        $this->CRLF,
                        $value,
                        $this->CRLF
                    );
                }
                // Remove o último CRLF da string, para evitar problemas na hora de salvar os dados
                $this->observacao = substr_replace($this->observacao, '', -34);

            } else {
                $this->hasDadosModificados = false;
            }

            return $this;
        }

        /**
         * @return bool
         */
        public function hasAlteracao()
        {
            return $this->hasDadosModificados;
        }
    }
