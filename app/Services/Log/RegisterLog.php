<?php

    namespace CSPlus\Services\Log;

    use Auth;
    use Exception;
    use Log;
    use Session;

    use CSPlus\Services\Log\Contracts\MemberLogContract;

    /**
     * RegisterLog
     *
     * Trait desenvolvido para que seja possível setar os logs com mais efeiciência
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 2.2.0
     * @since   08/07/2017
     */
    trait RegisterLog
    {
        /**
         * @param   int  $matricula
         * @param   string  $arquivo
         */
        public function setLog($matricula, $arquivo)
        {
            Session::now('matricula', $matricula);
            Session::now('arquivo', $arquivo);

            event('database.operation');
        }

        /**
         * Registra o log dos novos dados
         *
         * @param MemberLogContract $log
         */
        public function logNewData(MemberLogContract $log)
        {
            if(!Auth::isAdmin()) {
                LogMember::run($log);
            }
        }

        /**
         * @param   Exception  $e
         */
        public function logError(Exception $e)
        {
            Log::error($e->getMessage(), [
                $e->getLine(), $e->getTrace()]
            );
        }
    }
