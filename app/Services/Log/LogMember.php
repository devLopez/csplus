<?php

    namespace CSPlus\Services\Log;

    use CSPlus\Models\CSPlus\Contatos;
    use CSPlus\Services\Log\Contracts\MemberLogContract;

    /**
     * LogMember
     *
     * CLasse Responsável por salvar o Log de alteração na base de dados
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 1.0.0
     * @since   13/06/2017
     */
    class LogMember
    {
        /**
         * Roda a procedure que salva o log na base
         *
         * @param   MemberLogContract  $log
         */
        public static function run(MemberLogContract $log)
        {
            try {
                if($log->hasAlteracao()) {
                    Contatos::saveLog(
                        $log->getLog()
                    );
                }

            } catch (Exception $e) {
                Log::alert($e->getMessage(), [
                    'trace' => $e->getTrace()
                ]);
            }
        }
    }