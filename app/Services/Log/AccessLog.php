<?php

    namespace CSPlus\Services\Log;

    use Log;

    use CSPlus\Services\Support\Date;
    use CSPlus\Repositories\ContatosRepository as Logs;
    use Illuminate\Http\Request;

    /**
     * AccessLog
     *
     * Realiza a criação de logs de acesso para usuários (associados)
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 1.0.0
     * @since   21/06/2017
     */
    class AccessLog
    {
        /**
         * @var Logs
         */
        protected $logs;

        /**
         * @var Date
         */
        protected $date;

        /**
         * @var Request
         */
        protected $request;

        /**
         * @param   Logs  $logs
         * @param   Request  $request
         * @param   Date  $date
         */
        public function __construct(Logs $logs, Request $request, Date $date)
        {
            $this->logs     = $logs;
            $this->request  = $request;
            $this->date     = $date;
        }

        /**
         * @param   int  $matricula
         */
        public function generate($matricula)
        {
            try {
                $this->logs->save([
                    'COTA'          => $matricula,
                    'DATAHORA'      => $this->date->timestamp('Y-m-d H:i:s'),
                    'DATA'          => $this->date->today('Y-m-d'),
                    'HORA'          => $this->date->time(),
                    'SITUACAO'      => 'Acesso WEB',
                    'OBSERVACAO'    => 'IP associado: ' . $this->request->ip()
                ]);

            } catch (\Exception $e) {
                Log::notice('Erro ao registrar o log de acesso', ['erro' => $e->getMessage()]);
            }
        }
    }