<?php

    namespace CSPlus\Services\Log\Contracts;

    /**
     * LogContract
     *
     * Interface que realiza a padronização dos métodos de logs administrativos
     * do sistema csplus
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 5.0.0
     * @since   13/06/2017
     */
    interface MemberLogContract
    {
        /**
         * @return  array
         */
        public function getLog();

        /**
         * @return  array
         */
        public function getDadosModificados();

        /**
         * @return  $this
         */
        public function setLog();

        /**
         * @return  $this
         */
        public function setDadosModificados();

        /**
         * @return mixed
         */
        public function setObservacaoLog();

        /**
         * @return  bool
         */
        public function hasAlteracao();
    }
