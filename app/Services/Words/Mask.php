<?php

    namespace CSPlus\Services\Words;

    use JansenFelipe\Utils\Utils;
    use JansenFelipe\Utils\Mask as MaskDefinitions;

    /**
     * Mask
     *
     * Realiza a criação de máscaras para uma string
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 1.0.0
     * @since   05/05/2017
     */
    class Mask
    {
        /**
         * Realiza a criação da máscara para strings diversas
         *
         * @param   string  $txt
         * @param   string  $mascara
         * @return  string
         */
        public static function mask($txt, $mascara)
        {
            if ($mascara == MaskDefinitions::TELEFONE) {
                $mascara = strlen($txt) == 10 ? '(##) ####-####' : '(##) #####-####';
            }

            if ($mascara == MaskDefinitions::DOCUMENTO) {
                if (strlen($txt) == 11) {
                    $mascara = MaskDefinitions::CPF;
                } elseif (strlen($txt) == 14) {
                    $mascara = MaskDefinitions::CNPJ;
                } else {
                    return $txt;
                }
            }

            if (empty($txt))
                return '';

            $txt = Utils::unmask($txt);
            $qtd = 0;
            for ($x = 0; $x < strlen($mascara); $x++) {
                if ($mascara[$x] == "#")
                    $qtd++;
            }

            if ($qtd > strlen($txt)) {
                $txt = str_pad($txt, $qtd, "0", STR_PAD_LEFT);
            } elseif ($qtd < strlen($txt)) {
                return $txt;
            }

            if ($txt <> '') {
                $string = str_replace(" ", "", $txt);

                for ($i = 0; $i < strlen($string); $i++) {
                    $pos = strpos($mascara, "#");

                    $mascara[$pos] = $string[$i];
                }

                return $mascara;
            }

            return $txt;
        }

        /**
         * Remove a máscara de uma string
         *
         * @param   string  $txt
         * @return  string
         */
        public static function unmask($txt)
        {
            return Utils::unmask($txt);
        }
    }
