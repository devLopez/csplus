<?php

if(!function_exists('br_timestamp')) {
    /**
     * Converte um timestamp para o timestamp brasileiro
     *
     * @param   string  $data
     * @return  string
     */
    function br_timestamp($data)
    {
        return ($data) ? date('d/m/Y H:i:s', strtotime($data)) : '';
    }
}

if(!function_exists('field_attr')) {
    /**
     * Define os atributos de um campo, de acordo o acesso do usuario
     *
     * @param   $name
     * @param   bool  $array
     * @param   array  $extra
     * @return  array|string
     */
    function field_attr($name, $array = false, $extra = [])
    {
        $isAdmin    = \Policy::isAdmin();
        $fields     = session('usuario.propriedades');

        $adminAccess    = "class='form-control edit' readonly='readonly'";
        $userAccess     = "class='form-control' disabled='disabled'";

        if($array) {
            $adminAccess    = array_merge(['readonly', 'class' => 'form-control edit'], $extra);
            $userAccess     = array_merge(['disabled', 'class' => 'form-control'], $extra);
        }

        if(!$isAdmin && array_key_exists($name, $fields)) {
            return $userAccess;
        }

        return $adminAccess;
    }
}

if(!function_exists('format_money') || !function_exists('money2br')) {
    /**
     * Formata um número inteiro para o formato float
     *
     * @param   mixed  $money
     * @return  string
     */
    function format_money($money)
    {
        return number_format($money, 2, '.' , '');
    }

    /**
     * Converte um valor float para a moeda brasileira
     *
     * @param   mixed  $money
     * @return  string
     */
    function money2br($money)
    {
        return number_format($money, 2, ',' , '.');
    }
}

if(!function_exists('mask_phone')) {
    /**
     * Insere a máscara em um número de telefone
     *
     * @param  string  $phone
     * @return string
     */
    function mask_phone($phone)
    {
        $phone_mask = JansenFelipe\Utils\Mask::TELEFONE;
        return \JansenFelipe\Utils\Utils::mask($phone, $phone_mask);
    }
}

if(!function_exists('meses_br')) {
    /**
     * Retorna o nome dos mêses em português
     *
     * @return array
     */
    function meses_br()
    {
        return [
            '01'    => 'Janeiro',
            '02'    => 'Fevereiro',
            '03'    => 'Março',
            '04'    => 'Abril',
            '05'    => 'Maio',
            '06'    => 'Junho',
            '07'    => 'Julho',
            '08'    => 'Agosto',
            '09'    => 'Setembro',
            '10'    => 'Outubro',
            '11'    => 'Novembro',
            '12'    => 'Dezembro',
        ];
    }
}

if(!function_exists('expiration_year')) {
    /**
     * Retorna um array com anos de expiração de cartões
     *
     * @return array
     */
    function expiration_year()
    {
        $year = [];

        for($y = date('Y'); $y <= date('Y', strtotime('+ 15 years')); $y++) {
            $year[$y] = $y;
        }

        return $year;
    }
}
