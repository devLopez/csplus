<?php

    namespace CSPlus\Services\Support;

    use Carbon\Carbon;

    /**
     * Date
     *
     * Classe responsável pelo gerenciamento de datas no sistema
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 1.2.0
     * @since   21/06/2017
     */
    class Date
    {
        /**
         * @param   string  $format
         * @return  string
         */
        public function lastDayMonth($format = 'd/m/Y')
        {
            $date = new Carbon('last day of this month');

            return $date->format($format);
        }

        /**
         * @param   string  $format
         * @return  string
         */
        public function today($format = 'd/m/Y')
        {
            return Carbon::today()->format($format);
        }

        /**
         * @param   string  $date
         * @return  string
         */
        public function toBr($date)
        {
            if($date) {
                return Carbon::parse($date)->format('d/m/Y');
            }

            return '';
        }

        /**
         * @param   string  $date
         * @return  string
         */
        public function toSql($date)
        {
            if($date) {
                return Carbon::createFromFormat('d/m/Y', $date)->format('Y-m-d');
            }

            return '';
        }

        /**
         * @param   string  $date
         * @param   string  $format
         * @return  string
         */
        public function format($date, $format = 'd/m/Y')
        {
            if($date) {
                return Carbon::parse($date)->format($format);
            }

            return '';
        }

        /**
         * @param   string  $format
         * @return  string
         */
        public function time($format = 'H:i:s')
        {
            return $this->now($format);
        }

        /**
         * @param   string  $format
         * @return  string
         */
        public function timestamp($format = 'd/m/Y H:i:s')
        {
            return $this->now($format);
        }

        /**
         * @param   string  $format
         * @return  string
         */
        private function now($format)
        {
            return Carbon::now()->format($format);
        }
    }
