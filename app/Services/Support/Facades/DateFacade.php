<?php

    namespace CSPlus\Services\Support\Facades;

    use Illuminate\Support\Facades\Facade;

    /**
     * DateFacade
     *
     * Facade responsável pelo service de date
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 1.0.0
     * @since   17/05/2017
     */
    class DateFacade extends Facade
    {
        /**
         * @return string
         */
        public static function getFacadeAccessor()
        {
            return 'date';
        }
    }
