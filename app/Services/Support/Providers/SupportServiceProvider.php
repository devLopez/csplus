<?php

    namespace CSPlus\Services\Support\Providers;

    use CSPlus\Services\Support\Date;
    use Illuminate\Support\ServiceProvider;

    class SupportServiceProvider extends ServiceProvider
    {
        public function boot()
        {
            //
        }

        public function register()
        {
            $this->app->singleton('date', function() {
                return new Date;
            });
        }
    }
