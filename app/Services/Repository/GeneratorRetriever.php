<?php

    namespace CSPlus\Services\Repository;

    /**
     * GeneratorRetriever
     *
     * Trait desenvolvida para implementação do método do contract ModelContract
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 1.0.0
     * @since   21/06/2017
     */
    trait GeneratorRetriever
    {
        /**
         * @return  string
         */
        public function getGenerator()
        {
            return $this->generator;
        }
    }