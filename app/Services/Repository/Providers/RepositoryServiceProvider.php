<?php

    namespace CSPlus\Services\Repository\Providers;

    use Illuminate\Support\ServiceProvider;

    use Illuminate\Contracts\Support\Arrayable;
    use Illuminate\Contracts\Container\Container as ContainerContract;
    use Illuminate\Support\Collection;
    use Illuminate\Container\Container;

    /**
     * RepositoryServiceProvider
     *
     * Registra o provedor de serviços do repositório
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 1.0.0
     * @since   04/02/2017
     * @package CSPlus\Services\Repository\Providers
     */
    class RepositoryServiceProvider extends ServiceProvider
    {
        /**
         * Registra os parâmetros do provider
         *
         * @return  void
         */
        public function register()
        {
            $this->registerBindings();
        }

        /**
         * Registra o binding das classe
         *
         * @return  void
         */
        private function registerBindings()
        {
            $this->app->bind(Arrayable::class, Collection::class);
            $this->app->bind(ContainerContract::class, Container::class);
        }
    }