<?php

    namespace CSPlus\Services\Repository;

    use CSPlus\Services\Repository\Exceptions\ModelNotSavedException;

    /**
     * FirebirdOperations
     *
     * Trait responsável por gereciar operações especícas do banco de dados Firebird
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 1.0.0
     * @since   24/06/2017
     */
    trait FirebirdOperations
    {
        /**
         * @return  string
         */
        protected function getModelDriver()
        {
            return $this->model->getConnection()->getConfig('driver');
        }

        /**
         * @param   array $data
         * @return  \Illuminate\Database\Eloquent\Model
         * @throws  ModelNotSavedException
         */
        public function firebirdSave(array $data)
        {
            $data = array_filter(array_map('utf8_decode', $data));
            $data = array_map('trim', $data);

            // Recebe o nome da chave primária do model
            $primaryKey                 = $this->model->getKeyName();
            $this->model->$primaryKey   = $this->getId();

            foreach ($data as $k => $v) {
                $this->model->$k = $v;
            }

            if($this->model->save()) {
                return $this->model;
            }

            throw new ModelNotSavedException;
        }
    }