<?php

    namespace CSPlus\Services\Repository\Exceptions;

    use Exception;

    /**
     * ModelNotSavedException
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 1.0.0
     * @since   01/02/2016
     * @package CSPlus\Services\Repository\Exceptions
     */
    class ModelNotSavedException extends Exception
    {
        /**
         * @var string
         */
        protected $message = 'Não foi possível salvar os dados. Tente novamente';
    }