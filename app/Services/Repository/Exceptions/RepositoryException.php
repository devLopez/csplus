<?php

    namespace CSPlus\Services\Repository\Exceptions;

    use Exception;

    /**
     * RepositoryException
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 1.0.0
     * @since   01/02/2016
     * @package CSPlus\Services\Repository\Exceptions
     */
    class RepositoryException extends Exception
    {

    }