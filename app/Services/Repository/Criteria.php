<?php

    namespace CSPlus\Services\Repository;

    /**
     * Criteria
     *
     * define a diretriz para o funcionamento do uso de Criterias
     * pelo sistema
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 1.0.0
     * @since   04/02/2017
     * @package CSPlus\Services\Repository
     */
    abstract class Criteria
    {
        /**
         * Apply a criteria on a model
         *
         * @param   mixed  $model
         * @param   Repository  $repository
         * @return  mixed
         */
        public abstract function apply($model, Repository $repository);
    }