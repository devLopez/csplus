<?php

	namespace CSPlus\Services\Repository;

	use CSPlus\Services\Repository\Contracts\RepositoryContract;
	use CSPlus\Services\Repository\Contracts\CriteriaContract;

	use CSPlus\Services\Repository\Exceptions\ModelNotSavedException;
	use CSPlus\Services\Repository\Exceptions\RepositoryException;

	use Illuminate\Contracts\Container\Container;
	use Illuminate\Contracts\Support\Arrayable;
	use Illuminate\Database\Eloquent\Model;

	use CSPlus\Models\CSPlus\Generator;

	/**
	 * Repository
	 *
	 * Classe responsável pelo gerenciamento dos repositórios do sistema
	 *
	 * @author	Matheus Lopes Santos <fale_com_lopez@hotmail.com>
	 * @version	1.2.0
	 * @since	24/06/2017
	 * @package CSPlus\Services\Repository
	 */
	abstract class Repository implements RepositoryContract, CriteriaContract
	{
	    use FirebirdOperations;

		/**
		 * @var \Illuminate\Contracts\Container\Container
		 */
		protected $app;

        /**
         * @var \Illuminate\Contracts\Support\Arrayable
         */
		protected $criteria;

		/**
		 * @var	\Illuminate\Database\Eloquent\Model
		 */
		protected $model;

		/**
		 * @var bool
		 */
		protected $skipCriteria = false;

		/**
		 * @var bool
		 */
		protected $preventCriteriaOverwriting = true;

        /**
         * @param   Container  $container
         * @param   Arrayable  $arrayable
         */
		public function __construct(Container $container, Arrayable $arrayable)
		{
			$this->app      = $container;
			$this->criteria = $arrayable;

			$this->resetScope();
			$this->makeModel();
		}

		/**
		 * @return	Model
		 */
		public abstract function model();

		/**
		 * @param   array  $columns
		 * @return  \Illuminate\Support\Collection
		 */
		public function all($columns = ['*'])
		{
			$this->applyCriteria();

			return $this->model->get($columns);
		}

		/**
		 * @param   array  $relations
		 * @return  $this
		 */
		public function with(array $relations)
		{
			$this->model = $this->model->with($relations);

			return $this;
		}

		/**
		 * @param   string  $value
		 * @param   string|null $key
		 * @return  \Illuminate\Database\Eloquent\Collection
		 */
		public function pluck($value, $key = null)
		{
			$this->applyCriteria();

			$lists = $this->model->pluck($value, $key);

			if (is_array($lists)) {
				return $lists;
			}

			return $lists->all();
		}

		/**
		 * @param   integer  $perPage
		 * @param   array  $columns
		 * @return  mixed
		 */
		public function paginate($perPage = 15, $columns = array('*'))
		{
			$this->applyCriteria();

			return $this->model->paginate($perPage, $columns);
		}

		/**
		 * @param 	array  $data
		 * @return 	\Illuminate\Database\Eloquent\Model
		 * @throws 	ModelNotSavedException
		 */
		public function save(array $data)
		{
		    if($this->getModelDriver() == 'firebird') {
                return $this->firebirdSave($data);
            }

            foreach ($data as $k => $v) {
                $this->model->$k = $v;
            }

            if($this->model->save()) {
                return $this->model;
            }

            throw new ModelNotSavedException;
		}

		/**
		 * @param 	array  $data
		 * @return 	bool
		 * @throws 	ModelNotSavedException
		 */
		public function insert(array $data)
		{
			if($this->model->insert($data)) {
				return true;
			}

			throw new ModelNotSavedException('Não foi possível salvar alguns registros. Tente novamente');
		}

		/**
		 * @param 	int $id
		 * @param 	array $data
		 * @return 	\Illuminate\Database\Eloquent\Model
		 * @throws 	ModelNotSavedException
		 */
		public function update($id, array $data)
		{
			$model = $this->find($id);

			foreach ($data as $k => $v) {
				$model->$k = $v;
			}

			if($model->save()) {
				return $model;
			}

			throw new ModelNotSavedException;
		}

		/**
		 * @param 	int  $id
		 * @return 	bool
		 */
		public function delete($id)
		{
			$model = $this->find($id);

			return $model->destroy($id);
		}

		/**
		 * @param 	int $id
		 * @param 	array $columns
		 * @return 	\Illuminate\Database\Eloquent\Collection
		 */
		public function find($id, $columns = array('*'))
		{
			$this->applyCriteria();

			return $this->model->findOrFail($id, $columns);
		}

		/**
		 * @param 	array  $columns
		 * @return 	\Illuminate\Database\Eloquent\Model
		 */
		public function first($columns = ['*'])
		{
			$this->applyCriteria();

			return $this->model->first($columns);
		}

		/**
		 * @param 	array  $columns
		 * @return 	\Illuminate\Database\Eloquent\Model
		 */
		public function last($columns = ['*'])
		{
			$this->applyCriteria();

			$primaryKey = $this->model->getKeyName();

			return $this->model
						->orderBy($primaryKey, 'desc')
						->first();
		}

		/**
		 * @param 	string  $attribute
		 * @param 	mixed  $value
		 * @param 	array  $columns
		 * @return 	\Illuminate\Database\Eloquent\Model
		 */
		public function findBy($attribute, $value, $columns = array('*'))
		{
			return $this->model->where($attribute, '=', $value)->first($columns);
		}

		/**
		 * @param 	string  $attribute
		 * @param 	mixed  $value
		 * @param 	array  $columns
		 * @return	\Illuminate\Database\Eloquent\Collection
		 */
		public function findAllBy($attribute, $value, $columns = array('*'))
		{
			return $this->model->where($attribute, '=', $value)->get($columns);
		}

        /**
         * @param   string  $field
         * @param   string  $operator
         * @param   mixed  $value
         * @param   bool  $or
         * @return  $this
         */
		public function where($field, $operator, $value, $or = false)
		{
			if($or) {
				$this->model->orWhere($field, $operator, $value);
			} else {
				$this->model->where($field, $operator, $value);
			}

			return $this;
		}

		/**
		 * @return  Model
		 * @throws  RepositoryException
		 */
		public function makeModel()
		{
			return $this->setModel($this->model());
		}

		/**
		 * @param 	\Illuminate\Database\Eloquent\Model  $eloquentModel
		 * @return	\Illuminate\Database\Eloquent\Model
		 * @throws	RepositoryException
		 */
		public function setModel($eloquentModel)
		{
			$model = $this->app->make($eloquentModel);

			if (!$model instanceof Model) {
				throw new RepositoryException("Class {$model} must be an instance of Illuminate\\Database\\Eloquent\\Model");
			}

			return $this->model = $model;
		}

		/**
		 * @return  integer
		 */
		public function count()
		{
			$this->applyCriteria();

			return $this->model->count();
		}

		/**
		 * @param   string  $column
		 * @return  int|float
		 */
		public function max($column)
		{
			$this->applyCriteria();

			return $this->model->max($column);
		}

		/**
		 * @param   string  $column
		 * @return  int|float
		 */
		public function min($column)
		{
			$this->applyCriteria();

			return $this->model->min($column);
		}

		/**
		 * @param   string  $column
		 * @return  int|float
		 */
		public function avg($column)
		{
			$this->applyCriteria();

			return $this->model->avg($column);
		}

		/**
		 * @param   string  $column
		 * @return  int|float
		 */
		public function sum($column)
		{
			$this->applyCriteria();
			
			return $this->model->sum($column);
		}

        /**
         * Realiza a geração de um novo id para novo model
         *
         * @return  mixed
         */
		public function getId()
        {
            $generator = $this->model->getGenerator();

            return Generator::newId($generator);
        }

        /**
         * @return  \Illuminate\Database\Eloquent\Builder
         */
        public function getBuilder()
        {
            return $this->model->query();
        }

		/**
		 * @return	PDO
		 */
		public function getPdo()
		{
			return $this->model->getConnection()->getPdo();
		}

        /**
         * Reset querying's scope
         *
         * @return   $this
         */
        public function resetScope()
        {
            $this->skipCriteria(false);
            return $this;
        }

        /**
         * Skip the criteria's querying scope
         *
         * @param   boolean  $status
         * @return  $this
         */
        public function skipCriteria($status = true)
        {
            $this->skipCriteria = $status;
            return $this;
        }

        /**
         * Return all criterias
         *
         * @return   \Illuminate\Contracts\Support\Arrayable
         */
        public function getCriteria()
        {
            return $this->criteria;
        }

        /**
         * Apply a criteria on a model
         *
         * @param   Criteria  $criteria
         * @return  $this
         */
        public function getByCriteria(Criteria $criteria)
        {
            $this->model = $criteria->apply($this->model, $this);

            return $this;
        }

        /**
         * Push Criteria into Criteria's collection
         *
         * @param   Criteria  $criteria
         * @return  $this
         */
        public function pushCriteria(Criteria $criteria)
        {
            if ($this->preventCriteriaOverwriting) {
                // Find existing criteria
                $key = $this->criteria->search(function ($item) use ($criteria) {
                    return (is_object($item) && (get_class($item) == get_class($criteria)));
                });

                // Remove old criteria
                if (is_int($key)) {
                    $this->criteria->offsetUnset($key);
                }
            }

            $this->criteria->push($criteria);
            return $this;
        }

        /**
         * Apply a criteria on a model
         *
         * @return   $this
         */
        public function applyCriteria()
        {
            if ($this->skipCriteria === true){
                return $this;
            }

            foreach ($this->getCriteria() as $criteria) {
                if ($criteria instanceof Criteria) {
                    $this->model = $criteria->apply($this->model, $this);
                }
            }

            return $this;
        }
    }
