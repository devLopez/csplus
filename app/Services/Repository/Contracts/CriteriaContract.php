<?php

    namespace CSPlus\Services\Repository\Contracts;

    use CSPlus\Services\Repository\Criteria;

    /**
     * CriteriaContract
     *
     * Define os métodos que o repositório deve possuir
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 1.0.0
     * @since   04/02/2017
     * @package Services\Repository\Contracts
     */
    interface CriteriaContract
    {
        /**
         * @param   bool $status
         * @return  $this
         */
        public function skipCriteria($status = true);

        /**
         * @return  mixed
         */
        public function getCriteria();

        /**
         * @param   Criteria  $criteria
         * @return  $this
         */
        public function getByCriteria(Criteria $criteria);

        /**
         * @param   Criteria  $criteria
         * @return  $this
         */
        public function pushCriteria(Criteria $criteria);

        /**
         * @return $this
         */
        public function  applyCriteria();
    }