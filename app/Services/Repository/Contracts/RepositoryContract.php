<?php

	namespace CSPlus\Services\Repository\Contracts;


	interface RepositoryContract
	{
		/**
		 * @param	array  $columns
		 */
		public function all($columns = ['*']);

		/**
		 * @param	array  $relations
		 */
		public function with(array $relations);

		/**
		 * @param 	string   $value
		 * @param 	string|null  $key
		 */
		public function pluck($value, $key = null);

		/**
		 * @param 	int  $perPage
		 * @param 	array  $columns
		 */
		public function paginate($perPage = 15, $columns = ['*']);

		/**
		 * @param	array  $data
		 */
		public function save(array $data);

		/**
		 * @param 	array  $data
		 */
		public function insert(array $data);

		/**
		 * @param 	int  $id
		 * @param 	array  $data
		 */
		public function update($id, array $data);

		/**
		 * @param	int  $id
		 */
		public function delete($id);

		/**
		 * @param 	int  $id
		 * @param 	array $columns
		 */
		public function find($id, $columns = ['*']);

		/**
		 * @param	array  $columns
		 */
		public function first($columns = ['*']);

		/**
		 * @param 	array  $columns
		 */
		public function last($columns = ['*']);

		/**
		 * @param 	string  $field
		 * @param 	mixed  $value
		 * @param 	array  $columns
		 */
		public function findBy($field, $value, $columns = ['*']);

		/**
		 * @param 	string  $field
		 * @param 	mixed  $value
		 * @param 	array  $columns
		 */
		public function findAllBy($field, $value, $columns = ['*']);

		/**
		 * @param 	mixed  $where
		 * @param 	array $columns
		 */
		public function where($field, $operator, $value, $or = false);

		/**
		 * @return  integer
		 */
		public function count();

		/**
		 * @param   string  $column
		 * @return  int|float
		 */
		public function max($column);

		/**
		 * @param   string  $column
		 * @return  int|float
		 */
		public function min($column);

		/**
		 * @param   string  $column
		 * @return  int|float
		 */
		public function avg($column);

		/**
		 * @param   string  $column
		 * @return  int|float
		 */
		public function sum($column);

        /**
         * @return  integer
         */
		public function getId();

        /**
         * @return  \Illuminate\Database\Eloquent\Builder
         */
		public function getBuilder();
	}
