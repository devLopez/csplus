<?php

    namespace CSPlus\Services\Repository\Contracts;

    /**
     * ModelContract
     *
     * Interface de padronização dos models do sistema
     *
     * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
     * @version 1.0.0
     * @package Services\Repository\Contracts
     */
    interface ModelContract
    {
        /**
         * @return  string
         */
        public function getGenerator();
    }