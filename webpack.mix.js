const { mix } = require('laravel-mix');

mix.webpackConfig({
    resolve: {
        modules: [
            path.resolve(__dirname, 'node_modules'),
            path.resolve(__dirname, 'resources/assets/js/app/helpers/'),
            path.resolve(__dirname, 'resources/assets/js/app/helpers/Forms'),
            path.resolve(__dirname, 'resources/assets/js/app/helpers/Http'),
            path.resolve(__dirname, 'resources/assets/js/app/helpers/Notification')
        ]
    }
});

mix.disableNotifications();

mix.copy('resources/assets/fonts', 'public/assets/fonts')
   .copy('resources/assets/css', 'public/assets/css')
   .copyDirectory('resources/assets/img', 'public/assets/img');

mix.sass('resources/assets/sass/nifty/nifty.scss', 'public/assets/css/nifty.css')
    .sass('resources/assets/sass/font-awesome/font-awesome.scss', 'public/assets/css/font-awesome.css')
    .sass('resources/assets/sass/pdf.scss', 'public/assets/css/pdf.css')
    .options({
        processCssUrls: false
    });

mix.copy('resources/assets/js/bootstrap', 'public/assets/js/bootstrap')
    .copy('resources/assets/js/libs', 'public/assets/js/libs')
    .copy('resources/assets/js/plugin', 'public/assets/js/plugin')
    .copy('resources/assets/js/nifty.js', 'public/assets/js/nifty.js')
    .js('resources/assets/js/App.js', 'public/assets/js/csplus.js');

mix.js([
    'resources/assets/js/app/controllers/backbone/bancarios.js',
    'resources/assets/js/app/controllers/backbone/complementares.js',
    'resources/assets/js/app/controllers/backbone/dependentes.js',
    'resources/assets/js/app/controllers/backbone/pessoais.js',
    'resources/assets/js/app/controllers/backbone/profissionais.js'
], 'public/assets/js/app/controllers/Pessoais.js');