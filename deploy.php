<?php
namespace Deployer;

require 'recipe/laravel.php';

// Configuration
set('repository', 'https://devLopez@bitbucket.org/devLopez/csplus.git');
set('git_tty', true);
set('allow_anonymous_stats', false);
set('writable_mode', 'chmod');
set('keep_releases', 5);

// Hosts
    
host('beta')
    ->hostname('beta.csmobile.com.br')
    ->stage('testing')
    ->user('root')
    ->set('deploy_path', '/var/www/html/beta.csmobile.com.br')
    ->set('branch', 'develop');

host('production')
    ->hostname('csmobile.com.br')
    ->stage('production')
    ->user('root')
    ->set('deploy_path', '/var/www/html/csmobile.com.br')
    ->set('branch', 'master');

// Tasks

task('deploy', [
    'deploy:prepare',
    'deploy:release',
    'deploy:update_code',
    'deploy:shared',
    'deploy:symlink',
    'deploy:vendors',
    'sys:write',
    'assets',
    'deploy:unlock',
    'success'
]);

task('sys:write', 'sudo chmod -R 777 ./bootstrap/cache/ ./storage/');
task('assets', 'yarn && yarn run prod');

after('deploy:failed', 'deploy:unlock');
