const { route } = require('Router');

require('CSPlus');


require('./app/controllers/Associado');
require('./app/controllers/Auth');
require('./app/controllers/Categorias');
require('./app/controllers/Convites');
require('./app/controllers/Debitos');
require('./app/controllers/MemberSearch');
require('./app/controllers/Parentescos');
require('./app/controllers/Upload');
require('./app/controllers/pagseguro/Payment');
require('./app/controllers/pagseguro/Transacoes');
