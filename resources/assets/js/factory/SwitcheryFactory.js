/**
 * SwitcheryFactory
 *
 * Cria uma factory para a classe Switchery
 *
 * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
 * @version 1.0.0
 * @since   15/07/2017
 */
class SwitcheryFactory {
	/**
	 * @param  {string} elementId
	 * @return {void}
	 */
	createOne(elementId) {
		var switchery = new Switchery(
			document.getElementById(elementId)
		);
	}

	/**
	 * @param  {string} selector
	 * @return {void}
	 */
	createMultiple(selector) {
		var elems = Array.prototype.slice.call(document.querySelectorAll(selector));

        elems.forEach(function(html) {
          var switchery = new Switchery(html);
        });
	}
}

module.exports = SwitcheryFactory;