let Alert       = require('Alert');
let Axios       = require('axios');
let Validator   = require('Validator');

Axios.defaults.headers.common['X-CSRF-TOKEN'] = Laravel.token;

/**
 * Http
 *
 * Classe responsável por realizar requisições http ao backend
 *
 * @author  Matheus Lopes Santos <fale_com_lopez@homail.com>
 * @version 1.1.0
 * @since   08/85/2017
 */
class Http {

    /**
    * Realiza uma requisição POST ao backend
    *
    * @param   {string} url
    * @param   {string|object} data
    * @param   {function} callback
    */
    static post(url, data = {}, form = null, callback = null) {
        Alert.loading('Processando pedido...');

        Axios.post(url, data)
            .then(xhr => {
                Alert.unload();

                if(callback) {
                    callback(xhr.data);
                }
            }).catch(xhr => {
                Alert.unload();

                var status = xhr.response.status;
                var errors = xhr.response.data;

                if(status == 422 && form != null) {
                    Validator.render(form, errors);
                    return;
                }

                Alert.error(errors);
            });
    }

    /**
     * Realiza uma requisição GET ao backend
     *
     * @param  {string} url
     * @param  {object} data
     * @param  {function} callback
     * @return {void}
     */
    static get(url, data = {}, callback) {
        Alert.loading('Processando pedido...');

        if(Object.keys(data).length !== 0) {
            var params = $.param(data);
            url += '?' + params;
        }

        Axios.get(url)
            .then(xhr => {
                Alert.unload();

                callback(xhr.data);
            })/*.catch(xhr => {
                Alert.unload();
                Alert.error(xhr.response.data);
            });*/
    }

    /**
     * Retorna a URL + recurso solicitado
     *
     * @param   {string} resource
     * @returns {string}
     */
    static url(resource) {
        var url = $('body').data('url');

        return (resource) ? url + resource : url;
    }

    /**
     * Executa a configuração de Ajax do jQuery
     *
     * @return  {void}
     */
    static runAjaxConfiguration() {
        $.ajaxSetup({
            error: function(xhr) {
                switch (xhr.status) {
                    case 401:
                        Alert.error('Você não tem permissão para realizar esta operação');
                        break;
                    case 422:
                        Alert.error('Recurso improcessável. Contate o administrador');
                        break;
                    default:
                        break;
                }
            },
            headers: {
                'X-CSRF-TOKEN': Laravel.token
            }
        });

        $(document).ajaxStart(function () {
            Alert.loading('Processando Requisição...');
        }).ajaxComplete(function () {
            Alert.unload();
        });
    }
}

window.Http     = Http;
module.exports  = Http;
