let Http    = require('Http');
let Utils   = require('Utils');

/**
 * CEP
 *
 * Classe responsável pela busca de cep na backend
 *
 * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
 * @version 1.1.0
 * @since   16/05/2017
 */
class CEP {
    /**
     * Realiza a preparaçao para busca de um endereço
     *
     * @param   {string} cep
     * @param   {object} logradouro
     * @param   {object} bairro
     * @param   {object} cidade
     * @param   {object} uf
     */
    static buscar(cep, logradouro, bairro, cidade, uf) {
        cep = Utils.limpaCep(cep);

        if(logradouro.val() != '' || bairro.val() != '' || cidade.val() != '' || uf.val() != '') {
            if(!confirm('Deseja realmente modificar o endereço já cadastrado?')) {
                return false;
            }
        }

        Http.get(CSPlus.baseUrl('/backend/endereco/' + cep), '', e => {
            logradouro.val(e.logradouro);
            bairro.val(e.bairro);
            cidade.val(e.localidade);
            uf.val(e.uf);
        });
    }
}

window.CEP      = CEP;
module.exports  = CEP;
