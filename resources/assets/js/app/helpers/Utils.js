/**
 * Utils
 *
 * Classe que abriga métodos diversos e helpers úteis para o sistema
 *
 * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
 * @version 1.1.0
 * @since   09/06/2017
 */
class Utils {

    /**
     * Realiza a limpeza do cep
     * @param  {string} cep
     * @return {string}
     */
    static limpaCep(cep) {
        return cep.replace([',', '.', '/', '-'], '');
    }

    /**
     * Converte uma data qualquer para datas no padrão BR
     *
     * @param   {string} data
     * @returns {string}
     */
    static dataBR (data) {
        if(!data) {
            return '';
        } else {
            if(data.indexOf('-') != -1) {
                data = data.split('-');
                return data[2] +'/'+ data[1] +'/'+ data[0];
            } else {
                return data;
            }
        }
    }

    /**
     * Seta uma Selectbox para um valor padrão
     *
     * @param   {string} valor
     * @param   {object} select
     * @return  {void}
     */
    static setSelectBox (valor, select) {
        select.find('option').each(function() {
            if($(this).val() == valor) {
                $(this).prop('selected', true);
            }
        });
    }

    /**
     * Habilita um formulário para edição
     *
     * @param   {object} $form
     */
    static habilitaFormulario ($form) {
        $form.find('[readonly]').each( function () {
            $(this).prop('readonly', false).attr('autocomplete', 'off');
        });

        $('select:not(:disabled), textarea:not(:disabled)').removeAttr('readonly').addClass('edit');

        $form.find(':checkbox:not(:disabled)').each(function () {
            $(this).closest('.checkbox').removeClass('state-disabled');
        });
    }

    /**
     * Habilita um formulário para edição
     *
     * @param   {object} $form
     * @param   {string} isAdmin
     */
    static desabilitaFormulario ($form) {
        $form.find('input:not(:disabled)').each( function () {
            $(this).prop('readonly', true);
        });

        $('select:not(:disabled), textarea:not(:disabled)').attr('readonly', true).removeClass('edit');

        $form.find(':checkbox:not(:disabled)').each(function () {
            $(this).closest('.checkbox').addClass('state-disabled');
        });
    }

    /**
     * Realiza a marcação de uma checkbox
     *
     * @param   {object} $checkbox
     * @param   {string} value
     */
    static setCheckBox ($checkbox, value) {
        ($checkbox.val() == value) ? $checkbox.prop('checked', true) : $checkbox.prop('checked', false);
    }

    /**
     * Limpa os campos do formulário
     *
     * @param $form
     */
    static clearFields ($form) {
        $form.find('input').each( function () {
            $(this).val('');
        });
    }

    /**
     * Insere um valor de option em uma selectbox existente
     *
     * @param  {object} el
     * @param  {string} value
     * @param  {string} text
     * @return {void}
     */
    static appendSelectValue(el, value, text) {
        var option = `<option value="${value}">${text}</option>`;
        el.append(option);
    }

    /**
     * Permite somente numeros em uma string
     *
     * @param   {string} value
     * @returns {integer}
     */
    static numbersOnly(value) {
        if(value) {
            var number = value.replace(/[^0-9]/g, '');

            if(isNaN(parseInt(number)) && isFinite(number)) {
                return '';
            }

            return parseInt(number);
        }
    }
}

window.Utils    = Utils;
module.exports  = Utils;
