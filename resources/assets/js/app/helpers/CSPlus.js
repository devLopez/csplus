let Alert   = require('Alert');
let Form    = require('Form');
let Http    = require('Http');
let Utils   = require('Utils');

/**
 * CSPlus
 *
 * Classe desenvolvida para abrigar os principais métodos utilizados na execução
 * do sistema
 *
 * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
 * @version 2.0.0
 * @since   09/06/2017
 */
class CSPlus{

    /**
     * Exibe notificação de sucesso para o usuário
     *
     * @param  {string} msg
     */
    static alertSucesso(msg) {
        Alert.success(msg);
    }

    /**
     * Exibe notificação de erro para o associado
     *
     * @param  {string} msg
     */
    static alertErro(msg) {
        Alert.error(msg);
    }

    /**
     * Retorna a url base do sistema
     *
     * @param  {string|null} $_resource Potencial recurso a ser inserido na url base
     * @return {string}
     */
    static baseUrl($_resource = null) {
        return Http.url($_resource);
    }

    /**
     * Exibe mensagem genérica durante uma operação
     *
     * @param  {string} msg
     */
    static msgGenerica(msg) {
        Alert.loading(msg);
    }

    /**
     * Esconde uma mensagem genérica exibida
     *
     * @return {void}
     */
    static hideGenerica() {
        Alert.unload();
    }

    /**
     * Exibe mensagem de alerta para o usuário
     *
     * @param  {string}  msg
     * @param  {string}  type
     * @return {void}
     */
    static alert(msg, type = 'success') {
        return Alert.notify(msg, type);
    }

    /**
     * Exibe uma mensagem de confirmação para o usuário
     *
     * @param  {string}  msg
     * @param  {string}  type
     * @return {void}
     */
    static confirm(msg, type = 'success') {
        return Alert.confirm(msg, type);
    }

    /**
     * Desenha a breadcrumb nas páginas do site
     *
     * @param  {array|null} opt_breadCrumbs
     * @return {void}
     */
    static drawBreadCrumb(opt_breadCrumbs = null) {
        var bread_crumb = $('.breadcrumb');

        var a = $("nav div div div div li.active > a, nav div div div div li.active-link > a"),
            b = a.length;


        bread_crumb.empty().append($("<li>Home</li>"));
        a.each(function() {
            bread_crumb.append(
                $("<li></li>").html(
                    $.trim(
                        $(this).clone().children(".badge").remove().end().text()
                    )
                )
            ), --b || (document.title = bread_crumb.find("li:last-child").text())
        });

        if (opt_breadCrumbs != undefined) {
            $.each(opt_breadCrumbs, function(index, value) {
                bread_crumb.append($("<li></li>").html(value));
                document.title = bread_crumb.find("li:last-child").text();
            });
        }

        bread_crumb.find("li:last-child").addClass('active');
    }

    /**
     * Realiza o carregamento de forma assíncrona de um javascript
     *
     * @param  {string}   scriptName
     * @param  {Function} callback
     * @return {void}
     */
    static loadScript(scriptName, callback) {

        var body    = document.getElementsByTagName('body')[0],
            script  = document.createElement('script');

        script.type     = 'text/javascript';
        script.src      = scriptName;
        script.onload   = callback;

        body.appendChild(script);
        // clear DOM reference
        body = null;
        script = null;

        if (callback) {
            callback();
        }
    }

    /**
     * Roda os principais plugins utilizados em formulários
     *
     * @return {void}
     */
    static runAllForms() {
        Form.runAllForms();
	}
}

Form.runFormConfiguration();
Http.runAjaxConfiguration();

CSPlus.drawBreadCrumb();

$(function(){
    $('[data-number]').on('blur', function(){
        var field = $(this);
        var value = field.val();

        if(value) {
            var new_value = Utils.numbersOnly(value);

            field.val(new_value);
        }
    });

    /**
     * Adiciona a máscara para telfones brasileiros de 8 e 9 dígitos
     * 
     * @see https://jsfiddle.net/2L3fsea6/
     */
    $('[data-telefone]').focusout(function(){
        var phone, element;

        element = $(this);
        phone   = element.val().replace(/\D/g, '');

        if(phone.length > 10) {
            element.mask("(99) 99999-999?9", {placeholder: '*'});
        } else {
            element.mask("(99) 9999-9999?9", {placeholder: '*'});
        }
    }).trigger('focusout');

    $('[data-documento]').focusout(function() {
        var doc, element;

        element = $(this);
        doc     = element.val().replace(/\D/g, '');

        if(doc.length <= 11) {
            element.mask("999.999.999-99", {placeholder: '*'});
        } else {
            element.mask('99.999.999/9999-99', {placeholder: '*'});
        }
    }).trigger('focusout');
});

// References on Window
window.CSPlus = CSPlus;

export { CSPlus as default }
