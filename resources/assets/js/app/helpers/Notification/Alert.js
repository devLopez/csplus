const Swal = require('sweetalert');

/**
 * Alert
 *
 * Classe responsável por abrigar as funções de notificações do sistema
 *
 * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
 * @version 1.0.0
 * @since   09/06/2017
 */
class Alert {
    /**
     * Exibe mensagem de sucesso ao user
     *
     * @param   {string} msg
     */
    static success(msg) {
        $.niftyNoty({
            type        : "primary",
            container   : "floating",
            title       : "Sucesso",
            message     : msg,
            closeBtn    : false,
            timer       : 5000,
            icon        : "fa fa-bullhorn fa-2x swing animated"
        });
    }

    /**
     * Exibe mensagem de erro ao usuário
     *
     * @param   {string} msg
     */
    static error(msg) {
        $.niftyNoty({
            type        : "danger",
            container   : "floating",
            title       : "Algo ruim ocorreu aqui",
            message     : msg,
            closeBtn    : false,
            timer       : 5000,
            icon        : "fa fa-bell fa-2x swing animated"
        });
    }

    /**
     * Exibe mensagem de carregamento durante ajax
     *
     * @param   {string} msg
     */
    static loading(msg) {
        $.blockUI({
            message: msg,
            css: {
                border                  : 'none',
                padding                 : '15px',
                backgroundColor         : '#000',
                '-webkit-border-radius' : '10px',
                '-moz-border-radius'    : '10px',
                'border-radius'         : '10px',
                opacity                 : .5,
                color                   : '#fff',
                zIndex                  : 999999
            }
        });
    }

    /**
     * Remove a mensagem de carregamento
     *
     * @return void
     */
    static unload() {
        $.unblockUI();
    }

    /**
     * Exibe um alerta ao associado
     *
     * @param   {string}  msg
     * @param   {string}  messageType
     * @param   {string|null}  button
     */
    static notify(msg, messageType = 'success', button = true) {

        let title = '';

        switch (messageType) {
            case 'error' :
                title = 'Temos um problema aqui...';
                break;
            case 'warning' :
                title = 'Algo está para ocorrer...';
                break;
            case 'info' :
                title = 'Atenção';
                break;
            default :
                title = 'Sucesso';
                break;
        }

        return Swal({
            title: title,
            text: msg,
            icon: messageType,
            button: button
        });
    }

    /**
     * Exibe prompt de confirmação ao usuário
     *
     * @param   {string}  msg
     * @param   {string}  messageType
     */
    static confirm(msg, messageType = 'success') {
        let title = '';

        switch (messageType) {
            case 'error' :
                title = 'Temos um problema aqui...';
                break;
            case 'warning' :
                title = 'Algo está para ocorrer...';
                break;
            case 'info' :
                title = 'Atenção';
                break;
            default :
                title = 'Sucesso';
                break;
        }

        return swal({
            Swal: title,
            text: msg,
            icon: messageType,
            buttons: ['Cancelar', 'Ok'],
            dangerMode: true
        });
    }
}

module.exports = Alert;
