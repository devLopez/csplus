let Alert       = require('Alert');
let Form        = require('Form');
let Http        = require('Http');
let Validator   = require('Validator');

/**
 * Model
 *
 * Classe desenvolvida para gerenciamento dos cadastros
 * de form genérica no sistema
 *
 * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
 * @version 1.2.0
 * @since   11/05/2017
 */
class Model {

    /**
     * @param {object} form O formulário a ser tradado
     */
    constructor(form) {
        this.form = new Form(form);
    }

    /**
     * Salva os novos dados
     *
     * @param {callback} callback Função callback a ser executada
     */
    save(callback) {

        var url     = this.form.getUrl();
        var data    = this.form.getData();
        var form    = this.form.getForm();

        Validator.reset(form);

        Http.post(url, data, form, model => {
            callback(model);

            Alert.success('O registro foi inserido');
        });
    }

    /**
     * Realiza uma consulta GET em uma url
     *
     * @param  {function} callback
     */
    fetch(callback) {
        var url     = this.form.getUrl();
        var data    = this.form.getData();

        Http.get(url, data, xhr => {
            callback(xhr);
        });
    }
}

module.exports = Model;
