/**
 * Validator
 *
 * Classe desenvolvida para gerenciamento dos erros de validação vindos de uma
 * requisição ajax
 *
 * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
 * @version 1.1.0
 * @since   09/06/2017
 */
class Validator {
    /**
     * Renderiza os erros recebidos
     *
     * @param  {object} form
     * @param  {object} errors
     * @return {void}
     */
    static render(form, errors) {

        $.each(errors, function(campo, erro) {

            var el = $('[name=' + campo + ']');

            var error_msg   = `<small class="help-block text-danger">${erro[0]}</small>`;
            var wrapper     = el.parent();
            wrapper.addClass('has-error').append(error_msg);
        });
    }

    /**
     * Reseta o formulário, removendo as classes de erro presentes
     *
     * @param   {object} form
     * @return  {void}
     */
    static reset(form) {
        form.find('div.form-group.has-error').each(function() {
            $(this).removeClass('has-error');
        });

        form.find('.help-block').each(function(){
            $(this).remove();
        })
    }
}

module.exports = Validator;
