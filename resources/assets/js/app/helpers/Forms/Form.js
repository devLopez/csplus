let Alert = require('Alert');

/**
 * Form
 *
 * Classe desenvolvida para gerenciamento dos formulários diversos utilizados no
 * sistema
 *
 * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
 * @version 1.2.0
 * @since   14/07/2017
 */
class Form {
    /**
     * Construtor da classe
     *
     * @param  {object} form
     */
    constructor(form) {
        this.form = form;
    }

    /**
     * @return {object}
     */
    getForm() {
        return this.form;
    }

    /**
     * @return {string}
     */
    getUrl() {
        return this.form.attr('action');
    }

    /**
     * @return {string}
     */
    getData() {
        return this.form.serialize();
    }

    /**
     * @return {string}
     */
    getMethod() {
        return this.form.attr('method');
    }

    /**
     * Executa os plugins de formulário
     *
     * @return  {void}
     */
    static runAllForms() {
        /*
         * MASKING
         * Dependency: js/plugin/masked-input/
         */
        if ($.fn.mask) {
            $('[data-mask]').each(function() {

                var $this = $(this),
                    mask = $this.attr('data-mask') || 'error...';

                $this.mask(mask, {
                    placeholder : '*'
                });

                //clear memory reference
                $this = null;
            });
        }

        /*
         * JQUERY UI DATE
         * Dependency: js/libs/jquery-ui-1.10.3.min.js
         * Usage: <input class="datepicker" />
         */
        if ($.fn.datepicker) {
            $('.datepicker').each(function() {

                var $this = $(this),
                    dataDateFormat = $this.attr('data-dateformat') || 'dd.mm.yy';

                $this.datepicker({
                    dateFormat : dataDateFormat,
                    prevText : '<i class="fa fa-chevron-left"></i>',
                    nextText : '<i class="fa fa-chevron-right"></i>',
                });

                //clear memory reference
                $this = null;
            });
        }

        /*
         * AJAX BUTTON LOADING TEXT
         * Usage: <button type="button" data-loading-text="Loading..." class="btn btn-xs btn-default ajax-refresh"> .. </button>
         */
        $('button[data-loading-text]').on('click', function() {
            var btn = $(this);
            btn.button('loading');
            setTimeout(function() {
                btn.button('reset');
                //clear memory reference
                btn = null;
            }, 3000);

        });
    }

    /**
     * Executa operacçoes que devem ser aplicados aos formulários de
     * forma global
     */
    static runFormConfiguration() {
        $('form.not-ajax').on('submit', () => {
            Alert.loading('Preparando seu pedido...');
        });
    }
}

module.exports = Form;
