let Model = require('Model');

/**
 * Parentesco
 *
 * Classe desenvolvida para gerenciamento do cadastro de parentescos
 *
 * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
 * @version 1.0.0
 * @since   09/05/2017
 */
class Parentesco extends Model {
    /**
     * Construtor da classe
     *
     * @param  {object} form
     * @return {void}
     */
    constructor(form) {
        super(form);
    }
}

module.exports = Parentesco;