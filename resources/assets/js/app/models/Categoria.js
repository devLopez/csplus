let Model = require('Model');

/**
 * Categoria
 *
 * Classe desenvolvida para o cadastro de categorias
 *
 * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
 * @since   02/05/2017
 * @version 1.0.0
 */
class Categoria extends Model {

    /**
     * @param {string} url
     */
    constructor(data) {
        super(data);
    }
}

module.exports = Categoria;
