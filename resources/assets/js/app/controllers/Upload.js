let Alert       = require('Alert');
let Dropzone    = require('../../plugin/dropzone/dropzone.js');

/**
 * Upload
 *
 * Classe desenvolvida para gerenciamento do upload de imagens para os associados
 *
 * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
 * @version 1.1.0
 * @since   09/06/2017
 */
class Upload {
    /**
     * Abre a janela modal para troca da fotografia do associado ou dependente
     *
     * @param  {int} tipo
     * @return {void}
     */
    static openModal(tipo) {
        $('#modal-upload').modal('show');
        $('#foto-tipo').val(tipo);
    }
}

$('[data-altera-foto]').on('click', function(){
    let tipo = $(this).data('tipo');

    Upload.openModal(tipo);
});

Dropzone.autoDiscover = false;

$('#dropzone').dropzone({
    addRemoveLinks : true,
    maxFilesize: 0.5,
    dictDefaultMessage: '<span class="text-center"><span class="font-lg visible-xs-block visible-sm-block visible-lg-block"><span class="font-lg"><i class="fa fa-caret-right text-danger"></i> <span class="font-xs">Arraste a foto aqui para iniciar</span></span><span>&nbsp&nbsp<h4 class="display-inline"> (Ou Clique)</h4></span>',
    dictResponseError: 'Erro ao fazer o upload',
    acceptedFiles: 'image/jpg, .jpg, .jpeg',

    addedfile(file) {
        if ( ! confirm('Deseja realmente alterar a foto?') ) {
            this.removeFile(file);
            $('#modal-upload').modal('hide');
        }
    },

    success(file, xhr) {
        if(xhr.success) {
            $('#modal-upload').modal('hide');

            Alert.notify('A foto foi alterada com sucesso', 'success')
                 .then(() => {
                     location.reload();
                 });

        } else {
            Alert.error(xhr.error);
            return false;
        }
    },

    error(file, errorMessage, xhr) {
        Alert.error(errorMessage);
    },

    complete(file) {
        this.removeFile(file);
    }
});
