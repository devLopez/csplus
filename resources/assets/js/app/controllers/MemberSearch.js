let Alert   = require('Alert');
let Http    = require('Http');

/**
 * MemberSearch
 *
 * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
 * @version 1.0.0
 * @since   09/08/2017
 */
class MemberSearch {
    /**
     * @param   {string}  searchMode
     * @return  {void}
     */
    constructor(searchMode) {
        this.searchMode = searchMode;

        this.url = (searchMode == 'associado') ? Http.url('/backend/indicante/associado') : Http.url('/backend/indicante/corretor');
    }

    /**
     * @return  {void}
     */
    finishModal() {
        $('#nome-associado').val('');
        $('#indicando-wrap').hide();
        $('#indicando-search-result').html('');
    }

    /**
     * @param   {string}  nome
     * @return  {void}
     */
    find(nome) {
        var $this = this;

        Http.get(this.url, {nome: nome}, function(e) {
            if(e.associados === false) {
                Alert.error('Nenhum associado encontrado com o nome passado');
            } else {
                $this.render(e.associados);
            }
        });
    }

    /**
     * @param   {object}  associados
     * @return  {void}
     */
    render(associados) {
        var tr = '';

        for(var associado in associados) {
            let a = associados[associado];

            tr += `<tr class="table-select" data-matricula="${a.cota}" data-nome="${a.nome}">
                        <td>${a.cota}</td>
                        <td>${a.nome}</td>
                   </tr>`;
        }

        $('#indicando-wrap').fadeIn();
        $('#indicando-search-result').html(tr);
    }

    /**
     * @param   {object}  member
     * @return  {void}
     */
    setMemberValue(member) {
        $('#modal-get-indicando').modal('hide');

        var matricula   = member.data('matricula');
        var responsavel = member.data('nome');

        if(this.searchMode == 'associado') {
            $('#responsavel').val(matricula);
            $('#nome-responsavel').text(responsavel);
        } else {
            $('#corretor').val(matricula);
            $('#nome-colaborador').text(responsavel);
        }
    }

    /**
     * @return  {void}
     */
    bootstrapModal() {
        var $this = this;
        $('#modal-get-indicando').on('hide.bs.modal', function() {
            $this.finishModal()
        }).on('shown.bs.modal', function() {
            $('#nome-associado').focus();
        })
    }
}

var memberSearch = null;

$('[href=#modal-get-indicando]').on('click', function() {
    var modo = $(this).data('mode');

    memberSearch = new MemberSearch(modo);
    memberSearch.bootstrapModal();
});

$('#indicando-wrap').hide();

$(document).on('click', '[data-matricula]', function(e) {
    memberSearch.setMemberValue($(this));
});

$('#nome-associado').on('keyup', function(e) {
    var nome = $(this).val();

    if(e.keyCode == 13) {
        memberSearch.find(nome);
    }
});

module.exports = MemberSearch;
