let Alert   = require('Alert');
let Form    = require('Form');
let Http    = require('Http');

let painelTransacoes    = $('#painel-transacoes');
let emptyRecords        = $('#empty-records');

/**
 * Transacao
 *
 * Classe desenvolvida para gerenciamento das transações financeiras com o
 * pagseguro
 *
 * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
 * @version 1.2.0
 * @since   09/06/2017
 */
class Transacao {

    /**
     * Construtor da classe
     *
     * @return {void}
     */
    constructor() {
        this.form = new Form(
            $('#form-consulta-transacoes')
        );

        this.status = new Array();
        this.status[1] = '<span class="label label-table label-default">Aguardando pagamento</span>';
        this.status[2] = '<span class="label label-table label-warning">Em análise</span>';
        this.status[3] = '<span class="label label-table label-success">Paga</span>';
        this.status[4] = '<span class="label label-table label-mint">Disponível</span>';
        this.status[5] = '<span class="label label-table label-mint">Em disputa</span>';
        this.status[6] = '<span class="label label-table label-mint">Devolvida</span>';
        this.status[7] = '<span class="label label-table label-danger">Cancelada</span>';
        this.status[8] = '<span class="label label-table label-dark">Debitado</span>';
        this.status[9] = '<span class="label label-table label-dark">Retenção temporária</span>';
    }

    /**
     * Recebe as transações disponíveis
     *
     * @return {void}
     */
    fetchTransactions() {
        let $this   = this;
        let url     = this.form.getUrl();
        // var data    = this.form.getData();

        let data = {
            data_inicio: $('[name=data_inicio]').val(),
            data_final: $('[name=data_final]').val()
        };

        Http.get(url, data, model => {

            if(model.transacoes.length == 0) {
                emptyRecords.fadeIn();
                painelTransacoes.hide();
            } else {
                painelTransacoes.fadeIn();
                emptyRecords.hide();

                let template = '';

                $.each(model.transacoes, function(index, t) {

                    let link = $this._generateLinks(
                        t.status, t.codigoNotificacao
                    );

                    template += `
                        <tr>
                            <td>${t.ultimoEvento}</td>
                            <td>${$this.status[t.status]}</td>
                            <td>R$${t.valor}</td>
                            <td class="text-center">
                                ${link}
                            </td>
                        </tr>
                    `;
                });

                $('#table-transacoes').empty().append(template);

                $(document).trigger('nifty.ready');
            }
        });
    }

    /**
     * Realiza a geração dos links para busca dos dados do pagamento
     *
     * @param  {int} status
     * @param  {string} notificationCode
     * @return {string}
     */
    _generateLinks(status, notificationCode) {
        let url = $('#notification-url').val();

        if(status == 3 || status == 7) {
            return `
                <a href="javascript:void(0)"
                    class="add-tooltip"
                    title="Receber dados do Pagamento"
                    data-code="${notificationCode}"
                    data-url="${url}"
                >
                    <i class="fa fa-download"></i>
                </a>
            `;
        }

        return '';
    }

    /**
     * Envia o código de notificação para atualizar os dados de um pagamento
     *
     * @param  {string} url
     * @param  {string} code
     * @return {void}
     */
    sendNotificationCode(url, code) {
        let data = {
            notificationCode: code
        };

        Http.post(url, data, null, dados => {
            if(dados.atualizado) {
                Alert.success('Os dados da transação foram alterados.');
            }
        });
    }
}

let transacao = new Transacao();

$('#form-consulta-transacoes').on('submit', e => {
    e.preventDefault();

    transacao.fetchTransactions();
});

$(document).on('click', '[data-code]', function() {

    transacao.sendNotificationCode(
        $(this).data('url'),
        $(this).data('code')
    );
});
