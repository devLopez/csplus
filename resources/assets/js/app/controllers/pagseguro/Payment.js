let Alert   = require('Alert');
let Form    = require('Form');
let Http    = require('Http');
let Numeral = require('../../../plugin/numeral/numeral');
let Utils   = require('Utils');

let expirationMonth     = $('#expirationMonth option:checked');
let expirationYear      = $('#expirationYear option:checked');
let cardBrand           = $('#cardBrand');
let cvv                 = $('#cvv');
let cardNumber          = $('#cardNumber');
let cardToken           = $('#cardToken');
let senderHash          = $('#senderHash');
let amount              = $('#amount');
let cobrancas           = $('#qtde_cobrancas');
let installments        = $('#installments');
let installmentAmount   = $('#installmentAmount');

/**
 * Payment
 *
 * Classe responsável pela realização do pagamento das mensalidades via PagSeguro
 * ou boleto bancário
 *
 * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
 * @version 1.1.0
 * @since   09/06/2017
 */
class Payment {

    /**
     * Construtor da class
     * @return {void}
     */
    constructor() {
        // Elemento do formulário
        this.paymentForm = $('#form-payment');

        // Valor base para a transação
        this.valorBase = $('#valor-base').val();

        // Define o campo para o valor da transação
        this.campoValor = amount;

        // Define a quantidade de parcelas sem juros
        this.parcelasSemJuros = $('#parcelas-sem-juros').val();

        // Recebe o endereço de redirecionamento após o pagamento
        this.redirecionamento = $('#redirecionamento').val();

        this.setCardBrand();

        Form.runAllForms();
    }

    /**
     * Seta o senderHash no formulário
     *
     * @return  {object}  this
     */
    setSenderHash() {
        senderHash.val(
            PagSeguroDirectPayment.getSenderHash()
        );

        return this;
    }

    /**
     * Seta a bandeira do cartão de crédito no formulário
     *
     * @return {void}
     */
    setCardBrand() {
        let $this = this;

        cardNumber.blur(function () {

            let ccNumber = Utils.numbersOnly(
                $(this).val()
            );

            if(ccNumber) {

                if (!isNaN(ccNumber)) {

                    PagSeguroDirectPayment.getBrand({
                        cardBin: ccNumber,
                        success: function(data) {
                            let brand = JSON.stringify(data.brand.name).replace(/"/g, '');

                            cardBrand.val(brand);

                            $this.setInstallmentAmount();
                        },
                        error: function() {
                            Alert.error('Ocorreu um erro na busca da bandeira do cartão. tente novamente');
                            $('#cardNumber').focus();
                        }
                    });

                } else {
                    Alert.error('É necessário informar o número do cartão de crédito');
                    cardNumber.focus();
                }
            }
        });
    }

    /**
     * Recupera o token do cartão de crédito e seta no formulário
     *
     * @return {void}
     */
    setCardToken() {
        let $this = this;

        let parametros = {
            cardNumber: cardNumber.val(),
            brand: cardBrand.val(),
            cvv: cvv.val(),
            expirationMonth: expirationMonth.val(),
            expirationYear: expirationYear.val(),
            success: function(data) {
                let token = JSON.stringify(data.card.token).replace(/"/g, '');

                cardToken.val(token);

                $this.doPayment();
            },
            error: function(data) {
                $this.integrationErrors(data);
                return false;
            },
            complete: function() {
                Alert.unload();
            }
        };

        PagSeguroDirectPayment.createCardToken(parametros);
    }

    /**
     * Seta o valor das parcelas
     *
     * @return {void}
     */
    setInstallmentAmount() {
        Alert.loading('Gerando parcelas...');

        let $this = this;

        try {
            let url = route('debitos.parcelas', {
                'valor': amount.val(),
                'bandeira': cardBrand.val(),
                'cobrancas': cobrancas.val()
            });

            Http.get(url, '', installments => {
                $this.processInstallments(installments);
            });
        } catch (err) {
            Alert.error(err.message);
        }
    }

    /**
     * Processa as parcelas recebidas do back-end
     *
     * @param  {array} processedInstallments
     * @return {void}
     */
    processInstallments(processedInstallments) {
        let html = '';

        installments.find("option").remove();

        for(let i = 0; i < processedInstallments.length; i++) {

            let preco       = processedInstallments[i].price;
            let parcela     = processedInstallments[i].installmentAmount;
            let valor_total = processedInstallments[i].totalAmount;

            if(i === 0) {
                html += `<option value="${i+1}" data-valor-parcela="${preco}">
                        ${i+1}x de ${Numeral(preco).format('R$0.00')} - (Total: ${Numeral(preco).format('R$0.00')})
                    </option>`;
            } else {
                html += `<option value="${i+1}" data-valor-parcela="${parcela}">
                        ${i+1}x de ${Numeral(preco).format('R$0.00')} - (Total: ${Numeral(valor_total).format('R$0.00')})
                    </option>`;
            }
        }

        installments.append(html);
    }

    /**
     * Seta o valor da parcela em um campo hidden
     *
     * @return  {void}
     */
    setValorParcela() {
        let option = installments.find(":selected");

        installmentAmount.val(
            option.data('valor-parcela')
        );

        return this;
    }

    /**
     * Habilita os campos para pagamento com cartão de crédito
     *
     * @return {void}
     */
    habilitaCartao() {
        this.paymentForm.find('input:disabled, select:disabled').each(function() {
            $(this).removeAttr('disabled')
                   .prop('autocomplete', 'off');
        });

        cardNumber.focus();
    }

    /**
     * Desabilita os campos para pagamento com cartão de crédito
     *
     * @return {void}
     */
    desabilitaCartao() {
        this.paymentForm.find('input:not("[name=tipo_pagamento]"):not("[name=numero_participantes]"):not(:hidden), select')
            .each(function() {
                $(this).prop('disabled', true);
            });
    }

    /**
     * Realiza a busca dos erros encontrados na backend do PagSeguro e envia para
     * que o backend possa traduzir estes erros
     *
     * @param  {array} data
     * @return {void}
     */
    integrationErrors(data) {
        let $this = this;

        let arrayErrors = new Array();

        $.each(data.errors, function(key, value) {
            arrayErrors.push(key);
        });

        if(data.error === true) {

            let url = route('debitos.erros');

            Http.get(url, {errors: arrayErrors}, erros => {
                this.manageErrors(erros);
            });
        }
    }

    /**
     * Recebe os erros traduzidos e injeta-os na página
     *
     * @param  {array} errors
     * @return {void}
     */
    manageErrors(errors) {
        let resposta = JSON.parse(JSON.stringify(errors));

        // Recebe os índices para um array
        let erros = '';

        for(let x in resposta) {
            erros = erros + `<li>${resposta[x]}</li>`;
        }

        $('#ajax-validation-errors').removeClass('hidden').find('ul').append('li').html(erros);
    }

    /**
     * Realiza a requisiçãoa de pagamento
     *
     * @return {void}
     */
    doPayment() {
        let $this   = this;

        let url     = $this.paymentForm.attr('action');
        let dados   = $this.paymentForm.serialize();

        Alert.loading('Enviando solicitação de Pagamento');

        $.ajax({
            url: url,
            type: 'POST',
            data: dados,
            dataType: 'json',
            success: function(e) {
                if( e.url_pagamento.length > 0 ) {

                    Alert.notify('O boleto para pagamento foi gerado', 'success', 'Visualizar Boleto')
                        .then(() => {
                            window.open(e.url_pagamento);
                            location.href = $this.redirecionamento;
                        });

                } else {

                    Alert.notify('A cobrança foi gerada com sucesso')
                        .then(() => {
                            location.href = $this.redirecionamento;
                        });
                }
            },
            error: function(xhr) {
                $this.manageErrors(JSON.parse(xhr.responseText));
            },
            complete: function() {
                Alert.unload();
            }
        });
    }
}

let payment = new Payment();

if($('[name=tipo_pagamento]:checked').val() === 'boleto') {
    payment.desabilitaCartao();
} else {
    payment.habilitaCartao();
}

$("[name=tipo_pagamento]").on('change', function(e){
    if($(this).val() === 'boleto') {
        payment.desabilitaCartao();
    } else {
        payment.habilitaCartao();
    }
});

$('#form-payment').on('submit', function(e) {
    e.preventDefault();

    Alert.loading('Realizando integração com os sistemas de pagamento. Isto pode levar um minuto....');

    if($('[name=tipo_pagamento]:checked').val() === 'boleto') {
        payment.doPayment();
    } else {
        if(cardNumber.val() === '') {
            return false;
        }

        payment.setValorParcela()
               .setSenderHash()
               .setCardToken();
    }
});
