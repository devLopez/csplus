let Alert   = require('Alert');
let CEP     = require('CEP');
let Form    = require('Form');
let Http    = require('Http');

/**
 * Associado
 *
 * classe responsável pelo gerenciamento de operações diversas
 * a serem executadas sobre o associado
 *
 * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
 * @version 2.1.0
 * @since   09/06/2017
 */
class Associado {

    /**
     * Realiza a configuração de pessoa física e jurídica
     *
     * @param {string} pessoa
     */
    static configuraPessoa(pessoa) {
        if(pessoa== 'F') {
            $('#cic').mask('999.999.999-99', {placeholder: '*'});
            $('#estado_civil, #sexo').prop('disabled', false);
            $('#change_nascimento').text('Data de Nascimento');
            $('#change_rg').text('Identidade');
        } else {
            $('#cic').mask('99.999.999/9999-99', {placeholder: '*'});
            $('#estado_civil, #sexo').prop('disabled', true);
            $('#change_nascimento').text('Início das Atividades');
            $('#change_rg').text('Insc. Estadual');
        }

        $('#cic').focus();
    }

    /**
     * Realiza a verifição de existência do novo usuário na base de dados
     *
     * @param {string} nome
     * @param {string} cpf
     */
    static existe(nome, cpf) {

        if(nome == '') {
            Alert.error('Por favor, informe o seu nome');
            $('[name=NOMEDOTITULAR]').focus();
            return false;
        }

        if(cpf == '') {
            Alert.error('Por favor, informe o número de seu documento');
            $('[name=CIC]').focus();
            return false;
        }

        var url = `nome=${nome}&cpf=${cpf}`;

        Http.get(Http.url('/backend/associado/existe'), url, e => {
            if(e.existe == true) {
                Alert.error('Não é possível continuar pois este nome e CPF já se encontram cadastrados em nossa base de dados. Tente novamente');
                $('button[type="submit"]').prop('disabled', true);
            } else {
                $('button[type="submit"]').prop('disabled', false);
            }
        });
    }

    /**
     * Realiza a busca das categorias cadastradas
     *
     * @param   {string} tipo
     * @return  {void}
     */
    static getCategorias(tipo) {
        var url = Http.url('/cadastro/categorias/' + tipo);

        Http.get(url, '', e => {
            var html = '';

            $.each(e, function(codigo, descricao){
                html += `<option value="${codigo}">${descricao}</option>`;
            });

            $('#select-categoria').html(html);
        });
    }
}

$(function () {
    // Roda as funções de formulário
    Form.runAllForms();

    // Verifica se o novo associado é físico ou jurídico
    $('[name=JURIDICO]').on('change', function() {
        Associado.configuraPessoa(
            $(this).val()
        );
    });

    $('[name=CIC]').on('blur', function() {
        let cpf     = $('[name=CIC]').val();
        let nome    = $('[name=NOMEDOTITULAR]').val();

        Associado.existe(nome, cpf);
    });

    $('[name=NOMEDOTITULAR]').on('blur', function(){
        if($('[name=CIC]').val() != '') {
            $('[name=CIC]').trigger('blur');
        }
    });

    // Realiza a busca do endereço residencial
    $('#cep-residencial').blur(function() {
        var num_cep = $(this).val();

        if(num_cep != '') {
            CEP.buscar(
                num_cep,
                $('#endereco-residencial'),
                $('#bairro-residencial'),
                $('#cidade-residencial'),
                $('#estado-residencial')
            );
        }
    });

    $('#cep-comercial').on('blur', function() {
        var num_cep = $(this).val();

        if(num_cep != '') {
            CEP.buscar(
                $(this).val(),
                $('#endereco-comercial'),
                $('#bairro-comercial'),
                $('#cidade-comercial'),
                $('#estado-comercial')
            );
        }
    });

    // Realiza a busca das categorias de cota para o correto cadastro
    $('[name=tipo_associado]').on('change', function(){
        Associado.getCategorias(
            $(this).val()
        );
    });

    $('[name=tipo_associado]:checked').each(function(){
        Associado.getCategorias(
            $(this).val()
        );
    });
});
