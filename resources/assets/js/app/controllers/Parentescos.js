let Utils = require('Utils');

import Parentesco from '../models/Parentesco';

$('#form-cad-parentesco').on('submit', function(e) {
    e.preventDefault();

    let form        = $(this);
    let parentesco  = new Parentesco(form);

    parentesco.save(model => {

        Utils.appendSelectValue(
            $('[name=GRAUPARENTESCO]'),
            model.id,
            model.descricao
        );

        Utils.clearFields(form)
        $('#modal-cadastra-parentesco').modal('hide');
    });
});
