import SwitcheryFactory from '../../factory/SwitcheryFactory';

/**
 * Convites
 *
 * Classe desenvolvida para gerenciamento dos convites
 *
 * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
 * @version 1.0.0
 * @since   19/07/2017
 */
class Convites {
    /**
     * Exceuta o Switchery para ops checkboxes
     *
     * @return {void}
     */
    static runSwitchery() {
        (new SwitcheryFactory).createMultiple('.checkbox-emit-convite');
    }

    /**
     * Recarrega a página após a geração do convite
     *
     * @return  {void}
     */
    static reloadData() {
        $('#btn-gera-convites').on('click', function () {

            setTimeout(function () {
                location.reload(true);
            }, 1000);
        });
    }
}

Convites.runSwitchery();
Convites.reloadData();