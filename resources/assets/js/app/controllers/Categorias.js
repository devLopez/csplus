let Utils = require('Utils');

import Categoria from '../models/Categoria';


$('#form-cad-categoria').on('submit', function(e) {
    e.preventDefault();

    let form        = $(this);
    let categoria   = new Categoria(form);

    categoria.save(model => {

        Utils.appendSelectValue(
            $('[name=CATEGORIADACOTA]'),
            model.id,
            model.descricao
        );

        Utils.clearFields(form);
        $('#modal-cadastra-categoria').modal('hide');
    });
});
