import SwitcheryFactory from '../../factory/SwitcheryFactory';

/**
 * Debitos
 *
 * Classe desenvolvida para gerenciamento da tela de débitos
 *
 * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
 * @version 1.0.0
 * @since 	15/07/2017
 */
class Debitos {
	/**
	 * Inicializa o Switchery nas páginas de débito
	 *
	 * @return  {void}
	 */
	static initSwitchery() {
		(new SwitcheryFactory).createMultiple('.checkbox-pay');
	}
}

window.Debitos = Debitos;