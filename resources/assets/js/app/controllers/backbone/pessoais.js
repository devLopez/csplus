const Alert = require('Alert');

let PessoaisController = Backbone.View.extend({
    /**
     * Recebe o acesso do usuário logado
     */
    acesso: $("#acesso").val(),
    /**
     * Define o elemento principal no qual este módulo irá controlar
     */
    el: $('#tab-pessoais'),
    /**
     * Define os campos do formulário que este módulo controla
     */
    campos: {
        nome        : $('#nome-titular'),
        aquisicao   : $('#data-aquisicao'),
        apelido     : $('#apelido'),
        categoria   : $('#categoria-cota'),
        cep         : $('#cep-residencial'),
        endereco    : $('#endereco-residencial'),
        numero      : $('#numero-residencial'),
        complemento : $('#complemento-residencial'),
        bairro      : $('#bairro-residencial'),
        cidade      : $('#cidade-residencial'),
        estado      : $('#estado-residencial'),
        telefone1   : $('#telefone-r1'),
        telefone2   : $('#telefone-r2'),
        email       : $('#email-pessoal'),
        nascimento  : $('#data-nascimento'),
        rg          : $('#rg'),
        expedidor   : $('#rg-expedidor'),
        expedicao   : $('#rg-expedicao'),
        sexo        : $('#sexo'),
        estadoCivil : $('#estado-civil'),
        observacoes : $('#observacoes')
    },
    /**
     * Define os eventos possíveis de serem executados
     */
    events: {
        "click #link-edit-pessoais"     : "habilitaEdicao",
        "click #edit-pessoais"          : "habilitaEdicao",
        "click #cancela-pessoais"       : "desabilitaEdicao",
        "click #link-cancela-pessoais"  : "desabilitaEdicao",
        "click #link-salva-pessoais"    : 'salvarEdicao',
        "submit #form-edit-pessoais"    : 'salvarEdicao'
    },
    initialize() {
        this.resetCampos();
    },
    /**
     * Habilita a edição dos dados pessoais
     */
    habilitaEdicao() {
        $('#cancela-pessoais, #link-cancela-pessoais').show();
        $('#edit-pessoais, #link-edit-pessoais').hide();
        $('#salva-pessoais').prop('disabled', false);
        $('#link-salva-pessoais').removeClass('disabled');

        Utils.habilitaFormulario($('#form-edit-pessoais'), this.acesso);

        CSPlus.runAllForms();
    },
    /**
     * Desabilita a edição dos dados pessoais
     */
    desabilitaEdicao() {
        $('#cancela-pessoais, #link-cancela-pessoais').hide();
        $('#edit-pessoais, #link-edit-pessoais').show();
        $('#salva-pessoais').prop('disabled', true);
        $('#link-salva-pessoais').addClass('disabled');

        Utils.desabilitaFormulario($('#form-edit-pessoais'), this.acesso);

        this.resetCampos();
    },
    /**
     * Reseta os campos para seus valores padrão
     */
    resetCampos() {
        this.campos.nome.val( dadosPessoais.get('NOMEDOTITULAR') );
        this.campos.aquisicao.val( Utils.dataBR(dadosPessoais.get('DATAAQUISICAO')) );
        this.campos.apelido.val( dadosPessoais.get('APELIDO') );
        this.campos.cep.val( dadosPessoais.get('CEPRESIDENCIAL') );
        this.campos.endereco.val( dadosPessoais.get('ENDRES_LOGRADOURO') );
        this.campos.numero.val( dadosPessoais.get('ENDRES_NUMERO') );
        this.campos.complemento.val( dadosPessoais.get('ENDRES_COMPLEMENTO') );
        this.campos.bairro.val( dadosPessoais.get('BAIRRORESIDENCIAL') );
        this.campos.cidade.val( dadosPessoais.get('CIDADERESIDENCIAL') );
        this.campos.estado.val( dadosPessoais.get('ESTADORESIDENCIAL') );
        this.campos.telefone1.val( dadosPessoais.get('TELEFONER1') );
        this.campos.telefone2.val( dadosPessoais.get('TELEFONER2') );
        this.campos.email.val( dadosPessoais.get('CAIXAPOSTAL') );
        this.campos.nascimento.val( Utils.dataBR(dadosPessoais.get('DATANASCIMENTO')) );
        this.campos.rg.val( dadosPessoais.get('RG') );
        this.campos.expedidor.val( dadosPessoais.get('RG_ORGAO_EXPEDIDOR') );
        this.campos.expedicao.val( Utils.dataBR(dadosPessoais.get('RG_DATA_EXPEDICAO')) );
        this.campos.observacoes.val( dadosPessoais.get('OBSERVACOES') );

        Utils.setSelectBox(dadosPessoais.get('CATEGORIADACOTA'), this.campos.categoria);

        if(dadosPessoais.get('PJ') == 'F') {
            Utils.setSelectBox(dadosPessoais.get('SEXO'), this.campos.sexo);
            Utils.setSelectBox(dadosPessoais.get('ESTADOCIVIL'), this.campos.estadoCivil);
        }
    },
    /**
     * Seta os novos dados no model
     */
    setDados() {
        dadosPessoais.set({
            NOMEDOTITULAR       : this.campos.nome.val(),
            DATAAQUISICAO       : this.campos.aquisicao.val(),
            APELIDO             : this.campos.apelido.val(),
            CATEGORIADACOTA     : this.campos.categoria.val(),
            CEPRESIDENCIAL      : this.campos.cep.val(),
            ENDRES_LOGRADOURO   : this.campos.endereco.val(),
            ENDRES_NUMERO       : this.campos.numero.val(),
            ENDRES_COMPLEMENTO  : this.campos.complemento.val(),
            BAIRRORESIDENCIAL   : this.campos.bairro.val(),
            CIDADERESIDENCIAL   : this.campos.cidade.val(),
            ESTADORESIDENCIAL   : this.campos.estado.val(),
            TELEFONER1          : this.campos.telefone1.val(),
            TELEFONER2          : this.campos.telefone2.val(),
            CAIXAPOSTAL         : this.campos.email.val(),
            DATANASCIMENTO      : this.campos.nascimento.val(),
            RG                  : this.campos.rg.val(),
            RG_ORGAO_EXPEDIDOR  : this.campos.expedidor.val(),
            RG_DATA_EXPEDICAO   : this.campos.expedicao.val(),
            SEXO                : this.campos.sexo.val(),
            ESTADOCIVIL         : this.campos.estadoCivil.val(),
            OBSERVACOES         : this.campos.observacoes.val()
        });
    },
    /**
     * Salva os novos dados
     *
     * @param   {object} event
     */
    salvarEdicao(event) {
        event.preventDefault();

        let $this   = this;
        let nome    = dadosPessoais.get('NOMEDOTITULAR');

        Alert.confirm("Deseja atualizar os dados pessoais de " + nome + "?", 'warning')
            .then(confirmed => {

                if( confirmed ) {
                    this.setDados();

                    dadosPessoais.save({
                        modo: 'pessoais',
                        arquivo: $('#arquivo-pessoais').val(),
                        _method: 'PUT'
                    }, {
                        contentType: "application/json",
                        wait: true,
                        success: function (model, xhr) {

                            Alert.success(xhr.message);
                            $this.desabilitaEdicao();

                            return;
                        },
                        error: function (model, xhr) {
                            Alert.error(xhr.responseText);
                        }
                    });
                }
            });
    }
});


let pessoais = new PessoaisController();

$(document).on('shown.bs.tab', function () {
    pessoais.desabilitaEdicao();
});
