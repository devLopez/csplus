const Alert = require('Alert');

let BancariosController = Backbone.View.extend({
    /**
     * Define o elemento onde este controller terá maior ação
     */
    el: $('#tab-bancarios'),
    /**
     * Define o acesso do usuário logado
     */
    acesso: $("#acesso").val(),
    /**
     * Define os campos do formulário
     */
    campos: {
        banco           : $('#banco'),
        agencia         : $('#agencia'),
        tipoConta       : $('#tipo-conta'),
        contaCorrente   : $('#conta-corrente'),
        digito          : $('#digito'),
        debitoConta     : $('#debito-aprovado')
    },
    /**
     * Define os eventos possíveis na página
     */
    events: {
        "click #link-edit-bancarios"     : "habilitaEdicao",
        "click #edit-bancarios"          : "habilitaEdicao",
        "click #cancela-bancarios"       : "desabilitaEdicao",
        "click #link-cancela-bancarios"  : "desabilitaEdicao",
        "click #link-salva-bancarios"    : 'salvarEdicao',
        "submit #form-edit-bancarios"    : 'salvarEdicao',
    },
    /**
     * Inicializa a classe
     */
    initialize() {
        this.resetCampos();
    },
    /**
     * Habilita o formulário para edição dos dados
     */
    habilitaEdicao() {
        $('#cancela-bancarios, #link-cancela-bancarios').show();
        $('#edit-bancarios, #link-edit-bancarios').hide();
        $('#salva-bancarios').prop('disabled', false);
        $('#link-salva-bancarios').removeClass('disabled');

        Utils.habilitaFormulario($('#form-edit-bancarios'), this.acesso);
    },
    /**
     * Desabilita o formulário para edição de dados
     */
    desabilitaEdicao() {
        $('#cancela-bancarios, #link-cancela-bancarios').hide();
        $('#edit-bancarios, #link-edit-bancarios').show();
        $('#salva-bancarios').prop('disabled', true);
        $('#link-salva-bancarios').addClass('disabled');

        Utils.desabilitaFormulario($('#form-edit-bancarios'), this.acesso);

        this.resetCampos();
    },
    /**
     * Seta nos campos do formulário os seus valores padrão
     */
    resetCampos() {
        Utils.setSelectBox(dadosPessoais.get('BANCO'), this.campos.banco);
        Utils.setSelectBox(dadosPessoais.get('TIPOCONTA'), this.campos.tipoConta);

        this.campos.agencia.val(dadosPessoais.get('AGENCIA'));
        this.campos.contaCorrente.val(dadosPessoais.get('CONTACORRENTE'));
        this.campos.digito.val(dadosPessoais.get('DIGITOCC'));

        Utils.setCheckBox(this.campos.debitoConta, dadosPessoais.get('DEBITOCONTAAPROVADO'));
    },
    /**
     * Seta os dados editados no model para persistência
     */
    setDados() {

        let debito = (this.campos.debitoConta.is(':checked')) ? 'T' : 'F';

        dadosPessoais.set({
            BANCO               : this.campos.banco.val(),
            AGENCIA             : this.campos.agencia.val(),
            TIPOCONTA           : this.campos.tipoConta.val(),
            CONTACORRENTE       : this.campos.contaCorrente.val(),
            DIGITOCC            : this.campos.digito.val(),
            DEBITOCONTAAPROVADO : debito
        });
    },
    /**
     * Salva os dados na base de dados e no model
     *
     * @param   {object}  event
     */
    salvarEdicao(event) {
        event.preventDefault();

        let $this   = this;
        let nome    = dadosPessoais.get('NOMEDOTITULAR');

        Alert.confirm("Deseja atualizar os dados bancários de " + nome + "?", 'warning')
            .then(confirmed => {

                if ( confirmed ) {
                    this.setDados();

                    dadosPessoais.save({
                        modo: 'bancarios',
                        arquivo: $('#arquivo-bancarios').val(),
                        _method: 'PUT'
                    }, {
                        contentType: "application/json",
                        wait: true,
                        success: function (model, xhr) {
                            Alert.success(xhr.message);
                            $this.desabilitaEdicao();
                        },
                        error: function (model, xhr) {
                            Alert.error(xhr.responseText);
                        }
                    })
                }

            });
    }
});

let bancarios = new BancariosController();

$(document).on('shown.bs.tab', function () {
    bancarios.desabilitaEdicao();
});
