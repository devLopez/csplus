const Alert = require('Alert');

let ProfissionaisController = Backbone.View.extend({
    /**
     * Recebe o acesso do usuário logado
     */
    acesso : $("#acesso").val(),
    /**
     * Define o elemento básico que este model trabalhará
     */
    el: $("#tab-profissionais"),
    campos: {
        'empresa'       : $('#empresa'),
        'funcao'        : $('#funcao'),
        'departamento'  : $("#departamento-empresa"),
        'matricula'     : $("#matricula-empresa"),
        'registro'      : $("#registro-classe"),
        'situacao'      : $('#situacao-empresa'),
        'com_comercial' : $('#complementocomercial'),
        'admissao'      : $('#admissao-empresa'),
        'afastamento'   : $('#afastamento-empresa'),
        'email'         : $('#caixapostal-empresa'),
        'telefone1'     : $('#telefone-1'),
        'telefone2'     : $('#telefone-2'),
        'cep'           : $('#cep-comercial'),
        'logradouro'    : $('#endereco-comercial'),
        'numero'        : $('#numero-comercial'),
        'complemento'   : $('#complemento-comercial'),
        'bairro'        : $('#bairro-comercial'),
        'cidade'        : $('#cidade-comercial'),
        'estado'        : $('#estado-comercial'),
        'end_cobranca'  : $('#endereco-cobranca'),
        'desc_folha'    : $('#desconto-folha'),
        'aposentado'    : $('#aposentado')
    },
    /**
     * Define os eventos possíveis
     */
    events: {
        "click #link-edit-profissionais"    : "habilitaEdicao",
        "click #edit-profissionais"         : "habilitaEdicao",
        "click #cancela-profissionais"      : "desabilitaEdicao",
        "click #link-cancela-profissionais" : "desabilitaEdicao",
        "click #link-salva-profissionais"   : 'salvarEdicao',
        "submit #form-edit-profissionais"   : 'salvarEdicao'
    },
    initialize() {
        this.resetCampos();
    },
    /**
     * habilita os campos do formulário para edição
     */
    habilitaEdicao() {
        $('#cancela-profissionais, #link-cancela-profissionais').show();
        $('#edit-profissionais, #link-edit-profissionais').hide();
        $('#salva-profissionais').prop('disabled', false);
        $('#link-salva-profissionais').removeClass('disabled');

        Utils.habilitaFormulario($('#form-edit-profissionais'), this.acesso);

        CSPlus.runAllForms();
    },
    /**
     * Desabilita o formulário para edição
     */
    desabilitaEdicao() {
        $('#cancela-profissionais, #link-cancela-profissionais').hide();
        $('#edit-profissionais, #link-edit-profissionais').show();
        $('#salva-profissionais').prop('disabled', true);
        $('#link-salva-profissionais').addClass('disabled');

        Utils.desabilitaFormulario($('#form-edit-profissionais'), this.acesso);

        this.resetCampos();
    },
    /**
     * Reseta os dados dos campos para os seus valores originais
     */
    resetCampos() {
        this.campos.empresa.val( dadosPessoais.get('EMPRESA') );
        this.campos.funcao.val( dadosPessoais.get('FUNCAO') );
        this.campos.departamento.val( dadosPessoais.get('DEPARTAMENTO_EMPRESA') );
        this.campos.matricula.val( dadosPessoais.get('MATRICULA_EMPRESA') );
        this.campos.registro.val( dadosPessoais.get('REGISTRO_CLASSE') );
        this.campos.registro.val( dadosPessoais.get('SITUACAO_EMPRESA') );
        this.campos.com_comercial.val( dadosPessoais.get('COMPLEMENTOCOMERCIAL')) ;
        this.campos.admissao.val( Utils.dataBR(dadosPessoais.get('ADMISSAO_EMPRESA')) );
        this.campos.afastamento.val( Utils.dataBR(dadosPessoais.get('AFASTAMENTO_EMPRESA')) );
        this.campos.email.val( dadosPessoais.get('CAIXAPOSTAL_EMPRESA') );
        this.campos.telefone1.val( dadosPessoais.get('TELEFONEC1') );
        this.campos.telefone2.val( dadosPessoais.get('TELEFONEC2') );
        this.campos.cep.val( dadosPessoais.get('CEPCOMERCIAL') );
        this.campos.logradouro.val( dadosPessoais.get('ENDCOM_LOGRADOURO') );
        this.campos.numero.val( dadosPessoais.get('ENDCOM_NUMERO') );
        this.campos.complemento.val( dadosPessoais.get('ENDCOM_COMPLEMENTO') );
        this.campos.bairro.val( dadosPessoais.get('BAIRROCOMERCIAL') );
        this.campos.cidade.val( dadosPessoais.get('CIDADECOMERCIAL') );
        this.campos.estado.val( dadosPessoais.get('ESTADOCOMERCIAL') );

        Utils.setCheckBox(this.campos.end_cobranca, dadosPessoais.get('ENDERECOCOBRANCA'));
        Utils.setCheckBox(this.campos.desc_folha, dadosPessoais.get('DESCONTOFOLHA'));
        Utils.setCheckBox(this.campos.aposentado, dadosPessoais.get('APOSENTADO'));
    },
    /**
     * Seta novos dados no model
     */
    setDados() {
        let end_cobranca    = (this.campos.end_cobranca.is(':checked')) ? 'T' : 'F';
        let desc_folha      = (this.campos.desc_folha.is(':checked')) ? 'T' : 'F';
        let aposentado      = (this.campos.aposentado.is(':checked')) ? 'T' : 'F';

        dadosPessoais.set({
            EMPRESA                 : this.campos.empresa.val(),
            FUNCAO                  : this.campos.funcao.val(),
            DEPARTAMENTO_EMPRESA    : this.campos.departamento.val(),
            MATRICULA_EMPRESA       : this.campos.matricula.val(),
            REGISTRO_CLASSE         : this.campos.registro.val(),
            SITUACAO_EMPRESA        : this.campos.registro.val(),
            COMPLEMENTOCOMERCIAL    : this.campos.com_comercial.val(),
            ADMISSAO_EMPRESA        : this.campos.admissao.val(),
            AFASTAMENTO_EMPRESA     : this.campos.afastamento.val(),
            CAIXAPOSTAL_EMPRESA     : this.campos.email.val(),
            TELEFONEC1              : this.campos.telefone1.val(),
            TELEFONEC2              : this.campos.telefone2.val(),
            CEPCOMERCIAL            : this.campos.cep.val(),
            ENDCOM_LOGRADOURO       : this.campos.logradouro.val(),
            ENDCOM_NUMERO           : this.campos.numero.val(),
            ENDCOM_COMPLEMENTO      : this.campos.complemento.val(),
            BAIRROCOMERCIAL         : this.campos.bairro.val(),
            CIDADECOMERCIAL         : this.campos.cidade.val(),
            ESTADOCOMERCIAL         : this.campos.estado.val(),
            ENDERECOCOBRANCA        : end_cobranca,
            DESCONTOFOLHA           : desc_folha,
            APOSENTADO              : aposentado,
        });
    },
    /**
     * Salva a edição dos dados
     */
    salvarEdicao(event) {
        event.preventDefault();

        let $this   = this;
        let nome    = dadosPessoais.get('NOMEDOTITULAR');

        Alert.confirm("Deseja atualizar os dados prossifissionais de " + nome + "?", 'warning')
            .then(confirmed => {

                if ( confirmed ) {
                    this.setDados();

                    dadosPessoais.save({
                        modo: 'profissionais',
                        arquivo: $('#arquivo-profissionais').val(),
                        _method: 'PUT'
                    }, {
                        contentType: "application/json",
                        wait: true,
                        success: function (model, xhr) {
                            Alert.success(xhr.message);
                            $this.desabilitaEdicao();
                        },
                        error: function (model, xhr) {
                            Alert.error(xhr.responseText);
                        }
                    })
                }

            });
    }
});

let profissionais = new ProfissionaisController();

$(document).on('shown.bs.tab', function () {
    profissionais.desabilitaEdicao();
});
