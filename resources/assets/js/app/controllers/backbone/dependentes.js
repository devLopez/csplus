const Alert = require('Alert');

let dadosDependente = '';

let DependentesController = Backbone.View.extend({
    /**
     * Define o elemento que este controller será responsável
     */
    el: $('#tab-dependentes'),
    /**
     * Define o acesso do usuário logado
     */
    acesso: $('#acesso').val(),
    /**
     * Define os campos do formulário
     */
    campos: {
        cota        : $('#cota-dependente'),
        codigo      : $('#codigo-dependente'),
        nome        : $('#nome-dependente'),
        cpf         : $('#cic-dependente'),
        rg          : $('#rg-dependente'),
        expedicao   : $('#data-expedicao'),
        expedidor   : $('#orgao-expedidor'),
        profissao   : $('#profissao'),
        pai         : $('#pai-dependente'),
        mae         : $('#mae-dependente'),
        telefone1   : $('#telefone1'),
        telefone2   : $('#telefone2'),
        email       : $('#email'),
        sexo        : $('#sexo-dependente'),
        estadoCivil : $('#estado-civil-dependente'),
        nascimento  : $('#nascimento'),
        parentesco  : $('#parentesco')
    },
    /**
     * Define os possíveis eventos na página
     */
    events: {
        'click #novo-dependente'        : 'novoDependente',
        'click #edit-dependente'        : 'habilitaEdicao',
        'click #cancela-dependente'     : 'desabilitaEdicao',
        'submit #form-edit-dependente'  : 'salvarDependente',
        'click .inativa-dependente'     : 'inativar'
    },
    /**
     * Inicializa a classe
     */
    initialize() {

        if ( $('#inbox-table tbody tr').length > 0 ) {
            let td = $('#inbox-table').find('td:first');

            this.setModel(
                td.data('dados')
            );

            this.showDados();

            $('#status').css('display', 'block');
        } else {
            $('#status').css('display', 'none');
        }
    },
    /**
     * Exibe os dados contidos no JSON nos campos do formulário
     */
    showDados() {
        if( dadosDependente ) {

            this.campos.cota.val(dadosDependente.get('COTADEPENDENTE'));
            this.campos.codigo.val(dadosDependente.get('CODIGODEPENDENTE'));
            this.campos.nome.val(dadosDependente.get('NOMEDEPENDENTE'));
            this.campos.cpf.val(dadosDependente.get('CIC'));
            this.campos.rg.val(dadosDependente.get('RG'));
            this.campos.expedidor.val(dadosDependente.get('RG_ORGAO_EXPEDIDOR'));
            this.campos.profissao.val(dadosDependente.get('FUNCAO'));
            this.campos.pai.val(dadosDependente.get('PAI'));
            this.campos.mae.val(dadosDependente.get('MAE'));
            this.campos.telefone1.val(dadosDependente.get('TELEFONE'));
            this.campos.telefone2.val(dadosDependente.get('TELEFONE2'));
            this.campos.email.val(dadosDependente.get('CAIXAPOSTAL'));

            Utils.setSelectBox(
                dadosDependente.get('SEXO'),
                this.campos.sexo
            );

            Utils.setSelectBox(
                dadosDependente.get('ESTADOCIVIL'),
                this.campos.estadoCivil
            );

            this.campos.expedicao.val(
                Utils.dataBR(dadosDependente.get('RG_DATA_EXPEDICAO'))
            );

            this.campos.nascimento.val(
                Utils.dataBR(dadosDependente.get('DATANASCIMENTO'))
            );

            Utils.setSelectBox(
                dadosDependente.get('GRAUPARENTESCO'),
                this.campos.parentesco
            );

            this.setFoto(
                dadosDependente.get('FOTO')
            );

            this.setStatus(
                dadosDependente.get('ATIVO')
            );

            $('#edita-foto-dependente').data('tipo', dadosDependente.get('CODIGODEPENDENTE'));
        }
    },
    setDados() {
        dadosDependente.set({
            COTADEPENDENTE      : this.campos.cota.val(),
            CODIGODEPENDENTE    : this.campos.codigo.val(),
            NOMEDEPENDENTE      : this.campos.nome.val(),
            CIC                 : this.campos.cpf.val(),
            RG                  : this.campos.rg.val(),
            RG_DATA_EXPEDICAO   : this.campos.expedicao.val(),
            RG_ORGAO_EXPEDIDOR  : this.campos.expedidor.val(),
            FUNCAO              : this.campos.profissao.val(),
            PAI                 : this.campos.pai.val(),
            MAE                 : this.campos.mae.val(),
            TELEFONE            : this.campos.telefone1.val(),
            TELEFONE2           : this.campos.telefone2.val(),
            CAIXAPOSTAL         : this.campos.email.val(),
            SEXO                : this.campos.sexo.val(),
            ESTADOCIVIL         : this.campos.estadoCivil.val(),
            DATANASCIMENTO      : this.campos.nascimento.val(),
            GRAUPARENTESCO      : this.campos.parentesco.val()
        });
    },
    /**
     * Seta os novos dados no atributo da tabela de seleção dos dependentes
     */
    setTableDados() {
        novosDados = JSON.stringify(dadosDependente);

        $('[data-id='+ dadosDependente.id +']').data('dados', JSON.parse(novosDados));
    },
    /**
     * Define o status do dependente atual
     *
     * @param ativo
     */
    setStatus(ativo) {
        if( ativo == 'T' ) {
            $('#status').text('Ativo').removeClass('label-danger').addClass('label-success');
            $('#foto_dependente').removeClass('busy').addClass('online');
        } else {
            $('#status').text('Inativo').removeClass('label-success').addClass('label-danger');
            $('#foto_dependente').removeClass('online').addClass('busy');
        }
    },
    /**
     * Seta a foto do dependente selecionado
     *
     * @param foto
     */
    setFoto(foto) {
        if( foto ) {
            $('#foto_dependente').attr('src', 'data:image/jpeg;base64,'+ foto);
        } else {
            $('#foto_dependente').attr('src', '/assets/img/profile-photos/male.png');
        }
    },
    /**
     * Seta os dados do dependente selecionado em um novo model
     *
     * @param dados
     */
    setModel(dados) {
        dadosDependente = new DependenteModel(dados);
    },
    /**
     * Habilita o formulário para edição de dados
     */
    habilitaEdicao() {

        $('#salvar-dependente').prop('disabled', false);
        $('#cancela-dependente').show();
        $('#edit-dependente').hide();
        $('#novo-dependente').prop('disabled', true);

        Utils.habilitaFormulario($('#form-edit-dependente'), this.acesso);
        CSPlus.runAllForms();
    },
    /**
     * Desabilita o formulário para edição de dados
     */
    desabilitaEdicao() {
        Utils.desabilitaFormulario($('#form-edit-dependente'), this.acesso)

        this.showDados();

        $('#salvar-dependente').prop('disabled', true);
        $('#cancela-dependente').hide();
        $('#edit-dependente').show();
        $('#novo-dependente').prop('disabled', false);

        if( $('#form-edit-dependente').data('novo') == true ) {
            let td = $('#inbox-table').find('td:first');

            this.setModel(td.data('dados'));
            this.showDados();
        }
    },
    /**
     * Inativa um associado cadastrado
     *
     * @param   event
     */
    inativar(event) {
        let idDependente    = $(event.currentTarget).data('dependente');
        let idAssociado     = $(event.currentTarget).data('associado');
        let arquivo         = $('#arquivo-dependentes').val();
        let $this           = this;

        Alert.confirm('Deseja realmente inativar este dependente?', 'warning')
            .then(confirmed => {

                if ( confirmed ) {
                    let url = Http.url('/painel/associados/dependente/'+ idAssociado + '/' + idDependente + '/inativar');
                    let data = {
                        ativo : 'F',
                        arquivo : arquivo
                    };

                    Http.post(url, data, null, function(model) {
                        CSPlus.alertSucesso('Dependente Inativado');

                        $('[data-dependente='+ idDependente +']').remove();
                        $this.setDependenteInativo(idDependente);
                    });
                }

            })
    },
    /**
     * Insere no json do dependente o status de inativo
     *
     * @param dependente
     */
    setDependenteInativo: function (dependente) {
        let dados = $('[data-id='+ dependente +']').data('dados');

        dados.ATIVO = 'F';

        $('[data-id='+ dependente +']').data('dados', dados);

        if( dependente == dadosDependente.id ) {
            this.setModel($('[data-id='+ dependente +']').data('dados'));
            this.showDados();
        }
    },
    /**
     * Salva as alterações nos dados de um dependente
     *
     * @param event
     */
    salvarDependente: function (event) {
        event.preventDefault();

        if( $(event.currentTarget).data('novo') ) {
            this.salvarNovoDependente();
            return;
        }

        let $this   = this;
        let nome    = dadosDependente.get('NOMEDEPENDENTE');

        Alert.confirm("Deseja atualizar os dados de " + nome + "?", 'warning')
            .then(confirmed => {

                if ( confirmed ) {
                    this.setDados();

                    dadosDependente.save({
                        arquivo: $('#arquivo-dependentes').val(),
                        _method: 'PUT'
                    }, {
                        contentType: "application/json",
                        success: function (model, xhr) {
                            Alert.success(xhr.message);
                            $this.desabilitaEdicao();
                            $this.setTableDados();
                        },
                        error: function (model, xhr) {
                            Alert.error(xhr.responseText);
                        }
                    })
                }

            });
    },
    /**
     * Habilita o formulário para criação de novo dependete
     */
    novoDependente: function () {
        this.habilitaEdicao();

        $('#nome-dependente').focus();
        $('#form-edit-dependente').data('novo', true);

        Utils.clearFields(
            $('#form-edit-dependente')
        );
    },
    /**
     * Salva um novo dependente na base de dados
     */
    salvarNovoDependente: function () {
        let dados   = $('#form-edit-dependente').serialize();
        let $this   = this;

        Http.post(urlNovoDependente, dados, null, function(model) {
            Alert.success('O dependente foi cadastrado');

            url = location.href;
            url = url.replace('/dependentes', '');
            url = url.replace('/editar', '');
            url = url.replace('/profissionais', '');


            location.href = url + '/dependentes';
        });
    }
});

// Instanncia o Objeto
let dependente = new DependentesController();

// Habilita a troca de dados entre os dependentes cadastros
$(document).on('click', '[data-dados]', function() {
    dependente.setModel(
        $(this).data('dados')
    );

    dependente.showDados();
});

$(document).on('shown.bs.tab', function () {
    dependente.desabilitaEdicao();
});
