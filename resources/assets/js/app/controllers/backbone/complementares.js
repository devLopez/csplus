const Alert = require('Alert');

let ComplementaresController = Backbone.View.extend({
    /**
     * Elemento onde todas as ações ocorrerão
     */
    el: $('#tab-complementares'),
    /**
     * Realiza a construção da classe
     */
    initialize: function () {
        this.resetaCampos();
    },
    /**
     * Determina o acesso do usuário
     */
    acesso: $("#acesso").val(),
    /**
     * Determina os campos disponíveis no formulário
     */
    campos: {
        nacionalidade   : $('#nacionalidade'),
        naturalidade    : $('#naturalidade'),
        pai             : $('#pai'),
        mae             : $('#mae'),
        sangue          : $('#tipo-sanguineo'),
        responsavel     : $('#responsavel'),
        corretor        : $('#corretor'),
        benemerito      : $('#benemerito')
    },
    /**
     * Determina os possíveis eventos na página
     */
    events: {
        "click #link-edit-complementares"       : "habilitaEdicao",
        "click #edit-complementares"            : "habilitaEdicao",
        "click #cancela-complementares"         : "desabilitaEdicao",
        "click #link-cancela-complementares"    : "desabilitaEdicao",
        "click #link-salva-complementares"      : 'salvarEdicao',
        "submit #form-edit-complementares"      : 'salvarEdicao',
        "keyup #responsavel"                    : 'getResponsavel',
        "keyup #corretor"                       : 'getCorretor'
    },
    /**
     * Habilita o formulário para edição
     */
    habilitaEdicao() {
        $('#cancela-complementares, #link-cancela-complementares').show();
        $('#edit-complementares, #link-edit-complementares').hide();
        $('#salva-complementares').prop('disabled', false);
        $('#link-salva-complementares').removeClass('disabled');

        Utils.habilitaFormulario($('#form-edit-complementares'), this.acesso);
    },
    /**
     * Desabilita o formulário para edição
     */
    desabilitaEdicao() {
        $('#cancela-complementares, #link-cancela-complementares').hide();
        $('#edit-complementares, #link-edit-complementares').show();
        $('#salva-complementares').prop('disabled', true);
        $('#link-salva-complementares').addClass('disabled');

        Utils.desabilitaFormulario($('#form-edit-complementares'), this.acesso);
        this.resetaCampos();
    },
    /**
     * Reseta os valores dos campos para seus valores padrão
     */
    resetaCampos() {
        this.campos.nacionalidade.val(dadosPessoais.get('NACIONALIDADE'));
        this.campos.naturalidade.val(dadosPessoais.get('NATURALIDADE'));
        this.campos.pai.val(dadosPessoais.get('PAI'));
        this.campos.mae.val(dadosPessoais.get('MAE'));
        this.campos.sangue.val(dadosPessoais.get('SANGUE'));
        this.campos.responsavel.val(dadosPessoais.get('RESPONSAVEL'));
        this.campos.corretor.val(dadosPessoais.get('CORRETOR'));

        Utils.setCheckBox(this.campos.benemerito, dadosPessoais.get('BENEMERITO'));
    },
    /**
     * Seta os novos dados no model
     */
    setDados() {

        let benemerito = (this.campos.benemerito.is(':checked')) ? 'T' : 'F';

        dadosPessoais.set({
            'NACIONALIDADE' : this.campos.nacionalidade.val(),
            'NATURALIDADE'  : this.campos.naturalidade.val(),
            'PAI'           : this.campos.pai.val(),
            'MAE'           : this.campos.mae.val(),
            'SANGUE'        : this.campos.sangue.val(),
            'RESPONSAVEL'   : this.campos.responsavel.val(),
            'CORRETOR'      : this.campos.corretor.val(),
            'BENEMERITO'    : benemerito
        });
    },
    /**
     * Salva os novos dados no model e envia cópia a ser salva no banco de dados
     *
     * @param   {object}  event
     */
    salvarEdicao: function (event) {
        event.preventDefault();

        let $this   = this;
        let nome    = dadosPessoais.get('NOMEDOTITULAR');

        CSPlus.confirm("Deseja atualizar os dados complementares de " + nome + "?", 'warning')
            .then(confirmed => {

                if ( confirmed ) {
                    this.setDados();

                    dadosPessoais.save({
                        modo: 'complementares',
                        arquivo: $('#arquivo-complementares').val(),
                        _method: 'PUT'
                    }, {
                        contentType: "application/json",
                        wait: true,
                        success: function (model, xhr) {
                            Alert.success(xhr.message);
                            $this.desabilitaEdicao();
                        },
                        error: function (model, xhr) {
                            Alert.error(xhr.responseText);
                        }
                    })
                }

            });
    },
    /**
     * Realiza a busca do nome de um associado, baseado em sua matrícula
     *
     * @param event
     */
    getResponsavel: function (event) {
        let matricula = event.target.value;

        if( !matricula.NaN ) {
            Http.get(CSPlus.baseUrl('/backend/associado/' + matricula), '', function(e) {
                let nome = ( e != false ) ? e.nome : '';
                $('#nome-responsavel').text(nome);
            });
        } else {
            Alert.error('Digite apenas números');
        }
    },
    /**
     * Realiza a busca de um corretor, baseado no ID
     *
     * @param event
     */
    getCorretor: function (event) {
        let colaborador = event.target.value;

        if( !colaborador.NaN ) {
            Http.get(CSPlus.baseUrl('/backend/colaborador/' + colaborador), '', function(e) {
                var nome = (e != false) ? e.nome : '';
                $('#nome-colaborador').text(nome);
            });
        } else {
            Alert.error('Digite apenas números');
        }
    }
});

let complementares = new ComplementaresController();

$(document).on('shown.bs.tab', function () {
    complementares.desabilitaEdicao();
});
