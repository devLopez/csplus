import SwitcheryFactory from '../../factory/SwitcheryFactory';

/**
 * Auth
 *
 * Classe desenvolvida para gerenciamento da tela de login
 *
 * @author  Matheus Lopes Santos <fale_com_lopez@hotmail.com>
 * @version 1.0.0
 * @since 	15/07/2017
 */
class Auth {
	constructor() {
		(new SwitcheryFactory).createOne('is_juridica');
	}

	/**
	 * @param  {string} mask  Máscara a ser aplcada
	 * @return {this}
	 */
	maskField(mask) {
		var input = $('[data-mask]');

        input.mask(mask, {placeholder: '*'});
        input.focus();

        return this;
	}

	/**
	 * Executa a troca de máscara no switch
	 * 
	 * @return {this}
	 */
	changeMaskField() {
		var $this = this;

		$('#is_juridica').on('change', function(){
            if($(this).is(':checked')) {
                $this.maskField('99.999.999/9999-99');
            } else {
                $this.maskField('999.999.999-99');
            }
        });

        if($('#is_juridica').is(':checked')) {
            this.maskField('99.999.999/9999-99');
        }

        return this;
	}
}

window.Auth = Auth;
