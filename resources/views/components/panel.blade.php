<div class="panel">
    <div class="panel-heading">
        <div class="panel-control">
            @if(isset($panel_button))
                {{ $panel_button }}
            @endif
        </div>
        <h3 class="panel-title">{{ $title }}</h3>
    </div>

    @if(isset($body))
        {{ $body }}
    @else
        <div class="panel-body">
            {{ $slot }}
        </div>
    @endif

    @if(isset($footer))
        <div class="panel-footer">
            {{ $footer }}
        </div>
    @endif
</div>
