<div class="modal fade" id="{{ $id }}" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="pci-cross pci-circle"></i>
                </button>

                <h4 class="modal-title">
                    {{ $title }}
                </h4>

            </div>

            {{ $slot }}

            @if(isset($body))
                <div class="modal-body">
                    {{ $body }}
                </div>
            @endif

            @if(isset($footer))
                <div class="modal-footer">
                    {{ $footer }}
                </div>
            @endif

        </div>
    </div>
</div>