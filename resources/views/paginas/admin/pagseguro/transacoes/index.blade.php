@extends('template.default')

@section('titulo-pagina', 'Busca de Associados')

@push('css')
    <link rel="stylesheet" href="{{ asset('assets/js/plugin/datepicker/bootstrap-datepicker.min.css') }}">
@endpush

@section('conteudo')

<input type="hidden" id="notification-url" value="{{ route('get.payment') }}">

<div id="transacoes">
    <div class="row">
        <div class="col col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="panel">

                {!! Form::open([
                    'route' => 'transacoes.consultar',
                    'method' => 'GET',
                    'id' => 'form-consulta-transacoes'
                ]) !!}

                <div class="panel-body">

                    <div class="row">

                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="form-group has-feedback">
                                <label class="control-label"><b>Data Inicial</b></label>
                                {!! Form::text('data_inicio', null, ['required', 'data-mask' => '99/99/9999', 'class' => 'form-control']) !!}
                                <i class="fa fa-calendar form-control-feedback"></i>
                            </div>

                        </div>

                        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                            <div class="form-group has-feedback">
                                <label class="control-label"><b>Data Final</b></label>
                                {!! Form::text('data_final', null, ['required', 'data-mask' => '99/99/9999', 'class' => 'form-control']) !!}
                                <i class="fa fa-calendar form-control-feedback"></i>
                            </div>

                        </div>

                    </div>

                </div>
                <div class="panel-footer text-right">
                    <button type="submit" class="btn btn-primary">Pesquisar</button>
                </div>
                {!! Form::close() !!}

            </div>
        </div>
    </div>

    <div class="row">
        <div class="col col-lg-12 col-md-12">

            @include('errors.empty-records', ['message' => 'Não existem transações a serem exibidas'])

            <div class="panel" id="painel-transacoes" style="display: none">

                <div class="panel-heading">
                    <h3 class="panel-title">Transações Disponíveis</h3>
                </div>

                <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-hover table-condensed">
                            <thead>
                            <tr>
                                <th>Data</th>
                                <th>Status da Transação</th>
                                <th>Valor</th>
                                <th class="text-center">Opções</th>
                            </tr>
                            </thead>
                            <tbody id="table-transacoes">

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>



        </div>
    </div>
</div>
@endsection

@section('javascript')
<script type="text/javascript" src="{{ asset('assets/js/plugin/datepicker/bootstrap-datepicker.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/plugin/datepicker/locales/pt-br.js') }}"></script>
    <script type="text/javascript">
        $('#menu-pagseguro').addClass('active');
        $('#menu-ps-transacoes').addClass('active-link');
        CSPlus.drawBreadCrumb();

        var $inicio = $('[name=data_inicio]');
        var $final  = $('[name=data_final]');

        $inicio.datepicker({
            format: "dd/mm/yyyy",
            autoclose: true,
            todayHighlight: false,
            language: 'pt-BR',
            startDate: '-6m',
            endDate: '-1d'
        });

        $final.datepicker({
            format: "dd/mm/yyyy",
            autoclose: true,
            todayHighlight: false,
            language: 'pt-BR',
            startDate: '-6m',
            endDate: '-1d'
        });
    </script>
@endsection
