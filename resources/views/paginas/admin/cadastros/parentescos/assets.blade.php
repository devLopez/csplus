@push('scripts')
    @if(isset($modal) && $modal == false)
        <script type="text/javascript">
            $('#menu-cadastros').addClass('active');
            $('#menu-cad-parentescos').addClass('active-link');
            CSPlus.drawBreadCrumb();
        </script>
    @endif
@endpush
w