@extends('template.default')

@section('titulo-pagina', 'Cadastro de Parentescos')

@include('Admin::cadastros.parentescos.assets', ['modal' => false])

@section('conteudo')
<div class="row">
    <div class="col col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="panel">
            {!! Form::open(['route' => 'post.parentesco', 'method' => 'POST', 'id' => 'form-cad-parentesco']) !!}
                <div class="panel-body">
                    @include('Admin::cadastros.parentescos.form')
                </div>
                <div class="panel-footer text-right">
                    <button type="submit" class="btn btn-success">Salvar Parentesco</button>
                </div>

            {!! Form::close() !!}
        </div>
    </div>
</div>
@endsection
