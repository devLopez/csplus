@include('Admin::cadastros.parentescos.assets')

<div class="bootbox modal fade" tabindex="-1" role="dialog" id="modal-cadastra-parentesco">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="pci-cross pci-circle"></i>
                </button>
                <h4 class="modal-title"><b>Cadastro de Parentesco</b></h4>
            </div>

            {!! Form::open(['route' => 'post.parentesco', 'method' => 'POST', 'id' => 'form-cad-parentesco']) !!}

                <div class="modal-body">
                    @include('Admin::cadastros.parentescos.form')
                </div>

                <div class="modal-footer">
                    <button type="reset" class="btn btn-default" data-dismiss="modal" aria-label="Cancelar">Cancelar</button>
                    <button type="submit" class="btn btn-success">Salvar Parentesco</button>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
