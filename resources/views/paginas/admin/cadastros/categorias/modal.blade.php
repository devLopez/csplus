@include('Admin::cadastros.categorias.assets')

<div class="bootbox modal fade" tabindex="-1" role="dialog" id="modal-cadastra-categoria">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="pci-cross pci-circle"></i>
                </button>
                <h4 class="modal-title"><b>Cadastro de Categoria</b></h4>
            </div>

            {!! Form::open(['route' => 'post.categoria', 'method' => 'POST', 'id' => 'form-cad-categoria']) !!}

                <div class="modal-body">
                    @include('Admin::cadastros.categorias.form')
                </div>

                <div class="modal-footer">
                    <button type="reset" class="btn btn-default" data-dismiss="modal" aria-label="Cancelar">Cancelar</button>
                    <button type="submit" class="btn btn-success">Salvar Categoria</button>
                </div>
            {!! Form::close() !!}
        </div>
    </div>
</div>
