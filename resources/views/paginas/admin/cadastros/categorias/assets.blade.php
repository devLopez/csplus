@push('css')
    <link rel="stylesheet" href="{{ asset('assets/js/plugin/summernote/summernote.css') }}">
@endpush

@push('scripts')
    <script src="{{ asset('assets/js/plugin/summernote/summernote.js') }}"></script>
    <script src="{{ asset('assets/js/plugin/summernote/lang/summernote-pt-BR.js') }}"></script>
    <script type="text/javascript">
        new Switchery(document.getElementById('chk-inativo'));
        new Switchery(document.getElementById('chk-nao-socio'));

        $('#txt-ficha-inscricao').summernote({
            height: '230px',
            lang: 'pt-BR',
            toolbar: [
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['font', ['strikethrough', 'superscript', 'subscript']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['height', ['height']]
            ]
        });

        @if(isset($modal) && $modal == false)
            $('#menu-cadastros').addClass('active');
            $('#menu-cad-categorias').addClass('active-link');
            CSPlus.drawBreadCrumb();
        @endif
    </script>
@endpush
