@extends('template.default')

@section('titulo-pagina', 'Cadastro de Categorias')

@include('Admin::cadastros.categorias.assets', ['modal' => false])

@section('conteudo')
<div class="row">
    <div class="col col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="panel">
            {!! Form::open(['route' => 'post.categoria', 'method' => 'POST', 'id' => 'form-cad-categoria']) !!}
                <div class="panel-body">
                    @include('Admin::cadastros.categorias.form')
                </div>
                <div class="panel-footer text-right">
                    <button type="submit" class="btn btn-success">Salvar Categoria</button>
                </div>

            {!! Form::close() !!}
        </div>
    </div>
</div>
@endsection
