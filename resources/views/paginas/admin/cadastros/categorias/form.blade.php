{!! Form::hidden('INATIVA', 'F') !!}
{!! Form::hidden('CATEGORIA_NAOSOCIO', 'F') !!}

<div class="row">
    <div class="col-xs-12">
        <div class="form-group">
            <label class="control-label"><b>Nome da Categoria</b></label>
            {!! Form::text('DESCRICAO', null, ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
        {!! Form::checkbox('INATIVA', 'T', null, ['id' => 'chk-inativo', 'data-switchery' => 'true']) !!}
        Categoria Inativa
    </div>
    <div class="col-lg-5 col-md-5 col-sm-6 col-xs-12">
        {!! Form::checkbox('CATEGORIA_NAOSOCIO', 'T', null, ['id' => 'chk-nao-socio', 'data-switchery' => 'true']) !!}
        Categoria p/ Não Sócio
    </div>
</div>

<div class="row pad-top">
    <div class="col-lg-12">
        <div class="form-group">
            <label class="control-label"><b>Texto da Ficha de Inscrição</b></label>
            {!! Form::textarea('TEXTO_FICHAINSCRICAO', null, ['class' => 'form-control', 'id' => 'txt-ficha-inscricao']) !!}
        </div>
    </div>
</div>
