@extends('template.default')

@section('titulo-pagina', 'Relatório de Dependentes')

@section('conteudo')
    <div class="row">
        <div class="col col-lg-12">

            <div class="panel">
                <div class="panel-heading">
                    <h3 class="panel-title">Opções de Geração</h3>
                </div>
                {!! Form::open(['route' => 'rel.dependentes.gerar', 'method' => 'GET', 'target' => '_blank']) !!}
                    <div class="panel-body">

                        @include('Relatorio::dependentes.form')
                    </div>

                    <div class="panel-footer text-right">
                        <button class="btn btn-primary" type="submit">Pesquisar</button>
                    </div>
                {!! Form::close() !!}
            </div>

        </div>
    </div>
@endsection

@section('javascript')
    <script>
        $('#menu-relatorios').addClass('active');
        $('#menu-rel-dependentes').addClass('active-link');

        CSPlus.drawBreadCrumb();

        // Carrega o plugin da máscara de campos
        CSPlus.loadScript('{{ asset('assets/js/plugin/masked-input/jquery.maskedinput.js') }}', function () {
            CSPlus.runAllForms();
        });
    </script>
@endsection
