@inject('associados', 'CSPlus\Repositories\Associados\AssociadoRepository')
@inject('parentescos', 'CSPlus\Repositories\Associados\ParentescosRepository')

@php
    $parentesco     = $parentescos->pluck('DESCRICAO', 'CODIGO');
    $sexo           = $associados->getGenero();
    $estado_civil   = $associados->getEstadoCivil()
@endphp

<div class="row">
    <div class="col-sm-4 col-xs-12">
        <div class="form-group">
            <label class="control-label">Parentesco</label>
            {!! Form::select('parentesco', $parentesco, old('parentesco'), ['placeholder' => 'Todos', 'class' => 'form-control']) !!}
        </div>
    </div>

    <div class="col-sm-4 col-xs-12">
        <div class="form-group">
            <label class="control-label">Sexo</label>
            {!! Form::select('sexo', $sexo, old('sexo'), ['placeholder' => 'Todos', 'class' => 'form-control']) !!}
        </div>
    </div>

    <div class="col-sm-4 col-xs-12">
        <div class="form-group">
            <label class="control-label">Estado Civil</label>
            {!! Form::select('estado_civil', $estado_civil, old('estado_civil'), ['placeholder' => 'Todos', 'class' => 'form-control']) !!}
        </div>
    </div>
</div>

<div class="row">
    <div class="col-sm-4 col-xs-12">
        <label>Data de Nascimento</label>
        <div class="row">

            <div class="col-sm-12">
                <div class="form-group has-feedback">
                    {!! Form::text('nascimento_inicio', old('nascimento_inicio'), ['data-mask' => '99/99/9999', 'class' => 'datepicker', 'placeholder' => '**/**/****', 'class' => 'form-control']) !!}
                    <i class="fa fa-calendar form-control-feedback"></i>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="form-group has-feedback">
                    {!! Form::text('nascimento_final', old('nascimento_final'), ['data-mask' => '99/99/9999', 'class' => 'datepicker', 'placeholder' => '**/**/****', 'class' => 'form-control']) !!}
                    <i class="fa fa-calendar form-control-feedback"></i>
                </div>
            </div>
        </div>
    </div>

    <div class="col-sm-4 col-xs-12">
        <div class="form-group">
            <label class="control-label">Idade</label>
            {!! Form::text('idade', old('idade'), ['class' => 'form-control']) !!}
        </div>
    </div>
</div>
