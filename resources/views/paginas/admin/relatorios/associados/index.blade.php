@extends('template.default')

@section('titulo-pagina', 'Relatório de Associados')

@section('conteudo')
    <div class="row">
        <div class="col col-lg-12 col-md-12 col-sm-12 col-xs-12">

            <div class="panel">
                <header class="panel-heading">
                    <h3 class="panel-title">Opções de Geração</h3>
                </header>

                {!! Form::open(['route' => 'rel.associado.gerar', 'method' => 'GET', 'class' => 'smart-form', 'target' => '_blank']) !!}
                    <div class="panel-body">
                        @include('Relatorio::associados.form')
                    </div>
                    <div class="panel-footer text-right">
                        <button class="btn btn-primary" type="submit">Pesquisar</button>
                    </div>
                {!! Form::close() !!}

            </div>

        </div>
    </div>
@endsection

@section('javascript')
    <script>
        $('#menu-relatorios').addClass('active');
        $('#menu-rel-associado').addClass('active-link');
        CSPlus.drawBreadCrumb();

        // Carrega o plugin da máscara de campos
        CSPlus.loadScript('{{ asset('assets/js/plugin/masked-input/jquery.maskedinput.js') }}', function () {
            CSPlus.runAllForms();
        });

        $(function(){
            new Switchery(document.getElementById('aposentados'));
            new Switchery(document.getElementById('desconto-folha'));
        });
    </script>
@endsection
