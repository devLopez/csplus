<div class="row">
    <div class="col-lg-4 col-md-4 col-sm-6 col-lg-12">
        <div class="form-group">
            <label class="contro-label">Tipo de Relatório</label>
            {!! Form::select('tipo', $tipo, old('tipo'), ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <div class="form-group">
            <label class="control-label">Situação</label>
            {!! Form::select('situacao', $situacao, old('situacao'), ['placeholder' => 'Todos', 'class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
        <div class="form-group">
            <label class="control-label">Selecionar entre as matriculas</label>
            <div class="row">
                <div class="col-xs-6">
                    {!! Form::text('matricula_inicio', old('matricula_inicio'), ['class' => 'form-control']) !!}
                </div>
                <div class="col-xs-6">
                    {!! Form::text('matricula_termino', old('matricula_termino'), ['class' => 'form-control']) !!}
                </div>
            </div>
        </div>
    </div>
</div>



<div class="row">

    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">

        <div class="row">
            <div class="col-xs-12">
                {!! Form::checkbox('aposentados', 'T', old('aposentados'), ['id' => 'aposentados', 'data-switchery' => 'true']) !!}
                Seleciona Aposentados
            </div>
        </div>

        <div class="row pad-top">
            <div class="col-xs-12">
                {!! Form::checkbox('desconto_folha', 'T', old('desconto_folha'), ['id' => 'desconto-folha', 'data-switchery' => 'true']) !!}
                Sócios com desc. em folha
            </div>
        </div>
    </div>


    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
        <div class="form-group">
            <label class="control-label">Banco</label>
            {!! Form::select('banco', $banco, old('banco'), ['placeholder' => 'Todos', 'class' => 'form-control']) !!}
        </div>
    </div>
    <div class="col-lg-6 col-md-6 col-sm-3 col-xs-12">
        <div class="form-group">
            <label class="control-label">Categoria da Cota</label>
            {!! Form::select('categoria', $categorias, old('categoria'), ['placeholder' => 'Todas', 'class' => 'form-control']) !!}
        </div>
    </div>

</div>



<div class="row">
    <div class="col-sm-4">
        <label class="control-label">Data de Aquisição</label>

        <div class="row">
            <div class="col-sm-12">
                <div class="form-group has-feedback">
                    {!! Form::text('aquisicao_inicio', old('aquisicao_inicio'), ['data-mask' => '99/99/9999', 'class' => 'form-control', 'placeholder' => '**/**/****']) !!}
                    <i class="fa fa-calendar form-control-feedback"></i>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-12">
                <div class="form-group has-feedback">
                    {!! Form::text('aquisicao_final', old('aquisicao_final'), ['data-mask' => '99/99/9999', 'class' => 'form-control', 'placeholder' => '**/**/****']) !!}
                    <i class="fa fa-calendar form-control-feedback"></i>
                </div>
            </div>
        </div>
    </div>

    <div class="col-sm-4">
        <label class="control-label">Data de Admissão</label>

        <div class="row">
            <div class="col-sm-12">
                <div class="form-group has-feedback">
                    <i class="fa fa-calendar form-control-feedback"></i>
                    {!! Form::text('admissao_inicio', old('admissao_inicio'), ['data-mask' => '99/99/9999', 'class' => 'form-control', 'placeholder' => '**/**/****']) !!}
                </div>
            </div>
            <div class="col-sm-12">
                <div class="form-group has-feedback">
                    <i class="fa fa-calendar form-control-feedback"></i>
                    {!! Form::text('admissao_final', old('admissao_final'), ['data-mask' => '99/99/9999', 'class' => 'form-control', 'placeholder' => '**/**/****']) !!}
                </div>
            </div>
        </div>
    </div>

    <div class="col-sm-4">
        <label>Data de Afastamento</label>
        <div class="row">
            <div class="col-sm-12">
                <div class="form-group has-feedback">
                    {!! Form::text('afastamento_inicio', old('afastamento_inicio'), ['data-mask' => '99/99/9999', 'class' => 'form-control', 'placeholder' => '**/**/****']) !!}
                    <i class="fa fa-calendar form-control-feedback"></i>
                </div>
            </div>
            <div class="col-sm-12">
                <div class="form-group has-feedback">
                    {!! Form::text('afastamento_final', old('afastamento_final'), ['data-mask' => '99/99/9999', 'class' => 'form-control', 'placeholder' => '**/**/****']) !!}
                    <i class="fa fa-calendar form-control-feedback"></i>
                </div>
            </div>
        </div>
    </div>
</div>
