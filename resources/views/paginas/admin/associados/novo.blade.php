@extends('template.default')

@inject('associadoRepository', 'CSPlus\Repositories\Associados\AssociadoRepository')
@inject('categoriasRepository', 'CSPlus\Repositories\Associados\CategoriasRepository')

@php
    $sexo           = $associadoRepository->getGenero();
    $estadoCivil    = $associadoRepository->getEstadoCivil();

    $categorias     = $categoriasRepository->pluck('DESCRICAO', 'CODIGO');
@endphp

@section('titulo-pagina', 'Cadastro de Associado')

@section('conteudo')

    <div class="row" id="validation_errors_container" style="display: none">
        <div class="col col-lg-12">
            <div class="alert alert-danger" id="validation_errors"></div>
        </div>
    </div>

    <div class="row">
        <div class="col col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <div class="panel">

                {!! Form::open(['route' => 'associado.salvar']) !!}
                    <div class="panel-body">

                        <input type='hidden' name="arquivo-novo-associado" value="paginas.admin.associados.novo.php">

                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label class="control-label">Nome do Associado</label>
                                    {!! Form::text('NOMEDOTITULAR', old('NOMEDOTITULAR'), ['autofocus', 'required', 'maxlength' => 40, 'class' => 'form-control']) !!}
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label class="control-label">Tipo de Documento</label>
                                    <div class="radio">
                                        {!! Form::radio('JURIDICO', 'F', old('JURIDICO'), ['checked', 'id' => 'radio-cpf', 'class' => 'magic-radio']) !!}
	                                    <label for="radio-cpf">CPF</label>

                                        {!! Form::radio('JURIDICO', 'T', old('JURIDICO'), ['id' => 'radio-cnpj', 'class' => 'magic-radio']) !!}
	                                    <label for="radio-cnpj">CNPJ</label>
	                                </div>
                                </div>
                            </div>
                            <div class="col col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                <div class="form-group">
                                    <label class="control-label">Num. do Documento</label>
                                    {!! Form::text('CIC', old('CIC'), ['data-mask'=>'999.999.999-99', 'id'=>'cic', 'maxlength'=>18, 'class' => 'form-control']) !!}
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                <label class="control-label">Categoria da Cota</label>
                                <div class="input-group">
                                    {!! Form::select('CATEGORIADACOTA', array_map('utf8_encode', $categorias), old('CATEGORIADACOTA'), ['class' => 'form-control']) !!}

                                    <span class="input-group-addon">
                                        <a href="#modal-cadastra-categoria" class="add-tooltip" title="Adicionar Categoria" data-toggle="modal">
                                            <i class="fa fa-plus"></i>
                                        </a>
                                    </span>

                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label class="control-label">Sexo</label>
                                    {!! Form::select('SEXO', $sexo, old('SEXO'), ['id'=>"sexo", 'class'=>"form-control"]) !!}
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                <label class="control-label">Estado Civil</label>
                                {!! Form::select('ESTADOCIVIL', $estadoCivil, old('ESTADOCIVIL'), ['id' => 'estado-civil', 'class'=>'form-control', 'class' => 'form-control']) !!}
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label class="control-label">CEP</label>
                                    {!! Form::text('CEPRESIDENCIAL', old('CEPRESIDENCIAL'), ['id'=>"cep-residencial", 'maxlength'=>9, 'data-mask'=>"99999-999", 'class' => 'form-control']) !!}
                                </div>
                            </div>
                            <div class="col col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label class="control-label">Endereço</label>
                                    {!! Form::text('ENDRES_LOGRADOURO', old('ENDRES_LOGRADOURO'), ['id'=>"endereco-residencial", 'maxlength' => 40, 'class' => 'form-control']) !!}
                                </div>
                            </div>
                            <div class="col col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label class="control-label">Número</label>
                                    {!! Form::text('ENDRES_NUMERO', old('ENDRES_NUMERO'), ['id'=>"numero-residencial", 'maxlength'=>11, 'class' => 'form-control']) !!}
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label class="control-label">Complemento</label>
                                    {!! Form::text('ENDRES_COMPLEMENTO', old('ENDRES_COMPLEMENTO'), ['id'=>"complemento-residencial", 'maxlength'=>15, 'class' => 'form-control']) !!}
                                </div>
                            </div>
                            <div class="col col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label class="control-label">Bairro</label>
                                    {!! Form::text('BAIRRORESIDENCIAL', old('BAIRRORESIDENCIAL'), ['id'=>"bairro-residencial", 'maxlength'=>20, 'class' => 'form-control']) !!}
                                </div>
                            </div>
                            <div class="col col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label class="control-label">Cidade</label>
                                    {!! Form::text('CIDADERESIDENCIAL', old('CIDADERESIDENCIAL'), ['id'=>"cidade-residencial", 'maxlength'=>20, 'class' => 'form-control']) !!}
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label class="control-label">Estado</label>
                                    {!! Form::text('ESTADORESIDENCIAL', old('ESTADORESIDENCIAL'), ['id'=>"estado-residencial", 'maxlength'=>2, 'class' => 'form-control']) !!}
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label class="control-label">
                                        Telefone 1
                                    </label>
                                    {!! Form::text('TELEFONER1', old('TELEFONER1'), ['id'=>"telefone-residencial", 'maxlength'=>20, 'class' => 'form-control', 'data-telefone' => 'true']) !!}
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label class="control-label">Telefone 2</label>
                                    {!! Form::text('TELEFONER2', old('TELEFONER2'), ['id'=>"telefone_residencial2", 'maxlength'=>20, 'class' => 'form-control', 'data-telefone' => 'true']) !!}
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label class="control-label">E-mail</label>
                                    {!! Form::email('CAIXAPOSTAL', old('CAIXAPOSTAL'), ['maxlength'=>100, 'class' => 'form-control']) !!}
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label class="control-label">
                                        <span id="change_nascimento">Data de Nascimento</span>
                                    </label>
                                    {!! Form::text('DATANASCIMENTO', old('DATANASCIMENTO'), ['id'=>"data_nascimento", 'data-mask'=>"99/99/9999", 'class' => 'form-control']) !!}
                                </div>

                            </div>
                            <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                <div class="form-group">
                                    <label class="control-label">
                                        <span id="change_rg">Identidade</span>
                                    </label>
                                    {!! Form::text('RG', old('RG'), ['id'=>"rg", 'maxlength'=>20, 'class' => 'form-control']) !!}
                                </div>
                            </div>
                        </div>

                    </div>

                    <div class="panel-footer text-right">
                        <button type="submit" class="btn btn-primary">Salvar</button>
                    </div>
                {!! Form::close() !!}

            </div>
        </div>
    </div>

    @include('Admin::cadastros.categorias.modal')
@endsection

@section('javascript')
    <script type="text/javascript" src="{{ asset('assets/js/plugin/masked-input/jquery.maskedinput.js') }}"></script>
    <script type="text/javascript">
        $(function () {
            $('#menu-novo').addClass('active-link');
            CSPlus.drawBreadCrumb();
        });
    </script>
@endsection
