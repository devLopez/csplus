@extends('template.default')

@section('titulo-pagina', 'Busca de Associados')

@section('conteudo')
<div class="row">
    <div class="col col-lg-12 col-md-12 col-sm-12 col-xs-12">
        @component('Components::panel')
            @slot('title', 'Nova Consulta')

            @slot('body')
                {!! Form::model(Request::old(), $form) !!}

                    <div class="panel-body">

                        <div class="row">

                            @if($controlaCotaAlfa == 'T')
                                <div class="col-lg-2 col-md-2 col-sm-2 col-xs-2">

                                    <div class="radio">
                                        {!! Form::radio('busca_por', 'nome', true, ['required', 'class'=>'radiobox', 'class' => 'magic-radio', 'id' => 'radio-busca-nome']) !!}
                                        <label for="radio-busca-nome">Busca por Nome</label>
                                    </div>
                                    <label class="radio">
                                        {!! Form::radio('busca_por', 'matricula', '', ['required', 'class'=>'radiobox', 'class' => 'magic-radio', 'id' => 'radio-busca-matricula']) !!}
                                        <label for="radio-busca-matricula">Busca por Matrícula</label>
                                    </label>

                                </div>
                            @endif

                            <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
                                <div class="radio">
                                    {!! Form::radio('tipo', 'after', true, ['required', 'checked', 'class' => 'magic-radio', 'id' => 'radio-inicia-com']) !!}
                                    <label for="radio-inicia-com">Nome inicia com</label>
                                </div>
                                <div class="radio">
                                    {!! Form::radio('tipo', 'both', false, ['class'=>'magic-radio', 'required', 'id' => 'radio-contem']) !!}
                                    <label for="radio-contem">Nome contém</label>
                                </div>
                            </div>

                            <div class="col col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                <div class="row">
                                    <div class="col col-xs-12">
                                        @php
                                            if(empty($termos)) {
                                                $checked = true;
                                            } else if(isset($termos['ativo'])) {
                                                $checked = true;
                                            } else {
                                                $checked = false;
                                            }
                                        @endphp

                                        {!! Form::checkbox('ativo', 'T', $checked, ['id' => 'ativo', 'data-switchery' => 'true']) !!}
                                        Somente Ativos
                                    </div>
                                </div>

                                <div class="row pad-top">
                                    <div class="col col-xs-12">
                                        {!! Form::checkbox('tipo_cadastro', 'A', old('tipo_cadastro'), ['id' => 'tipo-cadastro', 'data-switchery' => 'true']) !!}
                                        Incluir Não-Sócios
                                    </div>
                                </div>

                            </div>

                            <div class="col col-lg-3 col-md-3 col-sm-6 col-xs-6">
                                <div class="form-group">
                                    <label class="control-label"><b>Nome</b></label>
                                    {!! Form::text('texto', '', ['autofocus', 'maxlength' => 100,  'class'=> 'form-control']) !!}
                                </div>
                            </div>

                        </div>

                    </div>
                    <div class="panel-footer text-right">
                        <button type="submit" class="btn btn-primary">Pesquisar</button>
                    </div>
                {!! Form::close() !!}
            @endslot
        @endcomponent
    </div>
</div>

<div class="row">
    <div class="col col-lg-12 col-md-12 col-sm-12 col-xs-12">

        @if($associados->isNotEmpty())
            <div style="width:200px; margin:0 auto">
                @php
                    $termos = Request::query();
                    $texto  = $termos['texto'] ?? 'null';

                    unset($termos['texto']);
                @endphp
                {!!
                    $associados->appends($termos)->appends('texto', $texto)->render()
                !!}
            </div>

            @component('Components::panel')
                @slot('title', 'Resultados da Busca')

                <div class="table-responsive">
                    <table class="table table-striped table-hover table-condensed">
                        <thead>
                            <tr>
                                <th>Matrícula</th>
                                <th>Nome</th>
                                <th>Nascimento</th>
                                <th>Telefone</th>
                                <th class="text-center">Ações</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($associados as $a)
                                <tr>
                                    <td>{{ $a->NUMERODACOTA }}</td>
                                    <td>{{ utf8_encode($a->NOMEDOTITULAR) }}</td>
                                    <td>{{ $a->DATANASCIMENTO }}</td>
                                    <td>{{ $a->TELEFONER1 }}</td>
                                    <td class="text-center">
                                        <a href="{{ route('associados.exibir', $a->NUMERODACOTA) }}" title="Exibir" class="add-tooltip">
                                            <i class="fam-folder-user"></i>
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            @endcomponent
        @else
            @include('errors.empty-records', ['message' => 'Não foram encontrados associados para a pesquisa'])
        @endif
    </div>
</div>
@endsection

@section('javascript')
    <script type="text/javascript">
        var consulta = {
            init: function () {
                $('[name=busca_por]').on('change', function(){
                    if($(this).val() == 'matricula') {
                        $('[name=tipo]').prop('disabled', true);
                    } else {
                        $('[name=tipo]').prop('disabled', false);
                    }
                });
            }
        };

        $(document).ready(function () {
            $('#menu-consulta').addClass('active-link');
            CSPlus.drawBreadCrumb();

            consulta.init();

            new Switchery(document.getElementById('ativo'));
            new Switchery(document.getElementById('tipo-cadastro'));
        });
    </script>
@endsection
