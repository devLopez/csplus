@extends('template.eventos')

@section('conteudo')

    <div class="row">
        <div class="col col-lg-12 col-md-12">
            <div class="jarviswidget margin-top-10">
                <header>
                    <h2>Dados do Colaborador</h2>
                </header>
                <div>
                    <div class="widget-body no-padding">

                        <div class="row">
                            <div class="col col-lg-3 col-md-3 col-sm-6 col-xs-12">
                                <img src="{{ asset('img.old') }}" title="CSPlus" alt="CSPlus">
                            </div>
                            <div class="col col-lg-9 col-md-9 col-sm-6 col-xs-12">
                                <h1>1º ENCONTRO NACIONAL DE USUÁRIOS DO CSPLUS</h1>
                                <h2>09 de SETEMBRO 2016 (sexta-feira)</h2>
                                <h2>BELO HORIZONTE / MG</h2>
                            </div>
                        </div>


                        <div class="row">

                            @include('errors.eventos.flash-message')

                            <div id="wrapper">
                                <div class="col col-lg-6 col-md-6">

                                    {!! Form::open($form) !!}
                                        <input type="hidden" name="NUMERODACOTA" id="cota" value="">

                                        <fieldset>
                                            <div class="row">
                                                <section class="col col-lg-12 col-md-12 col-xs-12">
                                                    <label><b>Nome do responsável pela inscrição</b></label>
                                                    <label class="input">
                                                        {!! Form::text('nome', old('nome'), ['autofocus', 'required']) !!}
                                                    </label>
                                                </section>
                                            </div>
                                            <div class="row">
                                                <section class="col col-lg-6 col-md-6 col-xs-6">
                                                    <label><b>CPF</b></label>
                                                    <label class="input">
                                                        {!! Form::text('cpf', old('cpf'), ['required', 'data-mask' => '999.999.999-99']) !!}
                                                    </label>
                                                </section>
                                                <section class="col col-lg-6 col-md-6 col-xs-6">
                                                    <label><b>Cargo</b></label>
                                                    <label class="select">
                                                        {!! Form::select('cargo', $cargos, old('cargo'), ['required', 'placeholder' => 'Selecione...']) !!}
                                                        <i></i>
                                                    </label>
                                                </section>
                                            </div>
                                            <div class="row">
                                                <section class="col col-lg-6 col-md-6 col-xs-6">
                                                    <label><b>CNPJ da Empresa em que trabalha</b></label>
                                                    <label class="input">
                                                        {!! Form::text('cnpj', old('cnpj'), ['required', 'id' => 'cnpj', 'data-mask' => '99.999.999/9999-99']) !!}
                                                        <i></i>
                                                    </label>
                                                </section>
                                                <section class="col col-lg-6 col-md-6 col-xs-6">
                                                    <label><b>Nome do Cliente</b></label>
                                                    <label class="input">
                                                        {!! Form::text('NOMEDOASSOCIADO', old('NOMEDOASSOCIADO'), ['readonly', 'id' => 'nome_associado']) !!}
                                                    </label>
                                                </section>
                                            </div>

                                            <div class="row">
                                                <section class="col col-lg-12 col-md-12 col-xs-12">
                                                    <label><b>Inscrição (disponível após identificacao do Cliente)</b></label>
                                                    <label class="select state-disabled">
                                                        {!! Form::select('quantidade', $precos, old('quantidade'), ['required', 'disabled', 'id' => 'inscricao', 'placeholder' => 'Selecione...']) !!}
                                                        <i></i>
                                                    </label>
                                                </section>
                                            </div>
                                        </fieldset>
                                        <footer>
                                            <button type="submit" class="btn btn-primary" disabled id="enviar" data-loading-text="Salvando inscrição...">
                                                Salvar Inscrição
                                            </button>
                                        </footer>

                                    {!! Form::close() !!}
                                </div>
                                <div class="col col-lg-6 col-md-6">
                                    <form class="smart-form">
                                        <fieldset>
                                            <table class="table table-hover">
                                                <thead>
                                                <tr>
                                                    <th colspan="4" class="text-center">Tabela de Valores</th>
                                                </tr>
                                                <tr>
                                                    <th>Até o Dia</th>
                                                    <th>29/Julho</th>
                                                    <th>29/Agosto</th>
                                                    <th>Setembro</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td>Valor por Participante</td>
                                                    <td>
                                                        20% desc.<br>
                                                        R$280,00
                                                    </td>
                                                    <td>
                                                        10% desc.<br>
                                                        R$ 315,00
                                                    </td>
                                                    <td>R$350,00</td>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </fieldset>
                                    </form>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

    @push('scripts')
        <script type="text/javascript" src="{{ asset('assets/js/bootstrap/bootstrap.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/js/plugin/masked-input/jquery.maskedinput.js') }}"></script>
        <script type="text/javascript" src="{{ asset('assets/js/plugin/blockUI/jquery.blockUI.js') }}"></script>
        <script type="text/javascript">

            @if(session('cobranca'))
                $('#wrapper').hide();
            @endif

            @if(session('erro'))
                $('#nome-clube').text($('#nome_associado').val());
                habilitaCampos();
            @endif

            // Habilita as máscaras dos campos
            $('[data-mask]').each(function () {
                mask = $(this).data('mask');

                $(this).mask(mask, {placeholder: '*'});
            });

            // Realiza a busca dos dados do clube
            $('#cnpj').on('blur', function () {
                cnpj = $(this).val();
                if(cnpj) {

                    $.post('{{ url('eventos/clube') }}', {cnpj: cnpj}, function (e) {
                        if(e == false) {
                            alert('Os dados do clube não foram encontrados. Entre em contato com a ClubSystem para realizar o seu cadastro');
                            desabilitaCampos();
                            return false;
                        }

                        $('#nome_associado').val(e.NOMEDOTITULAR);
                        $('#cota').val(e.NUMERODACOTA);

                        habilitaCampos();
                    }, 'json');
                } else {
                    $('#nome-clube').text('');
                    $('#cota').val('');

                    desabilitaCampos();
                }
            });

            // Exibe mensagem no botão de enviar
            $('#enviar').click(function () {
                $(this).button('loading');
            });

            // Habilita os campos do formulário
            function habilitaCampos() {
                $('.select').removeClass('state-disabled');
                $('#inscricao').prop('disabled', false);
                $('#enviar').prop('disabled', false);
            }

            // Desabilita os campos do formulário
            function desabilitaCampos() {
                $('.select').addClass('state-disabled');
                $('#inscricao').prop('disabled', true);
                $('#enviar').prop('disabled', true);
            }

            // Configurações de Ajax
            $(document).ajaxStart(function () {
                $.blockUI({
                    message: 'Carregando dados do cliente',
                    css: {
                        border                  : 'none',
                        padding                 : '15px',
                        backgroundColor         : '#000',
                        '-webkit-border-radius' : '10px',
                        '-moz-border-radius'    : '10px',
                        'border-radius'         : '10px',
                        opacity                 : .5,
                        color                   : '#fff',
                        zIndex                  : 999999
                    }
                });
            }).ajaxComplete(function () {
                $.unblockUI();
            });

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                },
                error: function () {
                    $.unblockUI();
                }
            });
        </script>
    @endpush
@endsection
