@extends('template.default')

@php($matricula = Auth::matricula())

@section('titulo-pagina', 'Meu Dashboard')

@section('conteudo')

    @if(!empty($dashboard))

        <div class="row">
            <div class="col col-lg-4 col-sm-4">
                <div class="panel media middle">
                    <div class="media-left pad-all">
                        <span class="icon-wrap icon-wrap-sm icon-circle bg-success">
                            <i class="fa fa-users fa-2x"></i>
                        </span>
                    </div>
                    <div class="media-body pad-all">
                        <p class="text-2x mar-no text-semibold">{{ $dashboard->DEPENDENTES }}</p>
                        <p class="text-muted mar-no">DEPENDENTE(S)</p>
                    </div>
                </div>
            </div>

            <div class="col col-lg-4 col-sm-4">

                <a href="{{ route('convites.index', [$matricula]) }}">
                    <div class="panel media middle">
                        <div class="media-left pad-all">
                            <span class="icon-wrap icon-wrap-sm icon-circle bg-primary">
                                <i class="fa fa-ticket fa-2x"></i>
                            </span>
                        </div>
                        <div class="media-body pad-all">
                            <p class="text-2x mar-no text-semibold">{{ $dashboard->CONVITES }}</p>
                            <p class="text-muted mar-no">CONVITE(S) EMITIDO(S)</p>
                        </div>
                    </div>
                </a>

            </div>

            <div class="col col-lg-4 col-sm-4">

                <a href="{{ route('debitos.index', [$matricula]) }}">
                    <div class="panel media middle">
                        <div class="media-left pad-all">
                            <span class="icon-wrap icon-wrap-sm icon-circle bg-info">
                                <i class="fa fa-dollar fa-2x"></i>
                            </span>
                        </div>
                        <div class="media-body pad-all">
                            <p class="text-2x mar-no text-semibold">{{ Date::toBr($dashboard->DEBITO) }}</p>
                            <p class="text-muted mar-no">MÊS EM ABERTO</p>
                        </div>
                    </div>
                </a>

            </div>
        </div>

        <div class="row">
            @if($dashboard->EMAIL != null)
                <div class="col col-lg-4 col-sm-4">

                    <a href="{{ route('associados.exibir', [$matricula]) }}">
                        <div class="panel media middle">
                            <div class="media-left pad-all">
                            <span class="icon-wrap icon-wrap-sm icon-circle bg-danger">
                                <i class="fa fa-envelope fa-2x"></i>
                            </span>
                            </div>
                            <div class="media-body pad-all">
                                <p class="text-muted mar-no">{{ $dashboard->EMAIL }}</p>
                            </div>
                        </div>
                    </a>

                </div>
            @endif

            @if($dashboard->ENDERECO_RESIDENCIAL != null)
                <div class="col col-lg-4 col-sm-4">

                    <a href="{{ route('associados.exibir', [$matricula]) }}">
                        <div class="panel media middle">
                            <div class="media-left pad-all">
                                <span class="icon-wrap icon-wrap-sm icon-circle bg-danger">
                                    <i class="fa fa-home fa-2x"></i>
                                </span>
                            </div>
                            <div class="media-body pad-all">
                                <p class="text-muted mar-no">{{ $dashboard->ENDERECO_RESIDENCIAL }}</p>
                            </div>
                        </div>
                    </a>

                </div>
            @endif

            @if($dashboard->ENDERECO_COMERCIAL != null)

                <div class="col col-lg-4 col-sm-4">

                    <a href="{{ route('associados.exibir', [$matricula, 'profissionais']) }}">
                        <div class="panel media middle">
                            <div class="media-left pad-all">
                            <span class="icon-wrap icon-wrap-sm icon-circle bg-danger">
                                <i class="fa fa-building fa-2x"></i>
                            </span>
                            </div>
                            <div class="media-body pad-all">
                                <p class="text-muted mar-no">{{ $dashboard->ENDERECO_COMERCIAL }}</p>
                            </div>
                        </div>
                    </a>

                </div>
            @endif
        </div>

    @else
    @endif
@endsection

@section('javascript')
    <script type="text/javascript">
        $('#menu-dashboard').addClass('active-link');
        CSPlus.drawBreadCrumb();
    </script>
@endsection
