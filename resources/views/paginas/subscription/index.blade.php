@extends('template.subscription')
@section('titulo', 'Novo Cadastro')

@inject('associadoRepository', 'CSPlus\Repositories\Associados\AssociadoRepository')
@inject('categoriasRepository', 'CSPlus\Repositories\Associados\CategoriasRepository')

@php
    $cliente = Client::clube();

    if(!$erro) {
        $sexo           = $associadoRepository->getGenero();
        $estadoCivil    = $associadoRepository->getEstadoCivil();

        $categorias = $categoriasRepository->formatCategories(
            $categoriasRepository->getCategoriesForSubscription(false)
        );
    }
@endphp

@section('conteudo')
    @if($erro)
        <div class="alert alert-block alert-danger">
            <h4><i class="fa fa-times"></i> Atenção</h4>
            <p>{{ $erro }}</p>
        </div>
    @else

        <div class="row">
            <div class="col-lg-12">
                <div class="panel">
                    <header class="panel-heading">
                        <h2 class="panel-title">Novo Cadastro</h2>
                    </header>

                    {!! Form::open(['route' => 'cadastro.store', 'id' => 'form-cad-associado']) !!}
                        <div class="panel-body">

                            @include('errors.eventos.flash-message')

                            {!! Form::hidden('clube', $clube) !!}

                            {!! Form::hidden('ATIVO', 'T') !!}
                            {!! Form::hidden('DATAAQUISICAO', date('Y-m-d')) !!}
                            {!! Form::hidden('DATACADASTRO', date('Y-m-d')) !!}

                            <div class="row">
                                <div class="col-xs-12">
                                    <img class="center-block" src="{{ asset('assets/img/logos/logo_'.$cliente.'.png') }}" width="220">
                                </div>
                            </div>

                            @if($cliente == 301)
                                <div class="row">
                                    <div class="col-lg-12">
                                        <div class="form-group">
                                            <label class="control-label"></label>
                                            <div class="radio">
                                                <input type="radio" name="tipo_associado" class="magic-radio" value="aluno" checked="checked" id="radio-aluno">
                                                <label for="radio-aluno"><b>Sou aluno PPH</b></label>

                                                <input type="radio" name="tipo_associado" class="magic-radio" value="piloto" id="radio-piloto">
                                                <label for="radio-piloto"><b>Sou Piloto (PCH/ PLH)</b></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endif


                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                    <div class="form-group {{ ($errors->has('CATEGORIADACOTA')) ? 'has-error' : '' }}">
                                        <label class="control-label"><b>Categoria</b></label>

                                        @if($cliente == 301)
                                            {!! Form::select('CATEGORIADACOTA', [], old('CATEGORIADACOTA'), ['required', 'class' => 'form-control', 'id' => 'select-categoria']) !!}
                                        @else
                                            {!! Form::select('CATEGORIADACOTA', $categorias, old('CATEGORIADACOTA'), ['required', 'class' => 'form-control']) !!}
                                        @endif

                                        @if($errors->has('CATEGORIADACOTA'))
                                            <div class="note note-danger">{{ $errors->first('CATEGORIADACOTA') }}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-lg-8 col-md-8 col-sm-8 col-xs-12">
                                    <div class="form-group {{ ($errors->has('NOMEDOTITULAR')) ? 'has-error' : '' }}">
                                        <label class="control-label"><b>Nome</b></label>
                                        {!! Form::text('NOMEDOTITULAR', old('NOMEDOTITULAR'), ['autofocus', 'required', 'maxlength' => 40, 'class' => 'form-control']) !!}

                                        @if($errors->has('NOMEDOTITULAR'))
                                            <div class="note note-danger">{{ $errors->first('NOMEDOTITULAR') }}</div>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                    <div class="form-group {{ ($errors->has('CIC')) ? 'has-error' : '' }}">
                                        <label class="control-label"><b>CPF</b></label>
                                        {!! Form::text('CIC', old('CIC'), ['required', 'data-mask'=>'999.999.999-99', 'id'=>'cic', 'maxlength'=>18, 'class' => 'form-control']) !!}

                                        @if($errors->has('CIC'))
                                            <div class="note note-danger">{{ $errors->first('CIC') }}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4">
                                    <div class="form-group {{ ($errors->has('CAIXAPOSTAL')) ? 'has-error' : '' }}">
                                        <label class="control-label"><b>E-mail</b></label>
                                        {!! Form::email('CAIXAPOSTAL', old('CAIXAPOSTAL'), ['required', 'maxlength'=>100, 'class' => 'form-control']) !!}

                                        @if($errors->has('CAIXAPOSTAL'))
                                            <div class="note note-danger">{{ $errors->first('CAIXAPOSTAL') }}</div>
                                        @endif
                                    </div>
                                </div>

                                <div class="col-lg-4 col-md-4">
                                    <div class="form-group {{ ($errors->has('REGISTRO_CLASSE')) ? 'has-error' : '' }}">
                                        <label class="control-label"><b>Cód. ANAC</b></label>
                                        {!! Form::text('REGISTRO_CLASSE', old('REGISTRO_CLASSE'), ['required', 'maxlength'=> 6, 'class' => 'form-control']) !!}

                                        @if($errors->has('REGISTRO_CLASSE'))
                                            <div class="note note-danger">{{ $errors->first('REGISTRO_CLASSE') }}</div>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-4 col-md-4">
                                    <div class="form-group {{ ($errors->has('DATANASCIMENTO')) ? 'has-error' : '' }}">
                                        <label class="control-label"><b id="change_nascimento">Data de Nascimento</b></label>
                                        {!! Form::text('DATANASCIMENTO', old('DATANASCIMENTO'), ['required', 'data-mask'=>"99/99/9999", 'class' => 'form-control']) !!}

                                        @if($errors->has('DATANASCIMENTO'))
                                            <div class="note note-danger">{{ $errors->first('DATANASCIMENTO') }}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4">
                                    <div class="form-group {{ ($errors->has('NACIONALIDADE')) ? 'has-error' : '' }}">
                                        <label class="control-label"><b>Nacionalidade</b></label>
                                        {!! Form::text('NACIONALIDADE', old('NACIONALIDADE'), ['required', 'class' => 'form-control']) !!}

                                        @if($errors->has('NACIONALIDADE'))
                                            <div class="note note-danger">{{ $errors->first('NACIONALIDADE') }}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label"><b>Estado Civil</b></label>
                                        {!! Form::select('ESTADOCIVIL', $estadoCivil, old('ESTADOCIVIL'), ['id' => 'estado-civil', 'class'=>'form-control', 'class' => 'form-control']) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-4 col-md-4">
                                    <div class="form-group {{ ($errors->has('TELEFONER1')) ? 'has-error' : '' }}">
                                        <label class="control-label"><b>Telefone 1</b></label>
                                        {!! Form::text('TELEFONER1', old('TELEFONER1'), ['maxlength' => 20, 'data-telefone' => 'true', 'class' => 'form-control']) !!}

                                        @if($errors->has('TELEFONER1'))
                                            <div class="note note-danger">{{ $errors->first('TELEFONER1') }}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4">
                                    <div class="form-group {{ ($errors->has('TELEFONER2')) ? 'has-error' : '' }}">
                                        <label class="control-label"><b>Telefone 2</b></label>
                                        {!! Form::text('TELEFONER2', old('TELEFONER2'), ['required', 'maxlength' => 20, 'data-telefone' => 'true', 'class' => 'form-control']) !!}

                                        @if($errors->has('TELEFONER2'))
                                            <div class="note note-danger">{{ $errors->first('TELEFONER2') }}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4">
                                    <div class="form-group {{ ($errors->has('CEPRESIDENCIAL')) ? 'has-error' : '' }}">
                                        <label class="control-label"><b>CEP</b></label>
                                        {!! Form::text('CEPRESIDENCIAL', old('CEPRESIDENCIAL'), ['id'=>"cep-residencial", 'maxlength'=>9, 'data-mask'=>"99999-999", 'class' => 'form-control']) !!}

                                        @if($errors->has('CEPRESIDENCIAL'))
                                            <div class="note note-danger">{{ $errors->first('CEPRESIDENCIAL') }}</div>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-4 col-md-4">
                                    <div class="form-group {{ ($errors->has('ENDRES_LOGRADOURO')) ? 'has-error' : '' }}">
                                        <label class="control-label"><b>Endereço</b></label>
                                        {!! Form::text('ENDRES_LOGRADOURO', old('ENDRES_LOGRADOURO'), ['required', 'id'=>"endereco-residencial", 'maxlength'=>40, 'class' => 'form-control']) !!}

                                        @if($errors->has('ENDRES_LOGRADOURO'))
                                            <div class="note note-danger">{{ $errors->first('ENDRES_LOGRADOURO') }}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4">
                                    <div class="form-group {{ ($errors->has('ENDRES_NUMERO')) ? 'has-error' : '' }}">
                                        <label class="control-label"><b>Número</b></label>
                                        {!! Form::text('ENDRES_NUMERO', old('ENDRES_NUMERO'), ['required', 'id'=>"numero-residencial", 'maxlength'=>11, 'class' => 'form-control']) !!}

                                        @if($errors->has('ENDRES_NUMERO'))
                                            <div class="note note-danger">{{ $errors->first('ENDRES_NUMERO') }}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4">
                                    <div class="form-group {{ ($errors->has('ENDRES_COMPLEMENTO')) ? 'has-error' : '' }}">
                                        <label class="control-label"><b>Complemento</b></label>
                                        {!! Form::text('ENDRES_COMPLEMENTO', old('ENDRES_COMPLEMENTO'), ['maxlength'=>15, 'class' => 'form-control']) !!}

                                        @if($errors->has('ENDRES_COMPLEMENTO'))
                                            <div class="note note-danger">{{ $errors->first('ENDRES_COMPLEMENTO') }}</div>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-4 col-md-4">
                                    <div class="form-group {{ ($errors->has('BAIRRORESIDENCIAL')) ? 'has-error' : '' }}">
                                        <label class="control-label"><b>Bairro</b></label>
                                        {!! Form::text('BAIRRORESIDENCIAL', old('BAIRRORESIDENCIAL'), ['required', 'id'=>"bairro-residencial", 'maxlength'=>20, 'class' => 'form-control']) !!}

                                        @if($errors->has('BAIRRORESIDENCIAL'))
                                            <div class="note note-danger">{{ $errors->first('BAIRRORESIDENCIAL') }}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4">
                                    <div class="form-group {{ ($errors->has('CIDADERESIDENCIAL')) ? 'has-error' : '' }}">
                                        <label class="control-label"><b>Cidade</b></label>
                                        {!! Form::text('CIDADERESIDENCIAL', old('CIDADERESIDENCIAL'), ['required', 'id'=>"cidade-residencial", 'maxlength'=>20, 'class' => 'form-control']) !!}

                                        @if($errors->has('CIDADERESIDENCIAL'))
                                            <div class="note note-danger">{{ $errors->first('CIDADERESIDENCIAL') }}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4">
                                    <div class="form-group {{ ($errors->has('ESTADORESIDENCIAL')) ? 'has-error' : '' }}">
                                        <label class="control-label"><b>Estado</b></label>
                                        {!! Form::text('ESTADORESIDENCIAL', old('ESTADORESIDENCIAL'), ['required', 'id'=>"estado-residencial", 'maxlength' => 2, 'class' => 'form-control']) !!}

                                        @if($errors->has('ESTADORESIDENCIAL'))
                                            <div class="note note-danger">{{ $errors->first('ESTADORESIDENCIAL') }}</div>
                                        @endif
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-lg-8 col-md-8">
                                    <div class="form-group {{ ($errors->has('EMPRESA')) ? 'has-error' : '' }}">
                                        <label class="control-label"><b>Empresa</b></label>
                                        {!! Form::text('EMPRESA', old('EMPRESA'), ['maxlength' => 35, 'class' => 'form-control']) !!}

                                        @if($errors->has('EMPRESA'))
                                            <div class="note note-danger">{{ $errors->first('EMPRESA') }}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4">
                                    <div class="form-group {{ ($errors->has('CEPCOMERCIAL')) ? 'has-error' : '' }}">
                                        <label class="control-label"><b>Cep Comercial</b></label>
                                        {!! Form::text('CEPCOMERCIAL', old('CEPCOMERCIAL'), ['id'=>"cep-comercial", 'maxlength' => 9, 'data-mask' => '99999-999', 'class' => 'form-control']) !!}

                                        @if($errors->has('CEPCOMERCIAL'))
                                            <div class="note note-danger">{{ $errors->first('CEPCOMERCIAL') }}</div>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-4 col-md-4">
                                    <div class="form-group {{ ($errors->has('ENDCOM_LOGRADOURO')) ? 'has-error' : '' }}">
                                        <label class="control-label"><b>Endereço Comercial</b></label>
                                        {!! Form::text('ENDCOM_LOGRADOURO', old('ENDCOM_LOGRADOURO'), ['id'=>"endereco-comercial", 'maxlength'=>40, 'class' => 'form-control']) !!}

                                        @if($errors->has('ENDCOM_LOGRADOURO'))
                                            <div class="note note-danger">{{ $errors->first('ENDCOM_LOGRADOURO') }}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4">
                                    <div class="form-group {{ ($errors->has('ENDCOM_NUMERO')) ? 'has-error' : '' }}">
                                        <label class="control-label"><b>Número</b></label>
                                        {!! Form::text('ENDCOM_NUMERO', old('ENDCOM_NUMERO'), ['id'=>"numero-comercial", 'maxlength'=>11, 'class' => 'form-control']) !!}

                                        @if($errors->has('ENDCOM_NUMERO'))
                                            <div class="note note-danger">{{ $errors->first('ENDCOM_NUMERO') }}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4">
                                    <div class="form-group {{ ($errors->has('ENDCOM_COMPLEMENTO')) ? 'has-error' : '' }}">
                                        <label class="control-label"><b>Complemento</b></label>
                                        {!! Form::text('ENDCOM_COMPLEMENTO', old('ENDCOM_COMPLEMENTO'), ['id'=>"complemento-comercial", 'maxlength'=>15, 'class' => 'form-control']) !!}

                                        @if($errors->has('ENDCOM_COMPLEMENTO'))
                                            <div class="note note-danger">{{ $errors->first('ENDCOM_COMPLEMENTO') }}</div>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-4 col-md-4">
                                    <div class="form-group {{ ($errors->has('BAIRROCOMERCIAL')) ? 'has-error' : '' }}">
                                        <label class="control-label"><b>Bairro Comercial</b></label>
                                        {!! Form::text('BAIRROCOMERCIAL', old('BAIRROCOMERCIAL'), ['id'=>"bairro-comercial", 'maxlength'=>20, 'class' => 'form-control']) !!}

                                        @if($errors->has('BAIRROCOMERCIAL'))
                                            <div class="note note-danger">{{ $errors->first('BAIRROCOMERCIAL') }}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4">
                                    <div class="form-group {{ ($errors->has('CIDADECOMERCIAL')) ? 'has-error' : '' }}">
                                        <label class="control-label"><b>Cidade Comercial</b></label>
                                        {!! Form::text('CIDADECOMERCIAL', old('CIDADECOMERCIAL'), ['id'=>"cidade-comercial", 'maxlength'=>20, 'class' => 'form-control']) !!}

                                        @if($errors->has('CIDADECOMERCIAL'))
                                            <div class="note note-danger">{{ $errors->first('CIDADECOMERCIAL') }}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4">
                                    <div class="form-group {{ ($errors->has('ESTADOCOMERCIAL')) ? 'has-error' : '' }}">
                                        <label class="control-label"><b>Estado Comercial</b></label>
                                        {!! Form::text('ESTADOCOMERCIAL', old('ESTADOCOMERCIAL'), ['id'=>"estado-comercial", 'maxlength' => 2, 'class' => 'form-control']) !!}

                                        @if($errors->has('ESTADOCOMERCIAL'))
                                            <div class="note note-danger">{{ $errors->first('ESTADOCOMERCIAL') }}</div>
                                        @endif
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-lg-4 col-md-4">
                                    <div class="form-group {{ ($errors->has('TELEFONEC1')) ? 'has-error' : '' }}">
                                        <label class="control-label"><b>Telefone Comercial</b></label>
                                        {!! Form::text('TELEFONEC1', old('TELEFONEC1'), ['maxlength' => 20, 'data-telefone' => 'true', 'class' => 'form-control']) !!}

                                        @if($errors->has('TELEFONEC1'))
                                            <div class="note note-danger">{{ $errors->first('TELEFONEC1') }}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4">
                                    <div class="form-group {{ ($errors->has('TELEFONEC2')) ? 'has-error' : '' }}">
                                        <label class="control-label"><b>Fax</b></label>
                                        {!! Form::text('TELEFONEC2', old('TELEFONEC2'), ['maxlength' => 20, 'data-telefone' => 'true', 'class' => 'form-control']) !!}

                                        @if($errors->has('TELEFONEC2'))
                                            <div class="note note-danger">{{ $errors->first('TELEFONEC2') }}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4">
                                    <div class="form-group">
                                        <label class="control-label"><b>Equip./ Horas</b></label>
                                        {!! Form::text('OBSERVACOES', old('OBSERVACOES'), ['class' => 'form-control']) !!}
                                    </div>
                                </div>
                            </div>


                            <div class="row">
                                <div class="col-lg-4 col-md-4">
                                    <div class="form-group {{ ($errors->has('SENHA')) ? 'has-error' : '' }}">
                                        <label class="control-label"><b>Senha de Acesso</b></label>
                                        {!! Form::input('password', 'SENHA', null, ['required', 'maxlength' => 8, 'class' => 'form-control']) !!}

                                        @if($errors->has('SENHA'))
                                            <div class="note note-danger">{{ $errors->first('SENHA') }}</div>
                                        @endif
                                    </div>
                                </div>
                                <div class="col-lg-4 col-md-4">
                                    <div class="form-group">
                                        <label class="control-label"><b>Confirmação de Senha</b></label>
                                        {!! Form::input('password', 'SENHA_confirmation', null, ['required', 'maxlength' => 8, 'class' => 'form-control']) !!}
                                    </div>
                                </div>
                            </div>

                        </div>

                        <div class="panel-footer text-right">
                            <button type="submit" class="btn btn-primary">
                                Salvar meu Cadastro
                            </button>
                        </footer>
                    {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>

    @endif
@endsection
@section('javascript')
    <script type="text/javascript" src="{{ asset('assets/js/plugin/masked-input/jquery.maskedinput.js') }}"></script>
    <script>
        $('#form-cad-associado').on('submit', function(){
            CSPlus.msgGenerica('Realizando o seu Cadastro');
        });
    </script>
@endsection
