@if(!Policy::isAdmin())
    <div class="bootbox modal fade" tabindex="-1" role="dialog" id="modal-alterar-senha-socio">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <i class="pci-cross pci-circle"></i>
                    </button>
                    <h4 class="modal-title"><b>Alteração de Senha</b></h4>
                </div>
                {!! Form::open(['route' => 'user.pass']) !!}
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="form-group {{ $errors->has('SENHA') ? 'has-error' : '' }}">
                                    <label class="control-label"><b>Senha</b></label>
                                    {!! Form::input('password', 'SENHA', null, ['class' => 'form-control']) !!}

                                    @if($errors->has('SENHA'))
                                        <small class="help-block text-danger">{{ $errors->first('SENHA') }}</small>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-xs-12">

                                <div class="form-group {{ $errors->has('SENHA_confirmation') ? 'has-error' : '' }}">
                                    <label class="control-label"><b>Confirmação de Senha</b></label>
                                    {!! Form::input('password', 'SENHA_confirmation', null, ['class' => 'form-control']) !!}

                                    @if($errors->has('SENHA_confirmation'))
                                        <small class="help-block text-danger">{{ $errors->first('SENHA_confirmation') }}</small>
                                    @endif
                                </div>

                            </div>
                        </div>

                    </div>
                    <div class="modal-footer">
                        <button type="reset" class="btn btn-default" data-dismiss="modal" aria-label="Cancelar">Cancelar</button>
                        <button type="submit" class="btn btn-success">Alterar Senha</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


    @if($errors->has('SENHA_confirmation') || $errors->has('SENHA'))
        <script type="text/javascript">
            $('#modal-alterar-senha-socio').modal('show');
        </script>
    @endif
@endif
