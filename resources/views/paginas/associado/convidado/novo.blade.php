@extends('template.default')

@section('titulo-pagina', 'Cadastro de Convidado')

@section('conteudo')
    <div class="row">
        <div class="col-lg-12">
            @component('Components::panel')
                @slot('title', 'Cadastro de Convidado')
                @slot('body')
                    {!! Form::model($convidado, ['route' => ['convidado.salvar', $matricula]]) !!}
                        <div class="panel-body">
                            <input type="hidden" name="OBSERVACAO" value="SITE">

                            @include('paginas.associado.convidado.form')
                        </div>
                        <div class="panel-footer text-right">
                            <a class="btn btn-default" href="{{ route('convites.index', [$matricula]) }}">Cancelar</a>
                            <button class="btn btn-primary">Salvar e continuar</button>
                        </div>
                    {!! Form::close() !!}
                @endslot
            @endcomponent
        </div>
    </div>
@endsection

@section('javascript')
    <script type="text/javascript">
        CSPlus.loadScript('{{ asset('assets/js/plugin/masked-input/jquery.maskedinput.js') }}', function () {
            CSPlus.runAllForms();
        });

        $('#menu-convites').addClass('active-link');
        CSPlus.drawBreadCrumb(['Novo Convidado']);
    </script>
@endsection
