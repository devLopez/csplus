<div class="row">
    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <div class="form-group {{ ($errors->has('NOME')) ? 'has-error' : '' }}">
            <label class="control-label">Nome do Convidado</label>
            {!! Form::text('NOME', old('NOME'), ['required', 'autofocus', 'maxlength' => 40, 'class' => 'form-control']) !!}

            @if($errors->has('NOME'))
                <div class="note note-error">{{ $errors->first('NOME') }}</div>
            @endif
        </div>
    </div>

    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <div class="form-group {{ ($errors->has('CI')) ? 'has-error' : '' }}">
            <label class="control-label">Identidade</label>
            {!! Form::text('CI', old('CI'), ['required', 'maxlength' => 11, 'class' => 'form-control']) !!}

            @if($errors->has('CI'))
                <div class="note note-error">{{ $errors->first('CI') }}</div>
            @endif
        </div>
    </div>

    <div class="col col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <div class="form-group {{ ($errors->has('DATANASC')) ? 'has-error' : '' }}">
            <label class="control-label">Data de Nascimento</label>
            {!! Form::text('DATANASC', old('DATANASC'), ['required', 'maxlength' => 11, 'data-mask' => '99/99/9999', 'class' => 'form-control']) !!}

            @if($errors->has('DATANASC'))
                <div class="note note-error">{{ $errors->first('DATANASC') }}</div>
            @endif
        </div>
    </div>

</div>

<div class="row">

    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <div class="form-group {{ ($errors->has('REFERENCIA')) ? 'has-error' : '' }}">
            <label class="control-label">Referência (Ex.: 06/2016)</label>
            {!! Form::text('REFERENCIA', old('REFERENCIA'), ['required', 'maxlength' => 7, 'data-mask' => '99/9999', 'class' => 'form-control']) !!}

            @if($errors->has('REFERENCIA'))
                <div class="note note-error">{{ $errors->first('REFERENCIA') }}</div>
            @endif
        </div>
    </div>
    
</div>
