@extends('template.default')

@section('titulo-pagina', 'Busca de Mensalidades')

@if(Policy::isAdmin())
    @section('searchbox')
        <a class="btn btn-success pull-right" href="{{ route('associados.exibir', $matricula) }}">
            Voltar ao Associado
        </a>
    @endsection
@endif


@section('conteudo')
    <div class="row">
        <div class="col col-lg-12 col-md-12 col-sm-12 col-xs-12">
            @component('Components::panel')
                @slot('title', 'Busca de Mensalidades e Débitos')
                @slot('body')
                    {!! Form::open(['route' => ['debitos.index', $matricula], 'method' => 'GET', 'class' => 'smart-form', 'id' => 'form-busca-debitos']) !!}
                        <div class="panel-body">
                            @include('Socio::debitos.form')
                        </div>
                        <div class="panel-footer text-right">
                            <button type="submit" class="btn btn-primary">Buscar</button>
                        </div>
                    {!! Form::close() !!}
                @endslot
            @endcomponent
        </div>
    </div>

    <div class="row">
        <div class="col col-lg-12">
            @include('Socio::debitos.resultado-pesquisa')
        </div>
    </div>
@endsection

@section('javascript')
    <script type="text/javascript">
        $(function () {
            $('#menu-debitos').addClass('active-link');
            CSPlus.drawBreadCrumb();

            // Carrega o plugin da máscara de campos
            CSPlus.loadScript('{{ asset('assets/js/plugin/masked-input/jquery.maskedinput.js') }}', function () {
                CSPlus.runAllForms();
            });
        });
    </script>
@endsection
