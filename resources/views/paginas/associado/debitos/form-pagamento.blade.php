<input type="hidden" id="redirecionamento" value="{{ route('debitos.index', $matricula) }}">
<input type="hidden" id="qtde_cobrancas" value="{{ $debitos->count() }}" name="qtde_cobrancas">

@foreach($debitos as $d)
    {{ Form::hidden('produtos[]', $d->CODIGO) }}
@endforeach

{{-- Recebe os erros de validação via ajax --}}
<div id="ajax-validation-errors" class="hidden alert alert-warning">
    <ul class="list-unstyled"></ul>
</div>

<input type="hidden" name="amount" id="amount" value="{{ $valor }}">
<input type="hidden" name="senderHash" id="senderHash">
<input type="hidden" name="cardBrand" id="cardBrand">
<input type="hidden" name="cardToken" id="cardToken">
<input type="hidden" name="installmentAmount" id="installmentAmount">

<input type="hidden" name="parcelas_possiveis" id="parcelas-possiveis" value="{{ Payment::getParcelas() }}">
<input type="hidden" name="parcelas_sem_juros" id="parcelas-sem-juros" value="{{ Payment::getParcelasSemJuros() }}">

<div class="row">
    <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
        <div class="form-group">
            <label class="control-label"><b>Tipo de Pagamento</b></label>

            <div class="radio">
                <input type="radio" name="tipo_pagamento" value="boleto" checked="checked" class="magic-radio" id="radio-boleto">
                <label for="radio-boleto">Boleto</label>

                @if(Payment::usesPagseguro())
                    <input type="radio" name="tipo_pagamento" value="cartao" class="magic-radio" id="cartao">
                    <label for="cartao">Cartão de Crédito</label>
                @endif
            </div>
        </div>
    </div>
</div>

@if(Payment::usesPagseguro())
    <div class="row">
        <div class="col-sm-8 col-xs-12">
            <div class="form-group">
                <label class="control-label"><b>Número do Cartão</b> <small>(Somente números)</small></label>
                {!! Form::text('', null, ['disabled', 'required', 'id'=>'cardNumber', 'maxlength' => 19, 'data-number', 'class' => 'form-control']) !!}
            </div>
        </div>
        <div class="col-sm-4 col-xs-12">
            <div class="form-group">
                <label class="control-label">
                    <abbr title="Código de Segurança (Atrás do Cartão)">Cód. Seg.</abbr>
                </label>

                {!! Form::text('', null, ['disabled', 'required', 'id'=>'cvv', 'maxlength' => 4, 'data-number', 'class' => 'form-control']) !!}
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
            <div class="form-group">
                <label class="control-label">Mês de Vencimento</label>
                {!! Form::select('', meses_br(), null, ['disabled', 'required', 'id' => 'expirationMonth', 'class' => 'form-control']) !!}
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-6 col-xs-6">
            <div class="form-group">
                <label class="control-label">Ano</label>
                {!! Form::select('', expiration_year(), null, ['disabled', 'required', 'id' => 'expirationYear', 'class' => 'form-control']) !!}
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-12 col-sm-12">
            <div class="form-group">
                <label class="control-label">Nome impresso no Cartão</label>
                {!! Form::text('holderName', null, ['disabled', 'required', 'id'=>'holderName', 'class' => 'form-control']) !!}
            </div>

        </div>
    </div>

    <div class="row">
        <div class="col-lg-3 col-md-4 col-sm-3 col-xs-12">
            <div class="form-group">
                <label class="control-label">CPF do Titular do Cartão</label>
                {!! Form::text('holderCpf', null, ['disabled', 'required', 'id' => 'holderCpf', 'data-mask' => '999.999.999-99', 'class' => 'form-control']) !!}
            </div>
        </div>
        <div class="col-lg-3 col-md-4 col-sm-3 col-xs-12">
            <div class="form-group">
                <label class="control-label">Data de Nasc.</label>
                {!! Form::text('holderBirthDate', null, ['disabled', 'required', 'id' => 'holderBirthDate', 'data-mask' => '99/99/9999', 'maxlength' => 50, 'class' => 'form-control']) !!}
            </div>
        </div>
        <div class="col-lg-3 col-md-4 col-sm-3 col-xs-12">
            <div class="form-group">
                <label class="control-label">Cód. Área</label>
                {!! Form::text('holderAreaCode', null, ['disabled', 'required', 'maxlength' => 2, 'data-number', 'class' => 'form-control']) !!}
            </div>
        </div>
        <div class="col-lg-3 col-md-4 col-sm-3 col-xs-12">
            <div class="form-group">
                <label class="control-label">Telefone</label>
                {!! Form::text('holderPhone', null, ['disabled', 'required', 'maxlength' => 9, 'data-number', 'class' => 'form-control']) !!}
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-12">
            <div class="form-group">
                <label class="control-label">Parcelas</label>
                <select name="installments" id="installments" class="form-control" disabled></select>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-sm-6 pull-right">
            <div class="well no-border">
                <div class="fa-lg">
                    Valor Total:
                    <span class="pull-right" id="span-valor-total"> R${{ money2br($valor) }}</span>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-6 pull-right">
            <b>Esta compra está sendo realizada no</b>
            <img src="{{ asset('assets/img/brasil.gif') }}" width="30">
        </div>
    </div>
@endif
