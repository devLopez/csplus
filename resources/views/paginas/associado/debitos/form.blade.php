<div class="row">

    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <div class="form-group has-feedback">
            <label class="control-label">Pesquisar até</label>
            {!! Form::text('data_pesquisa', Date::lastDayMonth(), ['required', 'data-mask' => '99/99/9999', 'class' => 'form-control']) !!}
            <i class="fa fa-calendar form-control-feedback"></i>
        </div>

    </div>

    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <div class="form-group has-feedback">
            <label class="control-label">Vencimento para Cálculo</label>
            {!! Form::text('data_vencimento', Date::today(), ['required', 'data-mask' => '99/99/9999', 'class' => 'form-control']) !!}
            <i class="fa fa-calendar form-control-feedback"></i>
        </div>

    </div>

    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
        <div class="form-group">
            <label class="control-label">Mostar Itens</label>
            <div class="radio">
                <input type="radio" name="pesquisar_por" class="magic-radio" value="aberto" checked="checked" id="radio-aberto">
    			<label for="radio-aberto">Em aberto</label>

                <input type="radio" name="pesquisar_por" class="magic-radio" value="quitados" id="radio-quitado">
    			<label for="radio-quitado">Quitados</label>
    		</div>
        </div>
    </div>
</div>
