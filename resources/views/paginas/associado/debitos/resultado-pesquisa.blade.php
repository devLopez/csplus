@if(!isset($debitos) || $debitos->isEmpty())
    @php
        $msg = ($erro != null) ? $erro : 'Não foram encontrados débitos para a pesquisa';
    @endphp

    @include('errors.empty-records', ['message' => $msg])
@else
    @if($debitos !== null)
        @component('Components::panel')
            @slot('title', 'Resultado da Busca')

            @slot('body')
                {!! Form::open(['route' => ['debitos.pagar', $matricula], 'method' => 'get', 'class' => 'smart-form']) !!}
                    <div class="panel-body">
                        {!! Form::hidden('cobranca', $cobranca) !!}
                        <table class="table table-striped table-hover table-condensed">
                            <thead>
                                <tr>
                                    <th>Selecionar</th>
                                    <th>Produto</th>
                                    <th>Valor</th>
                                    <th>Histórico</th>
                                    <th>Referência</th>
                                    <th>Pagamento</th>
                                    <th>Valor Total</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($debitos as $d)
                                    @php($disabled = (trim($d->SITUACAO) == 'P') ? 'disabled' : '')
                                    <tr>
                                        <td>{!! Form::checkbox('debitos[]', $d->CODIGO, null, ['class' => 'checkbox-pay', $disabled]) !!}</td>
                                        <td>{{ utf8_encode($d->PRODUTO) }}</td>
                                        <td>R${{ money2br($d->VALOR) }}</td>
                                        <td>{{ $d->HISTORICO }}</td>
                                        <td>{{ $d->REFERENCIA }}</td>
                                        <td>{{ Date::toBr($d->DATAPAGAMENTO) }}</td>
                                        <td>R${{ money2br($d->VALORTOTAL) }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <div class="panel-footer text-right">
                        <button type="submit" class="btn btn-primary" {{ ($pesquisa == 'quitados') ? 'disabled' : '' }}>
                            Realizar Pagamento
                        </button>
                    </div>
                {!! Form::close() !!}
            @endslot
        @endcomponent
    @endif
@endif

@push('scripts')
<script type="text/javascript">
    Debitos.initSwitchery();
</script>
@endpush
