@extends('template.default')
@inject('invoice', 'CSPlus\Services\Payment\Invoice')

@php
    if(!$debitos->isEmpty()) {
        $invoice->setItems($debitos);
        $valor = $invoice->sumItems($desconto);
    } else {
        $valor = 0.00;
    }
@endphp

@section('titulo-pagina', 'Pagamento e Finalização')

@section('searchbox')
    <a class="btn btn-primary pull-right" href="{{ URL::previous() }}">
        Voltar aos Débitos
    </a>
@endsection

@section('conteudo')
<div class="row">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

        @component('Components::panel')
            @slot('title', 'Detalhes do Pagamento')

            @if($debitos->isEmpty())
                @include('errors.empty-records', ['message' => 'Não é possível continuar com o pagamento pois não foram encontrados os itens selecionados'])
            @else
                @slot('body')
                    {!! Form::open(['route' => ['debitos.pay', $matricula], 'class' => 'smart-form', 'id' => 'form-payment']) !!}
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12 col-xs-12">
                                    <div class="table-responsible">
                                        <table class="table table-striped table-hover table-condensed">
                                            <thead>
                                                <tr>
                                                    <th>Produto</th>
                                                    <th>Valor</th>
                                                    <th>Histórico</th>
                                                    <th>Referência</th>
                                                    <th>Total</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                @foreach($debitos as $d)
                                                    <tr>
                                                        <td>{{ utf8_encode($d->PRODUTO) }}</td>
                                                        <td>R${{ money2br($d->VALOR) }}</td>
                                                        <td>{{ $d->HISTORICO }}</td>
                                                        <td>{{ $d->REFERENCIA }}</td>
                                                        <td>R${{ money2br($d->VALORTOTAL) }}</td>
                                                    </tr>
                                                @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12 col-sm-12 col-xs-12">
                                    @include('Socio::debitos.form-pagamento')
                                </div>
                            </div>

                        </div>
                        <div class="panel-footer text-right">
                            <button type="submit" class="btn btn-success"> <i class="fa fa-dollar"></i> Realizar Pagamento</button>
                        </div>
                    {!! Form::close() !!}
                @endslot
            @endif
        @endcomponent
    </div>
</div>
@endsection

@section('javascript')
    @if(Client::usesPagseguro())

        @php
            try {
                $sessionId = Payment::getSessionToken();
            } catch(\Exception $e) {
                $sessionId = false;
            }
        @endphp

        <script type="text/javascript" src="{{ Payment::getJavascriptUrl() }}"></script>
        <script src="{{ asset('assets/js/plugin/masked-input/jquery.maskedinput.js') }}"></script>
        <script type="text/javascript">

            @if($sessionId !== false)
                PagSeguroDirectPayment.setSessionId('{{ Payment::getSessionToken() }}');
            @else
                $(function(){
                    CSPlus.alertErro('O pagamento via PagSeguro está indisponível no momento. Você pode pagar via boleto ou tentar novamente mais tarde');
                    $('#cartao').prop('disabled', true);
                    $('#label-cartao').addClass('state-disabled');
                });
            @endif

        </script>
    @endif
    <script type="text/javascript">
    	$(function(){
    		$('#menu-debitos').addClass('active-link');
    		CSPlus.drawBreadCrumb(['Pagamento']);
    	})
    </script>
@endsection
