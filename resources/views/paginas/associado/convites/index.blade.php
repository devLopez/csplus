@extends('template.default')

@section('titulo-pagina', 'Meus Convites')

@section('searchbox')
    <a class="btn btn-primary pull-right" href="{{ route('convidado.index', [$matricula]) }}">
        Novo Convite
    </a>

    @if(Policy::isAdmin())
        <a class="btn btn-success pull-right" href="{{ route('associados.exibir', $matricula) }}">
            Voltar ao Associado
        </a>
    @endif
@endsection

@section('conteudo')

    @if($convites->isEmpty())
        @include('errors.empty-records', ['message' => 'Não há convites cadastrados para este associado'])
    @else

        @component('Components::panel')
            @slot('title', 'Convites Emitidos')
            @slot('body')
                {!! Form::open(['route' => 'convites.gerar', 'class' => 'smart-form', 'target' => '_blank']) !!}
                    <div class="panel-body">
                        <input type="hidden" value="{{ $matricula }}" name="associado">

                        <div class="table-responsive">
                            <table class="table table-striped table-hover table-condensed">
                                <thead>
                                    <tr>
                                        <th>Imprimir</th>
                                        <th>Nome</th>
                                        <th>Identidade</th>
                                        <th>Ref.</th>
                                        <th>Tipo</th>
                                        <th>Impresso</th>
                                        <th class="text-center">Gerar Novo</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($convites as $c)
                                        @php
                                            if($c->VENCIDO == 0) {
                                                $vencido = Form::checkbox('convites[]', $c->CODIGO, null, ['class' => 'checkbox-emit-convite']);
                                            } else {
                                                $vencido = "<label class='label label-warning txt-color-blueDark'>{$c->DEFINICAO}</label>";
                                            }

                                            $class = (trim($c->TIPO) == 'GRATUITO') ? 'label-success' : 'label-danger';
                                        @endphp
                                        <tr>
                                            <td>{!! $vencido !!}</td>
                                            <td>{{ utf8_encode($c->NOME) }}</td>
                                            <td>{{ $c->CI }}</td>
                                            <td>{{ Date::toBr($c->DATAINI) }}</td>
                                            <td>
                                                <label class='label {{ $class }}'>{{ trim($c->TIPO) }}</label>
                                            </td>
                                            <td>{{ trim($c->IMPRESSO) }}</td>
                                            <td class="text-center">
                                                <a href="{{ route('convidado.index', [$matricula, $c->CODIGO]) }}"
                                                   class="add-tooltip" title="Re-emitir"
                                                >
                                                    <i class="fam-lightning"></i>
                                                </a>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="panel-footer text-right">
                        <button class="btn btn-primary" type="submit" id="btn-gera-convites">
                            Gerar convites Selecionados
                        </button>
                    </div>
                {!! Form::close() !!}
            @endslot
        @endcomponent
    @endif
@endsection

@section('javascript')
    <script type="text/javascript">
        $(function () {
            $('#menu-convites').addClass('active-link');
            CSPlus.drawBreadCrumb();
        });
    </script>
@endsection
