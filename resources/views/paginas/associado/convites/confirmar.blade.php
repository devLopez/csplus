@extends('template.default')

@section('titulo-pagina', 'Confirmar Emissão do Convite')

@section('conteudo')
    <div class="row">
        <div class="col-lg-12">
            @component('Components::panel')
                @slot('title', 'Confirmação do Convite')

                {{ utf8_encode($convite->RCNV_MENSAGEM) }}

                @if ($convite->RCNV_SITUACAO == 'T' || $convite->RCNV_CONVITEGRATUITO == 'T')
                    <b>Tipo de convite a emitir:</b> GRATUITO <br>
                @else
                    <strong>
                        A geracao deste convite irá gerar uma taxa de cobrançaa a ser incluída no próximo boleto de cobrança
                        de mensalidade.
                    </strong><br>
                @endif

                <b>Nome do convidado:</b> {{ $convidado->NOME }}<br><br>

                <div class="alert alert-warning">
                    Este convite será conferido na portaria do Clube, quando deverá ser apresentado documento de identificação do convidado.
                </div>

                {{-- FORMULÁIOS PARA CRIAÇÃO DO CONVITE --}}
                <div class="row">
                    @if($convite->RCNV_CONVITEGRATUITO == 'T' || $convite->RCNV_SITUACAO == 'T')
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            {!! Form::open(['route' => ['convite.salvar', $matricula]]) !!}
                                <input type="hidden" name="mesmoconvidado" value="{{ $convite->RCNV_LIMITEMESMOCONVIDADO }}">
                                <input type="hidden" name="convidado" value="{{ $convidado->CODIGO }}">
                                <input type="hidden" name="referencia" value="{{ $referencia }}">
                                <input type="hidden" name="pago" value="F">

                                <button class="btn btn-success">Gerar convite gratuito</button>
                            {!! Form::close() !!}
                        </div>
                    @endif

                    @if($convite->RCNV_SITUACAO != 'T')
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            {!! Form::open(['route' => ['convite.salvar', $matricula]]) !!}
                                <input type="hidden" name="mesmoconvidado" value="{{ $convite->RCNV_LIMITEMESMOCONVIDADO }}">
                                <input type="hidden" name="convidado" value="{{ $convidado->CODIGO }}">
                                <input type="hidden" name="referencia" value="{{ $referencia }}">
                                <input type="hidden" name="pago" value="T">

                                <div class="form-group">
                                    {!! Form::select('tipoconvite', $valores, old('TIPOCONVITE'), ['required', 'class' => 'form-control', 'placeholder' => 'Selecione o tipo de Convite']) !!}
                                </div>

                                <button class="btn btn-primary">Gerar convite Pago</button>
                            {!! Form::close() !!}
                        </div>
                    @endif
                </div>

            @endcomponent
        </div>
    </div>
@endsection

@section('javascript')
    <script type="text/javascript">
        $(function () {
            $('#menu-convites').addClass('active');
            CSPlus.drawBreadCrumb(['Confirmação do convite']);
        });
    </script>
@endsection
