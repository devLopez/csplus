@extends('template.default')

@section('titulo-pagina', 'Termo de Aceite')

@section('conteudo')
    @component('Components::panel')
        @slot('title', 'TERMO DE RESPONSABILIDADE DE LOGIN E SENHA PARA RETIRADA DE CONVITES ONLINE')
        @slot('body')
            {!! Form::open(['route' => ['convite.aceitar', $matricula, $convite], 'class' => 'smart-form']) !!}
                <div class="panel-body">
                    <p>
                        1) O Associado declara que recebe, neste ato, o login e senha para acesso
                        ao sistema de emissão de convites online, ficando, a partir
                        desta data, sob sua guarda e responsabilidade.
                    </p>
                    <p>
                        2) O login e senha neste ato recebido é pessoal e
                        intransferível, sendo de responsabilidade exclusiva do
                        associado o uso indevido da mesma, inclusive por terceiros.
                    </p>
                    <p>
                        3) O Associado declara
                        neste ato conhecer a regra de emissão de convites estabelecida
                        pelo Barroca Tenis Clube.
                    </p>
                    <p>
                        4) Caso o Associado
                        venha perder o login e senha de acesso, deverá o mesmo
                        dirigir-se à  Secretaria do Clube para que possa solicitar uma
                        nova senha.
                    </p>
                    <p>
                        5) Fica o Associado
                        ciente que o convite pago emitido via Internet terá seu valor
                        acrescido no boleto bancário juntamente com o condomínio
                        mensal, <b><em>o que já fica devidamente autorizado</em></b>.
                    </p>
                    <p>
                        6) O convite emitido via Internet que estiver em desacordo com a regra de emissão poderá
                        ter seu valor cobrado no boleto bancário juntamente com o condominio mensal,
                        <b><em>o que já fica devidamente autorizado</em></b>.
                    </p>
                    <p>
                        7) O cadastro do convidado deverá ocorrer considerando: <b>a)</b> Nome completo sem abreviação,
                        <b>b)</b>   Data de nascimento (<em>formato 00/00/0000</em>),
                        <b>c)</b> Numero de identidade (<em>Ex: MG152512525</em>)
                        e referência (<em>Ex: 06/2016</em>).
                    </p>
                    <p>
                        8) O Associado se compromete que, após receber a senha provisória, fará
                        imediatamente a troca da mesma.
                    </p>
                    <p>
                        9) Os convites para crianças até 11 anos de idade, deveráo também
                        ser emitidos no sistema de Convite Online do BTC.
                    </p>
                    <p>
                        10) Todo e qualquer
                        convite emitido deverá ser impresso e apresentado na portaria
                        do Clube, juntamente com o documento de identificação
                        do convidado.
                    </p>

                    <div class="row pad-top">
                        <div class="col-lg-12 col-xs-12">

                            <input type="checkbox" name="aceite" required class="magic-checkbox" id="aceite">
                            <label for="aceite">
                                Declaro, por fim, que conheço e aceito, sem nenhuma restrição,
                                todos os termos e regras definidas e especificadas neste documento, não existindo nenhuma dúvida
                                quanto à política de utilização do Sistema de Emissão do Convite Online,
                                bem como em relação às proibições e consequentes penalizações
                                decorrentes de seu uso inadequado
                            </label>
                        </div>
                    </div>
                </div>
                <div class="panel-footer">
                    <button type="submit" class="btn btn-primary">Aceitar e Continuar</button>
                </div>
            {!! Form::close() !!}
        @endslot
    @endcomponent

@endsection
