@extends('template.default')

@section('titulo-pagina', 'Termo de Aceite')

@section('conteudo')
    @component('Components::panel')
        @slot('title', 'TERMO DE RESPONSABILIDADE DE LOGIN E SENHA PARA UTILIZAÇÃO DO CSPLUS ONLINE')
        @slot('body')
            {!! Form::open(['route' => ['termos.aceitar', $matricula]]) !!}
                <div class="panel-body">
                    <p>
                        {!! utf8_encode($categoria->TEXTO_FICHAINSCRICAO) !!}
                    </p>
                    <div class="row pad-top">
                        <div class="col-lg-12 col-xs-12">
                            <div class="form-group {{ $errors->has('aceite') ? 'has-error' : '' }}">

                                <div class="box-inline">
                                    <input name="aceite" value="true" type="checkbox" class="magic-checkbox" id="aceite">
                                </div>

                                <span>
                                    Declaro ter lido as informações apresentadas no contrato acima e aceito os termos do mesmo
                                </span>

                                @if($errors->has('aceite'))
                                    <div class="note text-danger">{{ $errors->first('aceite') }}</div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-footer text-right">
                    <button class="btn btn-success">
                        Aceitar Termos de Uso
                    </button>
                </div>
            {!! Form::close() !!}
        @endslot
    @endcomponent
@endsection

@section('javascript')
    <script type="text/javascript">
        $(function(){
            var checkbox    = document.getElementById('aceite');
            var switchery   = new Switchery(checkbox);
        });
    </script>
@endsection