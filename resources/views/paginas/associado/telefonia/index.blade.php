@extends('template.default')

@section('titulo-pagina', 'Consumo Telefônico')

@if(Policy::isAdmin())
    @section('searchbox')
        <a class="btn btn-success pull-right" href="{{ route('associados.exibir', $matricula) }}">
            Voltar ao Associado
        </a>
    @endsection
@endif

@section('conteudo')
    @if($operadoras->isEmpty())
        @include('errors.empty-records', ['message' => 'Não há operadoras disponíveis para exibir'])
    @else
        @component('Components::panel')
            @slot('title', 'Pesquisa Telefônica')

            @slot('body')
                {!! Form::open($form) !!}
                    <div class="panel-body">
                        <fieldset>
                            <div class="row">
                                <section class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">Operadora</label>
                                        {!! Form::select('operadora', array_reverse($select), null, ['required', 'class' => 'form-control']) !!}
                                    </div>
                                </section>

                                <section class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">Data Inicial</label>
                                        {!! Form::text('data_inicio', Date::toBr($vencimento), ['required', 'maxlength' => 10, 'class' => 'form-control', 'data-mask' => '99/99/9999']) !!}
                                    </div>
                                </section>

                                <section class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                                    <div class="form-group">
                                        <label class="control-label">Data Final</label>
                                        {!! Form::text('data_final', Date::toBr($data_final), ['required', 'maxlength' => 10, 'class' => 'form-control','data-mask' => '99/99/9999']) !!}
                                    </div>
                                </section>

                            </div>
                        </fieldset>
                    </div>
                    <div class="panel-footer text-right">
                        <button type="submit" class="btn btn-primary">Pesquisar</button>
                    </div>
                {!! Form::close() !!}
            @endslot

        @endcomponent
    @endif
@endsection

@section('javascript')
    <script type="text/javascript">
        $('#menu-telefonia').addClass('active-link');
        CSPlus.drawBreadCrumb();

        // Carrega o plugin da máscara de campos
        CSPlus.loadScript('{{ asset('assets/js/plugin/masked-input/jquery.maskedinput.js') }}', function () {
            CSPlus.runAllForms();
        });
    </script>
@endsection
