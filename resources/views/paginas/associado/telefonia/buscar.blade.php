@extends('template.default')

@section('titulo-pagina', 'Contas de Consumo de Telefonia')

@section('searchbox')
    <a class="btn btn-primary pull-right" href="{{ route('telefonia.index', [$matricula]) }}">
        Nova Pesquisa
    </a>

    @if(Policy::isAdmin())
        <a class="btn btn-success pull-right" href="{{ route('associados.exibir', $matricula) }}">
            Voltar ao Associado
        </a>
    @endif
@endsection


@section('conteudo')

    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
            @if($contas->isEmpty())
                @include('errors.empty-records', ['message' => 'Não existem dados para serem exibidos'])
            @else
                @component('Components::panel')
                    @slot('title', 'Minhas Contas')

                    <div class="table-responsible">

                            <table class="table table-condensed" id="table-telefonia">
                                <thead>
                                <tr>
                                    <th>Número do Telefone</th>
                                    <th>Operadora</th>
                                    <th>Data de Vencimento</th>
                                    <th>Valor Total</th>
                                    <th class="text-center">Status</th>
                                    <th class="text-center">Ações</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($contas as $c)
                                    <tr>
                                        <td>{{ mask_phone($c->TELEFONE) }}</td>
                                        <td>{{ $c->OPERADORA }}</td>
                                        <td>{{ Date::toBr($c->VENCIMENTO) }}</td>
                                        <td>R${{ money2br($c->VALOR) }}</td>
                                        <td class="text-center">
                                            @if($c->DATA_PAG)
                                                <label class="label label-success">Paga</label>
                                            @else
                                                <label class="label label-warning">Em Aberto</label>
                                            @endif
                                        </td>
                                        <td class="text-center">
                                            <a href="{{
                                                route('telefonia.detalhado', [$matricula]).'?'.
                                                http_build_query(['vencimento' => $c->VENCIMENTO, 'telefone' => $c->TELEFONE ])
                                               }}"
                                               class="add-tooltip"
                                               title="Relatório Detalhado"
                                               data-relatorio="true"
                                            >
                                                <i class="fa fa-history"></i>
                                            </a>

                                            <a href="{{
                                                route('telefonia.agrupado', [$matricula]).'?'.
                                                http_build_query(['vencimento' => $c->VENCIMENTO, 'telefone' => $c->TELEFONE ])
                                               }}"
                                               class="add-tooltip"
                                               title="Relatório Agrupado"
                                               data-relatorio="true"
                                            >
                                                <i class="fa fa-file-text-o"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>

                        </div>
                @endcomponent
            @endif
        </div>
    </div>


    <div class="modal fade" id="modal-relatorio-contas" tabindex="-1" role="dialog" aria-labelledby="titulo-relatorio">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h4 class="modal-title" id="titulo-relatorio"></h4>
                </div>

                <div class="modal-body no-padding" id="corpo-relatorio" style="max-height:300px; overflow-x: scroll"></div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('javascript')
    <script type="text/javascript">
        $('#menu-telefonia').addClass('active-link');
        CSPlus.drawBreadCrumb(['Dados da Pesquisa']);

        $('[data-relatorio]').on('click', function (e) {
            e.preventDefault();

            url     = $(this).attr('href');
            titulo  = $(this).data('original-title');

            $('#titulo-relatorio').text(titulo);

            Http.get(url, '', function (e) {
                $('#corpo-relatorio').html(e);
                $('#modal-relatorio-contas').modal('show');
            });
        })
    </script>
@endsection
