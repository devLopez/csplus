@if($relatorio->isEmpty())
    @include('errors.empty-records', ['message' => 'Não há dados para serem exibidos'])
@else
    <div class="pad-btm">
        <a href="{{ $url.'&'.http_build_query(['download' => 'csv']) }}"
           class="btn btn-success"
           target="_blank"
        >
            <i class="fa fa-file-excel-o"></i> Exportar CSV
        </a>
        <a href="{{ $url.'&'.http_build_query(['download' => 'pdf']) }}"
           class="btn btn-danger"
           target="_blank"
        >
            <i class="fa fa-file-pdf-o"></i> Exportar PDF
        </a>
    </div>

    <table class="table table-hover table-condensed">
        <thead>
            <tr>
                <th>Hora</th>
                <th>Duração</th>
                <th>Histório</th>
                <th>Número</th>
                <th>Valor</th>
                <th>Tipo de Ligação</th>
                <th>Tarifa</th>
            </tr>
        </thead>
        <tbody>
            @foreach($relatorio as $r)
                <tr>
                    <td>{{ br_timestamp($r->DATA_HORA_INI) }}</td>
                    <td>{{ $r->DURACAO }}</td>
                    <td>{{ utf8_encode($r->HISTORICO) }}</td>
                    <td>{{ mask_phone($r->DISCADO) }}</td>
                    <td>R${{ money2br($r->VALOR) }}</td>
                    <td>
                        <label class="label label-success">{{ $r->TIPO_LIGACAO }}</label>
                    </td>
                    <td>R${{ money2br($r->TARIFA) }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
@endif