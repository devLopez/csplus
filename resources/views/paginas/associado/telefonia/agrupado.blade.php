@if($relatorio->isEmpty())
    @include('errors.empty-records', ['message' => 'Não há dados para serem exibidos'])
@else
    <div class="padding-10">
        <a href="{{ $url.'&'.http_build_query(['download' => 'csv']) }}"
           class="btn btn-success"
           target="_blank"
        >
            <i class="fa fa-file-excel-o"></i> Exportar CSV
        </a>
        <a href="{{ $url.'&'.http_build_query(['download' => 'pdf']) }}"
           class="btn btn-danger"
           target="_blank"
        >
            <i class="fa fa-file-pdf-o"></i> Exportar PDF
        </a>
    </div>

    <table class="table table-hover table-condensed">
        <thead>
        <tr>
            <th>Telefone</th>
            <th>Data</th>
            <th>Valor</th>
            <th>Titular</th>
            <th>Tipo de Ligação</th>
        </tr>
        </thead>
        <tbody>
        @foreach($relatorio as $r)
            <tr>
                <td>{{ mask_phone($r->TELEFONE) }}</td>
                <td>{{ br_timestamp($r->DATA_HORA_INI) }}</td>
                <td>R${{ money2br($r->VALOR) }}</td>
                <td>{{ utf8_encode($r->NOMEDOTITULAR) }}</td>
                <td>
                    <label class="label label-success">{{ $r->TIPO_LIGACAO }}</label>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@endif