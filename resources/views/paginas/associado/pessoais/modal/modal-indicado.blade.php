@component('Components::modal')
    @slot('id', 'modal-get-indicando')
    @slot('title', 'Busca de sócio indicando')

    @slot('body')
        <div class="form-horizontal">
            <div class="form-group">
                <label class="col-md-4 control-label" for="name"><b>Nome do Associado</b></label>
                <div class="col-md-6">
                    <input id="nome-associado" type="text" class="form-control" placeholder="Digite e pressione Enter">
                </div>
            </div>
        </div>

        <div id="indicando-wrap">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>Código</th>
                        <th>Nome</th>
                    </tr>
                </thead>
                <tbody id="indicando-search-result"></tbody>
            </table>
        </div>
    @endslot

    @slot('footer')
        <button type="button" data-dismiss="modal" class="btn btn-primary">
            Cancelar
        </button>
    @endslot
@endcomponent
