@extends('template.default')

@inject('associadoRepository', 'CSPlus\Repositories\Associados\AssociadoRepository')
@inject('categoriasRepository', 'CSPlus\Repositories\Associados\CategoriasRepository')
@inject('parentescosRepository', 'CSPlus\Repositories\Associados\ParentescosRepository')
@inject('bancosRepository', 'CSPlus\Repositories\Financeiro\BancosRepository')
@inject('tiposContaRepository', 'CSPlus\Repositories\Financeiro\TipoContaRepository')

@php
    $sexo           = $associadoRepository->getGenero();
    $estadoCivil    = $associadoRepository->getEstadoCivil();
    $tiposConta     = $tiposContaRepository->getTiposConta();

    $categorias     = $categoriasRepository->pluck('DESCRICAO', 'CODIGO');
    $parentescos    = $parentescosRepository->pluck('DESCRICAO', 'CODIGO');
    $bancos         = $bancosRepository->pluck('NOME', 'CODIGO');

    if($bancos) {
        $bancos = array_map('utf8_encode', $bancos);
    }
@endphp

@section('titulo-pagina', 'Dados do Associado')

@if(Policy::isAdmin())
    @section('searchbox')
        <a href="{{ route('associado.novo') }}" class="btn btn-primary hidden-sm hidden-xs pull-right">Adicionar Associado</a>
        <a href="{{ route('associado.novo') }}" class="btn btn-primary hidden-lg hidden-md btn-block">Adicionar Associado</a>
    @endsection
@endif

@push('css')
    <link rel="stylesheet" href="{{ asset('assets/js/plugin/dropzone/dropzone.css') }}">
@endpush

@section('conteudo')
    <input type="hidden" id="acesso" value="{{ Client::access() }}">

    @if(!$associado)
        @include('errors.empty-records', ['message' => 'Ocorreu um erro na busca dos dados do associado'])
    @else
        <input type="hidden" id="dados_associado" value='{{ json_encode(array_map('utf8_encode', $associado->toArray()), JSON_HEX_APOS) }}'>

        <div class="row">
            <div class="col col-lg-12 col-md-12 col-sm-12 col-xs-12">

                <div class="panel">
                    <div class="pad-all">

                        <div class="panel-body pad-no mar-top">

                            <div class="text-center mar-btm">
                                <div class="edit-foto mar-btm">
                                    <a href="#" title="Alterar Foto" data-tipo="0" data-altera-foto="" class="add-tooltip">
                                        @if(Policy::isAdmin())
                                            @if($foto)
                                                <img src="data:image/jpeg;base64,{{ $foto }}" alt="Eu" class="img-lg img-circle center add-tooltip online">
                                            @else
                                                @if(trim($associado->SEXO) == 'M')
                                                    <img src="{{ asset('assets/img/profile-photos/male.png') }}" alt="Eu" class="img-lg img-circle center add-tooltip online">
                                                @else
                                                    <img src="{{ asset('assets/img/profile-photos/female.png') }}" alt="Eu" class="img-lg img-circle center add-tooltip online">
                                                @endif
                                            @endif
                                        @else
                                            @if(session('usuario.foto'))
                                                <img src="data:image/jpeg;base64,{{ $foto }}" alt="Eu" class="img-lg img-circle center add-tooltip online">
                                            @else
                                                <img src="{{ asset('assets/img/profile-photos/'.session('usuario.sexo')) }}" alt="Eu" class="img-lg img-circle center add-tooltip online">
                                            @endif
                                        @endif
                                    </a>
                                </div>
                                <p class="text-lg text-semibold mar-no text-main">{{ ucwords( utf8_encode($associado->NOMEDOTITULAR) ) }}</p>
                            </div>

                            <ul class="nav nav-tabs in" id="tab-dados-associado">
                                <li class="active">
                                    <a href="#tab-pessoais" data-toggle="tab">
                                        <i class="fam-user"></i> <span class="hidden-sm hidden-xs">Pessoais</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#tab-profissionais" data-toggle="tab">
                                        <i class="fam-user-gray"></i> <span class="hidden-sm hidden-xs">Profissionais</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#tab-complementares" data-toggle="tab">
                                        <i class="fam-script-lightning"></i> <span class="hidden-sm hidden-xs">Complementares</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#tab-bancarios" data-toggle="tab">
                                        <i class="fam-coins"></i> <span class="hidden-sm hidden-xs">Bancários</span>
                                    </a>
                                </li>
                                <li>
                                    <a href="#tab-dependentes" data-toggle="tab">
                                        <i class="fam-group"></i> <span class="hidden-sm hidden-xs">Dependentes</span>
                                    </a>
                                </li>

                                @if(Policy::isAdmin())
                                    <li>
                                        <a href="{{ route('debitos.index', [$associado->NUMERODACOTA]) }}">
                                            <i class="fam-creditcards"></i> <span class="hidden-sm hidden-xs">Débitos</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ route('convites.index', [$associado->NUMERODACOTA]) }}">
                                            <i class="fam-tag-red"></i> <span class="hidden-sm hidden-xs">Convites</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ route('telefonia.index', [$associado->NUMERODACOTA]) }}">
                                            <i class="fam-telephone"></i> <span class="hidden-sm hidden-xs">Telefonia</span>
                                        </a>
                                    </li>
                                @endif
                            </ul>

                            <div class="tab-content padding-top-10">
                                <div class="tab-pane active" id="tab-pessoais">
                                    @include('Socio::pessoais.tabs.dados-pessoais')
                                </div>
                                <div class="tab-pane fade" id="tab-profissionais">
                                    @include('Socio::pessoais.tabs.dados-profissionais')
                                </div>
                                <div class="tab-pane fade" id="tab-complementares">
                                    @include('Socio::pessoais.tabs.dados-complementares')
                                </div>
                                <div class="tab-pane fade" id="tab-bancarios">
                                    @include('Socio::pessoais.tabs.dados-bancarios')
                                </div>
                                <div class="tab-pane fade" id="tab-dependentes">
                                    @include('Socio::pessoais.tabs.dados-dependentes')
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

            </div>
        </div>

        @component('Components::modal')
            @slot('id', 'modal-upload')
            @slot('title', 'Alteração de Foto')
            @slot('body')
                <div class="alert alert-warning">
                    É importante que a foto escolhida seja no formato <b>RETRATO</b>, pois, de outra forma,
                    sua foto pode ser cortada nas laterais
                </div>
                {!! Form::open(['route' => 'api.foto.upload', 'class'=>"dropzone", 'id'=>"dropzone"]) !!}
                    <input type="hidden" name="matricula" id="foto-cota" value="{{ $associado->NUMERODACOTA }}">
                    <input type="hidden" name="tipo" id="foto-tipo" value="">
                    <div class="fallback">
                        <input name="file" type="file">
                    </div>
                {!! Form::close() !!}
            @endslot
            @slot('footer')
                <button type="reset" class="btn btn-default" data-dismiss="modal" id="cancelar-upload">Cancelar</button>
            @endslot
        @endcomponent
    @endif

    @include('Socio::pessoais.modal.modal-indicado')

@endsection

@section('javascript')
    <!-- Utilização com backbone js -->
    <script type="text/javascript" src="{{ asset('assets/js/libs/underscore.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/js/libs/backbone.js') }}"></script>
    <script type="text/javascript">
        $.ajaxPrefilter(function(options, originalOptions, xhr) {
            return xhr.setRequestHeader('X-CSRF-TOKEN', Laravel.token);
        });

        // Cria um novo model contendo os dados do associado
        var AssociadoModel = Backbone.Model.extend({
            idAttribute: 'NUMERODACOTA',
            urlRoot: '{{ url('painel/associados/associado') }}'
        });

        var dadosPessoais = new AssociadoModel(
            JSON.parse($('#dados_associado').val())
        );

        var DependenteModel = Backbone.Model.extend({
            idAttribute: 'CODIGODEPENDENTE',
            urlRoot: '{{ url('painel/associados/dependentes/'.$associado->NUMERODACOTA) }}'
        });
    </script>

    <script type="text/javascript" src="{{ asset('assets/js/plugin/masked-input/jquery.maskedinput.js') }}"></script>
    <script src="{{ asset('assets/js/app/controllers/Pessoais.js') }}"></script>

    <script type="text/javascript">
        $(function() {
            $('#menu-associado').addClass('active-link');
            CSPlus.drawBreadCrumb();
        });

        @if(Policy::isAdmin())
            CSPlus.drawBreadCrumb(['Associados', 'Dados do Associado']);

            @if($action == 'editar')
                $('#link-edit-pessoais').trigger('click');
            @endif
        @endif

        @if($action == 'dependentes')
            $('[href="#tab-dependentes"]').trigger('click');
        @endif

        @if($action == 'profissionais')
            $('[href="#tab-profissionais"]').trigger('click');
        @endif
    </script>
@endsection
