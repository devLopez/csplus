<div class="row pad-top pad-btm">
    <div class="col col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="pull-right">
            <a class="btn btn-default" id="link-edit-profissionais">Editar</a>
            <a class="btn btn-default" id="link-cancela-profissionais" style="display: none;">Cancelar Edição</a>
            <a class="btn btn-primary disabled" id="link-salva-profissionais">Salvar Edição</a>
        </div>
    </div>
</div>

<div class="row">
    <div class="col col-lg-12">

        <form id="form-edit-profissionais">
            <input type="hidden" id="arquivo-profissionais" value="associado.pessoais.tabs.dados-profissionais.php">

            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label class="control-label">Empresa onde trabalha</label>
                        <input type="text" value="" {!! field_attr('EMPRESA') !!} name="EMPRESA" id="empresa" maxlength="35" class="form-control">
                    </div>
                </div>

                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label class="control-label">Cargo</label>
                        <input type="text" value="" {!! field_attr('FUNCAO') !!} name="FUNCAO" id="funcao" maxlength="40" class="form-control">
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label class="control-label">Departamento</label>
                        <input type="text" value="" {!! field_attr('DEPARTAMENTO_EMPRESA') !!} name="DEPARTAMENTO_EMPRESA" id="departamento-empresa" maxlength="20" class="form-control">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label class="control-label">Matrícula</label>
                        <input type="text" value="" {!! field_attr('MATRICULA_EMPRESA') !!} name="MATRICULA_EMPRESA" id="matricula-empresa" maxlength="11" class="form-control">
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label class="control-label">Registro de classe</label>
                        <input type="text" value="" {!! field_attr('REGISTRO_CLASSE') !!} name="REGISTRO_CLASSE" id="registro-classe" maxlength="15" class="form-control">
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label class="control-label">Situação da Empresa</label>
                        <input type="text" value="" {!! field_attr('SITUACAO_EMPRESA') !!} name="SITUACAO_EMPRESA" id="situacao-empresa" maxlength="20" class="form-control">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label class="control-label">Complemento Comercial</label>
                        <input type="text" value="" {!! field_attr('COMPLEMENTOCOMERCIAL') !!} name="COMPLEMENTOCOMERCIAL" id="complementocomercial" maxlength="75" class="form-control">
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label class="control-label">Data de Admissão</label>
                        <input type="text" value="" {!! field_attr('ADMISSAO_EMPRESA') !!} name="ADMISSAO_EMPRESA" id="admissao-empresa" data-mask="99/99/9999" class="form-control">
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label class="control-label">Data de Afastamento</label>
                        <input type="text" value="" {!! field_attr('AFASTAMENTO_EMPRESA') !!} name="AFASTAMENTO_EMPRESA" id="afastamento-empresa" data-mask="99/99/9999" class="form-control">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label class="control-label">E-mail comercial</label>
                        <input type="text" value="" {!! field_attr('CAIXAPOSTAL_EMPRESA') !!} name="CAIXAPOSTAL_EMPRESA" id="caixapostal-empresa" maxlength="100" class="form-control">
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label class="control-label">Telefone Comercial 1</label>
                        <input type="text" value="" {!! field_attr('TELEFONEC1') !!} name="TELEFONEC1" id="telefone-1" maxlength="20" class="form-control" data-telefone="true">
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label class="control-label">Telefone Comercial 2</label>
                        <input type="text" value="" {!! field_attr('TELEFONEC2') !!} name="TELEFONEC2" id="telefone-2" maxlength="20" class="form-control" data-telefone="true">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label class="control-label">CEP</label>
                        <input type="text" value="" {!! field_attr('CEPCOMERCIAL') !!} name="CEPCOMERCIAL" id="cep-comercial" data-mask="99999-999" maxlength="9" class="form-control">
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label class="control-label">Endereço</label>
                        <input type="text" value="" {!! field_attr('ENDCOM_LOGRADOURO') !!} name="ENDCOM_LOGRADOURO" id="endereco-comercial" maxlength="40" class="form-control">
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label class="control-label">Número</label>
                        <input type="text" value="" {!! field_attr('ENDCOM_NUMERO') !!} name="ENDCOM_NUMERO" id="numero-comercial" maxlength="11" class="form-control">
                    </div>
                </div>
                <div class="col-lg-2 col-md-2 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label class="control-label">Complemento</label>
                        <input type="text" value="" {!! field_attr('ENDCOM_COMPLEMENTO') !!} name="ENDCOM_COMPLEMENTO" id="complemento-comercial" maxlength="15" class="form-control">
                    </div>
                </div>
            </div>


            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label class="control-label">Bairro</label>
                        <input type="text" value="" {!! field_attr('BAIRROCOMERCIAL') !!} name="BAIRROCOMERCIAL" id="bairro-comercial" maxlength="20" class="form-control">
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label class="control-label">Cidade</label>
                        <input type="text" value="" {!! field_attr('CIDADECOMERCIAL') !!} name="CIDADECOMERCIAL" id="cidade-comercial" maxlength="20" class="form-control">
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label class="control-label">Estado</label>
                        <input type="text" value="" {!! field_attr('ESTADOCOMERCIAL') !!} name="ESTADOCOMERCIAL" id="estado-comercial" maxlength="2" class="form-control">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                    <div class="checkbox">
                        <input id="endereco-cobranca" class="magic-checkbox" type="checkbox" name="ENDERECOCOBRANCA" value="T" {!! field_attr('ENDERECOCOBRANCA') !!}>
                        <label for="endereco-cobranca"><b>Endereço para cobrança?</b></label>

                        <input id="desconto-folha" class="magic-checkbox" type="checkbox" name="DESCONTOFOLHA" value="T" {!! field_attr('DESCONTOFOLHA') !!}>
                        <label for="desconto-folha"><b>Desconto em Folha?</b></label>

                        <input id="aposentado" class="magic-checkbox" type="checkbox" name="APOSENTADO" value="T" {!! field_attr('APOSENTADO') !!}>
                        <label for="aposentado"><b>Aposentado?</b></label>

                    </div>
                </div>
            </div>

            <footer id="footer-profissionais" class="pull-right">
                <button type="button" class="btn btn-default" id="edit-profissionais">Editar</button>
                <button type="button" class="btn btn-default" id="cancela-profissionais" style="display: none;">Cancelar Edição</button>
                <button type="submit" class="btn btn-primary" id="salva-profissionais" disabled>Salvar Edição</button>
            </footer>
        </form>
    </div>
</div>
