<div class="row pad-top pad-btm">
    <div class="col col-lg-12">
        <div class="pull-right">
            <a class="btn btn-default" id="link-edit-pessoais">Editar</a>
            <a class="btn btn-default" id="link-cancela-pessoais" style="display: none;">Cancelar Edição</a>
            <a class="btn btn-primary disabled" id="link-salva-pessoais">Salvar Edição</a>
        </div>
    </div>
</div>
<div class="row">
    <div class="col col-lg-12 col-md-12">
        <form id="form-edit-pessoais">
            <input type="hidden" id="arquivo-pessoais" value="associado.pessoais.tabs.dados-pessoais.php">

                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label class="control-label">Nome do Associado</label>
                            <input type="text" value="" {!! field_attr('NOMEDOTITULAR') !!} name="NOMEDOTITULAR" id="nome-titular" maxlength="40" class="form-control">
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label class="control-label">Admissão</label>
                            <input type="text" value="" {!! field_attr('DATAAQUISICAO') !!} name="DATAAQUISICAO" id="data-aquisicao" data-mask="99/99/9999" class="form-control">
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label class="control-label">
                                @if(Client::clube() == 301)
                                    Cód. ANAC
                                @else
                                    {{ ($associado->PJ == 'F') ? 'Apelido' : 'Nome Fantasia' }}
                                @endif
                            </label>
                            <input type="text" value="" {!! field_attr('APELIDO') !!} name="APELIDO" id="apelido" maxlength="40" class="form-control">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label class="control-label">Categoria da Cota</label>
                            {!! Form::select('CATEGORIADACOTA', array_map('utf8_encode', $categorias), null, field_attr('CATEGORIADACOTA', true, ['id' => 'categoria-cota', 'class' => 'form-control'])) !!}
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label class="control-label">CEP</label>
                            <input type="text" value="" {!! field_attr('CEPRESIDENCIAL') !!} id="cep-residencial" name="CEPRESIDENCIAL" data-mask="99999-999" max="9" class="form-control">
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label class="control-label">Endereço</label>
                            <input type="text" value="" {!! field_attr('ENDRES_LOGRADOURO') !!} id="endereco-residencial" name="ENDRES_LOGRADOURO" maxlength="40" class="form-control">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label class="control-label">Número</label>
                            <input type="text" value="" {!! field_attr('ENDRES_NUMERO') !!} name="ENDRES_NUMERO" id="numero-residencial" maxlength="11" class="form-control">
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label class="control-label">Complemento</label>
                            <input type="text" value="" {!! field_attr('ENDRES_COMPLEMENTO') !!} name="ENDRES_COMPLEMENTO" id="complemento-residencial" maxlength="15" class="form-control">
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label class="control-label">Bairro</label>
                            <input type="text" value="" {!! field_attr('BAIRRORESIDENCIAL') !!} id="bairro-residencial" name="BAIRRORESIDENCIAL" maxlength="20" class="form-control">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label class="control-label">Cidade</label>
                            <input type="text" value="" {!! field_attr('CIDADERESIDENCIAL') !!} id="cidade-residencial" name="CIDADERESIDENCIAL" maxlength="20" class="form-control">
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label class="control-label">Estado</label>
                            <input type="text" value="" {!! field_attr('ESTADORESIDENCIAL') !!} id="estado-residencial" name="ESTADORESIDENCIAL" maxlength="2" class="form-control">
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label class="control-label">
                                Telefone 1
                            </label>
                            <input type="text" value="" {!! field_attr('TELEFONER1') !!} name="TELEFONER1" id="telefone-r1" maxlength="20" class="form-control" data-telefone="true">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label class="control-label">Telefone 2</label>
                            <input type="text" value="" {!! field_attr('TELEFONER2') !!} name="TELEFONER2" id="telefone-r2" maxlength="20" class="form-control" data-telefone="true">
                        </div>
                    </div>

                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label class="control-label">E-mail</label>
                            <input type="text" value="" {!! field_attr('CAIXAPOSTAL') !!} name="CAIXAPOSTAL" id="email-pessoal" maxlength="100" class="form-control">
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label class="control-label">Data de Nascimento</label>
                            <input type="text" value="" {!! field_attr('DATANASCIMENTO') !!} name="DATANASCIMENTO" id="data-nascimento" data-mask="99/99/9999" class="form-control">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label class="control-label">{{ ($associado->PJ == 'F') ? 'Identidade' : 'Inscrição Estadual' }}</label>
                            <input type="text" value="" {!! field_attr('RG') !!} name="RG" id="rg" maxlength="20" class="form-control">
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label class="control-label">Órgão Expedidor</label>
                            <input type="text" value="" {!! field_attr('RG_ORGAO_EXPEDIDOR') !!} name="RG_ORGAO_EXPEDIDOR" id="rg-expedidor" maxlength="20" class="form-control">
                        </div>
                    </div>
                    <div class="col col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label class="control-label">Data de Expedição</label>
                            <input type="text" value="" {!! field_attr('RG_DATA_EXPEDICAO') !!} name="RG_DATA_EXPEDICAO" id="rg-expedicao" data-mask="99/99/9999" class="form-control">
                        </div>
                    </div>
                </div>

                @if($associado->PJ == 'F')
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label class="control-label">Sexo</label>
                                {!! Form::select('SEXO', $sexo, null, field_attr('SEXO', true, ['id' => 'sexo', 'class' => 'form-control'])) !!}
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                            <div class="form-group">
                                <label class="control-label">Estado Civil</label>
                                {!! Form::select('ESTADOCIVIL', $estadoCivil, null, field_attr('ESTADOCIVIL', true, ['id' => 'estado-civil', 'class' => 'form-control'])) !!}
                            </div>
                        </div>
                    </div>
                @else
                    <input type="hidden" name="SEXO" id="sexo" value="">
                    <input type="hidden" name="ESTADOCIVIL" id="estado-civil" value="">
                @endif

                <div class="row">
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <div class="form-group">
                            <label class="control-label">Observações</label>
                            <textarea {!! field_attr('OBSERVACOES') !!} id="observacoes" name="OBSERVACOES" class="form-control"></textarea>
                        </div>
                    </div>
                </div>


            <footer id="footer-pessoais" class="mar-top pull-right">
                <button type="button" class="btn btn-default" id="edit-pessoais">Editar</button>
                <button type="button" class="btn btn-default" id="cancela-pessoais" style="display: none;">Cancelar Edição</button>
                <button type="submit" class="btn btn-primary" id="salva-pessoais" disabled>Salvar Edição</button>
            </footer>

        </form>
    </div>
</div>
