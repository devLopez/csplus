<div class="row pad-top pad-btm">
    <div class="col col-lg-4 col-md-4 col-sm-12 col-xs-12">
        <table id="inbox-table" class="table table-striped table-hover table-condensed">
            <thead>
            <tr>
                <th>Nome do Dependente</th>
                <th></th>
            </tr>
            </thead>
            <tbody id="row-dependentes">
            @foreach($dependentes as $d)

                @php($d->FOTO = \CSPlus\Models\CSPlus\Associado\Fotos::getFoto($d->COTADEPENDENTE, $d->CODIGODEPENDENTE))

                <tr class="cursor-pointer">
                    <td data-id="{{ $d->CODIGODEPENDENTE }}" data-dados='{{ json_encode(array_map('utf8_encode', $d->toArray()), JSON_HEX_APOS) }}'>
                        {{
                            str_limit(
                                ucwords(
                                    strtolower(utf8_encode($d->NOMEDEPENDENTE))
                                ), 80
                            )
                        }}
                    </td>
                    <td>
                        @if($d->ATIVO == 'T')
                            <a class="inativa-dependente add-tooltip" href="javascript:void(0)" title="Inativar" data-dependente="{{ $d->CODIGODEPENDENTE }}" data-associado="{{ $d->COTADEPENDENTE }}">
                                <i class="fam-cross"></i>
                            </a>
                        @endif
                    </td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>

    <div class="col col-lg-8 col-md-8 col-sm-12 col-xs-12">

        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="edit-foto foto-socio-p">
                    <a href="#" title="Alterar Foto" id="edita-foto-dependente" data-tipo="" data-altera-foto="">
                        <img id="foto_dependente" src="{{ asset('assets/img/profile-photos/male.png') }}" alt="Eu" class="img-lg img-circle center add-tooltip" title="Alterar Foto">
                    </a>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-5 col-md-3 col-sm-3 col-xs-4">
                <span id="status" class="label label-table">&nbsp;</span>
            </div>
        </div>

        <form id="form-edit-dependente">
            <input type="hidden" id="arquivo-dependentes" value="associado.pessoais.tabs.dados-dependentes.php">
            <input type="hidden" name="COTADEPENDENTE" id="cota-dependente">
            <input type="hidden" name="CODIGODEPENDENTE" id="codigo-dependente">

            <div class="row pad-top">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="form-group">
                        <label class="control-label">Nome do Dependente</label>
                        <input type="text" name="NOMEDEPENDENTE" id="nome-dependente" {!! field_attr('NOMEDEPENDENTE') !!} class="form-control">
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                    <div class="form-group">
                        <label class="control-label">CPF</label>
                        <input type="text" name="CIC" id="cic-dependente" {!! field_attr('CIC') !!} data-mask="999.999.999-99" class="form-control">
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                    <div class="form-group">
                        <label class="control-label">RG</label>
                        <input type="text" name="RG" id="rg-dependente" {!! field_attr('RG') !!}>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <div class="form-group">
                        <label class="control-label">Data de expedição</label>
                        <input type="text" name="RG_DATA_EXPEDICAO" id="data-expedicao" {!! field_attr('RG_DATA_EXPEDICAO') !!} data-mask="99/99/9999" class="form-control">
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <div class="form-group">
                        <label class="control-label">Órgão Expedidor</label>
                        <input type="text" name="RG_ORGAO_EXPEDIDOR" id="orgao-expedidor" {!! field_attr('RG_ORGAO_EXPEDIDOR') !!} class="form-control">
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                    <div class="form-group">
                        <label class="control-label">Profissão</label>
                        <input type="text" name="FUNCAO" id="profissao" {!! field_attr('FUNCAO') !!} class="form-control">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="form-group">
                        <label class="control-label">Nome do Pai</label>
                        <input type="text" name="PAI" id="pai-dependente" {!! field_attr('PAI') !!} class="form-control">
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="form-group">
                        <label class="control-label">Nome do Mãe</label>
                        <input type="text" name="MAE" id="mae-dependente" {!! field_attr('MAE') !!} class="form-control">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                    <div class="form-group">
                        <label class="control-label">Telefone</label>
                        <input type="text" name="TELEFONE" id="telefone1" {!! field_attr('TELEFONE') !!} class="form-control" data-telefone="true">
                    </div>
                </div>
                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
                    <div class="form-group">
                        <label class="control-label">Celular</label>
                        <input type="text" name="TELEFONE2" id="telefone2" {!! field_attr('TELEFONE2') !!} class="form-control" data-telefone="true">
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="form-group">
                        <label class="control-label">Email</label>
                        <input type="email" name="CAIXAPOSTAL" id="email" {!! field_attr('CAIXAPOSTAL') !!} class="form-control">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="form-group">
                        <label class="control-label">Sexo</label>
                        {!! Form::select('SEXO', $sexo, null, field_attr('SEXO', true, ['id' => 'sexo-dependente', 'class' => "form-control"])) !!}
                    </div>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="form-group">
                        <label>Estado Civil</label>
                        {!! Form::select('ESTADOCIVIL', $estadoCivil, null, field_attr('ESTADOCIVIL', true,['id' => 'estado-civil-dependente', 'class' => "form-control"])) !!}
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="form-group">
                        <label class="control-label">Nascimento</label>
                        <input type="text" name="DATANASCIMENTO" id="nascimento" {!! field_attr('DATANASCIMENTO') !!} data-mask="99/99/9999" class="form-control">
                    </div>

                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                    <div class="form-group">
                        <label class="control-label">Parentesco</label>
                        <div class="input-group">

                            {!! Form::select('GRAUPARENTESCO', array_map('utf8_encode', $parentescos), null, field_attr('GRAUPARENTESCO', true, ['id' => 'parentesco', 'class' => "form-control"])) !!}

                            @if(Auth::isAdmin())
                            <span class="input-group-addon">
                                <a href="#modal-cadastra-parentesco" class="add-tooltip" title="Adicionar Parentesco" data-toggle="modal">
                                    <i class="fa fa-plus"></i>
                                </a>
                            </span>
                            @endif
                        </div>
                    </div>
                </div>
            </div>

            <footer class="pull-right">

                @if(Auth::isAdmin())
                    <button type="button" class="btn btn-primary" id="novo-dependente">Novo Dependente</button>
                @endif

                <button type="button" class="btn btn-default" id="edit-dependente">Editar</button>
                <button type="button" class="btn btn-default" id="cancela-dependente" style="display: none;">Cancelar
                    Edição
                </button>
                <button type="submit" class="btn btn-primary" id="salvar-dependente" disabled>Salvar Edição</button>
            </footer>
        </form>
    </div>
</div>

@include('Admin::cadastros.parentescos.modal')

<script type="text/javascript">
    @if(Client::access() == 'admin')
        /** Define a URL para o novo dependente **/
        var urlNovoDependente = '{{ url('painel/associados/dependente/novo/'.$associado->NUMERODACOTA) }}';
    @endif
</script>
