@php($dependentes = $associado->dependentes)

@if($dependentes->isEmpty())
    @include('errors.empty-records', ['message' => 'Nenhum dependente foi cadastrado'])

    @if(Policy::isAdmin())
        @include('Socio::pessoais.tabs.form-dados-dependentes')
    @endif
@else
    @include('Socio::pessoais.tabs.form-dados-dependentes')
@endif
