<div class="row pad-top pad-btm">
    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="pull-right">
            <a class="btn btn-default" id="link-edit-complementares">Editar</a>
            <a class="btn btn-default" id="link-cancela-complementares" style="display: none;">Cancelar Edição</a>
            <a class="btn btn-primary disabled" id="link-salva-complementares">Salvar Edição</a>
        </div>
    </div>
</div>


<div class="row">
    <div class="col col-lg-12">

        <form id="form-edit-complementares">
            <input type="hidden" id="arquivo-complementares" value="associado.pessoais.tabs.dados-complementares.php">

            @if($associado->PJ == 'F')
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label class="form-group">Nacionalidade</label>
                            <input type="text" value="" {!! field_attr('NACIONALIDADE') !!} name="NACIONALIDADE" id="nacionalidade" maxlength="10" class="form-control">
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label class="form-group">Naturalidade</label>
                            <input type="text" value="" {!! field_attr('NATURALIDADE') !!} name="NATURALIDADE" id="naturalidade" maxlength="30" class="form-control">
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label class="form-group">Nome do Pai</label>
                            <input type="text" value="" {!! field_attr('PAI') !!} name="PAI" id="pai" maxlength="40" class="form-control">
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label class="form-group">Nome da Mãe</label>
                            <input type="text" value="" {!! field_attr('MAE') !!} name="MAE" id="mae" maxlength="40" class="form-control">
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="form-group">
                            <label class="form-group">Tipo Sanguíneo</label>
                            <input type="text" value="" {!! field_attr('SANGUE') !!} name="SANGUE" id="tipo-sanguineo" maxlength="4" class="form-control">
                        </div>
                    </div>
                </div>
            @else
                <input type="hidden" name="NACIONALIDADE" value="" id="nacionalidade">
                <input type="hidden" name="NATURALIDADE" value="" id="naturalidade">
                <input type="hidden" name="PAI" value="" id="pai">
                <input type="hidden" name="MAE" value="" id="mae">
                <input type="hidden" name="SANGUE" value="" id="tipo-sanguineo">
            @endif

            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <label class="form-group">Indicado por</label>

                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">

                            <div class="input-group mar-btm">
					            <input type="text" value="" {!! field_attr('RESPONSAVEL') !!} name="RESPONSAVEL" id="responsavel" maxlength="11" class="form-control">
		                        <span class="input-group-addon">
                                    <a href="#modal-get-indicando" data-toggle="modal" rel="tooltip" title="Alterar Responsável" data-mode="associado">
                                        <i class="fam-magnifier"></i>
                                    </a>
                                </span>
		                    </div>

                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                            <span><b id="nome-responsavel"></b></span>
                        </div>
                    </div>

                </div>

                <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                    <label class="form-group">Corretor</label>

                    <div class="row">
                        <div class="col-lg-4 col-md-4 col-sm-4 col-xs-4">

                            <div class="input-group mar-btm">
					            <input type="text" value="" {!! field_attr('CORRETOR') !!} name="CORRETOR" id="corretor" maxlength="11">
		                        <span class="input-group-addon">
                                    <a href="#modal-get-indicando" data-toggle="modal" rel="tooltip" title="Alterar Corretor" data-mode="corretor">
                                        <i class="fam-magnifier"></i>
                                    </a>
                                </span>
		                    </div>

                        </div>
                        <div class="col-lg-8 col-md-8 col-sm-8 col-xs-8">
                            <span><b id="nome-colaborador"></b></span>
                        </div>
                    </div>

                </div>

            </div>

            <div class="row pad-btm">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <input id="benemerito" class="magic-checkbox" type="checkbox" name="BENEMERITO" value="T" {!! field_attr('BENEMERITO') !!}>
                    <label for="endereco-cobranca"><b>Benemérito</b></label>
                </div>
            </div>

            <footer class="pull-right">
                <button type="button" class="btn btn-default" id="edit-complementares">Editar</button>
                <button type="button" class="btn btn-default" id="cancela-complementares" style="display: none;">Cancelar Edição</button>
                <button type="submit" class="btn btn-primary" id="salva-complementares" disabled>Salvar Edição</button>
            </footer>
        </form>

    </div>
</div>
