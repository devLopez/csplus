<div class="row pad-top pad-btm">
    <div class="col col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="pull-right">
            <a class="btn btn-default" id="link-edit-bancarios">Editar</a>
            <a class="btn btn-default" id="link-cancela-bancarios" style="display: none;">Cancelar Edição</a>
            <a class="btn btn-primary disabled" id="link-salva-bancarios">Salvar Edição</a>
        </div>
    </div>
</div>

<div class="row">
    <div class="col col-lg-12">

        <form class="smart-form" id="form-edit-bancarios">
            <input type="hidden" id="arquivo-bancarios" value="associado.pessoais.tabs.dados-bancarios.php">

            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label class="control-label">Banco</label>
                        {!! Form::select('BANCO', $bancos, null, field_attr('BANCO', true, ['id' => 'banco','placeholder' => 'Selecione...', 'class' => 'form-control'])) !!}
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label class="control-label">Agência</label>
                        <input type="text" {!! field_attr('AGENCIA') !!} name="AGENCIA" id="agencia">
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label class="control-label">Tipo de Conta</label>
                        {!! Form::select('TIPOCONTA', $tiposConta, null, field_attr('TIPOCONTA', true, ['id' => 'tipo-conta', 'placeholder' => 'Selecione...', 'class' => 'form-control'])) !!}
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label class="control-label">Conta</label>
                        <input type="text" {!! field_attr('CONTACORRENTE') !!} name="CONTACORRENTE" id="conta-corrente">
                    </div>

                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                    <div class="form-group">
                        <label class="control-label">Dígito</label>
                        <input type="text" {!! field_attr('DIGITOCC') !!} name="DIGITOCC" id="digito" maxlength="1">
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                    <div class="checkbox">
                        <input id="debito-aprovado" class="magic-checkbox" type="checkbox" name="DEBITOCONTAAPROVADO" value="T" {!! field_attr('DEBITOCONTAAPROVADO') !!}>
                        <label for="debito-aprovado"><b>Débito em Conta Aprovado?</b></label>
                    </div>
                </div>
            </div>

            <footer class="pull-right">
                <button type="button" class="btn btn-default" id="edit-bancarios">Editar</button>
                <button type="button" class="btn btn-default" id="cancela-bancarios" style="display: none;">Cancelar Edição</button>
                <button type="submit" class="btn btn-primary" id="salva-bancarios" disabled>Salvar Edição</button>
            </footer>
        </form>

    </div>
</div>
