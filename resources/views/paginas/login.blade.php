@extends('template.login')
@section('titulo', 'CSPlus v8 - Login')

@php
    $acesso     = Client::access();
    $env        = App::environment();
    $cliente    = Client::clube();
    $logo       = false;

    if(File::exists(public_path('assets/img/logos/logo_'.$cliente.'.png'))) {
        $logo = true;
    }
@endphp

@section('conteudo')

    <div class="panel-body">
        <div class="mar-ver pad-btm">
            <h3 class="h4 mar-no">
                @if($logo)
                    <img src="{{ asset('assets/img/logos/logo_'.$cliente.'.png') }}" alt="Logo" width="150">
                @else
                    <img src="{{ asset('assets/img/csplus/logo-clubsystem.png') }}" width="150px">
                @endif
                <br>
                <br>
                CSPlusV8

                @if($env == 'testing')
                    <br>
                    <br>
                    <span class="label label-danger">MODO BETA</span>
                @endif
            </h3>
            @if($acesso == 'admin')
                <p class="text-muted">Acesso Administrativo</p>
            @endif
        </div>

        @if($erro)
            <div class="alert alert-block alert-danger">
            <h4><i class="fa fa-times"></i> Atenção</h4>
            <p>{{ $erro }}</p>
        </div>
        @else
            {!! Form::open(['route' => 'login.logar', 'id' => 'form-login', 'class' => 'not-ajax']) !!}
                @if(session('error'))
                    <div class="alert alert-danger">
                        {{ session('error') }}
                    </div>
                @endif

                <input type="hidden" name="acesso" value="{{ $acesso }}">

                @if($acesso == 'associado')
                    <div class="form-group text-left">
                        {!! Form::checkbox('juridica', 1, old('juridica'), ['id' => 'is_juridica']) !!}
                        {{--<input type="checkbox" id="is_juridica">--}}
                        <label for="" class="control-label">Pessoa Jurídica</label>
                    </div>
                @endif

                <div class="form-group">
                    @if($acesso == 'associado')
                        {!! Form::text('usuario', old('usuario'), ['required', 'autofocus', 'class' => 'form-control', 'data-mask' => '999.999.999-99', 'placeholder' => 'Digite o seu Usuário']) !!}
                    @else
                        {!! Form::text('usuario', old('usuario'), ['required', 'autofocus', 'class' => 'form-control', 'placeholder' => 'Digite o seu Usuário']) !!}
                    @endif
                </div>

                <div class="form-group">
                    <input type="password" class="form-control" name="password" required="" placeholder="Digite sua senha">
                </div>

                <button class="btn btn-primary btn-lg btn-block" type="submit" data-loading-text="Fazendo Login..." id="btn-login">
                    Fazer login
                </button>
            {!! Form::close() !!}
        @endif
    </div>

    <div class="pad-all">
        Caso tenha esquecido sua senha, entre em contato com seu clube
    </div>

@endsection

@section('javascript')
    <script src="{{ asset('assets/js/plugin/masked-input/jquery.maskedinput.js') }}"></script>
    <script type="text/javascript">
        (new Auth).maskField('999.999.999-99').changeMaskField();
    </script>
@endsection
