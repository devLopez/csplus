@extends('template.report')
@section('titulo', $relatorio)

@php
    $listados   = $associados->count();
    $dados      = $associados->chunk(9);
    $paginas    = $dados->count();

    $i = 1;
@endphp

@section('conteudo')

    @if($dados->isEmpty())
        @include('template.helpers.reports.topbar')

        <div class="card-body card-padding">
            @include('template.helpers.reports.report-title', ['relatorio' => $relatorio])

            <div class="alert alert-primary">
                Não existem dados para serem exibidos
            </div>
        </div>
    @else
        @foreach($dados as $associados)
            @include('template.helpers.reports.topbar')

            <div class="card-body card-padding">
                @include('template.helpers.reports.report-title', ['relatorio' => $relatorio])

                <table class="table table-striped table-condensed i-table">
                    <thead class="text-uppercase">
                    <tr>
                        <th class="c-gray">Matrícula</th>
                        <th class="c-gray">Titular</th>
                        <th class="c-gray">Nascimento</th>
                        <th class="c-gray">Dt. Aquisição</th>
                        <th class="c-gray">Último pagto.</th>
                        <th class="c-gray">Banco</th>
                        <th class="c-gray">Agência</th>
                        <th class="c-gray">Conta Corrente</th>
                        <th class="c-gray">Dt. Afastamento</th>
                        <th class="c-gray">Nacionalidade</th>
                        <th class="c-gray">Naturalidade</th>
                        <th class="c-gray">Sexo</th>
                        <th class="c-gray">Estado Civil</th>
                        <th class="c-gray">CPF</th>
                        <th class="c-gray">RG</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($associados as $a)
                        <tr>
                            <td>{{ $a->NUMERODACOTA }}</td>
                            <td>{{ utf8_encode($a->NOMEDOTITULAR) }}</td>
                            <td>{{ Date::toBr($a->DATANASCIMENTO) }}</td>
                            <td>{{ Date::toBr($a->DATAAQUISICAO) }}</td>
                            <td>{{ $a->ULTIMACOBRANCA }}</td>
                            <td>{{ $a->BANCO }}</td>
                            <td>{{ $a->AGENCIA }}</td>
                            <td>{{ $a->CONTACORRENTE }}</td>
                            <td>{{ Date::toBr($a->DATAAFASTAMENTO) }}</td>
                            <td>{{ utf8_encode($a->NACIONALIDADE) }}</td>
                            <td>{{ utf8_encode($a->NATURALIDADE) }}</td>
                            <td>{{ $a->SEXO }}</td>
                            <td>{{ $a->ESTADOCIVIL }}</td>
                            <td>{{ $a->CIC }}</td>
                            <td>{{ $a->RG }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

                @if($i == $paginas)
                    <div class="row m-b-25">
                        <div class="col col-xs-12 text-right">
                            <span class="c-gray"><b>TOTAL DE LISTADOS: {{ $listados }}</b></span><br>
                        </div>
                    </div>
                @endif

                <hr>

                <div class="report-footer">
                    <div class="col col-xs-12 text-center">
                        <b>
                            {{ $i }} / {{ $paginas }}
                        </b>
                    </div>
                </div>

            </div>

            @php($i++)
        @endforeach
    @endif
@endsection
