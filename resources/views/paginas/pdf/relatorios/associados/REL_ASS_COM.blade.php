@extends('template.report')
@section('titulo', $relatorio)

@php
    $listados   = $associados->count();
    $dados      = $associados->chunk(16);
    $paginas    = $dados->count();

    $i = 1;
@endphp

@section('conteudo')

    @if($dados->isEmpty())
        @include('template.helpers.reports.topbar')

        <div class="card-body card-padding">
            @include('template.helpers.reports.report-title', ['relatorio' => $relatorio])

            <div class="alert alert-primary">
                Não existem dados para serem exibidos
            </div>
        </div>
    @else
        @foreach($dados as $associados)
            @include('template.helpers.reports.topbar')

            <div class="card-body card-padding">
                @include('template.helpers.reports.report-title', ['relatorio' => $relatorio])

                <table class="table table-striped table-condensed i-table">
                    <thead class="text-uppercase">
                    <tr>
                        <th class="c-gray">Matrícula</th>
                        <th class="c-gray">Titular</th>
                        <th class="c-gray">Endereço</th>
                        <th class="c-gray">Bairro</th>
                        <th class="c-gray">Cidade</th>
                        <th class="c-gray">UF</th>
                        <th class="c-gray">CEP</th>
                        <th class="c-gray">Telefone 1</th>
                        <th class="c-gray">Telefone 2</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($associados as $a)
                        <tr>
                            <td>{{ $a->NUMERODACOTA }}</td>
                            <td>{{ utf8_encode($a->NOMEDOTITULAR) }}</td>
                            <td>{{ utf8_encode($a->ENDERECOCOMERCIAL) }}</td>
                            <td>{{ utf8_encode($a->BAIRROCOMERCIAL) }}</td>
                            <td>{{ utf8_encode($a->CIDADECOMERCIAL) }}</td>
                            <td>{{ utf8_encode($a->ESTADOCOMERCIAL) }}</td>
                            <td>{{ utf8_encode($a->CEPCOMERCIAL) }}</td>
                            <td>{{ $a->DDDCOMERCIAL1.$a->TELEFONEC1 }}</td>
                            <td>{{ $a->DDDCOMERCIAL2.$a->TELEFONEC2 }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

                @if($i == $paginas)
                    <div class="row m-b-25">
                        <div class="col col-xs-12 text-right">
                            <span class="c-gray"><b>TOTAL DE LISTADOS: {{ $listados }}</b></span><br>
                        </div>
                    </div>
                @endif

                <hr>

                <div class="report-footer">
                    <div class="col col-xs-12 text-center">
                        <b>
                            {{ $i }} / {{ $paginas }}
                        </b>
                    </div>
                </div>

            </div>

            @php($i++)
        @endforeach
    @endif
@endsection