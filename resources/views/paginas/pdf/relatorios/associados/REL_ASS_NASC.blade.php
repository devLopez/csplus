@extends('template.report')
@section('titulo', $relatorio)

@php
$listados   = $associados->count();
$dados      = $associados->chunk(28);
$paginas    = $dados->count();

$i = 1;
@endphp

@section('conteudo')

    @if($dados->isEmpty())
        @include('template.helpers.reports.topbar')

        <div class="card-body card-padding">
            @include('template.helpers.reports.report-title', ['relatorio' => $relatorio])

            <div class="alert alert-primary">
                Não existem dados para serem exibidos
            </div>
        </div>
    @else
        @foreach($dados as $associados)
            @include('template.helpers.reports.topbar')

            <div class="card-body card-padding">
                @include('template.helpers.reports.report-title', ['relatorio' => $relatorio])

                <table class="table table-striped table-condensed i-table">
                    <thead class="text-uppercase">
                    <tr>
                        <th class="c-gray">Matrícula</th>
                        <th class="c-gray">Titular</th>
                        <th class="c-gray">Data de Nascimento</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($associados as $a)
                        <tr>
                            <td>{{ $a->NUMERODACOTA }}</td>
                            <td>{{ utf8_encode($a->NOMEDOTITULAR) }}</td>
                            <td>{{ Date::toBr($a->DATANASCIMENTO) }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

                @if($i == $paginas)
                    <div class="row m-b-25">
                        <div class="col col-xs-12 text-right">
                            <span class="c-gray"><b>TOTAL DE LISTADOS: {{ $listados }}</b></span><br>
                        </div>
                    </div>
                @endif

                <hr>

                <div class="report-footer">
                    <div class="col col-xs-12 text-center">
                        <b>
                            {{ $i }} / {{ $paginas }}
                        </b>
                    </div>
                </div>

            </div>

            @php($i++)
        @endforeach
    @endif
@endsection
