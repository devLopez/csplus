@extends('template.report')
@section('titulo', 'Relatório de Dependentes')

@php
    $listados   = $relatorio->count();
    $dados      = $relatorio->chunk(15);
    $paginas    = $dados->count();

    $i = 1;
@endphp

@section('conteudo')

    @if($dados->isEmpty())
        @include('template.helpers.reports.topbar')

        <div class="card-body card-padding">
            @include('template.helpers.reports.report-title', ['relatorio' => $relatorio])

            <div class="alert alert-primary">
                Não existem dados para serem exibidos
            </div>
        </div>
    @else
        @foreach($dados as $dependentes)
            @include('template.helpers.reports.topbar')

            <div class="card-body card-padding">
                @include('template.helpers.reports.report-title', ['relatorio' => 'Relatório de Dependentes'])

                <table class="table table-striped table-condensed i-table">
                    <thead class="text-uppercase">
                    <tr>
                        <th class="c-gray">Matrícula</th>
                        <th class="c-gray">Mat. Dependente</th>
                        <th class="c-gray">Nome Dependente</th>
                        <th class="c-gray">Parentesco</th>
                        <th class="c-gray">Data Nascimento</th>
                        <th class="c-gray">Ativo</th>
                        <th class="c-gray">RG</th>
                        <th class="c-gray">CPF</th>
                        <th class="c-gray">Estado Civil</th>
                        <th class="c-gray">Sexo</th>
                        <th class="c-gray">Idade</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($dependentes as $r)
                        <tr>
                            <td>{{ $r->COTADEPENDENTE }}</td>
                            <td>{{ $r->CODIGODEPENDENTE }}</td>
                            <td>{{ utf8_encode($r->NOMEDEPENDENTE) }}</td>
                            <td>{{ utf8_encode($r->DESCRICAO) }}</td>
                            <td>{{ Date::toBr($r->DATANASCIMENTO) }}</td>
                            <td>
                                @if($r->ATIVO == 'T')
                                    <label class="label label-success">Ativo</label>
                                @else
                                    <label class="label label-warning">Inativo</label>
                                @endif
                            </td>
                            <td>{{ $r->RG }}</td>
                            <td>{{ $r->CIC }}</td>
                            <td>{{ $r->ESTADOCIVIL }}</td>
                            <td>{{ $r->SEXO }}</td>
                            <td>{{ $r->IDADE }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

                @if($i == $paginas)
                    <div class="row m-b-25">
                        <div class="col col-xs-12 text-right">
                            <span class="c-gray"><b>TOTAL DE LISTADOS: {{ $listados }}</b></span><br>
                        </div>
                    </div>
                @endif

                <hr>

                <div class="report-footer">
                    <div class="col col-xs-12 text-center">
                        <b>
                            {{ $i }} / {{ $paginas }}
                        </b>
                    </div>
                </div>

            </div>

            @php($i++)
        @endforeach
    @endif
@endsection
