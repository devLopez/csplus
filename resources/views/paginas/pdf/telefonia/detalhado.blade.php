@extends('template.report')
@section('titulo', 'Consumo de Telefonia - Detalhado')

@php
    $listados   = $relatorio->count();
    $valorTotal = $relatorio->sum('VALOR');
    $dados      = $relatorio->chunk(17);
    $paginas    = $dados->count();

    $i = 1;
@endphp

@section('conteudo')
    @if($dados->isEmpty())
        @include('template.helpers.reports.topbar')

        <div class="card-body card-padding">
            @include('template.helpers.reports.report-title', ['relatorio' => 'Consumo de Telefonia - Detalhado'])

            <div class="alert alert-primary">
                Não existem dados para serem exibidos
            </div>
        </div>
    @else
        @foreach($dados as $pagina)
            @include('template.helpers.reports.topbar')

            <div class="card-body card-padding">
                @include('template.helpers.reports.report-title', [
                    'relatorio' => 'Consumo de Telefonia - Detalhado',
                    'associado' => $associado
                ])


                <table class="table table-striped table-condensed i-table">
                    <thead class="text-uppercase">
                    <tr>
                        <th class="c-gray">Hora</th>
                        <th class="c-gray">Tipo de Ligação</th>
                        <th class="c-gray">Número</th>
                        <th class="c-gray">Histório</th>
                        <th class="c-gray">Duração</th>
                        <th class="c-gray">Tarifa</th>
                        <th class="c-gray">Valor</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($pagina as $r)
                        <tr>
                            <td>{{ br_timestamp($r->DATA_HORA_INI) }}</td>
                            <td>
                                <label class="label label-success">{{ $r->TIPO_LIGACAO }}</label>
                            </td>
                            <td>{{ mask_phone($r->DISCADO) }}</td>
                            <td>{{ utf8_encode($r->HISTORICO) }}</td>
                            <td>{{ $r->DURACAO }}</td>
                            <td>R${{ money2br($r->TARIFA) }}</td>
                            <td>R${{ money2br($r->VALOR) }}</td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

                @if($i == $paginas)
                    <div class="row m-b-25">
                        <div class="col col-xs-12 text-right">
                            <span class="c-gray"><b>TOTAL DE LISTADOS: {{ $listados }}</b></span><br>
                            <span class="c-gray"><b>VALOR TOTAL: R${{ money2br($valorTotal) }}</b></span><br>
                        </div>
                    </div>
                @endif

                <hr>

                <div class="report-footer">
                    <div class="col col-xs-12 text-center">
                        <b>
                            {{ $i }} / {{ $paginas }}
                        </b>
                    </div>
                </div>

            </div>

            @php($i++)
        @endforeach
    @endif
@endsection