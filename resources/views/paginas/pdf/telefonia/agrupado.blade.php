@extends('template.report')
@section('titulo', 'Consumo de Telefonia - Agrupado')

@php
    $listados   = $relatorio->count();
    $valorTotal = $relatorio->sum('VALOR');
    $dados      = $relatorio->chunk(17);
    $paginas    = $dados->count();

    $i = 1;
@endphp

@section('conteudo')
    @if($dados->isEmpty())
        @include('template.helpers.reports.topbar')

        <div class="card-body card-padding">
            @include('template.helpers.reports.report-title', ['relatorio' => 'Consumo de Telefonia - Detalhado'])

            <div class="alert alert-primary">
                Não existem dados para serem exibidos
            </div>
        </div>
    @else
        @foreach($dados as $pagina)
            @include('template.helpers.reports.topbar')

            <div class="card-body card-padding">
                @include('template.helpers.reports.report-title', [
                    'relatorio' => 'Consumo de Telefonia - Detalhado',
                    'associado' => $associado
                ])

                <table class="table i-table m-t-25 m-b-25">
                    <thead class="text-uppercase">
                        <tr>
                            <th class="c-gray">Telefone</th>
                            <th class="c-gray">Data</th>
                            <th class="c-gray">Valor</th>
                            <th class="c-gray">Titular</th>
                            <th class="c-gray">Tipo de Ligação</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>
                    <thead>
                        @foreach($relatorio as $r)
                            <tr>
                                <td>{{ mask_phone($r->TELEFONE) }}</td>
                                <td>{{ br_timestamp($r->DATA_HORA_INI) }}</td>
                                <td>R${{ money2br($r->VALOR) }}</td>
                                <td>{{ utf8_encode($r->NOMEDOTITULAR) }}</td>
                                <td>
                                    <label class="label label-success">{{ $r->TIPO_LIGACAO }}</label>
                                </td>
                            </tr>
                        @endforeach
                    </thead>
                </table>

                @if($i == $paginas)
                    <div class="row m-b-25">
                        <div class="col col-xs-12 text-right">
                            <span class="c-gray"><b>TOTAL DE LISTADOS: {{ $listados }}</b></span><br>
                            <span class="c-gray"><b>VALOR TOTAL: R${{ money2br($valorTotal) }}</b></span><br>
                        </div>
                    </div>
                @endif

                <hr>

                <div class="report-footer">
                    <div class="col col-xs-12 text-center">
                        <b>
                            {{ $i }} / {{ $paginas }}
                        </b>
                    </div>
                </div>

            </div>

            @php($i++)
        @endforeach

    @endif
@endsection