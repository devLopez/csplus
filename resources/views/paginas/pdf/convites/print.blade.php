@if($convites->isEmpty())
    <script type="text/javascript">
        alert('Não foram encontrados convites para impressão');
        window.close();
    </script>
@else

    {{-- Esta variável servirá para o controle de impressão --}}
    <?php $i = 0?>

    @foreach($convites as $convite)
        <table width="100%" cellpadding="4" cellspacing="0" style="margin-top: 25px">
            <col width="128*">
            <col width="128*">

            <tr valign="top">
                <td width="50%" height="93" style="border-top: 1px  double #808080; border-bottom: 1px double #808080; border-left: 1px  double #808080; border-right: none; padding-top: 0.1cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
                    <p>
                        <img src="{{ asset(session('logo')) }}" name="Figura1" align="left" border="0"><br/>
                    </p>
                </td>
                <td width="50%" style="border-top: 1px double #808080; border-bottom: 1px double #808080; border-left: 0px; border-right: 1px  double #808080; padding: 0.1cm">
                    <p align="right">
                        <b>C  O  N  V  I  T  E</b>
                    </p>
                    <p align="right"><font size="4" style="font-size: 16pt">
                        <b>Emiss&atilde;o:
                            {{ Date::toBr($convite->DATAEMISSAOCONVITE) }}
                        </b></font>
                    </p>
                </td>
            </tr>

            <tr valign="top">
                <td width="50%" style="border-top: none; border-bottom: none; border-left: 1px double #808080; border-right: none; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
                    <p><b>Validade</b>: {{ Date::toBr($convite->DATAINI) ." à " . Date::toBr($convite->DATAFIM) }}</p>
                </td>
                <td width="50%" style="border-top: none; border-bottom: none; border-left: none; border-right: 1px double #808080; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0.1cm">
                    <p>
                        <b>Convidado</b>: {{ $convite->NOME }} <br>
                        <b>Documento</b>: {{ $convite->CI }}
                    </p>
                </td>
            </tr>

            <tr valign="top">
                <td width="50%" style="border-top: none; border-bottom: 1px double #808080; border-left: 1px double #808080; border-right: none; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0cm">
                    <p>Respons&aacute;vel (associado):
                    </p>
                </td>
                <td width="50%" style="border-top: none; border-bottom: 1px double #808080; border-left: none; border-right: 1px double #808080; padding-top: 0cm; padding-bottom: 0.1cm; padding-left: 0.1cm; padding-right: 0.1cm">
                    <p>{{ $convite->NOMEDOTITULAR }}</p>
                </td>
            </tr>
            <tr>
                <td>
                    {{ $convite->DESCRICAO_TIPO_CONVITE }}
                </td>
            </tr>
        </table>
        <table width="100%">
            <tr>
                <td align="center" style="border-top: 1px double #808080; border-bottom: 1px double #808080; border-left: 1px double #808080; border-right: 1px double #808080">
                    <br>
                    <img src="data:image/jpg;base64,{{ BarCode::generate($convite->CODIGO_GERADO) }}">
                    <br>
                </td>
            </tr>
        </table>

        <?php $i++?>

        @if($i > 2)
            <div style="page-break-after: always;"></div>

            {{-- Reinicia o contador --}}
            <?php $i = 0?>
        @endif

    @endforeach
@endif
