@extends('template.subscription')
@section('titulo', 'Novo Cliente CSPlus')

{{--
    razão social ou nome
    cpf ou CNPJ
    endereço completo
    telefone
    email
    opcionalmente fornecer local para upload da logo
    dados do cartão de credito
    titular do cartão
    cpf do titular do cartão
    forma de pagamento, se credito a vista ou parcelado em até 10x com juros.
--}}

@section('conteudo')
    <div class="row">
        <div class="col-lg-12">
            <div class="panel">
                <header class="panel-heading">
                    <h2 class="panel-title">Novo Cliente CSPlus</h2>
                </header>
                {!! Form::open(['route' => 'bussiness.clientes.post', 'files' => true]) !!}
                    <div class="panel-body">

                        <div class="row">
                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                <div class="form-group {{ ($errors->has('nome')) ? 'has-error' : '' }}">
                                    <label class="control-label"><b>Nome (ou Razão Social)</b></label>
                                    {!! Form::text('NOMEDOTITULAR', old('NOMEDOTITULAR'), ['required', 'autofocus', 'class' => 'form-control']) !!}

                                    @if($errors->has('NOMEDOTITULAR'))
                                        <div class="note note-danger">{{ $errors->first('NOMEDOTITULAR') }}</div>
                                    @endif
                                </div>
                            </div>

                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                <div class="form-group {{ ($errors->has('CIC')) ? 'has-error' : '' }}">
                                    <label class="control-label"><b>CPF/ CNPJ</b></label>
                                    {!! Form::text('CIC', old('CIC'), ['required', 'class' => 'form-control']) !!}

                                    @if($errors->has('CIC'))
                                        <div class="note note-danger">{{ $errors->first('CIC') }}</div>
                                    @endif
                                </div>
                            </div>

                            <div class="col-lg-4 col-md-4">
                                <div class="form-group {{ ($errors->has('CAIXAPOSTAL')) ? 'has-error' : '' }}">
                                    <label class="control-label"><b>E-mail</b></label>
                                    {!! Form::email('CAIXAPOSTAL', old('CAIXAPOSTAL'), ['required', 'maxlength'=>100, 'class' => 'form-control']) !!}

                                    @if($errors->has('CAIXAPOSTAL'))
                                        <div class="note note-danger">{{ $errors->first('CAIXAPOSTAL') }}</div>
                                    @endif
                                </div>
                            </div>

                        </div>

                        <div class="row">
                            <div class="col-lg-4 col-md-4">
                                <div class="form-group {{ ($errors->has('CEPRESIDENCIAL')) ? 'has-error' : '' }}">
                                    <label class="control-label"><b>CEP</b></label>
                                    {!! Form::text('CEPRESIDENCIAL', old('CEPRESIDENCIAL'), ['id'=>"cep-residencial", 'maxlength'=>9, 'data-mask'=>"99999-999", 'class' => 'form-control']) !!}

                                    @if($errors->has('CEPRESIDENCIAL'))
                                        <div class="note note-danger">{{ $errors->first('CEPRESIDENCIAL') }}</div>
                                    @endif
                                </div>
                            </div>

                            <div class="col-lg-4 col-md-4">
                                <div class="form-group {{ ($errors->has('ENDRES_LOGRADOURO')) ? 'has-error' : '' }}">
                                    <label class="control-label"><b>Endereço</b></label>
                                    {!! Form::text('ENDRES_LOGRADOURO', old('ENDRES_LOGRADOURO'), ['required', 'id'=>"endereco-residencial", 'maxlength'=>40, 'class' => 'form-control']) !!}

                                    @if($errors->has('ENDRES_LOGRADOURO'))
                                        <div class="note note-danger">{{ $errors->first('ENDRES_LOGRADOURO') }}</div>
                                    @endif
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4">
                                <div class="form-group {{ ($errors->has('ENDRES_NUMERO')) ? 'has-error' : '' }}">
                                    <label class="control-label"><b>Número</b></label>
                                    {!! Form::text('ENDRES_NUMERO', old('ENDRES_NUMERO'), ['required', 'id'=>"numero-residencial", 'maxlength'=>11, 'class' => 'form-control']) !!}

                                    @if($errors->has('ENDRES_NUMERO'))
                                        <div class="note note-danger">{{ $errors->first('ENDRES_NUMERO') }}</div>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="row">

                            <div class="col-lg-2 col-md-2">
                                <div class="form-group {{ ($errors->has('ENDRES_COMPLEMENTO')) ? 'has-error' : '' }}">
                                    <label class="control-label"><b>Complemento</b></label>
                                    {!! Form::text('ENDRES_COMPLEMENTO', old('ENDRES_COMPLEMENTO'), ['maxlength'=>15, 'class' => 'form-control']) !!}

                                    @if($errors->has('ENDRES_COMPLEMENTO'))
                                        <div class="note note-danger">{{ $errors->first('ENDRES_COMPLEMENTO') }}</div>
                                    @endif
                                </div>
                            </div>

                            <div class="col-lg-4 col-md-4">
                                <div class="form-group {{ ($errors->has('BAIRRORESIDENCIAL')) ? 'has-error' : '' }}">
                                    <label class="control-label"><b>Bairro</b></label>
                                    {!! Form::text('BAIRRORESIDENCIAL', old('BAIRRORESIDENCIAL'), ['required', 'id'=>"bairro-residencial", 'maxlength'=>20, 'class' => 'form-control']) !!}

                                    @if($errors->has('BAIRRORESIDENCIAL'))
                                        <div class="note note-danger">{{ $errors->first('BAIRRORESIDENCIAL') }}</div>
                                    @endif
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4">
                                <div class="form-group {{ ($errors->has('CIDADERESIDENCIAL')) ? 'has-error' : '' }}">
                                    <label class="control-label"><b>Cidade</b></label>
                                    {!! Form::text('CIDADERESIDENCIAL', old('CIDADERESIDENCIAL'), ['required', 'id'=>"cidade-residencial", 'maxlength'=>20, 'class' => 'form-control']) !!}

                                    @if($errors->has('CIDADERESIDENCIAL'))
                                        <div class="note note-danger">{{ $errors->first('CIDADERESIDENCIAL') }}</div>
                                    @endif
                                </div>
                            </div>
                            <div class="col-lg-2 col-md-2">
                                <div class="form-group {{ ($errors->has('ESTADORESIDENCIAL')) ? 'has-error' : '' }}">
                                    <label class="control-label"><b>Estado</b></label>
                                    {!! Form::text('ESTADORESIDENCIAL', old('ESTADORESIDENCIAL'), ['required', 'id'=>"estado-residencial", 'maxlength' => 2, 'class' => 'form-control']) !!}

                                    @if($errors->has('ESTADORESIDENCIAL'))
                                        <div class="note note-danger">{{ $errors->first('ESTADORESIDENCIAL') }}</div>
                                    @endif
                                </div>
                            </div>
                        </div>


                        <div class="row">
                            <div class="col-lg-4 col-md-4">
                                <div class="form-group {{ ($errors->has('TELEFONER1')) ? 'has-error' : '' }}">
                                    <label class="control-label"><b>Celular</b></label>
                                    {!! Form::text('TELEFONER1', old('TELEFONER1'), ['maxlength' => 20, 'data-mask' => '(99)9999-9999?9', 'class' => 'form-control']) !!}

                                    @if($errors->has('TELEFONER1'))
                                        <div class="note note-danger">{{ $errors->first('TELEFONER1') }}</div>
                                    @endif
                                </div>
                            </div>
                            <div class="col-lg-4 col-md-4">
                                <div class="form-group {{ ($errors->has('TELEFONER2')) ? 'has-error' : '' }}">
                                    <label class="control-label"><b>Telefone</b></label>
                                    {!! Form::text('TELEFONER2', old('TELEFONER2'), ['required', 'maxlength' => 20, 'data-mask' => '(99)9999-9999?9', 'class' => 'form-control']) !!}

                                    @if($errors->has('TELEFONER2'))
                                        <div class="note note-danger">{{ $errors->first('TELEFONER2') }}</div>
                                    @endif
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-lg-4 col-md-4">
                                <div class="form-group {{ ($errors->has('logo')) ? 'has-error' : '' }}">
                                    <label class="control-label"><b>Logomarca</b></label>
                                    {!! Form::file('logo', old('logo'), ['class' => 'form-control']) !!}

                                    @if($errors->has('logo'))
                                        <div class="note note-danger">{{ $errors->first('logo') }}</div>
                                    @endif
                                </div>
                            </div>
                        </div>

                    </div>
                    <div class="panel-footer text-right">
                        <button type="submit" class="btn btn-success">
                            Criar meu acesso
                        </button>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection

@section('javascript')

    <script type="text/javascript" src="{{ asset('assets/js/plugin/masked-input/jquery.maskedinput.js') }}"></script>
    <script type="text/javascript">
        $(function () {
            CSPlus.runAllForms();

            $('#cep-comercial').on('blur', function () {
                if ($(this).val()) {
                    num_cep = $(this).val();
                    endereco = $('#endereco-comercial');
                    bairro = $('#bairro-comercial');
                    cidade = $('#cidade-comercial');
                    estado = $('#estado-comercial');

                    CEP.buscar(
                        $(this).val(),
                        endereco,
                        bairro,
                        cidade,
                        estado
                    );
                }
            });
        });
    </script>
@endsection
