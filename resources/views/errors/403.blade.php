@extends('template.errors')
@section('titulo', '403 - Acesso Proibído')

@section('code', 403)
@section('text-status', 'Acesso Proibido!')
@section('information')
    Você não possui privilégios suficientes para acessar esta página. Caso tenha certeza do seu acesso,
    entre em contato com o seu clube
@endsection
