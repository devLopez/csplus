@if(session('sucesso'))
    <div class="col col-lg-12 col-md-12 col-sm-12 col-xs-12 margin-bottom-10 margin-top-10">
        <div style="margin-bottom: 30px;">
            <img src="{{ asset('assets/img/confirm.png') }}" alt="Inscrição Realizada" title="Inscrição Realizada" style="display: block; margin: 0 auto;">
        </div>

        <div class="text-center">
            <h2>Sucesso!</h2>
            <h5>
                {{ session('sucesso') }}
            </h5>

            <br>

            @if(session('cobranca'))
                <a href="http://www.sistemasbrasil.com.br/abraphe/boleto/boleto.php?codigocobranca={{ session('cobranca') }}&cliente={{ Client::clube() }}"
                    class="btn btn-primary"
                    target="_blank"
                    style="position: relative; z-index: 99999"
                >
                    Clique aqui para gerar o boleto bancário
                </a>
            @endif
        </div>
    </div>
@endif

@if(session('erro'))
    <div class="col col-lg-12 col-md-12 col-sm-12 col-xs-12">
        <div class="alert alert-danger">
            <h4><i class="fa fa-times"></i> Atenção</h4>
            <p>{{ session('erro') }}</p>
        </div>
    </div>
@endif
