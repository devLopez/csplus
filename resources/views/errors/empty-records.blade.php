<div class="cls-content text-center pad-no" id="empty-records">
	<h1 class="error-code text-info">
        <i class="fa fa-bullhorn"></i>
    </h1>
	<p class="text-main text-semibold text-lg text-uppercase">{{ $message }}</p>
</div>
