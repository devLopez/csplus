    <!--[if IE 9]>
    <style>
        .error-text {
            color: #333 !important;
        }
    </style>
    <![endif]-->

    <!-- row -->
    <div class="row">

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <div class="row">
                <div class="col-sm-12">
                    <div class="text-center error-box">
                        <h1 class="error-text tada animated">
                            <i class="fa fa-exclamation-triangle text-danger error-icon-shadow"></i>
                        </h1>
                        <h2 class="font-xl"><strong>Ooopss!!!</strong></h2>
                        <p class="lead semi-bold">
                            <small>
                                {{ $erro }}
                            </small>
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end row -->
