<script type="text/javascript">
    $(function () {
        @if(session('sucesso'))
        CSPlus.alertSucesso('{{ session('sucesso') }}');
        @endif

        @if(session('erro'))
        CSPlus.alertErro('{{ session('erro') }}');
        @endif
    })
</script>
