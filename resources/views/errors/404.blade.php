@extends('template.errors')
@section('titulo', '404 - Não Encontrado')

@section('code', 404)
@section('text-status', 'Página não Encontrada')
@section('information')
    A página que você estava procurando não foi encontrada. Talvez ela tenha sido movida, ou abduzida por um <i class="fa fa-2x fa-android"></i>.
@endsection
