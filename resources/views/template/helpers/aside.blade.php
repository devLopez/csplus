@php
	$nome_usuario	= Auth::name();
	$sexo 			= (session('usuario.sexo')) ?: 'male.png';
	$foto			= session('usuario.foto');

	$env = App::environment();
@endphp

<nav id="mainnav-container">
	<div id="mainnav">

		<!--Menu-->
		<!--================================-->
		<div id="mainnav-menu-wrap">
			<div class="nano has-scrollbar">
				<div class="nano-content" tabindex="0" style="right: -15px;">

					<!--Profile Widget-->
					<!--================================-->
					<div id="mainnav-profile" class="mainnav-profile">
						<div class="profile-wrap">
							<div class="pad-btm">

								@if($env == 'testing')
									<span class="label label-danger pull-right">MODO BETA</span>
								@endif

								{!! Auth::photo() !!}
							</div>
							<a href="#profile-nav" class="box-block">
								<p class="mnp-name">{{ $nome_usuario }}</p>
							</a>
						</div>
					</div>


					<ul id="mainnav-menu" class="list-group">

						@if(!Policy::isAdmin())
							<li id="menu-dashboard">
								<a href="{{ route('painel.dashboard') }}">
									<i class="fam-house"></i>
									<span class="menu-title">Dashboard</span>
								</a>
							</li>
							<li id="menu-associado">
								<a href="{{ route('associados.exibir', Auth::matricula()) }}">
									<i class="fam-report-user"></i>
									<span class="menu-title">Dados Pessoais</span>
								</a>
							</li>
							<li id="menu-debitos">
								<a href="{{ route('debitos.index', Auth::matricula()) }}">
									<i class="fam-coins"></i>
									<span class="menu-title">Débitos</span>
								</a>
							</li>
							<li id="menu-convites">
								<a href="{{ route('convites.index', Auth::matricula()) }}">
									<i class="fam-tag-red"></i>
									<span class="menu-title">Convites</span>
								</a>
							</li>
							<li id="menu-telefonia">
								<a href="{{ route('telefonia.index', Auth::matricula()) }}">
									<i class="fam-telephone"></i>
									<span class="menu-title">Telefonia</span>
								</a>
							</li>
							<li>
								<a href="cartoes">
									<i class="fam-creditcards"></i>
									<span class="menu-title">2ª Via de Cartão</span>
								</a>
							</li>
							<li>
								<a href="#modal-alterar-senha-socio" data-toggle="modal">
									<i class="fam-lock-edit"></i>
									<span class="menu-title">Alterar Senha</span>
								</a>
							</li>
							@if(Auth::isDiretor())
								<li>
									<a href="#">
										<i class="fa fa-lock"></i>
										<span class="menu-title">Administrativo</span>
										<i class="arrow"></i>
									</a>
									<ul class="collapse" aria-expanded="false">
										<li><a href="#">Portaria</a></li>
										<li><a href="#">Acessos ao sistema</a></li>
										<li><a href="#">Entradas / Saídas</a></li>
										<li><a href="#">Quadro Social</a></li>
									</ul>
								</li>
							@endif
							@if(Auth::getCachedLinks()->isNotEmpty())
								<li>
									<a href="#">
										<i class="fam-link"></i>
										<span class="menu-title">Links</span>
										<i class="arrow"></i>
									</a>

									<ul class="collapse" aria-expanded="false">
										@foreach(Auth::getCachedLinks() as $link)
											<li>
												<a href="{{ $link->HIPERLINK }}" @if($link->TIPO_CHAMADA == 1 ) {{ 'target="_blank"' }}@endif>
													{{ $link->DESCRICAO }}
												</a>
											</li>
										@endforeach
									</ul>
								</li>
							@endif
						@else
							<li id="menu-consulta">
								<a href="{{ route('associados.consulta') }}">
									<i class="fam-magnifier"></i>
									<span class="menu-title">Consultar</span>
								</a>
							</li>
							<li id="menu-novo">
								<a href="{{ route('associado.novo') }}">
									<i class="fam-user-add"></i>
									<span class="menu-title">Novo Associado</span>
								</a>
							</li>
							<li id="menu-cadastros">
								<a href="#">
									<i class="fam-tag-blue-add"></i>
									<span class="menu-title">Cadastros</span>
									<i class="arrow"></i>
								</a>

								<ul class="collapse" aria-expanded="false">
									<li id="menu-cad-categorias">
										<a href="{{ route('view.get.categorias') }}">Categorias</a>
									</li>
									<li id="menu-cad-parentescos">
										<a href="{{ route('view.get.parentescos') }}">Parentescos</a>
									</li>
								</ul>
							</li>
							<li id="menu-relatorios">
								<a href="#">
									<i class="fam-page-white-acrobat"></i>
									<span class="menu-title">Relatórios</span>
									<i class="arrow"></i>
								</a>

								<ul class="collapse" aria-expanded="false">
									<li id="menu-rel-associado"><a href="{{ route('rel.associado.index') }}">Geral de Associados</a></li>
									<li id="menu-rel-dependentes"><a href="{{ route('rel.dependentes.index') }}">Dependentes</a></li>
									<li><a href="">Ficha do Associado</a></li>
									<li><a href="">Aniversariantes</a></li>
								</ul>
							</li>

							@if(Client::usesPagseguro())
								<li id="menu-pagseguro">
									<a href="#">
										<i class="fam-creditcards"></i>
										<span class="menu-title">PagSeguro</span>
										<i class="arrow"></i>
									</a>

									<ul class="collapse" aria-expanded="false">
										<li id="menu-ps-transacoes">
											<a href="{{ route('transacoes.index') }}">Transações</a>
										</li>
									</ul>
								</li>
							@endif

							<li>
								<a href="#modal-alterar-senha-admin" data-toggle="modal">
									<i class="fam-lock-edit"></i>
									<span class="menu-title">Alterar Senha</span>
								</a>
							</li>

						@endif

						<li>
							<a href="{{ route('login.logoff') }}">
								<i class="fam-door-out"></i>
								<span class="menu-title">Fazer Logoff</span>
							</a>
						</li>
					</ul>

				</div>
			<div class="nano-pane" style="display: none;"><div class="nano-slider" style="height: 1488px; transform: translate(0px, 0px);"></div></div></div>
		</div>
		<!--================================-->
		<!--End menu-->

	</div>
</nav>
