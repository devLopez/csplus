<div class="card-header ch-alt" style="page-break-before: always">
    <div class="row">
        <div class="col col-xs-6">
            <h4>CSPLUS WEB</h4>
        </div>
        <div class="col col-xs-6">
            <img class="i-logo pull-right" src="{{ asset('assets/img/csplus/logo-clubsystem.png') }}" alt="" width="150">
        </div>
    </div>
</div>
