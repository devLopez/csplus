<div class="row m-b-25">
    <div class="col-xs-6">
        <div>
             <span class="c-gray text-uppercase">
                 <b>{{ $relatorio }}</b>
             </span>
            <br>
            <span>Emissão - {{ date('d/m/Y H:i:s') }}</span>
        </div>
    </div>

    @if(isset($associado))
        <div class="col-xs-6">
            <div class="pull-right">
                 <span class="c-gray text-uppercase">
                     <b>Associado: {{ $associado->NOMEDOTITULAR }}</b>
                 </span>
                <br>
                <span>Cota - {{ $associado->NUMERODACOTA  }}</span>
            </div>
        </div>
    @endif
</div>