<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
<link rel="shortcut icon" href="{{ asset('assets/img/favicon.ico') }}" type="image/x-icon">

<!-- CSS -->
<link rel="stylesheet" href="{{ asset('assets/css/bootstrap.css') }}" />
<link rel="stylesheet" href="{{ asset('assets/css/font-awesome.css') }}" />
<link rel="stylesheet" href="{{ asset('assets/css/nifty.css') }}" />

<!-- Insere o jquery -->
<script type="text/javascript" src="{{ asset('assets/js/libs/jquery-2.0.2.min.js') }}"></script>
<meta name="csrf-token" content="{{ csrf_token() }}">
<script type="text/javascript">
    window.Laravel = {
        token: '{{ csrf_token() }}'
    };
</script>

@stack('css')

@routes
