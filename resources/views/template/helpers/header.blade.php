<header id="navbar">
    <div id="navbar-container" class="boxed">

        <!--Brand logo & name-->
        <!--================================-->
        <div class="navbar-header">
            <a href="index.html" class="navbar-brand">
                {{--<img src="img/logo.png" alt="Nifty Logo" class="brand-icon">--}}
                <div class="brand-title">
                    <span class="brand-text">CSPlusV8</span>
                </div>
            </a>
        </div>
        <!--================================-->
        <!--End brand logo & name-->


        <!--Navbar Dropdown-->
        <!--================================-->
        <div class="navbar-content clearfix">
            <ul class="nav navbar-top-links pull-left">

                <!--Navigation toogle button-->
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <li class="tgl-menu-btn">
                    <a class="mainnav-toggle" href="#">
                        <i class="fa fa-reorder"></i>
                    </a>
                </li>
                <!--~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~-->
                <!--End Navigation toogle button-->
            </ul>
        </div>
        <!--================================-->
        <!--End Navbar Dropdown-->

    </div>
</header>

{{--<header id="header">--}}
    {{--<div id="logo-group">--}}

        {{--<!-- PLACE YOUR LOGO HERE -->--}}
        {{--<span id="logo">--}}
            {{--<img src="{{ asset('img.old') }}" alt="ClubSystem Softwares" />--}}
        {{--</span>--}}
        {{--<!-- END LOGO PLACEHOLDER -->--}}

    {{--</div>--}}

    {{--<!-- #TOGGLE LAYOUT BUTTONS -->--}}
    {{--<!-- pulled right: nav area -->--}}
    {{--<div class="pull-right">--}}

        {{--<!-- collapse menu button -->--}}
        {{--<div id="hide-menu" class="btn-header pull-right">--}}
            {{--<span>--}}
                {{--<a href="javascript:void(0);" data-action="toggleMenu" title="Esconder Menu">--}}
                    {{--<i class="fa fa-reorder"></i>--}}
                {{--</a>--}}
            {{--</span>--}}
        {{--</div>--}}
        {{--<!-- end collapse menu -->--}}

        {{--<!-- logout button -->--}}
        {{--<div id="logout" class="btn-header transparent pull-right">--}}
            {{--<span>--}}
                {{--<a href="{{ route('login.logoff') }}" title="Logoff" data-action="userLogout" data-logout-msg="Você pode melhorar a segurança fechando a aba após o logout">--}}
                    {{--<i class="fa fa-sign-out"></i>--}}
                {{--</a>--}}
            {{--</span>--}}
        {{--</div>--}}
        {{--<!-- end logout button -->--}}

        {{--<!-- fullscreen button -->--}}
        {{--<div id="fullscreen" class="btn-header transparent pull-right">--}}
            {{--<span>--}}
                {{--<a href="javascript:void(0);" data-action="launchFullscreen" title="Tela Cheia">--}}
                    {{--<i class="fa fa-arrows-alt"></i>--}}
                {{--</a>--}}
            {{--</span>--}}
        {{--</div>--}}
        {{--<!-- end fullscreen button -->--}}
    {{--</div>--}}
    {{--<!-- end pulled right: nav area -->--}}
{{--</header>--}}
