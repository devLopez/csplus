<footer id="footer">
    <div class="pull-right pad-rgt">
        Versão do Sistema <i class="fa fa fa-cogs"></i> <strong>{{ config('version.current') }}</strong>
    </div>

    <p class="pad-lft">CSPlus Web © 2016-{{ date('Y') }}</p>
</footer>