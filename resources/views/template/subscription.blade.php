<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title>@yield('titulo')</title>
        @include('template/helpers/css')
        <meta name="csrf-token" content="{{ csrf_token() }}">
    </head>
    <body data-url="{{ url('/') }}">

        <div id="container" class="effect mainnav-out">

            @include('template.helpers.header')

            <div class="boxed">
                <div id="content-container">
                    <div id="page-content">
                        @yield('conteudo')
                    </div>
                </div>
            </div>

        </div>

        @include('template/helpers/js')
        @include('errors.flash-messages')
        @yield('javascript')
    </body>
</html>
