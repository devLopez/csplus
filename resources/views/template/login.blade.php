<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        @include('template.helpers.css')
        <title>@yield('titulo')</title>
    </head>
    <body>

        <div id="container" class="cls-container">
            <div id="bg-overlay" class="bg-img"></div>
            <div class="cls-content">
                <div class="cls-content-sm panel">
                    @yield('conteudo')
                </div>
            </div>
        </div>

        @include('template.helpers.js')
        @include('errors.flash-messages')

        @yield('javascript')

    </body>
</html>
