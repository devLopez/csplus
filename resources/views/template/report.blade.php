<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <meta charset="utf-8">
        <link rel="stylesheet" href="{{ asset('assets/css/pdf.css') }}" media="all">
        <link href='https://fonts.googleapis.com/css?family=Roboto:400,300,300italic,400italic,500,500italic' rel='stylesheet' type='text/css'>
        <title>@yield('titulo')</title>
    </head>
    <body style="font-size: 10px;">
        <div class="invoice">
            <div class="card" style="box-shadow: none">

                @yield('conteudo')

            </div>
        </div>
    </body>
</html>
