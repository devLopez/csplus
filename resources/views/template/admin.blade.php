<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title>CSPlusV8 - Painel Administrativo</title>
        @include('template/helpers/css')
    </head>
    <body data-url="{{ url('/') }}">

        <div id="container" class="effect mainnav-lg">
            @include('template/helpers/header')

            <div class="boxed">

                <div id="content-container">

                    <div id="page-title">
                        <h1 class="page-header text-overflow">
                            @yield('titulo-pagina')
                        </h1>

                        <div class="searchbox">
                            @yield('searchbox')
                        </div>
                    </div>

                    <ol class="breadcrumb"></ol>

                    <div id="page-content">
                        @yield('conteudo')
                    </div>
                </div>


                <nav id="mainnav-container">
                    <div id="mainnav">

                        <!--Menu-->
                        <!--================================-->
                        <div id="mainnav-menu-wrap">
                            <div class="nano has-scrollbar">
                                <div class="nano-content" tabindex="0" style="right: -15px;">

                                    <!--Profile Widget-->
                                    <!--================================-->
                                    <div id="mainnav-profile" class="mainnav-profile">
                                        <div class="profile-wrap">
                                            <div class="pad-btm">

                                                @if(App::environment() == 'testing')
                                                    <span class="label label-danger pull-right">MODO BETA</span>
                                                @endif

                                                <img class="img-circle img-sm img-border" src="{{ asset("assets/img/profile-photos/male.png") }}" alt="Eu">

                                            </div>
                                            <a href="#profile-nav" class="box-block">
                                                <p class="mnp-name">{{ Login::user()->name }}</p>
                                            </a>
                                        </div>
                                    </div>


                                    <ul id="mainnav-menu" class="list-group">
                                        <li id="menu-clientes">
                                            <a href="{{ route('clientes.index') }}">
                                                <i class="fa fa-users"></i>
                                                <span class="menu-title">Clientes</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ url('/log-viewer') }}" target="_blank">
                                                <i class="fa fa-clipboard"></i>
                                                <span class="menu-title">Logs do Sistema</span>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="{{ route('admin.logout') }}">
                                                <i class="fa fa-sign-out"></i>
                                                <span class="menu-title">Fazer Logoff</span>
                                            </a>
                                        </li>
                                    </ul>

                                </div>
                                <div class="nano-pane" style="display: none;">
                                    <div class="nano-slider" style="height: 1488px; transform: translate(0px, 0px);"></div>
                                </div>
                            </div>
                        </div>
                        <!--================================-->
                        <!--End menu-->

                    </div>
                </nav>

            </div>

            @include('template.helpers.footer')
        </div>

        @include('template.helpers.js')
        @include('errors.flash-messages')
        @yield('javascript')

    </body>
</html>
