<!DOCTYPE html>
<html lang="pt-br">
    <head>
        @include('template.helpers.css')
        <title>@yield('titulo')</title>
    </head>
    <body>
        <div id="container" class="cls-container">

            <div class="cls-header">
                <div class="cls-brand">
                    <a class="box-inline" href="index.html">
                        <span class="brand-title">ClubSystem <span class="text-thin">CSPlusV8</span></span>
                    </a>
                </div>
            </div>

            <div class="cls-content">
                @yield('conteudo')
                <h1 class="error-code text-info">@yield('code')</h1>
                <p class="text-main text-semibold text-lg text-uppercase">@yield('text-status')</p>
                <div class="pad-btm text-muted">
                    @yield('information')
                </div>
                <hr class="new-section-sm bord-no">
                <div class="pad-top">
                    <a class="btn btn-default" href="#" onclick="javascript:history.back()">Voltar à página anterior</a>
                    <a class="btn btn-default" href="{{ route('painel.dashboard') }}">Visitar o DashBoard</a>
                </div>
            </div>

        </div>
    </body>
</html>
