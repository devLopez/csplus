<!DOCTYPE html>
<html lang="pt-br">
    <head>
        <title>CSPlusV8 - Painel</title>
        @include('template/helpers/css')
    </head>
    <body data-url="{{ url('/') }}">

        <div id="container" class="effect mainnav-lg">
            @include('template/helpers/header')

            <div class="boxed">

                <div id="content-container">

                    <div id="page-title">
                        <h1 class="page-header text-overflow">
                            @yield('titulo-pagina')
                        </h1>

                        <div class="searchbox">
                            @yield('searchbox')
                        </div>
                    </div>

                    <ol class="breadcrumb"></ol>

                    <div id="page-content">
                        @yield('conteudo')
                    </div>
                </div>

                @include('template/helpers/aside')
            </div>

            @include('template.helpers.footer')
        </div>


        @include('template/helpers/js')
        @include('errors.flash-messages')
        @yield('javascript')
        @stack('scripts')

        @include('Admin::altera-senha-admin')
        @include('Socio::altera-senha-socio')

    </body>
</html>
