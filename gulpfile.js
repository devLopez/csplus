const gulp      = require('gulp');
const uglify    = require('gulp-uglify');
const minify    = require('gulp-minify-css');

gulp.task('compile', () => {
    gulp.src('public/assets/js/**/*.js')
        .pipe(uglify())
        .pipe(gulp.dest('public/assets/js'));

    gulp.src('public/assets/**/*.css')
        .pipe(minify({keepBreaks:false}))
        .pipe(gulp.dest('public/assets'));
});
