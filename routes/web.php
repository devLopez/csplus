<?php

Route::group(['namespace' => 'Domains\CSPlus'], function() {
    Route::get('/', 'LoginController@login');

    Route::group(['prefix' => 'login'], function () {
        Route::get('clube/{clube?}/{acesso?}', 'LoginController@login')->name('login.index');
        Route::post('logar', 'LoginController@logar')->name('login.logar');
        Route::get('logout', 'LoginController@logout')->name('login.logoff');
    });

    /*-----------------------------------------------------------------
     * Rotas utilizadas para aceite dos termos da aplicação
     * -----------------------------------------------------------------
     */
    Route::group(['prefix' => 'termos', 'middleware' => ['session', 'user']], function() {
        Route::get('/{matricula}', 'TermosController@index')->name('termos.ler');
        Route::post('/{matricula}', 'TermosController@aceiteTermos')->name('termos.aceitar');
    });

    Route::group(['prefix' => 'painel', 'middleware' => ['session', 'termo.principal']], function () {
        Route::get('/dashboard', 'PainelController@dashboard')->name('painel.dashboard');

        Route::group(['prefix' => 'associados', 'middleware' => 'session', 'namespace' => 'Associado'], function () {
            // Busca e exibição de associados
            Route::get('consulta', 'AssociadosController@consulta')->name('associados.consulta')->middleware(['admin', 'tonull']);
            Route::get('exibir/{matricula}/{action?}',  'AssociadosController@exibir')->name('associados.exibir')->middleware(['user']);

            // Alteração dos dados do associado
            Route::get('novo', 'AssociadosController@novo')->name('associado.novo')->middleware(['admin']);
            Route::post('novo', 'PessoaisController@salvar')->name('associado.salvar')->middleware(['admin']);
            Route::put('associado/{matricula}', 'PessoaisController@atualizarAssociado')->name('associados.update')->middleware(['user']);

            // Criação e alteração de dependentes
            Route::post('dependente/novo/{matricula}', 'DependentesController@novo')->name('dependentes.novo')->middleware(['admin']);
            Route::put('dependentes/{matricula}/{dependente}', 'DependentesController@atualizar')->name('dependentes.atualizar')->middleware(['user']);
            Route::post('dependente/{matricula}/{dependente}/inativar', 'DependentesController@inativar')->name('dependentes.inativar')->middleware(['user']);
        });

        /* -----------------------------------------------------------------
         * Rotas utilizadas para os débitos
         * -----------------------------------------------------------------
         */
        Route::group(['prefix' => 'debitos', 'middleware' => 'session', 'namespace' => 'Debitos', 'as' => 'debitos.'], function () {
            Route::get('matricula/{matricula?}', 'DebitosController@index')->name('index')->middleware(['user']);
            Route::get('pagar/matricula/{matricula?}', 'DebitosController@pagar')->name('pagar')->middleware(['user']);
            Route::post('pagar/matricula/{matricula}', 'PagseguroController@pagamento')->name('pay')->middleware(['user']);
            Route::get('pagseguro/errors', 'PagseguroController@getErrors')->name('erros');

            Route::get('parcelas', 'PagseguroController@getParcelas')->name('parcelas');
        });

        /* -----------------------------------------------------------------
         * Rotas utilizadas para os convites
         * -----------------------------------------------------------------
         */
        Route::group(['prefix' => 'convites', 'middleware' => 'session', 'namespace' => 'Convites'], function() {
            Route::get('/matricula/{matricula?}', 'ConvitesController@index')->name('convites.index')->middleware(['user']);
            Route::post('gerar', 'ConvitesController@gerar')->name('convites.gerar');

            Route::get('aceite/matricula/{matricula}/convite/{convite?}',  'ConvitesController@aceite')->name('convite.aceite')->middleware(['user']);
            Route::post('aceite/matricula/{matricula}/convite/{convite?}', 'ConvitesController@aceitarTermos')->name('convite.aceitar');

            Route::get('novo/matricula/{matricula}', 'ConvitesController@novo')->name('convite.novo')->middleware(['user', 'termo.convite']);
            Route::post('novo/matricula/{matricula}', 'ConvitesController@salvar')->name('convite.salvar')->middleware(['user', 'termo.convite']);
        });

        /* -----------------------------------------------------------------
         * Rotas utilizadas para os convidados
         * -----------------------------------------------------------------
         */
        Route::group(['prefix' => 'convidado', 'middleware' => ['session'], 'namespace' => 'Convites'], function() {
            Route::get('matricula/{matricula?}/convite/{convite?}', 'ConvidadosController@index')->name('convidado.index')->middleware('termo.convite');
            Route::post('salvar/matricula/{matricula?}',  'ConvidadosController@salvar')->name('convidado.salvar')->middleware(['user', 'termo.convite']);
        });

        /* -----------------------------------------------------------------
         * Rotas utilizadas para a Telefonia
         * -----------------------------------------------------------------
         */
        Route::group(['prefix' => 'telefonia', 'middleware' => ['session', 'user'], 'namespace' => 'Telefonia'], function() {
            Route::get('/matricula/{matricula}', 'TelefoniaController@index')->name('telefonia.index');
            Route::get('buscar/matricula/{matricula}',  'TelefoniaController@buscar')->name('telefonia.buscar');

            Route::get('relatorio/detalhado/matricula/{matricula}', 'TelefoniaController@detalhado')->name('telefonia.detalhado');
            Route::get('relatorio/agrupado/matricula/{matricula}',  'TelefoniaController@agrupado')->name('telefonia.agrupado');
        });

        /* -----------------------------------------------------------------
         * Rotas utilizadas para a Telefonia
         * -----------------------------------------------------------------
         */
        Route::group(['prefix' => 'pagseguro', 'namespace' => 'Pagseguro', 'middleware' => 'admin'], function(){
            Route::group(['prefix' => 'transacoes'], function() {
                Route::get('/', 'TransacoesController@index')->name('transacoes.index');
                Route::get('consultar', 'TransacoesController@consultar')->name('transacoes.consultar');
                Route::post('receber-transacao', 'TransacoesController@getPayment')->name('get.payment');
            });
        });

        /* -----------------------------------------------------------------
         * Rotas utilizadas para cadastros diversos
         * -----------------------------------------------------------------
         */
        Route::group(['prefix' => 'cadastros', 'middleware' => 'admin'], function(){
            Route::get('categorias', function() {
                return view('Admin::cadastros.categorias.index', ['modal' => false]);
            })->name('view.get.categorias');

            Route::get('parentescos', function() {
                return view('Admin::cadastros.parentescos.index', ['modal' => false]);
            })->name('view.get.parentescos');
        });
    });

    /* -----------------------------------------------------------------
     * Rota utilizada para o pagamento
     * -----------------------------------------------------------------
     */
    Route::group(['prefix' => 'painel'], function() {
        Route::group(['prefix' => 'payment', 'namespace' => 'Debitos'], function () {
            Route::post('clube/{clube}', 'PagseguroController@pagar')->name('payment.do')->middleware(['cors', 'ps.client']);
        });
    });


    /* -----------------------------------------------------------------
     * Rotas utilizadas para os Relatórios administrativos
     * -----------------------------------------------------------------
     */
    Route::group(['prefix' => 'relatorios', 'middleware' => ['session', 'admin'],'namespace' => 'Relatorios'], function() {

        Route::group(['prefix' => 'associado'], function() {
            Route::get('/', 'SocioController@index')->name('rel.associado.index');
            Route::get('gerar', 'SocioController@gerar')->name('rel.associado.gerar');
        });

        Route::group(['prefix' => 'dependentes'], function(){
            Route::get('/', 'DependentesController@index')->name('rel.dependentes.index');
            Route::get('gerar', 'DependentesController@gerar')->name('rel.dependentes.gerar');
        });
    });

    /* -----------------------------------------------------------------
     * Rotas utilizadas para o Eventos
     * -----------------------------------------------------------------
     */
    // Route::get('eventos',           ['as' => 'eventos.index', 'uses' => 'EventosController@index']);
    // Route::post('eventos',          ['as' => 'eventos.salvar', 'uses' => 'EventosController@store']);
    // Route::post('eventos/clube',    ['as' => 'eventos.clube', 'uses' => 'EventosController@getClube']);

    /* -----------------------------------------------------------------
     * Rotas utilizadas para novas inscrições
     * -----------------------------------------------------------------
     */
    Route::group(['prefix' => 'cadastro'], function() {
        Route::get('clube/{id?}', 'SubscriptionController@index')->name('cadastro.index');
        Route::post('clube/{id?}', 'SubscriptionController@store')->name('cadastro.store');
        Route::get('categorias/{tipo}', 'SubscriptionController@getCategorias');
    });

    Route::group(['prefix' => 'bussiness', 'namespace' => 'Bussines'], function () {

        Route::group(['prefix' => 'clientes'], function() {
            Route::get('novo', 'ClientesController@novo')->name('bussiness.clientes.get');
            Route::post('novo', 'ClientesController@salvar')->name('bussiness.clientes.post');
        });
    });

    /**
     * ROTAS PARA BACKEND API
     */
    Route::group(['prefix' => 'backend', 'namespace' => 'Api', 'middleware' => 'session'], function() {
        Route::post('foto/upload', 'ApiController@uploadFoto')->name('api.foto.upload');
        Route::get('endereco/{cep}', 'ApiController@cep')->name('api.cep');

        Route::get('associado/existe', 'AssociadoController@verifyExistenceOfUser')->name('api.associado.existe');
        Route::get('associado/{matricula?}', 'AssociadoController@getAssociado')->name('api.associado');
        Route::get('colaborador/{id?}', 'AssociadoController@getColaborador')->name('api.colaborador');

        Route::get('indicante/{tipo}', 'AssociadoController@getAssociadoByName')->name('api.indicante.nome');

        Route::group(['prefix' => 'password'], function(){
            Route::post('admin', 'UsersController@adminPassword')->name('admin.pass')->middleware(['admin']);
            Route::post('socio', 'UsersController@userPassword')->name('user.pass');
        });

        Route::group(['prefix' => 'cadastro'], function(){
            Route::post('categoria', 'CadastrosController@postCategoria')->name('post.categoria');
            Route::post('parentesco', 'CadastrosController@postParentesco')->name('post.parentesco');
        });
    });
});
