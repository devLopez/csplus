<?php

    return [
        'tipo'          => [
            'REL_ASS_CAT'   => 'Básico - Mostra Categoria',
            'REL_ASS_PROF'  => 'Dados Profissionais',
            'REL_ASS_END'   => 'Endereço Residencial',
            'REL_ASS_COM'   => 'Endereço Comercial',
            'REL_ASS_TEL'   => 'Telefones',
            'REL_ASS_EMAIL' => 'E-mails',
            'REL_ASS_NASC'  => 'Data de Nascimento',
            'REL_ASS_DOC'   => 'Documentos',
            'REL_ASS_GERAL' => 'Completa - Todos os Associados'
        ],
        'situacao'      => [
            'T' => 'Ativos',
            'F' => 'Inativos'
        ],
        'aposentado'    => [
            'T' => 'Sim',
            'F' => 'Não'
        ],
        'desconto'      => [
            'T' => 'Sim',
            'F' => 'Não'
        ],
        'orientacao'    => [
            'REL_ASS_CAT'   => 'portrait',
            'REL_ASS_PROF'  => 'landscape',
            'REL_ASS_END'   => 'landscape',
            'REL_ASS_COM'   => 'landscape',
            'REL_ASS_TEL'   => 'portrait',
            'REL_ASS_EMAIL' => 'portrait',
            'REL_ASS_NASC'  => 'portrait',
            'REL_ASS_DOC'   => 'landscape',
            'REL_ASS_GERAL' => '',
        ]
    ];