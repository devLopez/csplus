<?php

    return [
        'javascript_url' => [
            'sandbox'       => 'https://stc.sandbox.pagseguro.uol.com.br/pagseguro/api/v2/checkout/pagseguro.directpayment.js',
            'production'    => 'https://stc.pagseguro.uol.com.br/pagseguro/api/v2/checkout/pagseguro.directpayment.js'
        ],
        'notification_url' => [
            'sandbox'       => 'https://ws.sandbox.pagseguro.uol.com.br/v2/transactions/notifications/',
            'production'    => 'https://ws.pagseguro.uol.com.br/v2/transactions/notifications/'
        ]
    ];
