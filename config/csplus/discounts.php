<?php

    return [

        301 => [
            'vista' => [
                'aluno_semestral'   => 165.00,
                'aluno_anual'       => 300.00,
                'piloto_semestral'  => 225.00,
                'piloto_anual'      => 420.00
            ],
            'prazo' => [
                'aluno_semestral'   => 180.00,
                'aluno_anual'       => 360.00,
                'piloto_semestral'  => 240.00,
                'piloto_anual'      => 480.00
            ]
        ]

    ];